import jagex.client.Surface;

import java.awt.Component;

public class Screen extends Surface {

	public Client client;

	public Screen(int w, int h, int rsrc_cnt, Component component) {
		super(w, h, rsrc_cnt, component);
	}

	public void sprite_3d_plot(int x, int y, int w, int h, int id, int j1,
			int k1) {
		if (id >= 50000) {
			this.client.bubble_plot(x, y, w, h, id - 50000, j1, k1);
			return;
		}
		if (id >= 40000) {
			this.client.item_plot(x, y, w, h, id - 40000, j1, k1);
			return;
		}
		if (id >= 20000) {
			this.client.method87(x, y, w, h, id - 20000, j1, k1);
			return;
		}
		if (id >= 5000) {
			this.client.method88(x, y, w, h, id - 5000, j1, k1);
			return;
		}
		super.resize_sprite_plot(x, y, w, h, id);
	}
}