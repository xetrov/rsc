import jagex.DataUtil;
import jagex.client.AudioStream;
import jagex.client.Connection;
import jagex.client.Menu;
import jagex.client.Model;
import jagex.client.MultiplayerGame;
import jagex.client.Scene;
import jagex.client.Surface;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.io.IOException;
import java.math.BigInteger;
import java.net.InetAddress;

@SuppressWarnings("serial")
public class Client extends MultiplayerGame {
	public boolean is_members;
	public static String arc_base = "";
	public BigInteger RSA_EXP;
	public BigInteger RSA_MOD;
	int anInt662;
	boolean disallowed;
	boolean outofmem;
	public boolean normal_user;
	int ticks;
	int anInt667;
	int anInt668;
	int anInt669;
	int[] anIntArray670;
	int[] anIntArray671;
	int anInt672;
	int[] anIntArray673;
	int[] anIntArray674;
	int base_x;
	int anInt676;
	int base_y;
	int anInt678;
	int anInt679;
	Graphics graphics;
	Scene scene;
	Screen screen;
	int anInt683;
	int anInt684;
	int login_state;
	int wnd_w;
	int wnd_h;
	int view_dist;
	int ui_sprite_off;
	int anInt690;
	int item_sprite_off;
	int proj_sprite_off;
	int anInt693;
	int anInt694;
	int anInt695;
	int anInt696;
	int anInt697;
	int anInt698;
	int anInt699;
	int anInt700;
	int anInt701;
	int anInt702;
	int anInt703;
	Terrain terrain;
	int anInt705;
	int anInt706;
	int anInt707;
	int anInt708;
	int anInt709;
	int anInt710;
	int anInt711;
	int anInt712;
	int anInt713;
	int anInt714;
	int anInt715;
	int anInt716;
	int anInt717;
	int anInt718;
	int anInt719;
	boolean aBoolean720;
	int local_x;
	int local_y;
	int anInt723;
	int anInt724;
	int pitch;
	int anInt726;
	int anInt727;
	int anInt728;
	int local_player_cnt;
	int anInt730;
	int scene_reduction;
	Mob[] local_npcs;
	Mob[] local_players;
	Mob[] aClass14Array734;
	Mob local_player;
	int anInt736;
	int anInt737;
	int anInt738;
	int anInt739;
	int anInt740;
	int anInt741;
	int anInt742;
	Mob[] aClass14Array743;
	Mob[] aClass14Array744;
	Mob[] aClass14Array745;
	int[] anIntArray746;
	int anInt747;
	int anInt748;
	int[] anIntArray749;
	int[] anIntArray750;
	int[] anIntArray751;
	int[] anIntArray752;
	int anInt753;
	int anInt754;
	Model[] obj_models;
	int[] obj_x;
	int[] obj_y;
	int[] obj_orient;
	int[] anIntArray759;
	Model[] models;
	boolean[] aBooleanArray761;
	int anInt762;
	int anInt763;
	Model[] wall_models;
	int[] wall_x;
	int[] wall_y;
	int[] anIntArray767;
	int[] anIntArray768;
	boolean[] aBooleanArray769;
	int anInt770;
	int anInt771;
	int anInt772;
	int[] anIntArray773;
	int[] anIntArray774;
	int[] anIntArray775;
	int anInt776;
	String aString777;
	int[] anIntArray778;
	int[] anIntArray779;
	int[] anIntArray780;
	int[] anIntArray781;
	int[] anIntArray782;
	int anInt783;
	String[] skill_names = { "Attack", "Defense", "Strength", "Hits", "Ranged",
			"Prayer", "Magic", "Cooking", "Woodcutting", "Fletching",
			"Fishing", "Firemaking", "Crafting", "Smithing", "Mining",
			"Herblaw" };

	String[] aStringArray785 = { "Armour", "WeaponAim", "WeaponPower", "Magic",
			"Prayer" };
	Menu aClass6_786;
	int anInt787;
	int anInt788;
	int anInt789;
	Menu aClass6_790;
	int anInt791;
	int anInt792;
	long aLong793;
	Menu aClass6_794;
	int anInt795;
	int anInt796;
	int anInt797;
	String[] quest_names = { "Black knight's fortress", "Cook's assistant",
			"Demon slayer", "Doric's quest", "The restless ghost",
			"Goblin diplomacy", "Ernest the chicken", "Imp catcher",
			"Pirate's treasure", "Prince Ali rescue", "Romeo & Juliet",
			"Sheep shearer", "Shield of Arrav", "The knight's sword",
			"Vampire slayer", "Witch's potion", "Dragon slayer",
			"Witch's house (members)", "Lost city (members)",
			"Hero's quest (members)", "Druidic ritual (members)",
			"Merlin's crystal (members)" };
	boolean[] aBooleanArray799;
	boolean[] prayer_on;
	boolean aBoolean801;
	boolean auto_angle;
	boolean single_button_mouse;
	boolean sound_effects;
	boolean aBoolean805;
	int anInt806;
	int anInt807;
	int anInt808;
	int anInt809;
	int anInt810;
	int anInt811;
	String[] aStringArray812;
	String[] aStringArray813;
	int[] anIntArray814;
	int[] anIntArray815;
	int[] anIntArray816;
	int[] anIntArray817;
	int[] anIntArray818;
	int[] anIntArray819;
	int[] anIntArray820;
	int anInt821;
	int anInt822;
	int anInt823;
	int anInt824;
	Menu aClass6_825;
	int anInt826;
	int anInt827;
	int anInt828;
	int anInt829;
	int anInt830;
	int anInt831;
	String[] aStringArray832;
	int[] anIntArray833;
	boolean aBoolean834;
	String aString835;
	int anInt836;
	int[] anIntArray837;
	int[] anIntArray838;
	int anInt839;
	int[] anIntArray840;
	int[] anIntArray841;
	boolean aBoolean842;
	boolean aBoolean843;
	boolean aBoolean844;
	boolean aBoolean845;
	boolean aBoolean846;
	boolean aBoolean847;
	boolean aBoolean848;
	boolean aBoolean849;
	long aLong850;
	int anInt851;
	int[] anIntArray852;
	int[] anIntArray853;
	int anInt854;
	int[] anIntArray855;
	int[] anIntArray856;
	int anInt857;
	int anInt858;
	int anInt859;
	int anInt860;
	boolean aBoolean861;
	String aString862;
	int anInt863;
	int[] anIntArray864;
	int[] anIntArray865;
	int anInt866;
	int[] anIntArray867;
	int[] anIntArray868;
	boolean aBoolean869;
	boolean aBoolean870;
	int anInt871;
	int anInt872;
	long aLong873;
	boolean aBoolean874;
	boolean aBoolean875;
	int anInt876;
	int[] anIntArray877;
	int[] anIntArray878;
	int anInt879;
	int[] anIntArray880;
	int[] anIntArray881;
	boolean aBoolean882;
	int anInt883;
	int anInt884;
	int[] anIntArray885;
	int[] anIntArray886;
	int[] anIntArray887;
	int anInt888;
	int anInt889;
	boolean aBoolean890;
	int anInt891;
	int[] anIntArray892;
	int[] anIntArray893;
	int anInt894;
	int[] anIntArray895;
	int[] anIntArray896;
	int anInt897;
	int anInt898;
	int anInt899;
	int anInt900;
	boolean aBoolean901;
	int anInt902;
	String[] aStringArray903;
	int anInt904;
	int anInt905;
	int anInt906;
	String aString907;
	String aString908;
	boolean logged_in;
	boolean aBoolean910;
	int anInt911;
	String aString912;
	int prev_login;
	int anInt914;
	int anInt915;
	boolean aBoolean916;
	String aString917;
	int anInt918;
	int anInt919;
	int anInt920;
	int anInt921;
	boolean world_loading;
	int ui_state;
	Menu initial_ui;
	int register_button_id;
	int login_button_id;
	Menu register_ui;
	int register_status_field_id;
	int anInt929;
	int register_cancel_button_id;
	int register_submit_button_id;
	int register_user_field_id;
	int register_pass_field_id;
	int register_confirm_field_id;
	int register_agree_check_id;
	Menu login_ui;
	int login_status_field_id;
	int login_user_field_id;
	int login_pass_field_id;
	int login_ok_button_id;
	int login_cancel_button_id;
	int login_lost_button_id;
	int anInt943;
	int anInt944;
	String aString945;
	String aString946;
	String aString947;
	String aString948;
	Menu aClass6_949;
	int anInt950;
	int anInt951;
	int anInt952;
	int anInt953;
	int anInt954;
	int anInt955;
	int anInt956;
	int anInt957;
	int anInt958;
	int anInt959;
	int anInt960;
	int anInt961;
	int anInt962;
	int anInt963;
	int anInt964;
	boolean aBoolean965;
	Menu aClass6_966;
	int anInt967;
	int anInt968;
	int anInt969;
	int[] anIntArray970;
	int[] anIntArray971;
	int[] anIntArray972;
	int[] anIntArray973;
	int[] anIntArray974 = { 0, 1, 2, 3, 4 };
	String[] aStringArray975;
	boolean aBoolean976;
	Menu lost_ui;
	int anInt978;
	int anInt979;
	int anInt980;
	int anInt981;
	int anInt982;
	int anInt983;
	int anInt984;
	int anInt985;
	int anInt986;
	int anInt987;
	int[] anIntArray988;
	int[] anIntArray989;
	int anInt990;
	String[] aStringArray991;
	int[] anIntArray992;
	int[] anIntArray993;
	int[] anIntArray994;
	int[] anIntArray995;
	int anInt996;
	int[] anIntArray997;
	int[] anIntArray998;
	int[] anIntArray999;
	int[] anIntArray1000;
	int anInt1001;
	int[] anIntArray1002;
	int[] anIntArray1003;
	int[] anIntArray1004;
	int anInt1005;
	int entity_sprite_off;
	int[][] anIntArrayArray1007 = { { 11, 2, 9, 7, 1, 6, 10, 0, 5, 8, 3, 4 },
			{ 11, 2, 9, 7, 1, 6, 10, 0, 5, 8, 3, 4 },
			{ 11, 3, 2, 9, 7, 1, 6, 10, 0, 5, 8, 4 },
			{ 3, 4, 2, 9, 7, 1, 6, 10, 8, 11, 0, 5 },
			{ 3, 4, 2, 9, 7, 1, 6, 10, 8, 11, 0, 5 },
			{ 4, 3, 2, 9, 7, 1, 6, 10, 8, 11, 0, 5 },
			{ 11, 4, 2, 9, 7, 1, 6, 10, 0, 5, 8, 3 },
			{ 11, 2, 9, 7, 1, 6, 10, 0, 5, 8, 4, 3 } };
	boolean aBoolean1008;
	int anInt1009;
	int anInt1010;
	int anInt1011;
	int anInt1012;
	int anInt1013;
	int anInt1014;
	int anInt1015;
	int anInt1016;
	public int[] anIntArray1017 = { 16711680, 16744448, 16769024, 10543104,
			57344, 32768, 41088, 45311, 33023, 12528, 14680288, 3158064,
			6307840, 8409088, 16777215 };

	public int[] anIntArray1018 = { 16760880, 16752704, 8409136, 6307872,
			3158064, 16736288, 16728064, 16777215, 65280, 65535 };

	public int[] anIntArray1019 = { 15523536, 13415270, 11766848, 10056486,
			9461792 };

	int[] anIntArray1020 = { 0, 1, 2, 1 };

	int[] anIntArray1021 = { 0, 1, 2, 1, 0, 0, 0, 0 };

	int[] anIntArray1022 = { 0, 0, 0, 0, 0, 1, 2, 1 };
	byte[] sounds_arc;
	AudioStream audio_str;
	int anInt1025;
	int[] anIntArray1026;
	int[] anIntArray1027;
	int[] anIntArray1028;
	int[] anIntArray1029;
	String[] aStringArray1030 = { "Where were you born?",
			"What was your first teacher's name?",
			"What is your father's middle name?",
			"Who was your first best friend?",
			"What is your favourite vacation spot?",
			"What is your mother's middle name?",
			"What was your first pet's name?",
			"What was the name of your first school?",
			"What is your mother's maiden name?",
			"Who was your first boyfriend/girlfriend?",
			"What was the first computer game you purchased?",
			"Who is your favourite actor/actress?",
			"Who is your favourite author?", "Who is your favourite musician?",
			"Who is your favourite cartoon character?",
			"What is your favourite book?", "What is your favourite food?",
			"What is your favourite movie?" };

	public static void main(String[] as) {
		Client mudclient1 = new Client();
		mudclient1.normal_user = false;
		arc_base = "";
		mudclient1.start(mudclient1.wnd_w, mudclient1.wnd_h + 11, "Runescape by Andrew Gower", false);
		mudclient1.min_delta = 10;
	}

	public void load() {
		if (this.normal_user) {
			String s = getDocumentBase().getHost().toLowerCase();
			if ((!s.endsWith("jagex.com")) && (!s.endsWith("jagex.co.uk"))
					&& (!s.endsWith("runescape.com"))
					&& (!s.endsWith("runescape.co.uk"))
					&& (!s.endsWith("runescape.net"))
					&& (!s.endsWith("runescape.org"))
					&& (!s.endsWith("penguin")) && (!s.endsWith("puffin"))) {
				this.disallowed = true;
				return;
			}
		}
		set_key(this.RSA_EXP, this.RSA_MOD);
		int i = 0;
		for (int j = 0; j < 99; j++) {
			int k = j + 1;
			int i1 = (int) (k + 300.0D * Math.pow(2.0D, k / 7.0D));
			i += i1;
			this.anIntArray778[j] = (i & 0xFFFFFFC);
		}
		try {
			String s1 = getParameter("member");
			int j1 = Integer.parseInt(s1);
			if (j1 == 1)
				this.is_members = true;
		} catch (Exception _ex) {
		}
		if (!this.normal_user)
			this.is_members = true;
		this.port_off = 43594;
		this.mouse_off_y = 0;
		MultiplayerGame.time_out = 1000;
		MultiplayerGame.version = Versions.GAME;
		try {
			String _off = getParameter("poff");
			int off = Integer.parseInt(_off);
			this.port_off += off;
			System.out.println(new StringBuilder().append("Offset: ")
					.append(off).toString());
		} catch (Exception _ex) {
		}
		config_load();
		this.ui_sprite_off = 2000;
		this.anInt690 = (this.ui_sprite_off + 100);
		this.item_sprite_off = (this.anInt690 + 50);
		this.proj_sprite_off = (this.item_sprite_off + 300);
		this.anInt693 = 2510;
		this.anInt694 = (this.anInt693 + 10);
		this.graphics = getGraphics();
		set_rate(50);
		this.screen = new Screen(this.wnd_w, this.wnd_h + 12, 2700, this);
		this.screen.client = this;
		this.screen.set_rect(0, 0, this.wnd_w, this.wnd_h + 12);
		Menu.aBoolean285 = false;
		Menu.anInt286 = this.anInt690;
		this.aClass6_786 = new Menu(this.screen, 5);
		int l = this.screen.w - 199;
		byte byte0 = 36;
		this.anInt787 = this.aClass6_786.method320(l, byte0 + 24, 196, 90, 1,
				500, true);
		this.aClass6_790 = new Menu(this.screen, 5);
		this.anInt791 = this.aClass6_790.method320(l, byte0 + 40, 196, 126, 1,
				500, true);
		this.aClass6_794 = new Menu(this.screen, 5);
		this.anInt795 = this.aClass6_794.method320(l, byte0 + 24, 196, 226, 1,
				500, true);
		media_load();
		entities_load();
		this.scene = new Scene(this.screen, 15000, 15000, 1000);
		this.scene.set_bounds(this.wnd_w / 2, this.wnd_h / 2, this.wnd_w / 2,
				this.wnd_h / 2, this.wnd_w, this.view_dist);
		this.scene.clip_far_3d = 2400;
		this.scene.clip_far_2d = 2400;
		this.scene.fog_falloff = 1;
		this.scene.fog_dist = 2300;
		this.scene.set_light(-50, -10, -50);
		this.terrain = new Terrain(this.scene, this.screen);
		this.terrain.anInt56 = this.ui_sprite_off;
		textures_load();
		models_load();
		terrain_load();
		if (this.is_members)
			sounds_load();
		set_resource_loading_bar(100, "Starting game...");
		method59();
		prepare_ui();
		method69();
		method65();
		method68();
		method61();
		preload_draw();
		method74();
	}

	public void config_load() {
		byte[] buf = null;
		try {
			buf = load_file(
					new StringBuilder().append(arc_base).append("config")
							.append(Versions.CONFIG).append(".jag").toString(),
					"Configuration", 10);
		} catch (IOException ex) {
			System.out.println(new StringBuilder().append("Load error:")
					.append(ex).toString());
		}
		Config.decode(buf, this.is_members);
	}

	public void media_load() {
		byte[] media_arc = null;
		try {
			media_arc = load_file(
					new StringBuilder().append(arc_base).append("media")
							.append(Versions.MEDIA).append(".jag").toString(),
					"2d graphics", 20);
		} catch (IOException ioexception) {
			System.out.println(new StringBuilder().append("Load error:")
					.append(ioexception).toString());
		}
		byte[] index = DataUtil.entry_extract("index.dat", 0, media_arc);
		this.screen.sprite_define(this.ui_sprite_off,
				DataUtil.entry_extract("inv1.dat", 0, media_arc), index, 1);
		this.screen.sprite_define(this.ui_sprite_off + 1,
				DataUtil.entry_extract("inv2.dat", 0, media_arc), index, 6);
		this.screen.sprite_define(this.ui_sprite_off + 9,
				DataUtil.entry_extract("bubble.dat", 0, media_arc), index, 1);
		this.screen
				.sprite_define(this.ui_sprite_off + 10,
						DataUtil.entry_extract("runescape.dat", 0, media_arc),
						index, 1);
		this.screen.sprite_define(this.ui_sprite_off + 11,
				DataUtil.entry_extract("splat.dat", 0, media_arc), index, 3);
		this.screen.sprite_define(this.ui_sprite_off + 14,
				DataUtil.entry_extract("icon.dat", 0, media_arc), index, 8);
		this.screen.sprite_define(this.ui_sprite_off + 22,
				DataUtil.entry_extract("hbar.dat", 0, media_arc), index, 1);
		this.screen.sprite_define(this.ui_sprite_off + 23,
				DataUtil.entry_extract("hbar2.dat", 0, media_arc), index, 1);
		this.screen.sprite_define(this.ui_sprite_off + 24,
				DataUtil.entry_extract("compass.dat", 0, media_arc), index, 1);
		this.screen.sprite_define(this.ui_sprite_off + 25,
				DataUtil.entry_extract("buttons.dat", 0, media_arc), index, 2);
		this.screen
				.sprite_define(this.anInt690,
						DataUtil.entry_extract("scrollbar.dat", 0, media_arc),
						index, 2);
		this.screen.sprite_define(this.anInt690 + 2,
				DataUtil.entry_extract("corners.dat", 0, media_arc), index, 4);
		this.screen.sprite_define(this.anInt690 + 6,
				DataUtil.entry_extract("arrows.dat", 0, media_arc), index, 2);
		this.screen.sprite_define(this.proj_sprite_off,
				DataUtil.entry_extract("projectile.dat", 0, media_arc), index,
				Config.proj_sprite_cnt);

		int i = Config.item_max_sprite;
		for (int j = 1; i > 0; j++) {
			int k = i;
			i -= 30;
			if (k > 30)
				k = 30;
			this.screen.sprite_define(
					this.item_sprite_off + (j - 1) * 30,
					DataUtil.entry_extract(new StringBuilder()
							.append("objects").append(j).append(".dat")
							.toString(), 0, media_arc), index, k);
		}

		this.screen.sprite_resolve(this.ui_sprite_off);
		this.screen.sprite_resolve(this.ui_sprite_off + 9);
		for (int l = 11; l <= 26; l++) {
			this.screen.sprite_resolve(this.ui_sprite_off + l);
		}
		for (int i1 = 0; i1 < Config.proj_sprite_cnt; i1++) {
			this.screen.sprite_resolve(this.proj_sprite_off + i1);
		}
		for (int j1 = 0; j1 < Config.item_max_sprite; j1++)
			this.screen.sprite_resolve(this.item_sprite_off + j1);
	}

	@SuppressWarnings("unused")
	public void entities_load() {
		byte[] entity_arc = null;
		byte[] index = null;
		try {
			entity_arc = load_file(
					new StringBuilder().append(arc_base).append("entity")
							.append(Versions.ENTITIES).append(".jag")
							.toString(), "people and monsters", 30);
		} catch (IOException ioexception) {
			System.out.println(new StringBuilder().append("Load error:")
					.append(ioexception).toString());
		}
		index = DataUtil.entry_extract("index.dat", 0, entity_arc);
		byte[] mem_entity_arc = null;
		byte[] mem_index = null;
		if (this.is_members) {
			try {
				mem_entity_arc = load_file(
						new StringBuilder().append(arc_base).append("entity")
								.append(Versions.ENTITIES).append(".mem")
								.toString(), "member graphics", 45);
			} catch (IOException ioexception1) {
				System.out.println(new StringBuilder().append("Load error:")
						.append(ioexception1).toString());
			}
			mem_index = DataUtil.entry_extract("index.dat", 0, mem_entity_arc);
		}
		int i = 0;
		this.anInt1005 = 0;
		this.entity_sprite_off = this.anInt1005;

		for (int j = 0; j < Config.anInt531; j++) {
			String s = Config.entity_name[j];
			int k = 0;
			while (true)
				if (k < j) {
					if (Config.entity_name[k].equalsIgnoreCase(s)) {
						Config.entity_sprite_off[j] = Config.entity_sprite_off[k];
					} else {
						k++;
						continue;
					}
				} else {
					byte[] abyte7 = DataUtil
							.entry_extract(new StringBuilder().append(s)
									.append(".dat").toString(), 0, entity_arc);
					byte[] abyte4 = index;
					if ((abyte7 == null) && (this.is_members)) {
						abyte7 = DataUtil.entry_extract(new StringBuilder()
								.append(s).append(".dat").toString(), 0,
								mem_entity_arc);
						abyte4 = mem_index;
					}
					if (abyte7 != null) {
						this.screen.sprite_define(this.entity_sprite_off,
								abyte7, abyte4, 15);
						i += 15;
						if (Config.anIntArray535[j] == 1) {
							byte[] abyte8 = DataUtil.entry_extract(
									new StringBuilder().append(s)
											.append("a.dat").toString(), 0,
									entity_arc);
							byte[] abyte5 = index;
							if ((abyte8 == null) && (this.is_members)) {
								abyte8 = DataUtil.entry_extract(
										new StringBuilder().append(s)
												.append("a.dat").toString(), 0,
										mem_entity_arc);
								abyte5 = mem_index;
							}
							this.screen.sprite_define(
									this.entity_sprite_off + 15, abyte8,
									abyte5, 3);
							i += 3;
						}
						if (Config.anIntArray536[j] == 1) {
							byte[] abyte9 = DataUtil.entry_extract(
									new StringBuilder().append(s)
											.append("f.dat").toString(), 0,
									entity_arc);
							byte[] abyte6 = index;
							if ((abyte9 == null) && (this.is_members)) {
								abyte9 = DataUtil.entry_extract(
										new StringBuilder().append(s)
												.append("f.dat").toString(), 0,
										mem_entity_arc);
								abyte6 = mem_index;
							}
							System.out.println(new StringBuilder().append(s)
									.append(" - ")
									.append(this.entity_sprite_off).toString());
							this.screen.sprite_define(
									this.entity_sprite_off + 18, abyte9,
									abyte6, 9);
							i += 9;
						}
						if (Config.anIntArray534[j] != 0) {
							for (int l = this.entity_sprite_off; l < this.entity_sprite_off + 27; l++) {
								this.screen.sprite_resolve(l);
							}
						}
					}
					Config.entity_sprite_off[j] = this.entity_sprite_off;
					this.entity_sprite_off += 27;
				}
		}
		System.out.println(new StringBuilder().append("Loaded: ").append(i)
				.append(" frames of animation").toString());
	}

	public void textures_load() {
		byte[] abyte0 = null;
		try {
			abyte0 = load_file(
					new StringBuilder().append(arc_base).append("textures")
							.append(Versions.TEXTURES).append(".jag")
							.toString(), "Textures", 50);
		} catch (IOException ioexception) {
			System.out.println(new StringBuilder().append("Load error:")
					.append(ioexception).toString());
		}
		byte[] index = DataUtil.entry_extract("index.dat", 0, abyte0);
		this.scene.textures_allocate(Config.anInt528, 7, 11);
		for (int i = 0; i < Config.anInt528; i++) {
			String s = Config.aStringArray529[i];
			byte[] abyte2 = DataUtil.entry_extract(new StringBuilder()
					.append(s).append(".dat").toString(), 0, abyte0);
			this.screen.sprite_define(this.anInt693, abyte2, index, 1);
			this.screen.rect_fill(0, 0, 128, 128, 16711935);
			this.screen.sprite_plot(0, 0, this.anInt693);
			int j = this.screen.sprite_mask_w[this.anInt693];
			String s1 = Config.aStringArray530[i];
			if ((s1 != null) && (s1.length() > 0)) {
				byte[] abyte3 = DataUtil.entry_extract(new StringBuilder()
						.append(s1).append(".dat").toString(), 0, abyte0);
				this.screen.sprite_define(this.anInt693, abyte3, index, 1);
				this.screen.sprite_plot(0, 0, this.anInt693);
			}
			this.screen.row_sprite_define(this.anInt694 + i, 0, 0, j, j);
			int k = j * j;
			for (int l = 0; l < k; l++) {
				if (this.screen.sprite_pixels[(this.anInt694 + i)][l] == 65280)
					this.screen.sprite_pixels[(this.anInt694 + i)][l] = 16711935;
			}
			this.screen.sprite_posterize(this.anInt694 + i);
			this.scene
					.texture_define(i,
							this.screen.sprite_raster[(this.anInt694 + i)],
							this.screen.sprite_palette[(this.anInt694 + i)],
							j / 64 - 1);
		}
	}

	public void models_load() {
		Config.lookup_model("torcha2");
		Config.lookup_model("torcha3");
		Config.lookup_model("torcha4");
		Config.lookup_model("skulltorcha2");
		Config.lookup_model("skulltorcha3");
		Config.lookup_model("skulltorcha4");
		Config.lookup_model("firea2");
		Config.lookup_model("firea3");
		Config.lookup_model("fireplacea2");
		Config.lookup_model("fireplacea3");
		if (!is_applet()) {
			byte[] models_arc = null;
			try {
				models_arc = load_file(new StringBuilder().append("models")
						.append(Versions.MODELS).append(".jag").toString(),
						"3d models", 60);
			} catch (IOException ioexception) {
				System.out.println(new StringBuilder().append("Load error:")
						.append(ioexception).toString());
			}
			for (int j = 0; j < Config.model_cnt; j++) {
				int k = DataUtil.offset_entry(
						new StringBuilder().append(Config.model_names[j])
								.append(".ob3").toString(), models_arc);
				if (k != 0)
					this.models[j] = new Model(models_arc, k, true);
				else {
					this.models[j] = new Model(1, 1);
				}
				this.models[j].trans = true;
			}

			return;
		}
		set_resource_loading_bar(70, "Loading 3d models");
		for (int i = 0; i < Config.model_cnt; i++) {
			this.models[i] = new Model(new StringBuilder()
					.append("../gamedata/models/")
					.append(Config.model_names[i]).append(".ob2").toString());
			if (Config.model_names[i].equals("giantcrystal"))
				this.models[i].trans = true;
		}
	}

	public void terrain_load() {
		try {
			this.terrain.map_arc = load_file(
					new StringBuilder().append(arc_base).append("maps")
							.append(Versions.TERRAIN).append(".jag").toString(),
					"map", 70);
			if (this.is_members)
				this.terrain.mem_map_arc = load_file(
						new StringBuilder().append(arc_base).append("maps")
								.append(Versions.TERRAIN).append(".mem")
								.toString(), "members map", 75);
			this.terrain.land_arc = load_file(
					new StringBuilder().append(arc_base).append("land")
							.append(Versions.TERRAIN).append(".jag").toString(),
					"landscape", 80);
			if (this.is_members) {
				this.terrain.mem_land_arc = load_file(
						new StringBuilder().append(arc_base).append("land")
								.append(Versions.TERRAIN).append(".mem")
								.toString(), "members landscape", 85);
				return;
			}
		} catch (IOException ioexception) {
			System.out.println(new StringBuilder().append("Load error:")
					.append(ioexception).toString());
		}
	}

	public void sounds_load() {
		try {
			this.sounds_arc = load_file(new StringBuilder().append(arc_base)
					.append("sounds").append(Versions.SOUNDS).append(".mem")
					.toString(), "Sound effects", 90);
			this.audio_str = new AudioStream();
			return;
		} catch (Throwable throwable) {
			System.out.println(new StringBuilder()
					.append("Unable to init sounds:").append(throwable)
					.toString());
		}
	}

	public void method59() {
		this.aClass6_825 = new Menu(this.screen, 10);
		this.anInt826 = this.aClass6_825
				.method316(5, 269, 502, 56, 1, 20, true);
		this.anInt827 = this.aClass6_825.method317(7, 324, 498, 14, 1, 80,
				false, true);
		this.anInt828 = this.aClass6_825
				.method316(5, 269, 502, 56, 1, 20, true);
		this.anInt829 = this.aClass6_825
				.method316(5, 269, 502, 56, 1, 20, true);
		this.aClass6_825.select(this.anInt827);
	}

	public void update() {
		if (this.disallowed)
			return;
		if (this.outofmem)
			return;
		try {
			this.ticks += 1;
			if (this.login_state == 0) {
				this.idle_cntr = 0;
				method75();
			}
			if (this.login_state == 1) {
				this.idle_cntr += 1;
				method76();
			}
			this.click_state = 0;
			this.typed_key = 0;
			this.anInt679 += 1;
			if (this.anInt679 > 500) {
				this.anInt679 = 0;
				int i = (int) (Math.random() * 4.0D);
				if ((i & 0x1) == 1)
					this.base_x += this.anInt676;
				if ((i & 0x2) == 2)
					this.base_y += this.anInt678;
			}
			if (this.base_x < -50)
				this.anInt676 = 2;
			if (this.base_x > 50)
				this.anInt676 = -2;
			if (this.base_y < -50)
				this.anInt678 = 2;
			if (this.base_y > 50)
				this.anInt678 = -2;
			if (this.anInt821 > 0)
				this.anInt821 -= 1;
			if (this.anInt822 > 0)
				this.anInt822 -= 1;
			if (this.anInt823 > 0)
				this.anInt823 -= 1;
			if (this.anInt824 > 0) {
				this.anInt824 -= 1;
				return;
			}
		} catch (OutOfMemoryError _ex) {
			method60();
			this.outofmem = true;
		}
	}

	public void display() {
		if (this.disallowed) {
			Graphics g = getGraphics();
			g.setColor(Color.black);
			g.fillRect(0, 0, 512, 356);
			g.setFont(new Font("Helvetica", 1, 20));
			g.setColor(Color.white);
			g.drawString("Error - unable to load game!", 50, 50);
			g.drawString("To play RuneScape make sure you play from", 50, 100);
			g.drawString("http://www.runescape.com", 50, 150);
			set_rate(1);
			return;
		}
		if (this.outofmem) {
			Graphics g1 = getGraphics();
			g1.setColor(Color.black);
			g1.fillRect(0, 0, 512, 356);
			g1.setFont(new Font("Helvetica", 1, 20));
			g1.setColor(Color.white);
			g1.drawString("Error - out of memory!", 50, 50);
			g1.drawString("Close ALL unnecessary programs", 50, 100);
			g1.drawString("and windows before loading the game", 50, 150);
			g1.drawString("RuneScape needs about 48meg of spare RAM", 50, 200);
			set_rate(1);
			return;
		}
		try {
			if (this.login_state == 0) {
				this.screen.logged_in = false;
				pregame_display();
			}
			if (this.login_state == 1) {
				this.screen.logged_in = true;
				game_display();
				return;
			}
		} catch (OutOfMemoryError _ex) {
			method60();
			this.outofmem = true;
		}
	}

	public void cleanup() {
		method26();
		method60();
		if (this.audio_str != null)
			this.audio_str.stop();
	}

	public void method60() {
		try {
			if (this.screen != null) {
				this.screen.sprites_release();
				this.screen.pixels = null;
				this.screen = null;
			}
			if (this.scene != null) {
				this.scene.dispose();
				this.scene = null;
			}
			this.models = null;
			this.obj_models = null;
			this.wall_models = null;
			this.local_npcs = null;
			this.local_players = null;
			this.aClass14Array743 = null;
			this.aClass14Array744 = null;
			this.local_player = null;
			if (this.terrain != null) {
				this.terrain.ground = null;
				this.terrain.walls = ((Model[][]) null);
				this.terrain.rooves = ((Model[][]) null);
				this.terrain.mesh = null;
				this.terrain = null;
			}
			System.gc();
			return;
		} catch (Exception _ex) {
		}
	}

	public void on_key(int i) {
		if (this.login_state == 0) {
			if (this.ui_state == 0)
				this.initial_ui.on_key(i);
			if (this.ui_state == 1)
				this.register_ui.on_key(i);
			if (this.ui_state == 2)
				this.login_ui.on_key(i);
			if (this.ui_state == 3)
				this.lost_ui.on_key(i);
		}
		if (this.login_state == 1) {
			if (this.aBoolean1008) {
				this.aClass6_949.on_key(i);
				return;
			}
			if (this.aBoolean965) {
				if (this.anInt969 == -1)
					this.aClass6_966.on_key(i);
				return;
			}
			if ((this.anInt906 == 0) && (this.anInt905 == 0))
				this.aClass6_825.on_key(i);
			if ((this.anInt906 == 3) || (this.anInt906 == 4)
					|| (this.anInt906 == 5))
				this.anInt906 = 0;
		}
	}

	public void method15(int i, int j, int k) {
		this.anIntArray673[this.anInt672] = j;
		this.anIntArray674[this.anInt672] = k;
		this.anInt672 = (this.anInt672 + 1 & 0x1FFF);
		for (int l = 10; l < 4000; l++) {
			int i1 = this.anInt672 - l & 0x1FFF;
			if ((this.anIntArray673[i1] == j) && (this.anIntArray674[i1] == k)) {
				boolean flag = false;
				for (int j1 = 1; j1 < l; j1++) {
					int k1 = this.anInt672 - j1 & 0x1FFF;
					int l1 = i1 - j1 & 0x1FFF;
					if ((this.anIntArray673[l1] != j)
							|| (this.anIntArray674[l1] != k))
						flag = true;
					if ((this.anIntArray673[k1] != this.anIntArray673[l1])
							|| (this.anIntArray674[k1] != this.anIntArray674[l1]))
						break;
					if ((j1 == l - 1) && (flag) && (this.anInt919 == 0)
							&& (this.anInt918 == 0)) {
						logout();
						return;
					}
				}
			}
		}
	}

	public void method61() {
		this.login_state = 0;
		this.ui_state = 0;
		this.aString947 = "";
		this.aString948 = "";
		this.aString945 = "Please enter a username:";
		this.aString946 = new StringBuilder().append("*")
				.append(this.aString947).append("*").toString();
		this.local_player_cnt = 0;
		this.anInt741 = 0;
	}

	public void method62() {
		this.line_buf_b = "";
		this.line_b = "";
	}

	public void logout() {
		if (this.login_state == 0)
			return;
		if (this.anInt919 > 450) {
			method77("@cya@You can't logout during combat!", 3);
			return;
		}
		if (this.anInt919 > 0) {
			method77("@cya@You can't logout for 10 seconds after combat", 3);
			return;
		}
		this.conn.enter(6);
		this.conn.exit();
		this.anInt918 = 1000;
	}

	public void method64(String s) {
		if (this.audio_str == null)
			return;
		if (this.sound_effects) {
			return;
		}
		this.audio_str.set(this.sounds_arc, DataUtil.offset_entry(
				new StringBuilder().append(s).append(".pcm").toString(),
				this.sounds_arc), DataUtil.length_entry(new StringBuilder()
				.append(s).append(".pcm").toString(), this.sounds_arc));
	}

	public void method65() {
		this.aClass6_966 = new Menu(this.screen, 100);
		int i = 8;
		this.anInt967 = this.aClass6_966
				.centered_label_create(
						256,
						i,
						"@yel@Please provide 5 security questions in case you lose your password",
						1, true);
		i += 22;
		this.aClass6_966
				.centered_label_create(
						256,
						i,
						"If you ever lose your password, you will need these to prove you own your account.",
						1, true);
		i += 13;
		this.aClass6_966
				.centered_label_create(
						256,
						i,
						"Your answers are encrypted and are ONLY used for password recovery purposes.",
						1, true);
		i += 22;
		this.aClass6_966
				.centered_label_create(
						256,
						i,
						"@ora@IMPORTANT:@whi@ To recover your password you must give the EXACT same answers you",
						1, true);
		i += 13;
		this.aClass6_966
				.centered_label_create(
						256,
						i,
						"give here. If you think you might forget an answer, or someone else could guess the",
						1, true);
		i += 13;
		this.aClass6_966
				.centered_label_create(
						256,
						i,
						"answer, then press the 'different question' button to get a better question.",
						1, true);
		i += 35;
		for (int j = 0; j < 5; j++) {
			this.aClass6_966.rect_create(170, i, 310, 30);
			this.aStringArray975[j] = new StringBuilder().append("~:")
					.append(this.anIntArray974[j]).toString();
			this.anIntArray970[j] = this.aClass6_966
					.centered_label_create(
							170,
							i - 7,
							new StringBuilder()
									.append(j + 1)
									.append(": ")
									.append(this.aStringArray1030[this.anIntArray974[j]])
									.toString(), 1, true);
			this.anIntArray971[j] = this.aClass6_966.text_field_create(170,
					i + 7, 310, 30, 1, 80, false, true);
			this.aClass6_966.rect_create(370, i, 80, 30);
			this.aClass6_966.centered_label_create(370, i - 7, "Different", 1,
					true);
			this.aClass6_966.centered_label_create(370, i + 7, "Question", 1,
					true);
			this.anIntArray972[j] = this.aClass6_966.button_create(370, i, 80,
					30);
			this.aClass6_966.rect_create(455, i, 80, 30);
			this.aClass6_966.centered_label_create(455, i - 7, "Enter own", 1,
					true);
			this.aClass6_966.centered_label_create(455, i + 7, "Question", 1,
					true);
			this.anIntArray973[j] = this.aClass6_966.button_create(455, i, 80,
					30);
			i += 35;
		}

		this.aClass6_966.select(this.anIntArray971[0]);
		i += 10;
		this.aClass6_966.rect_create(256, i, 250, 30);
		this.aClass6_966.centered_label_create(256, i,
				"Click here when finished", 4, true);
		this.anInt968 = this.aClass6_966.button_create(256, i, 250, 30);
	}

	public void method66() {
		if (this.anInt969 != -1) {
			if (this.line_b.length() > 0) {
				this.aStringArray975[this.anInt969] = this.line_b;
				this.aClass6_966.set_text(
						this.anIntArray970[this.anInt969],
						new StringBuilder().append(this.anInt969 + 1)
								.append(": ")
								.append(this.aStringArray975[this.anInt969])
								.toString());
				this.aClass6_966
						.set_text(this.anIntArray971[this.anInt969], "");
				this.anInt969 = -1;
			}
			return;
		}
		this.aClass6_966.consume(this.mouse_x, this.mouse_y, this.click_state,
				this.mouse_state);
		for (int i = 0; i < 5; i++) {
			if (this.aClass6_966.selected(this.anIntArray972[i])) {
				for (boolean flag = false; !flag;) {
					this.anIntArray974[i] = ((this.anIntArray974[i] + 1) % this.aStringArray1030.length);
					flag = true;
					for (int k = 0; k < 5; k++) {
						if ((k != i)
								&& (this.anIntArray974[k] == this.anIntArray974[i])) {
							flag = false;
						}
					}
				}
				this.aStringArray975[i] = new StringBuilder().append("~:")
						.append(this.anIntArray974[i]).toString();
				this.aClass6_966
						.set_text(
								this.anIntArray970[i],
								new StringBuilder()
										.append(i + 1)
										.append(": ")
										.append(this.aStringArray1030[this.anIntArray974[i]])
										.toString());
				this.aClass6_966.set_text(this.anIntArray971[i], "");
			}
		}
		for (int j = 0; j < 5; j++) {
			if (this.aClass6_966.selected(this.anIntArray973[j])) {
				this.anInt969 = j;
				this.line_buf_b = "";
				this.line_b = "";
			}
		}
		if (this.aClass6_966.selected(this.anInt968)) {
			for (int l = 0; l < 5; l++) {
				String s = this.aClass6_966.text(this.anIntArray971[l]);
				if ((s == null) || (s.length() < 3)) {
					this.aClass6_966
							.set_text(
									this.anInt967,
									new StringBuilder()
											.append("@yel@Please provide a longer answer to question: ")
											.append(l + 1).toString());
					return;
				}
			}

			for (int i1 = 0; i1 < 5; i1++) {
				String s1 = this.aClass6_966.text(this.anIntArray971[i1]);
				for (int k1 = 0; k1 < i1; k1++) {
					String s3 = this.aClass6_966.text(this.anIntArray971[k1]);
					if (s1.equalsIgnoreCase(s3)) {
						this.aClass6_966
								.set_text(this.anInt967,
										"@yel@Each question must have a different answer");
						return;
					}
				}

			}

			this.conn.enter(208);
			for (int j1 = 0; j1 < 5; j1++) {
				String s2 = this.aStringArray975[j1];
				if ((s2 == null) || (s2.length() == 0))
					s2 = String.valueOf(j1 + 1);
				if (s2.length() > 50)
					s2 = s2.substring(0, 50);
				this.conn.byte_put(s2.length());
				this.conn.str_put(s2);
				this.conn.enc_cred_put(DataUtil.base47_encode(this.aClass6_966
						.text(this.anIntArray971[j1])), this.sess_id,
						this.RSA_EXP, this.RSA_MOD);
			}

			this.conn.exit();
			for (int l1 = 0; l1 < 5; l1++) {
				this.anIntArray974[l1] = l1;
				this.aStringArray975[l1] = new StringBuilder().append("~:")
						.append(this.anIntArray974[l1]).toString();
				this.aClass6_966.set_text(this.anIntArray971[l1], "");
				this.aClass6_966
						.set_text(
								this.anIntArray970[l1],
								new StringBuilder()
										.append(l1 + 1)
										.append(": ")
										.append(this.aStringArray1030[this.anIntArray974[l1]])
										.toString());
			}

			this.screen.clear();
			this.aBoolean965 = false;
		}
	}

	public void method67() {
		this.screen.skip_lines = false;
		this.screen.clear();
		this.aClass6_966.display();
		if (this.anInt969 != -1) {
			int i = 150;
			this.screen.rect_fill(26, i, 460, 60, 0);
			this.screen.rect_draw(26, i, 460, 60, 16777215);
			i += 22;
			this.screen.center_text_draw("Please enter your question", 256, i,
					4, 16777215);
			i += 25;
			this.screen.center_text_draw(
					new StringBuilder().append(this.line_buf_b).append("*")
							.toString(), 256, i, 4, 16777215);
		}
		this.screen.sprite_plot(0, this.wnd_h, this.ui_sprite_off + 22);
		this.screen.copy(this.graphics, 0, 0);
	}

	public void method68() {
		this.lost_ui = new Menu(this.screen, 100);
		int i = 10;
		this.anInt978 = this.lost_ui
				.centered_label_create(
						256,
						i,
						"@yel@To prove this is your account please provide the answers to",
						1, true);
		i += 15;
		this.anInt979 = this.lost_ui
				.centered_label_create(
						256,
						i,
						"@yel@your security questions. You will then be able to reset your password",
						1, true);
		i += 35;
		for (int j = 0; j < 5; j++) {
			this.lost_ui.rect_create(256, i, 410, 30);
			this.anIntArray988[j] = this.lost_ui.centered_label_create(256,
					i - 7,
					new StringBuilder().append(j + 1).append(": question?")
							.toString(), 1, true);
			this.anIntArray989[j] = this.lost_ui.text_field_create(256, i + 7,
					310, 30, 1, 80, true, true);
			i += 35;
		}

		this.lost_ui.select(this.anIntArray989[0]);
		this.lost_ui.rect_create(256, i, 410, 30);
		this.lost_ui
				.centered_label_create(
						256,
						i - 7,
						"If you know it, enter a previous password used on this account",
						1, true);
		this.anInt980 = this.lost_ui.text_field_create(256, i + 7, 310, 30, 1,
				80, true, true);
		i += 35;
		this.lost_ui.rect_create(151, i, 200, 30);
		this.lost_ui.centered_label_create(151, i - 7, "Choose a NEW password",
				1, true);
		this.anInt981 = this.lost_ui.text_field_create(146, i + 7, 200, 30, 1,
				80, true, true);
		this.lost_ui.rect_create(361, i, 200, 30);
		this.lost_ui.centered_label_create(361, i - 7, "Confirm new password",
				1, true);
		this.anInt982 = this.lost_ui.text_field_create(366, i + 7, 200, 30, 1,
				80, true, true);
		i += 35;
		this.lost_ui.rect_create(201, i, 100, 30);
		this.lost_ui.centered_label_create(201, i, "Submit", 4, true);
		this.anInt983 = this.lost_ui.button_create(201, i, 100, 30);
		this.lost_ui.rect_create(311, i, 100, 30);
		this.lost_ui.centered_label_create(311, i, "Cancel", 4, true);
		this.anInt984 = this.lost_ui.button_create(311, i, 100, 30);
	}

	public void method69() {
		this.aClass6_949 = new Menu(this.screen, 100);
		this.aClass6_949.centered_label_create(256, 10,
				"Design Your Character", 4, true);
		char c = '';
		int i = 34;
		this.aClass6_949.rect_create(c, i, 200, 25);
		this.aClass6_949.centered_label_create(c, i, "Appearance", 4, false);
		i += 15;
		this.aClass6_949.centered_label_create(c - '7', i + 110, "Front", 3,
				true);
		this.aClass6_949.centered_label_create(c, i + 110, "Side", 3, true);
		this.aClass6_949.centered_label_create(c + '7', i + 110, "Back", 3,
				true);
		byte byte0 = 54;
		i += 145;
		this.aClass6_949.method314(c - byte0, i, 53, 41);
		this.aClass6_949.centered_label_create(c - byte0, i - 8, "Head", 1,
				true);
		this.aClass6_949.centered_label_create(c - byte0, i + 8, "Type", 1,
				true);
		this.aClass6_949.sprite_create(c - byte0 - 40, i, Menu.anInt286 + 7);
		this.anInt950 = this.aClass6_949.button_create(c - byte0 - 40, i, 20,
				20);
		this.aClass6_949.sprite_create(c - byte0 + 40, i, Menu.anInt286 + 6);
		this.anInt951 = this.aClass6_949.button_create(c - byte0 + 40, i, 20,
				20);
		this.aClass6_949.method314(c + byte0, i, 53, 41);
		this.aClass6_949.centered_label_create(c + byte0, i - 8, "Hair", 1,
				true);
		this.aClass6_949.centered_label_create(c + byte0, i + 8, "Color", 1,
				true);
		this.aClass6_949.sprite_create(c + byte0 - 40, i, Menu.anInt286 + 7);
		this.anInt952 = this.aClass6_949.button_create(c + byte0 - 40, i, 20,
				20);
		this.aClass6_949.sprite_create(c + byte0 + 40, i, Menu.anInt286 + 6);
		this.anInt953 = this.aClass6_949.button_create(c + byte0 + 40, i, 20,
				20);
		i += 50;
		this.aClass6_949.method314(c - byte0, i, 53, 41);
		this.aClass6_949.centered_label_create(c - byte0, i, "Gender", 1, true);
		this.aClass6_949.sprite_create(c - byte0 - 40, i, Menu.anInt286 + 7);
		this.anInt954 = this.aClass6_949.button_create(c - byte0 - 40, i, 20,
				20);
		this.aClass6_949.sprite_create(c - byte0 + 40, i, Menu.anInt286 + 6);
		this.anInt955 = this.aClass6_949.button_create(c - byte0 + 40, i, 20,
				20);
		this.aClass6_949.method314(c + byte0, i, 53, 41);
		this.aClass6_949
				.centered_label_create(c + byte0, i - 8, "Top", 1, true);
		this.aClass6_949.centered_label_create(c + byte0, i + 8, "Color", 1,
				true);
		this.aClass6_949.sprite_create(c + byte0 - 40, i, Menu.anInt286 + 7);
		this.anInt956 = this.aClass6_949.button_create(c + byte0 - 40, i, 20,
				20);
		this.aClass6_949.sprite_create(c + byte0 + 40, i, Menu.anInt286 + 6);
		this.anInt957 = this.aClass6_949.button_create(c + byte0 + 40, i, 20,
				20);
		i += 50;
		this.aClass6_949.method314(c - byte0, i, 53, 41);
		this.aClass6_949.centered_label_create(c - byte0, i - 8, "Skin", 1,
				true);
		this.aClass6_949.centered_label_create(c - byte0, i + 8, "Color", 1,
				true);
		this.aClass6_949.sprite_create(c - byte0 - 40, i, Menu.anInt286 + 7);
		this.anInt958 = this.aClass6_949.button_create(c - byte0 - 40, i, 20,
				20);
		this.aClass6_949.sprite_create(c - byte0 + 40, i, Menu.anInt286 + 6);
		this.anInt959 = this.aClass6_949.button_create(c - byte0 + 40, i, 20,
				20);
		this.aClass6_949.method314(c + byte0, i, 53, 41);
		this.aClass6_949.centered_label_create(c + byte0, i - 8, "Bottom", 1,
				true);
		this.aClass6_949.centered_label_create(c + byte0, i + 8, "Color", 1,
				true);
		this.aClass6_949.sprite_create(c + byte0 - 40, i, Menu.anInt286 + 7);
		this.anInt960 = this.aClass6_949.button_create(c + byte0 - 40, i, 20,
				20);
		this.aClass6_949.sprite_create(c + byte0 + 40, i, Menu.anInt286 + 6);
		this.anInt961 = this.aClass6_949.button_create(c + byte0 + 40, i, 20,
				20);
		c = 'Ŵ';
		i = 35;
		this.aClass6_949.rect_create(c, i, 200, 25);
		this.aClass6_949
				.centered_label_create(c, i, "Character Type", 4, false);
		i += 22;
		this.aClass6_949.centered_label_create(c, i,
				"Each character type has different starting", 0, true);
		i += 13;
		this.aClass6_949.centered_label_create(c, i,
				"bonuses. But the choice you make here", 0, true);
		i += 13;
		this.aClass6_949.centered_label_create(c, i,
				"isn't permanent, and will change depending", 0, true);
		i += 13;
		this.aClass6_949.centered_label_create(c, i,
				"on how you play the game.", 0, true);
		i += 73;
		this.aClass6_949.method314(c, i, 215, 125);
		String[] as = { "Adventurer", "Warrior", "Wizard", "Ranger", "Miner" };

		this.anInt963 = this.aClass6_949.list_create(c, i + 2, as, 3, true);
		i += 82;
		this.aClass6_949.rect_create(c, i, 200, 30);
		this.aClass6_949.centered_label_create(c, i, "Start Game", 4, false);
		this.anInt962 = this.aClass6_949.button_create(c, i, 200, 30);
	}

	public void method70() {
		this.screen.skip_lines = false;
		this.screen.clear();
		this.aClass6_949.display();
		char c = '';
		byte byte0 = 50;
		this.screen.resize_tint_sprite_plot(c - ' ' - 55, byte0, 64, 102,
				Config.entity_sprite_off[this.anInt1011],
				this.anIntArray1017[this.anInt1014]);
		this.screen.resize_trans_sprite_plot(c - ' ' - 55, byte0, 64, 102,
				Config.entity_sprite_off[this.anInt1010],
				this.anIntArray1017[this.anInt1013],
				this.anIntArray1019[this.anInt1015], 0, false);
		this.screen.resize_trans_sprite_plot(c - ' ' - 55, byte0, 64, 102,
				Config.entity_sprite_off[this.anInt1009],
				this.anIntArray1018[this.anInt1012],
				this.anIntArray1019[this.anInt1015], 0, false);
		this.screen.resize_tint_sprite_plot(c - ' ', byte0, 64, 102,
				Config.entity_sprite_off[this.anInt1011] + 6,
				this.anIntArray1017[this.anInt1014]);
		this.screen.resize_trans_sprite_plot(c - ' ', byte0, 64, 102,
				Config.entity_sprite_off[this.anInt1010] + 6,
				this.anIntArray1017[this.anInt1013],
				this.anIntArray1019[this.anInt1015], 0, false);
		this.screen.resize_trans_sprite_plot(c - ' ', byte0, 64, 102,
				Config.entity_sprite_off[this.anInt1009] + 6,
				this.anIntArray1018[this.anInt1012],
				this.anIntArray1019[this.anInt1015], 0, false);
		this.screen.resize_tint_sprite_plot(c - ' ' + 55, byte0, 64, 102,
				Config.entity_sprite_off[this.anInt1011] + 12,
				this.anIntArray1017[this.anInt1014]);
		this.screen.resize_trans_sprite_plot(c - ' ' + 55, byte0, 64, 102,
				Config.entity_sprite_off[this.anInt1010] + 12,
				this.anIntArray1017[this.anInt1013],
				this.anIntArray1019[this.anInt1015], 0, false);
		this.screen.resize_trans_sprite_plot(c - ' ' + 55, byte0, 64, 102,
				Config.entity_sprite_off[this.anInt1009] + 12,
				this.anIntArray1018[this.anInt1012],
				this.anIntArray1019[this.anInt1015], 0, false);
		this.screen.sprite_plot(0, this.wnd_h, this.ui_sprite_off + 22);
		this.screen.copy(this.graphics, 0, 0);
	}

	public void method71() {
		this.aClass6_949.consume(this.mouse_x, this.mouse_y, this.click_state,
				this.mouse_state);
		if (this.aClass6_949.selected(this.anInt950))
			do
				this.anInt1009 = ((this.anInt1009 - 1 + Config.anInt531) % Config.anInt531);
			while (((Config.anIntArray534[this.anInt1009] & 0x3) != 1)
					|| ((Config.anIntArray534[this.anInt1009] & 4 * this.anInt1016) == 0));
		if (this.aClass6_949.selected(this.anInt951))
			do
				this.anInt1009 = ((this.anInt1009 + 1) % Config.anInt531);
			while (((Config.anIntArray534[this.anInt1009] & 0x3) != 1)
					|| ((Config.anIntArray534[this.anInt1009] & 4 * this.anInt1016) == 0));
		if (this.aClass6_949.selected(this.anInt952))
			this.anInt1012 = ((this.anInt1012 - 1 + this.anIntArray1018.length) % this.anIntArray1018.length);
		if (this.aClass6_949.selected(this.anInt953))
			this.anInt1012 = ((this.anInt1012 + 1) % this.anIntArray1018.length);
		if ((this.aClass6_949.selected(this.anInt954))
				|| (this.aClass6_949.selected(this.anInt955))) {
			for (this.anInt1016 = (3 - this.anInt1016); ((Config.anIntArray534[this.anInt1009] & 0x3) != 1)
					|| ((Config.anIntArray534[this.anInt1009] & 4 * this.anInt1016) == 0); this.anInt1009 = ((this.anInt1009 + 1) % Config.anInt531))
				;
			while (((Config.anIntArray534[this.anInt1010] & 0x3) != 2)
					|| ((Config.anIntArray534[this.anInt1010] & 4 * this.anInt1016) == 0))
				this.anInt1010 = ((this.anInt1010 + 1) % Config.anInt531);
		}

		if (this.aClass6_949.selected(this.anInt956))
			this.anInt1013 = ((this.anInt1013 - 1 + this.anIntArray1017.length) % this.anIntArray1017.length);
		if (this.aClass6_949.selected(this.anInt957))
			this.anInt1013 = ((this.anInt1013 + 1) % this.anIntArray1017.length);
		if (this.aClass6_949.selected(this.anInt958))
			this.anInt1015 = ((this.anInt1015 - 1 + this.anIntArray1019.length) % this.anIntArray1019.length);
		if (this.aClass6_949.selected(this.anInt959))
			this.anInt1015 = ((this.anInt1015 + 1) % this.anIntArray1019.length);
		if (this.aClass6_949.selected(this.anInt960))
			this.anInt1014 = ((this.anInt1014 - 1 + this.anIntArray1017.length) % this.anIntArray1017.length);
		if (this.aClass6_949.selected(this.anInt961))
			this.anInt1014 = ((this.anInt1014 + 1) % this.anIntArray1017.length);
		if (this.aClass6_949.selected(this.anInt962)) {
			this.conn.enter(236);
			this.conn.byte_put(this.anInt1016);
			this.conn.byte_put(this.anInt1009);
			this.conn.byte_put(this.anInt1010);
			this.conn.byte_put(this.anInt1011);
			this.conn.byte_put(this.anInt1012);
			this.conn.byte_put(this.anInt1013);
			this.conn.byte_put(this.anInt1014);
			this.conn.byte_put(this.anInt1015);
			this.conn.byte_put(this.aClass6_949.method332(this.anInt963));
			this.conn.exit();
			this.screen.clear();
			this.aBoolean1008 = false;
		}
	}

	public void prepare_ui() {
		this.initial_ui = new Menu(this.screen, 50);
		int y = 40;
		if (!this.is_members) {
			this.initial_ui.centered_label_create(256, 200 + y,
					"Click on an option", 5, true);
			this.initial_ui.rect_create(156, 240 + y, 120, 35);
			this.initial_ui.rect_create(356, 240 + y, 120, 35);
			this.initial_ui.centered_label_create(156, 240 + y, "New User", 5,
					false);
			this.initial_ui.centered_label_create(356, 240 + y,
					"Existing User", 5, false);
			this.register_button_id = this.initial_ui.button_create(156,
					240 + y, 120, 35);
			this.login_button_id = this.initial_ui.button_create(356, 240 + y,
					120, 35);
		} else {
			this.initial_ui.centered_label_create(256, 200 + y,
					"Welcome to RuneScape", 4, true);
			this.initial_ui.centered_label_create(256, 215 + y,
					"You need a member account to use this server", 4, true);
			this.initial_ui.rect_create(256, 250 + y, 200, 35);
			this.initial_ui.centered_label_create(256, 250 + y,
					"Click here to login", 5, false);
			this.login_button_id = this.initial_ui.button_create(256, 250 + y,
					200, 35);
		}
		this.register_ui = new Menu(this.screen, 50);
		y = 70;
		this.register_status_field_id = this.register_ui.centered_label_create(
				256, y + 8,
				"To create an account please enter all the requested details",
				4, true);
		y += 25;
		this.register_ui.rect_create(256, y + 17, 250, 34);
		this.register_ui.centered_label_create(256, y + 8, "Choose a Username",
				4, false);
		this.register_user_field_id = this.register_ui.text_field_create(256,
				y + 25, 200, 40, 4, 12, false, false);
		this.register_ui.select(this.register_user_field_id);
		y += 40;
		this.register_ui.rect_create(141, y + 17, 220, 34);
		this.register_ui.centered_label_create(141, y + 8, "Choose a Password",
				4, false);
		this.register_pass_field_id = this.register_ui.text_field_create(141,
				y + 25, 220, 40, 4, 20, true, false);
		this.register_ui.rect_create(371, y + 17, 220, 34);
		this.register_ui.centered_label_create(371, y + 8, "Confirm Password",
				4, false);
		this.register_confirm_field_id = this.register_ui.text_field_create(
				371, y + 25, 220, 40, 4, 20, true, false);
		y += 40;
		y += 20;
		this.register_agree_check_id = this.register_ui.check_box_create(60, y,
				14);
		this.register_ui.label_create(75, y,
				"I have read and agree to the terms+conditions listed at:", 4,
				true);
		y += 15;
		this.register_ui.centered_label_create(256, y,
				"http://www.runescape.com/runeterms.html", 4, true);
		y += 20;
		this.register_ui.rect_create(156, y + 17, 150, 34);
		this.register_ui.centered_label_create(156, y + 17, "Submit", 5, false);
		this.register_submit_button_id = this.register_ui.button_create(156,
				y + 17, 150, 34);
		this.register_ui.rect_create(356, y + 17, 150, 34);
		this.register_ui.centered_label_create(356, y + 17, "Cancel", 5, false);
		this.register_cancel_button_id = this.register_ui.button_create(356,
				y + 17, 150, 34);
		this.login_ui = new Menu(this.screen, 50);
		y = 230;
		this.login_status_field_id = this.login_ui.centered_label_create(256,
				y - 10, "Please enter your username and password", 4, true);
		y += 28;
		this.login_ui.rect_create(140, y, 200, 40);
		this.login_ui.centered_label_create(140, y - 10, "Username:", 4, false);
		this.login_user_field_id = this.login_ui.text_field_create(140, y + 10,
				200, 40, 4, 12, false, false);
		y += 47;
		this.login_ui.rect_create(190, y, 200, 40);
		this.login_ui.centered_label_create(190, y - 10, "Password:", 4, false);
		this.login_pass_field_id = this.login_ui.text_field_create(190, y + 10,
				200, 40, 4, 20, true, false);
		y -= 55;
		this.login_ui.rect_create(410, y, 120, 25);
		this.login_ui.centered_label_create(410, y, "Ok", 4, false);
		this.login_ok_button_id = this.login_ui.button_create(410, y, 120, 25);
		y += 30;
		this.login_ui.rect_create(410, y, 120, 25);
		this.login_ui.centered_label_create(410, y, "Cancel", 4, false);
		this.login_cancel_button_id = this.login_ui.button_create(410, y, 120,
				25);
		y += 30;
		this.login_ui.rect_create(410, y, 160, 25);
		this.login_ui.centered_label_create(410, y, "I've lost my password", 4,
				false);
		this.login_lost_button_id = this.login_ui
				.button_create(410, y, 160, 25);
		this.login_ui.select(this.login_user_field_id);
	}

	public void pregame_display() {
		this.logged_in = false;
		this.screen.skip_lines = false;
		this.screen.clear();
		if ((this.ui_state == 0) || (this.ui_state == 2)) {
			int i = this.ticks * 2 % 3072;
			if (i < 1024) {
				this.screen.sprite_plot(0, 10, 2500);
				if (i > 768)
					this.screen.trans_sprite_plot(0, 10, 2501, i - 768);
			} else if (i < 2048) {
				this.screen.sprite_plot(0, 10, 2501);
				if (i > 1792)
					this.screen.trans_sprite_plot(0, 10,
							this.ui_sprite_off + 10, i - 1792);
			} else {
				this.screen.sprite_plot(0, 10, this.ui_sprite_off + 10);
				if (i > 2816)
					this.screen.trans_sprite_plot(0, 10, 2500, i - 2816);
			}
		}
		if (this.ui_state == 0)
			this.initial_ui.display();
		if (this.ui_state == 1)
			this.register_ui.display();
		if (this.ui_state == 2)
			this.login_ui.display();
		if (this.ui_state == 3)
			this.lost_ui.display();
		this.screen.sprite_plot(0, this.wnd_h, this.ui_sprite_off + 22);
		this.screen.copy(this.graphics, 0, 0);
	}

	public void method74() {
		int i = 0;
		byte r_x = 50;
		byte r_y = 50;
		this.terrain.load(r_x * 48 + 23, r_y * 48 + 23, i);
		this.terrain.populate(this.models);
		char c = '☀';
		char c1 = 'ᤀ';
		char c2 = 'ь';
		char c3 = '͸';
		this.scene.clip_far_3d = 4100;
		this.scene.clip_far_2d = 4100;
		this.scene.fog_falloff = 1;
		this.scene.fog_dist = 4000;
		this.scene.set_camera(c, -this.terrain.calc_z(c, c1), c1, 912, c3, 0,
				c2 * '\002');
		this.scene.render();
		this.screen.darken();
		this.screen.darken();
		this.screen.rect_fill(0, 0, 512, 6, 0);
		for (int j = 6; j >= 1; j--) {
			this.screen.blur(0, j, 0, j, 512, 8);
		}
		this.screen.rect_fill(0, 194, 512, 20, 0);
		for (int k = 6; k >= 1; k--) {
			this.screen.blur(0, k, 0, 194 - k, 512, 8);
		}
		this.screen.sprite_plot(15, 15, this.ui_sprite_off + 10);
		this.screen.row_sprite_define(2500, 0, 0, 512, 200);
		this.screen.sprite_posterize(2500);
		c = '␀';
		c1 = '␀';
		c2 = 'ь';
		c3 = '͸';
		this.scene.clip_far_3d = 4100;
		this.scene.clip_far_2d = 4100;
		this.scene.fog_falloff = 1;
		this.scene.fog_dist = 4000;
		this.scene.set_camera(c, -this.terrain.calc_z(c, c1), c1, 912, c3, 0,
				c2 * '\002');
		this.scene.render();
		this.screen.darken();
		this.screen.darken();
		this.screen.rect_fill(0, 0, 512, 6, 0);
		for (int l = 6; l >= 1; l--) {
			this.screen.blur(0, l, 0, l, 512, 8);
		}
		this.screen.rect_fill(0, 194, 512, 20, 0);
		for (int i1 = 6; i1 >= 1; i1--) {
			this.screen.blur(0, i1, 0, 194 - i1, 512, 8);
		}
		this.screen.sprite_plot(15, 15, this.ui_sprite_off + 10);
		this.screen.row_sprite_define(2501, 0, 0, 512, 200);
		this.screen.sprite_posterize(2501);
		for (int j1 = 0; j1 < 64; j1++) {
			this.scene.remove(this.terrain.rooves[0][j1]);
			this.scene.remove(this.terrain.walls[1][j1]);
			this.scene.remove(this.terrain.rooves[1][j1]);
			this.scene.remove(this.terrain.walls[2][j1]);
			this.scene.remove(this.terrain.rooves[2][j1]);
		}

		c = '⮀';
		c1 = '⢀';
		c2 = 'Ǵ';
		c3 = 'Ÿ';
		this.scene.clip_far_3d = 4100;
		this.scene.clip_far_2d = 4100;
		this.scene.fog_falloff = 1;
		this.scene.fog_dist = 4000;
		this.scene.set_camera(c, -this.terrain.calc_z(c, c1), c1, 912, c3, 0,
				c2 * '\002');
		this.scene.render();
		this.screen.darken();
		this.screen.darken();
		this.screen.rect_fill(0, 0, 512, 6, 0);
		for (int k1 = 6; k1 >= 1; k1--) {
			this.screen.blur(0, k1, 0, k1, 512, 8);
		}
		this.screen.rect_fill(0, 194, 512, 20, 0);
		for (int l1 = 6; l1 >= 1; l1--) {
			this.screen.blur(0, l1, 0, 194, 512, 8);
		}
		this.screen.sprite_plot(15, 15, this.ui_sprite_off + 10);
		this.screen.row_sprite_define(this.ui_sprite_off + 10, 0, 0, 512, 200);
		this.screen.sprite_posterize(this.ui_sprite_off + 10);
	}

	public void method75() {
		if (this.anInt647 > 0)
			this.anInt647 -= 1;
		if (this.ui_state == 0) {
			this.initial_ui.consume(this.mouse_x, this.mouse_y,
					this.click_state, this.mouse_state);
			if (this.initial_ui.selected(this.register_button_id)) {
				this.ui_state = 1;
				this.register_ui.set_text(this.register_user_field_id, "");
				this.register_ui.set_text(this.register_pass_field_id, "");
				this.register_ui.set_text(this.register_confirm_field_id, "");
				this.register_ui.select(this.register_user_field_id);
				this.register_ui.set_toggle(this.register_agree_check_id, 0);
				this.register_ui
						.set_text(this.register_status_field_id,
								"To create an account please enter all the requested details");
			}
			if (this.initial_ui.selected(this.login_button_id)) {
				this.ui_state = 2;
				this.login_ui.set_text(this.login_status_field_id,
						"Please enter your username and password");
				this.login_ui.set_text(this.login_user_field_id, "");
				this.login_ui.set_text(this.login_pass_field_id, "");
				this.login_ui.select(this.login_user_field_id);
				return;
			}
		} else if (this.ui_state == 1) {
			this.register_ui.consume(this.mouse_x, this.mouse_y,
					this.click_state, this.mouse_state);
			if (this.register_ui.selected(this.register_user_field_id))
				this.register_ui.select(this.register_pass_field_id);
			if (this.register_ui.selected(this.register_pass_field_id))
				this.register_ui.select(this.register_confirm_field_id);
			if (this.register_ui.selected(this.register_confirm_field_id))
				this.register_ui.select(this.register_user_field_id);
			if (this.register_ui.selected(this.register_cancel_button_id))
				this.ui_state = 0;
			if (this.register_ui.selected(this.register_submit_button_id)) {
				if ((this.register_ui.text(this.register_user_field_id) == null)
						|| (this.register_ui.text(this.register_user_field_id)
								.length() == 0)
						|| (this.register_ui.text(this.register_pass_field_id) == null)
						|| (this.register_ui.text(this.register_pass_field_id)
								.length() == 0)) {
					this.register_ui
							.set_text(this.register_status_field_id,
									"@yel@Please fill in ALL requested information to continue!");
					return;
				}
				if (!this.register_ui.text(this.register_pass_field_id)
						.equalsIgnoreCase(
								this.register_ui
										.text(this.register_confirm_field_id))) {
					this.register_ui
							.set_text(this.register_status_field_id,
									"@yel@The two passwords entered are not the same as each other!");
					return;
				}
				if (this.register_ui.text(this.register_pass_field_id).length() < 5) {
					this.register_ui
							.set_text(this.register_status_field_id,
									"@yel@Your password must be at least 5 letters long");
					return;
				}
				if (this.register_ui.method332(this.register_agree_check_id) == 0) {
					this.register_ui
							.set_text(this.register_status_field_id,
									"@yel@You must agree to the terms+conditions to continue");
					return;
				}
				this.register_ui.set_text(this.register_status_field_id,
						"Please wait... Creating new account");
				pregame_display();
				reset_ticks();
				String s = this.register_ui.text(this.register_user_field_id);
				String s2 = this.register_ui.text(this.register_pass_field_id);
				register(s, s2);
			}
		} else {
			if (this.ui_state == 2) {
				this.login_ui.consume(this.mouse_x, this.mouse_y,
						this.click_state, this.mouse_state);
				if (this.login_ui.selected(this.login_cancel_button_id))
					this.ui_state = 0;
				if (this.login_ui.selected(this.login_user_field_id))
					this.login_ui.select(this.login_pass_field_id);
				if ((this.login_ui.selected(this.login_pass_field_id))
						|| (this.login_ui.selected(this.login_ok_button_id))) {
					this.aString947 = this.login_ui
							.text(this.login_user_field_id);
					this.aString948 = this.login_ui
							.text(this.login_pass_field_id);
					login(this.aString947, this.aString948, false);
				}
				if (!this.login_ui.selected(this.login_lost_button_id))
					return;
				this.aString947 = this.login_ui.text(this.login_user_field_id);
				this.aString947 = DataUtil.filter_str(this.aString947, 20);
				if (this.aString947.trim().length() == 0) {
					status_text(
							"You must enter your username to recover your password",
							"");
					return;
				}
				status_text(MultiplayerGame.response_status_text[6],
						MultiplayerGame.response_status_text[7]);
				try {
					if (is_applet())
						this.conn = new Connection(this.host, this,
								this.port_off);
					else
						this.conn = new Connection(this.host, null,
								this.port_off);
					this.conn.time_out = MultiplayerGame.time_out;
					this.conn.int_read();
					this.conn.enter(4);
					this.conn.long_put(DataUtil.encode_name(this.aString947));
					this.conn.send();
					this.conn.short_read();
					int rsp = this.conn.read();
					System.out.println(new StringBuilder()
							.append("Getpq response: ").append(rsp).toString());
					if (rsp == 0) {
						status_text(
								"Sorry, the recovery questions for this user have not been set",
								"");
						return;
					}
					for (int j = 0; j < 5; j++) {
						int k = this.conn.read();
						byte[] abyte0 = new byte[5000];
						this.conn.read(abyte0, k);
						String s5 = new String(abyte0, 0, k);
						if (s5.startsWith("~:")) {
							s5 = s5.substring(2);
							int j1 = 0;
							try {
								j1 = Integer.parseInt(s5);
							} catch (Exception _ex) {
							}
							s5 = this.aStringArray1030[j1];
						}
						this.lost_ui.set_text(this.anIntArray988[j], s5);
					}

					if (this.aBoolean976) {
						status_text(
								"Sorry, you have already attempted 1 recovery, try again later",
								"");
						return;
					}
					this.ui_state = 3;
					this.lost_ui
							.set_text(this.anInt978,
									"@yel@To prove this is your account please provide the answers to");
					this.lost_ui
							.set_text(this.anInt979,
									"@yel@your security questions. You will then be able to reset your password");
					for (int l = 0; l < 5; l++) {
						this.lost_ui.set_text(this.anIntArray989[l], "");
					}
					this.lost_ui.set_text(this.anInt980, "");
					this.lost_ui.set_text(this.anInt981, "");
					this.lost_ui.set_text(this.anInt982, "");
					return;
				} catch (Exception _ex) {
					status_text(MultiplayerGame.response_status_text[12],
							MultiplayerGame.response_status_text[13]);

					return;
				}
			}
			if (this.ui_state == 3) {
				this.lost_ui.consume(this.mouse_x, this.mouse_y,
						this.click_state, this.mouse_state);
				if (this.lost_ui.selected(this.anInt983)) {
					String s1 = this.lost_ui.text(this.anInt981);
					String s3 = this.lost_ui.text(this.anInt982);
					if (!s1.equalsIgnoreCase(s3)) {
						status_text(
								"@yel@The two new passwords entered are not the same as each other!",
								"");
						return;
					}
					if (s1.length() < 5) {
						status_text(
								"@yel@Your new password must be at least 5 letters long",
								"");
						return;
					}
					status_text(MultiplayerGame.response_status_text[6],
							MultiplayerGame.response_status_text[7]);
					try {
						if (is_applet())
							this.conn = new Connection(this.host, this,
									this.port_off);
						else
							this.conn = new Connection(this.host, null,
									this.port_off);
						this.conn.time_out = MultiplayerGame.time_out;
						int i1 = this.conn.int_read();
						String s4 = DataUtil.filter_str(
								this.lost_ui.text(this.anInt980), 20);
						String s6 = DataUtil.filter_str(
								this.lost_ui.text(this.anInt981), 20);
						this.conn.enter(8);
						this.conn.long_put(DataUtil
								.encode_name(this.aString947));
						this.conn.int_put(seed());
						this.conn.enc_cred_put(new StringBuilder().append(s4)
								.append(s6).toString(), i1, this.RSA_EXP,
								this.RSA_MOD);
						for (int k1 = 0; k1 < 5; k1++) {
							this.conn.enc_cred_put(DataUtil
									.base47_encode(this.lost_ui
											.text(this.anIntArray989[k1])), i1,
									this.RSA_EXP, this.RSA_MOD);
						}
						this.conn.send();
						this.conn.read();
						int l1 = this.conn.read();
						System.out.println(new StringBuilder()
								.append("Recover response: ").append(l1)
								.toString());
						if (l1 == 0) {
							this.ui_state = 2;
							status_text(
									"Sorry, recovery failed. You may try again in 1 hour",
									"");
							this.aBoolean976 = true;
							return;
						}
						if (l1 == 1) {
							this.ui_state = 2;
							status_text(
									"Your pass has been reset. You may now use the new pass to login",
									"");
							return;
						}
						this.ui_state = 2;
						status_text("Recovery failed! Attempts exceeded?", "");
						return;
					} catch (Exception _ex) {
						status_text(MultiplayerGame.response_status_text[12],
								MultiplayerGame.response_status_text[13]);
					}
				}
				if (this.lost_ui.selected(this.anInt984))
					this.ui_state = 0;
			}
		}
	}

	public void status_text(String s, String s1) {
		if (this.ui_state == 1)
			this.register_ui.set_text(this.register_status_field_id,
					new StringBuilder().append(s).append(" ").append(s1)
							.toString());
		if (this.ui_state == 2)
			this.login_ui.set_text(this.login_status_field_id,
					new StringBuilder().append(s).append(" ").append(s1)
							.toString());
		if (this.ui_state == 3) {
			this.lost_ui.set_text(this.anInt978, s);
			this.lost_ui.set_text(this.anInt979, s1);
		}
		this.aString946 = s1;
		pregame_display();
		reset_ticks();
	}

	public void method46() {
		this.anInt918 = 0;
		method77("@cya@Sorry, you can't logout at the moment", 3);
	}

	public void on_drop() {
		if (this.anInt918 != 0) {
			method45();
			return;
		}
		super.on_drop();
	}

	public void method45() {
		this.ui_state = 0;
		this.login_state = 0;
		this.anInt918 = 0;
	}

	public void method44() {
		this.anInt904 = 0;
		this.anInt918 = 0;
		this.ui_state = 0;
		this.login_state = 1;
		method62();
		this.screen.clear();
		this.screen.copy(this.graphics, 0, 0);
		for (int i = 0; i < this.anInt754; i++) {
			this.scene.remove(this.obj_models[i]);
			this.terrain.obj_remove(this.obj_x[i], this.obj_y[i],
					this.obj_orient[i]);
		}

		for (int j = 0; j < this.anInt763; j++) {
			this.scene.remove(this.wall_models[j]);
			this.terrain.wall_remove(this.wall_x[j], this.wall_y[j],
					this.anIntArray767[j], this.anIntArray768[j]);
		}

		this.anInt754 = 0;
		this.anInt763 = 0;
		this.anInt748 = 0;
		this.local_player_cnt = 0;
		for (int k = 0; k < this.anInt727; k++) {
			this.local_npcs[k] = null;
		}
		for (int l = 0; l < this.anInt728; l++) {
			this.local_players[l] = null;
		}
		this.anInt741 = 0;
		for (int i1 = 0; i1 < this.anInt739; i1++) {
			this.aClass14Array743[i1] = null;
		}
		for (int j1 = 0; j1 < this.anInt740; j1++) {
			this.aClass14Array744[j1] = null;
		}
		for (int k1 = 0; k1 < 50; k1++) {
			this.prayer_on[k1] = false;
		}
		this.anInt668 = 0;
		this.click_state = 0;
		this.mouse_state = 0;
		this.aBoolean882 = false;
		this.aBoolean890 = false;
	}

	public void method47() {
		String s = this.register_ui.text(this.register_user_field_id);
		String s1 = this.register_ui.text(this.register_pass_field_id);
		this.ui_state = 2;
		this.login_ui.set_text(this.login_status_field_id,
				"Please enter your username and password");
		this.login_ui.set_text(this.login_user_field_id, s);
		this.login_ui.set_text(this.login_pass_field_id, s1);
		pregame_display();
		reset_ticks();
		login(s, s1, false);
	}

	public void method76() {
		method30();
		if (this.anInt918 > 0)
			this.anInt918 -= 1;
		if ((this.idle_cntr > 4500) && (this.anInt919 == 0)
				&& (this.anInt918 == 0)) {
			this.idle_cntr -= 500;
			logout();
			return;
		}
		if ((this.local_player.anInt597 == 8)
				|| (this.local_player.anInt597 == 9))
			this.anInt919 = 500;
		if (this.anInt919 > 0)
			this.anInt919 -= 1;
		if (this.aBoolean1008) {
			method71();
			return;
		}
		if (this.aBoolean965) {
			method66();
			return;
		}
		for (int i = 0; i < this.local_player_cnt; i++) {
			Mob class14 = this.local_players[i];
			int k = (class14.anInt600 + 1) % 10;
			if (class14.anInt599 != k) {
				int i1 = -1;
				int l2 = class14.anInt599;
				int j4;
				if (l2 < k)
					j4 = k - l2;
				else
					j4 = 10 + k - l2;
				int j5 = 4;
				if (j4 > 2)
					j5 = (j4 - 1) * 4;
				if ((class14.waypoint_hist_x[l2] - class14.cur_x > this.anInt684 * 3)
						|| (class14.waypoint_hist_y[l2] - class14.cur_y > this.anInt684 * 3)
						|| (class14.waypoint_hist_x[l2] - class14.cur_x < -this.anInt684 * 3)
						|| (class14.waypoint_hist_y[l2] - class14.cur_y < -this.anInt684 * 3)
						|| (j4 > 8)) {
					class14.cur_x = class14.waypoint_hist_x[l2];
					class14.cur_y = class14.waypoint_hist_y[l2];
				} else {
					if (class14.cur_x < class14.waypoint_hist_x[l2]) {
						class14.cur_x += j5;
						class14.anInt596 += 1;
						i1 = 2;
					} else if (class14.cur_x > class14.waypoint_hist_x[l2]) {
						class14.cur_x -= j5;
						class14.anInt596 += 1;
						i1 = 6;
					}
					if ((class14.cur_x - class14.waypoint_hist_x[l2] < j5)
							&& (class14.cur_x - class14.waypoint_hist_x[l2] > -j5))
						class14.cur_x = class14.waypoint_hist_x[l2];
					if (class14.cur_y < class14.waypoint_hist_y[l2]) {
						class14.cur_y += j5;
						class14.anInt596 += 1;
						if (i1 == -1)
							i1 = 4;
						else if (i1 == 2)
							i1 = 3;
						else
							i1 = 5;
					} else if (class14.cur_y > class14.waypoint_hist_y[l2]) {
						class14.cur_y -= j5;
						class14.anInt596 += 1;
						if (i1 == -1)
							i1 = 0;
						else if (i1 == 2)
							i1 = 1;
						else
							i1 = 7;
					}
					if ((class14.cur_y - class14.waypoint_hist_y[l2] < j5)
							&& (class14.cur_y - class14.waypoint_hist_y[l2] > -j5))
						class14.cur_y = class14.waypoint_hist_y[l2];
				}
				if (i1 != -1)
					class14.anInt597 = i1;
				if ((class14.cur_x == class14.waypoint_hist_x[l2])
						&& (class14.cur_y == class14.waypoint_hist_y[l2]))
					class14.anInt599 = ((l2 + 1) % 10);
			} else {
				class14.anInt597 = class14.anInt598;
			}
			if (class14.anInt605 > 0)
				class14.anInt605 -= 1;
			if (class14.anInt607 > 0)
				class14.anInt607 -= 1;
			if (class14.anInt611 > 0)
				class14.anInt611 -= 1;
			if (this.anInt920 > 0) {
				this.anInt920 -= 1;
				if (this.anInt920 == 0)
					method77(
							"You have been granted another life. Be more careful this time!",
							3);
				if (this.anInt920 == 0) {
					method77(
							"You retain your skills. Your objects land where you died",
							3);
				}
			}
		}
		for (int j = 0; j < this.anInt741; j++) {
			Mob class14_1 = this.aClass14Array744[j];
			int j1 = (class14_1.anInt600 + 1) % 10;
			if (class14_1.anInt599 != j1) {
				int i3 = -1;
				int k4 = class14_1.anInt599;
				int k5;
				if (k4 < j1)
					k5 = j1 - k4;
				else
					k5 = 10 + j1 - k4;
				int l5 = 4;
				if (k5 > 2)
					l5 = (k5 - 1) * 4;
				if ((class14_1.waypoint_hist_x[k4] - class14_1.cur_x > this.anInt684 * 3)
						|| (class14_1.waypoint_hist_y[k4] - class14_1.cur_y > this.anInt684 * 3)
						|| (class14_1.waypoint_hist_x[k4] - class14_1.cur_x < -this.anInt684 * 3)
						|| (class14_1.waypoint_hist_y[k4] - class14_1.cur_y < -this.anInt684 * 3)
						|| (k5 > 8)) {
					class14_1.cur_x = class14_1.waypoint_hist_x[k4];
					class14_1.cur_y = class14_1.waypoint_hist_y[k4];
				} else {
					if (class14_1.cur_x < class14_1.waypoint_hist_x[k4]) {
						class14_1.cur_x += l5;
						class14_1.anInt596 += 1;
						i3 = 2;
					} else if (class14_1.cur_x > class14_1.waypoint_hist_x[k4]) {
						class14_1.cur_x -= l5;
						class14_1.anInt596 += 1;
						i3 = 6;
					}
					if ((class14_1.cur_x - class14_1.waypoint_hist_x[k4] < l5)
							&& (class14_1.cur_x - class14_1.waypoint_hist_x[k4] > -l5))
						class14_1.cur_x = class14_1.waypoint_hist_x[k4];
					if (class14_1.cur_y < class14_1.waypoint_hist_y[k4]) {
						class14_1.cur_y += l5;
						class14_1.anInt596 += 1;
						if (i3 == -1)
							i3 = 4;
						else if (i3 == 2)
							i3 = 3;
						else
							i3 = 5;
					} else if (class14_1.cur_y > class14_1.waypoint_hist_y[k4]) {
						class14_1.cur_y -= l5;
						class14_1.anInt596 += 1;
						if (i3 == -1)
							i3 = 0;
						else if (i3 == 2)
							i3 = 1;
						else
							i3 = 7;
					}
					if ((class14_1.cur_y - class14_1.waypoint_hist_y[k4] < l5)
							&& (class14_1.cur_y - class14_1.waypoint_hist_y[k4] > -l5))
						class14_1.cur_y = class14_1.waypoint_hist_y[k4];
				}
				if (i3 != -1)
					class14_1.anInt597 = i3;
				if ((class14_1.cur_x == class14_1.waypoint_hist_x[k4])
						&& (class14_1.cur_y == class14_1.waypoint_hist_y[k4]))
					class14_1.anInt599 = ((k4 + 1) % 10);
			} else {
				class14_1.anInt597 = class14_1.anInt598;
				if (class14_1.anInt595 == 43)
					class14_1.anInt596 += 1;
			}
			if (class14_1.anInt605 > 0)
				class14_1.anInt605 -= 1;
			if (class14_1.anInt607 > 0)
				class14_1.anInt607 -= 1;
			if (class14_1.anInt611 > 0) {
				class14_1.anInt611 -= 1;
			}
		}
		for (int l = 0; l < this.local_player_cnt; l++) {
			Mob class14_2 = this.local_players[l];
			if (class14_2.anInt620 > 0) {
				class14_2.anInt620 -= 1;
			}
		}
		if (this.aBoolean801) {
			if ((this.local_x - this.local_player.cur_x < -500)
					|| (this.local_x - this.local_player.cur_x > 500)
					|| (this.local_y - this.local_player.cur_y < -500)
					|| (this.local_y - this.local_player.cur_y > 500)) {
				this.local_x = this.local_player.cur_x;
				this.local_y = this.local_player.cur_y;
			}
		} else {
			if ((this.local_x - this.local_player.cur_x < -500)
					|| (this.local_x - this.local_player.cur_x > 500)
					|| (this.local_y - this.local_player.cur_y < -500)
					|| (this.local_y - this.local_player.cur_y > 500)) {
				this.local_x = this.local_player.cur_x;
				this.local_y = this.local_player.cur_y;
			}
			if (this.local_x != this.local_player.cur_x)
				this.local_x += (this.local_player.cur_x - this.local_x)
						/ (16 + (this.anInt719 - 500) / 15);
			if (this.local_y != this.local_player.cur_y)
				this.local_y += (this.local_player.cur_y - this.local_y)
						/ (16 + (this.anInt719 - 500) / 15);
			if (this.auto_angle) {
				int k1 = this.anInt723 * 32;
				int j3 = k1 - this.pitch;
				byte byte0 = 1;
				if (j3 != 0) {
					this.anInt724 += 1;
					if (j3 > 128) {
						byte0 = -1;
						j3 = 256 - j3;
					} else if (j3 > 0) {
						byte0 = 1;
					} else if (j3 < -128) {
						byte0 = 1;
						j3 = 256 + j3;
					} else if (j3 < 0) {
						byte0 = -1;
						j3 = -j3;
					}
					this.pitch += (this.anInt724 * j3 + 255) / 256 * byte0;
					this.pitch &= 255;
				} else {
					this.anInt724 = 0;
				}
			}
		}
		if (this.mouse_y > this.wnd_h - 4) {
			if ((this.mouse_x > 15) && (this.mouse_x < 96)
					&& (this.click_state == 1))
				this.anInt830 = 0;
			if ((this.mouse_x > 110) && (this.mouse_x < 194)
					&& (this.click_state == 1)) {
				this.anInt830 = 1;
				this.aClass6_825.anIntArray252[this.anInt826] = 999999;
			}
			if ((this.mouse_x > 215) && (this.mouse_x < 295)
					&& (this.click_state == 1)) {
				this.anInt830 = 2;
				this.aClass6_825.anIntArray252[this.anInt828] = 999999;
			}
			if ((this.mouse_x > 315) && (this.mouse_x < 395)
					&& (this.click_state == 1)) {
				this.anInt830 = 3;
				this.aClass6_825.anIntArray252[this.anInt829] = 999999;
			}
			this.click_state = 0;
			this.mouse_state = 0;
		}
		this.aClass6_825.consume(this.mouse_x, this.mouse_y, this.click_state,
				this.mouse_state);
		if ((this.anInt830 > 0) && (this.mouse_x >= 494)
				&& (this.mouse_y >= this.wnd_h - 66))
			this.click_state = 0;
		if (this.aClass6_825.selected(this.anInt827)) {
			String s = this.aClass6_825.text(this.anInt827);
			this.aClass6_825.set_text(this.anInt827, "");
			if (s.startsWith("::")) {
				if ((s.equalsIgnoreCase("::lostcon")) && (!this.normal_user))
					this.conn.close();
				else if ((s.equalsIgnoreCase("::closecon"))
						&& (!this.normal_user))
					method26();
				else
					method41(s.substring(2));
			} else {
				int k3 = DataUtil.encode_censor(s);
				method40(DataUtil.coded, k3);
				s = DataUtil.decode_censor(DataUtil.coded, 0, k3, true);
				this.local_player.anInt605 = 150;
				this.local_player.aString604 = s;
				method77(
						new StringBuilder()
								.append(this.local_player.aString590)
								.append(": ").append(s).toString(), 2);
			}
		}
		if (this.anInt830 == 0) {
			for (int l1 = 0; l1 < this.anInt831; l1++) {
				if (this.anIntArray833[l1] > 0)
					this.anIntArray833[l1] -= 1;
			}
		}
		if (this.anInt920 != 0)
			this.click_state = 0;
		if ((this.aBoolean861) || (this.aBoolean834)) {
			if (this.mouse_state != 0)
				this.anInt871 += 1;
			else
				this.anInt871 = 0;
			if (this.anInt871 > 300)
				this.anInt872 += 50;
			else if (this.anInt871 > 150)
				this.anInt872 += 5;
			else if (this.anInt871 > 50)
				this.anInt872 += 1;
			else if ((this.anInt871 > 20) && ((this.anInt871 & 0x5) == 0))
				this.anInt872 += 1;
		} else {
			this.anInt871 = 0;
			this.anInt872 = 0;
		}
		if (this.click_state == 1)
			this.anInt668 = 1;
		else if (this.click_state == 2)
			this.anInt668 = 2;
		this.scene.consume_click(this.mouse_x, this.mouse_y);
		this.click_state = 0;
		if (this.auto_angle) {
			if ((this.anInt724 == 0) || (this.aBoolean801)) {
				if (this.aBoolean25) {
					this.anInt723 = (this.anInt723 + 1 & 0x7);
					this.aBoolean25 = false;
					if (!this.aBoolean720) {
						if ((this.anInt723 & 0x1) == 0)
							this.anInt723 = (this.anInt723 + 1 & 0x7);
						for (int i2 = 0; (i2 < 8) && (!method81(this.anInt723)); i2++) {
							this.anInt723 = (this.anInt723 + 1 & 0x7);
						}
					}
				}

				if (this.aBoolean26) {
					this.anInt723 = (this.anInt723 + 7 & 0x7);
					this.aBoolean26 = false;
					if (!this.aBoolean720) {
						if ((this.anInt723 & 0x1) == 0)
							this.anInt723 = (this.anInt723 + 7 & 0x7);
						for (int j2 = 0; (j2 < 8) && (!method81(this.anInt723)); j2++) {
							this.anInt723 = (this.anInt723 + 7 & 0x7);
						}
					}
				}
			}
		} else if (this.aBoolean25)
			this.pitch = (this.pitch + 2 & 0xFF);
		else if (this.aBoolean26)
			this.pitch = (this.pitch - 2 & 0xFF);
		if ((this.aBoolean720) && (this.anInt719 > 550))
			this.anInt719 -= 4;
		else if ((!this.aBoolean720) && (this.anInt719 < 750))
			this.anInt719 += 4;
		if (this.anInt701 > 0)
			this.anInt701 -= 1;
		else if (this.anInt701 < 0)
			this.anInt701 += 1;
		this.scene.method228(17);
		this.anInt696 += 1;
		if (this.anInt696 > 5) {
			this.anInt696 = 0;
			this.anInt697 = (this.anInt697 + 1 & 0x3);
			this.anInt698 = ((this.anInt698 + 1) % 3);
		}
		for (int k2 = 0; k2 < this.anInt754; k2++) {
			int l3 = this.obj_x[k2];
			int l4 = this.obj_y[k2];
			if ((l3 >= 0) && (l4 >= 0) && (l3 < 96) && (l4 < 96)
					&& (this.obj_orient[k2] == 74)) {
				this.obj_models[k2].rotate(1, 0, 0);
			}
		}
		for (int i4 = 0; i4 < this.anInt1025; i4++) {
			this.anIntArray1028[i4] += 1;
			if (this.anIntArray1028[i4] > 50) {
				this.anInt1025 -= 1;
				for (int i5 = i4; i5 < this.anInt1025; i5++) {
					this.anIntArray1026[i5] = this.anIntArray1026[(i5 + 1)];
					this.anIntArray1027[i5] = this.anIntArray1027[(i5 + 1)];
					this.anIntArray1028[i5] = this.anIntArray1028[(i5 + 1)];
					this.anIntArray1029[i5] = this.anIntArray1029[(i5 + 1)];
				}
			}
		}
	}

	public void method77(String s, int i) {
		if ((i == 2) || (i == 4) || (i == 6)) {
			while ((s.length() > 5) && (s.charAt(0) == '@')
					&& (s.charAt(4) == '@'))
				s = s.substring(5);
			int j = s.indexOf(":");
			if (j != -1) {
				String s1 = s.substring(0, j);
				long l = DataUtil.encode_name(s1);
				for (int i1 = 0; i1 < this.ignore_cnt; i1++) {
					if (this.ignores[i1] == l)
						return;
				}
			}
		}
		if (i == 2)
			s = new StringBuilder().append("@yel@").append(s).toString();
		if ((i == 3) || (i == 4))
			s = new StringBuilder().append("@whi@").append(s).toString();
		if (i == 6)
			s = new StringBuilder().append("@cya@").append(s).toString();
		if (this.anInt830 != 0) {
			if ((i == 4) || (i == 3))
				this.anInt821 = 200;
			if ((i == 2) && (this.anInt830 != 1))
				this.anInt822 = 200;
			if ((i == 5) && (this.anInt830 != 2))
				this.anInt823 = 200;
			if ((i == 6) && (this.anInt830 != 3))
				this.anInt824 = 200;
			if ((i == 3) && (this.anInt830 != 0))
				this.anInt830 = 0;
			if ((i == 6) && (this.anInt830 != 3) && (this.anInt830 != 0))
				this.anInt830 = 0;
		}
		for (int k = this.anInt831 - 1; k > 0; k--) {
			this.aStringArray832[k] = this.aStringArray832[(k - 1)];
			this.anIntArray833[k] = this.anIntArray833[(k - 1)];
		}

		this.aStringArray832[0] = s;
		this.anIntArray833[0] = 300;
		if (i == 2)
			if (this.aClass6_825.anIntArray252[this.anInt826] == this.aClass6_825.anIntArray253[this.anInt826] - 4)
				this.aClass6_825.method326(this.anInt826, s, true);
			else
				this.aClass6_825.method326(this.anInt826, s, false);
		if (i == 5)
			if (this.aClass6_825.anIntArray252[this.anInt828] == this.aClass6_825.anIntArray253[this.anInt828] - 4)
				this.aClass6_825.method326(this.anInt828, s, true);
			else
				this.aClass6_825.method326(this.anInt828, s, false);
		if (i == 6) {
			if (this.aClass6_825.anIntArray252[this.anInt829] == this.aClass6_825.anIntArray253[this.anInt829] - 4) {
				this.aClass6_825.method326(this.anInt829, s, true);
				return;
			}
			this.aClass6_825.method326(this.anInt829, s, false);
		}
	}

	public void push_message(String s) {
		if (s.startsWith("@bor@")) {
			method77(s, 4);
			return;
		}
		if (s.startsWith("@que@")) {
			method77(new StringBuilder().append("@whi@").append(s).toString(),
					5);
			return;
		}
		if (s.startsWith("@pri@")) {
			method77(s, 6);
			return;
		}
		method77(s, 3);
	}

	public Mob method78(int i, int x, int y, int l) {
		if (this.local_npcs[i] == null) {
			this.local_npcs[i] = new Mob();
			this.local_npcs[i].idx = i;
			this.local_npcs[i].anInt592 = 0;
		}
		Mob entity = this.local_npcs[i];
		boolean preexist = false;
		for (int i1 = 0; i1 < this.anInt730; i1++) {
			if (this.aClass14Array734[i1].idx != i)
				continue;
			preexist = true;
			break;
		}

		if (preexist) {
			entity.anInt598 = l;
			int j1 = entity.anInt600;
			if ((x != entity.waypoint_hist_x[j1])
					|| (y != entity.waypoint_hist_y[j1])) {
				entity.anInt600 = (j1 = (j1 + 1) % 10);
				entity.waypoint_hist_x[j1] = x;
				entity.waypoint_hist_y[j1] = y;
			}
		} else {
			entity.idx = i;
			entity.anInt599 = 0;
			entity.anInt600 = 0;
			int tmp200_199 = x;
			entity.cur_x = tmp200_199;
			entity.waypoint_hist_x[0] = tmp200_199;
			int tmp214_213 = y;
			entity.cur_y = tmp214_213;
			entity.waypoint_hist_y[0] = tmp214_213;
			entity.anInt598 = (entity.anInt597 = l);
			entity.anInt596 = 0;
		}
		this.local_players[(this.local_player_cnt++)] = entity;
		return entity;
	}

	public Mob method79(int i, int j, int k, int l, int i1) {
		if (this.aClass14Array743[i] == null) {
			this.aClass14Array743[i] = new Mob();
			this.aClass14Array743[i].idx = i;
		}
		Mob class14 = this.aClass14Array743[i];
		boolean flag = false;
		for (int j1 = 0; j1 < this.anInt742; j1++) {
			if (this.aClass14Array745[j1].idx != i)
				continue;
			flag = true;
			break;
		}

		if (flag) {
			class14.anInt595 = i1;
			class14.anInt598 = l;
			int k1 = class14.anInt600;
			if ((j != class14.waypoint_hist_x[k1])
					|| (k != class14.waypoint_hist_y[k1])) {
				class14.anInt600 = (k1 = (k1 + 1) % 10);
				class14.waypoint_hist_x[k1] = j;
				class14.waypoint_hist_y[k1] = k;
			}
		} else {
			class14.idx = i;
			class14.anInt599 = 0;
			class14.anInt600 = 0;
			int tmp197_196 = j;
			class14.cur_x = tmp197_196;
			class14.waypoint_hist_x[0] = tmp197_196;
			int tmp211_210 = k;
			class14.cur_y = tmp211_210;
			class14.waypoint_hist_y[0] = tmp211_210;
			class14.anInt595 = i1;
			class14.anInt598 = (class14.anInt597 = l);
			class14.anInt596 = 0;
		}
		this.aClass14Array744[(this.anInt741++)] = class14;
		return class14;
	}

	public void handle_frame(int op, int sz, byte[] payload) {
		try {
			if (op == 255) {
				this.anInt730 = this.local_player_cnt;
				for (int k = 0; k < this.anInt730; k++) {
					this.aClass14Array734[k] = this.local_players[k];
				}
				int l7 = 8;
				this.anInt736 = DataUtil.bits_get(payload, l7, 10);
				l7 += 10;
				this.anInt737 = DataUtil.bits_get(payload, l7, 12);
				l7 += 12;
				int i14 = DataUtil.bits_get(payload, l7, 4);
				l7 += 4;
				boolean flag1 = method99(this.anInt736, this.anInt737);
				this.anInt736 -= this.anInt709;
				this.anInt737 -= this.anInt710;
				int i23 = this.anInt736 * this.anInt684 + 64;
				int i26 = this.anInt737 * this.anInt684 + 64;
				if (flag1) {
					this.local_player.anInt600 = 0;
					this.local_player.anInt599 = 0;
					this.local_player.cur_x = (this.local_player.waypoint_hist_x[0] = i23);
					this.local_player.cur_y = (this.local_player.waypoint_hist_y[0] = i26);
				}
				this.local_player_cnt = 0;
				this.local_player = method78(this.anInt738, i23, i26, i14);
				int j29 = DataUtil.bits_get(payload, l7, 8);
				l7 += 8;
				for (int i34 = 0; i34 < j29; i34++) {
					Mob class14_3 = this.aClass14Array734[(i34 + 1)];
					int l39 = DataUtil.bits_get(payload, l7, 1);
					l7++;
					if (l39 != 0) {
						int l41 = DataUtil.bits_get(payload, l7, 1);
						l7++;
						if (l41 == 0) {
							int i43 = DataUtil.bits_get(payload, l7, 3);
							l7 += 3;
							int i44 = class14_3.anInt600;
							int k44 = class14_3.waypoint_hist_x[i44];
							int l44 = class14_3.waypoint_hist_y[i44];
							if ((i43 == 2) || (i43 == 1) || (i43 == 3))
								k44 += this.anInt684;
							if ((i43 == 6) || (i43 == 5) || (i43 == 7))
								k44 -= this.anInt684;
							if ((i43 == 4) || (i43 == 3) || (i43 == 5))
								l44 += this.anInt684;
							if ((i43 == 0) || (i43 == 1) || (i43 == 7))
								l44 -= this.anInt684;
							class14_3.anInt598 = i43;
							class14_3.anInt600 = (i44 = (i44 + 1) % 10);
							class14_3.waypoint_hist_x[i44] = k44;
							class14_3.waypoint_hist_y[i44] = l44;
						} else {
							int j43 = DataUtil.bits_get(payload, l7, 4);
							if ((j43 & 0xC) == 12) {
								l7 += 2;
								continue;
							}
							class14_3.anInt598 = DataUtil.bits_get(payload, l7,
									4);
							l7 += 4;
						}
					}
					this.local_players[(this.local_player_cnt++)] = class14_3;
				}

				int j37 = 0;
				while (l7 + 24 < sz * 8) {
					int i40 = DataUtil.bits_get(payload, l7, 11);
					l7 += 11;
					int i42 = DataUtil.bits_get(payload, l7, 5);
					l7 += 5;
					if (i42 > 15)
						i42 -= 32;
					int k43 = DataUtil.bits_get(payload, l7, 5);
					l7 += 5;
					if (k43 > 15)
						k43 -= 32;
					int j14 = DataUtil.bits_get(payload, l7, 4);
					l7 += 4;
					int j44 = DataUtil.bits_get(payload, l7, 1);
					l7++;
					int j23 = (this.anInt736 + i42) * this.anInt684 + 64;
					int j26 = (this.anInt737 + k43) * this.anInt684 + 64;
					method78(i40, j23, j26, j14);
					if (j44 == 0)
						this.anIntArray746[(j37++)] = i40;
				}
				if (j37 > 0) {
					this.conn.enter(254);
					this.conn.short_put(j37);
					for (int j40 = 0; j40 < j37; j40++) {
						Mob npc = this.local_npcs[this.anIntArray746[j40]];
						this.conn.short_put(npc.idx);
						this.conn.short_put(npc.anInt592);
					}

					this.conn.exit();
					j37 = 0;
					return;
				}
			} else {
				if (op == 254) {
					for (int l = 1; l < sz;) {
						if (DataUtil.unsign(payload[l]) == 255) {
							int i8 = 0;
							int k14 = this.anInt736 + payload[(l + 1)] >> 3;
							int j19 = this.anInt737 + payload[(l + 2)] >> 3;
							l += 3;
							for (int k23 = 0; k23 < this.anInt748; k23++) {
								int k26 = (this.anIntArray749[k23] >> 3) - k14;
								int k29 = (this.anIntArray750[k23] >> 3) - j19;
								if ((k26 != 0) || (k29 != 0)) {
									if (k23 != i8) {
										this.anIntArray749[i8] = this.anIntArray749[k23];
										this.anIntArray750[i8] = this.anIntArray750[k23];
										this.anIntArray751[i8] = this.anIntArray751[k23];
										this.anIntArray752[i8] = this.anIntArray752[k23];
									}
									i8++;
								}
							}

							this.anInt748 = i8;
							continue;
						}
						int j8 = DataUtil.short_get(payload, l);
						l += 2;
						int l14 = this.anInt736 + payload[(l++)];
						int k19 = this.anInt737 + payload[(l++)];
						if ((j8 & 0x8000) == 0) {
							this.anIntArray749[this.anInt748] = l14;
							this.anIntArray750[this.anInt748] = k19;
							this.anIntArray751[this.anInt748] = j8;
							this.anIntArray752[this.anInt748] = 0;
							for (int l23 = 0; l23 < this.anInt754; l23++) {
								if ((this.obj_x[l23] != l14)
										|| (this.obj_y[l23] != k19))
									continue;
								this.anIntArray752[this.anInt748] = Config.anIntArray547[this.obj_orient[l23]];
								break;
							}

							this.anInt748 += 1;
						} else {
							j8 &= 32767;
							int i24 = 0;
							for (int l26 = 0; l26 < this.anInt748; l26++) {
								if ((this.anIntArray749[l26] != l14)
										|| (this.anIntArray750[l26] != k19)
										|| (this.anIntArray751[l26] != j8)) {
									if (l26 != i24) {
										this.anIntArray749[i24] = this.anIntArray749[l26];
										this.anIntArray750[i24] = this.anIntArray750[l26];
										this.anIntArray751[i24] = this.anIntArray751[l26];
										this.anIntArray752[i24] = this.anIntArray752[l26];
									}
									i24++;
								} else {
									j8 = -123;
								}
							}
							this.anInt748 = i24;
						}
					}

					return;
				}
				if (op == 253) {
					for (int i1 = 1; i1 < sz;) {
						if (DataUtil.unsign(payload[i1]) == 255) {
							int k8 = 0;
							int i15 = this.anInt736 + payload[(i1 + 1)] >> 3;
							int l19 = this.anInt737 + payload[(i1 + 2)] >> 3;
							i1 += 3;
							for (int j24 = 0; j24 < this.anInt754; j24++) {
								int i27 = (this.obj_x[j24] >> 3) - i15;
								int l29 = (this.obj_y[j24] >> 3) - l19;
								if ((i27 != 0) || (l29 != 0)) {
									if (j24 != k8) {
										this.obj_models[k8] = this.obj_models[j24];
										this.obj_models[k8].anInt323 = k8;
										this.obj_x[k8] = this.obj_x[j24];
										this.obj_y[k8] = this.obj_y[j24];
										this.obj_orient[k8] = this.obj_orient[j24];
										this.anIntArray759[k8] = this.anIntArray759[j24];
									}
									k8++;
								} else {
									this.scene.remove(this.obj_models[j24]);
									this.terrain.obj_remove(this.obj_x[j24],
											this.obj_y[j24],
											this.obj_orient[j24]);
								}
							}

							this.anInt754 = k8;
							continue;
						}
						int l8 = DataUtil.short_get(payload, i1);
						i1 += 2;
						int j15 = this.anInt736 + payload[(i1++)];
						int i20 = this.anInt737 + payload[(i1++)];
						int k24 = 0;
						for (int j27 = 0; j27 < this.anInt754; j27++) {
							if ((this.obj_x[j27] != j15)
									|| (this.obj_y[j27] != i20)) {
								if (j27 != k24) {
									this.obj_models[k24] = this.obj_models[j27];
									this.obj_models[k24].anInt323 = k24;
									this.obj_x[k24] = this.obj_x[j27];
									this.obj_y[k24] = this.obj_y[j27];
									this.obj_orient[k24] = this.obj_orient[j27];
									this.anIntArray759[k24] = this.anIntArray759[j27];
								}
								k24++;
							} else {
								this.scene.remove(this.obj_models[j27]);
								this.terrain.obj_remove(this.obj_x[j27],
										this.obj_y[j27], this.obj_orient[j27]);
							}
						}
						this.anInt754 = k24;
						if (l8 != 60000) {
							int i30 = this.terrain.method149(j15, i20);
							int k37;
							int j34;
							if ((i30 == 0) || (i30 == 4)) {
								j34 = Config.anIntArray544[l8];
								k37 = Config.anIntArray545[l8];
							} else {
								k37 = Config.anIntArray544[l8];
								j34 = Config.anIntArray545[l8];
							}
							int k40 = (j15 + j15 + j34) * this.anInt684 / 2;
							int j42 = (i20 + i20 + k37) * this.anInt684 / 2;
							int l43 = Config.model_id[l8];
							Model class7_1 = this.models[l43].copy();
							this.scene.add(class7_1);
							class7_1.anInt323 = this.anInt754;
							class7_1.rotate(0, i30 * 32, 0);
							class7_1.translate(k40,
									-this.terrain.calc_z(k40, j42), j42);
							class7_1.set_light(true, 48, 48, -50, -10, -50);
							this.terrain.obj_plot(j15, i20, l8);
							if (l8 == 74)
								class7_1.translate(0, -480, 0);
							this.obj_x[this.anInt754] = j15;
							this.obj_y[this.anInt754] = i20;
							this.obj_orient[this.anInt754] = l8;
							this.anIntArray759[this.anInt754] = i30;
							this.obj_models[(this.anInt754++)] = class7_1;
						}
					}

					return;
				}
				if (op == 252) {
					int j1 = 1;
					this.anInt772 = (payload[(j1++)] & 0xFF);
					for (int i9 = 0; i9 < this.anInt772; i9++) {
						int k15 = DataUtil.short_get(payload, j1);
						j1 += 2;
						this.anIntArray773[i9] = (k15 & 0x7FFF);
						this.anIntArray775[i9] = (k15 / 32768);
						if (Config.item_stackable[(k15 & 0x7FFF)] == 0) {
							this.anIntArray774[i9] = DataUtil.smart_get(
									payload, j1);
							if (this.anIntArray774[i9] >= 128)
								j1 += 4;
							else
								j1++;
						} else {
							this.anIntArray774[i9] = 1;
						}
					}

					return;
				}
				if (op == 250) {
					int k1 = DataUtil.short_get(payload, 1);
					int j9 = 3;
					for (int l15 = 0; l15 < k1; l15++) {
						int j20 = DataUtil.short_get(payload, j9);
						j9 += 2;
						Mob class14 = this.local_npcs[j20];
						byte byte6 = payload[j9];
						j9++;
						if (byte6 == 0) {
							int j30 = DataUtil.short_get(payload, j9);
							j9 += 2;
							if (class14 != null) {
								class14.anInt607 = 150;
								class14.anInt606 = j30;
							}
						} else if (byte6 == 1) {
							byte byte7 = payload[j9];
							j9++;
							if (class14 != null) {
								String s2 = DataUtil.decode_censor(payload, j9,
										byte7, true);
								boolean flag3 = false;
								for (int l40 = 0; l40 < this.ignore_cnt; l40++) {
									if (this.ignores[l40] == class14.aLong589)
										flag3 = true;
								}
								if (!flag3) {
									class14.anInt605 = 150;
									class14.aString604 = s2;
									method77(
											new StringBuilder()
													.append(class14.aString590)
													.append(": ")
													.append(class14.aString604)
													.toString(), 2);
								}
							}
							j9 += byte7;
						} else if (byte6 == 2) {
							int k30 = DataUtil.unsign(payload[j9]);
							j9++;
							int k34 = DataUtil.unsign(payload[j9]);
							j9++;
							int l37 = DataUtil.unsign(payload[j9]);
							j9++;
							if (class14 != null) {
								class14.anInt608 = k30;
								class14.anInt609 = k34;
								class14.anInt610 = l37;
								class14.anInt611 = 200;
								if (class14 == this.local_player) {
									this.anIntArray779[3] = k34;
									this.anIntArray780[3] = l37;
									this.aBoolean910 = false;
									this.aBoolean916 = false;
								}
							}
						} else if (byte6 == 3) {
							int l30 = DataUtil.short_get(payload, j9);
							j9 += 2;
							int l34 = DataUtil.short_get(payload, j9);
							j9 += 2;
							if (class14 != null) {
								class14.anInt617 = l30;
								class14.anInt619 = l34;
								class14.anInt618 = -1;
								class14.anInt620 = this.anInt695;
							}
						} else if (byte6 == 4) {
							int i31 = DataUtil.short_get(payload, j9);
							j9 += 2;
							int i35 = DataUtil.short_get(payload, j9);
							j9 += 2;
							if (class14 != null) {
								class14.anInt617 = i31;
								class14.anInt618 = i35;
								class14.anInt619 = -1;
								class14.anInt620 = this.anInt695;
							}
						} else if (byte6 == 5) {
							if (class14 != null) {
								class14.anInt592 = DataUtil.short_get(payload,
										j9);
								j9 += 2;
								class14.aLong589 = DataUtil.long_get(payload,
										j9);
								j9 += 8;
								class14.aString590 = DataUtil
										.decode_name(class14.aLong589);
								int j31 = DataUtil.unsign(payload[j9]);
								j9++;
								for (int j35 = 0; j35 < j31; j35++) {
									class14.sprites[j35] = DataUtil.unsign(payload[j9]);
									j9++;
								}

								for (int i38 = j31; i38 < 12; i38++) {
									class14.sprites[i38] = 0;
								}
								class14.anInt613 = (payload[(j9++)] & 0xFF);
								class14.anInt614 = (payload[(j9++)] & 0xFF);
								class14.anInt615 = (payload[(j9++)] & 0xFF);
								class14.anInt616 = (payload[(j9++)] & 0xFF);
								class14.anInt612 = (payload[(j9++)] & 0xFF);
								class14.anInt623 = (payload[(j9++)] & 0xFF);
							} else {
								j9 += 14;
								int k31 = DataUtil.unsign(payload[j9]);
								j9 += k31 + 1;
							}
						} else if (byte6 == 6) {
							byte byte8 = payload[j9];
							j9++;
							if (class14 != null) {
								String s3 = DataUtil.decode_censor(payload, j9,
										byte8, false);
								class14.anInt605 = 150;
								class14.aString604 = s3;
								if (class14 == this.local_player)
									method77(
											new StringBuilder()
													.append(class14.aString590)
													.append(": ")
													.append(class14.aString604)
													.toString(), 5);
							}
							j9 += byte8;
						}
					}

					return;
				}
				if (op == 249) {
					for (int l1 = 1; l1 < sz;) {
						if (DataUtil.unsign(payload[l1]) == 255) {
							int k9 = 0;
							int i16 = this.anInt736 + payload[(l1 + 1)] >> 3;
							int k20 = this.anInt737 + payload[(l1 + 2)] >> 3;
							l1 += 3;
							for (int l24 = 0; l24 < this.anInt763; l24++) {
								int k27 = (this.wall_x[l24] >> 3) - i16;
								int l31 = (this.wall_y[l24] >> 3) - k20;
								if ((k27 != 0) || (l31 != 0)) {
									if (l24 != k9) {
										this.wall_models[k9] = this.wall_models[l24];
										this.wall_models[k9].anInt323 = (k9 + 10000);
										this.wall_x[k9] = this.wall_x[l24];
										this.wall_y[k9] = this.wall_y[l24];
										this.anIntArray767[k9] = this.anIntArray767[l24];
										this.anIntArray768[k9] = this.anIntArray768[l24];
									}
									k9++;
								} else {
									this.scene.remove(this.wall_models[l24]);
									this.terrain.wall_remove(this.wall_x[l24],
											this.wall_y[l24],
											this.anIntArray767[l24],
											this.anIntArray768[l24]);
								}
							}

							this.anInt763 = k9;
							continue;
						}
						int l9 = DataUtil.short_get(payload, l1);
						l1 += 2;
						int j16 = this.anInt736 + payload[(l1++)];
						int l20 = this.anInt737 + payload[(l1++)];
						byte byte5 = payload[(l1++)];
						int l27 = 0;
						for (int i32 = 0; i32 < this.anInt763; i32++) {
							if ((this.wall_x[i32] != j16)
									|| (this.wall_y[i32] != l20)
									|| (this.anIntArray767[i32] != byte5)) {
								if (i32 != l27) {
									this.wall_models[l27] = this.wall_models[i32];
									this.wall_models[l27].anInt323 = (l27 + 10000);
									this.wall_x[l27] = this.wall_x[i32];
									this.wall_y[l27] = this.wall_y[i32];
									this.anIntArray767[l27] = this.anIntArray767[i32];
									this.anIntArray768[l27] = this.anIntArray768[i32];
								}
								l27++;
							} else {
								this.scene.remove(this.wall_models[i32]);
								this.terrain.wall_remove(this.wall_x[i32],
										this.wall_y[i32],
										this.anIntArray767[i32],
										this.anIntArray768[i32]);
							}
						}
						this.anInt763 = l27;
						if (l9 != 65535) {
							this.terrain.wall_plot(j16, l20, byte5, l9);
							Model class7 = method100(j16, l20, byte5, l9,
									this.anInt763);
							this.wall_models[this.anInt763] = class7;
							this.wall_x[this.anInt763] = j16;
							this.wall_y[this.anInt763] = l20;
							this.anIntArray768[this.anInt763] = l9;
							this.anIntArray767[(this.anInt763++)] = byte5;
						}
					}

					return;
				}
				if (op == 248) {
					this.anInt742 = this.anInt741;
					this.anInt741 = 0;
					for (int i2 = 0; i2 < this.anInt742; i2++) {
						this.aClass14Array745[i2] = this.aClass14Array744[i2];
					}
					int i10 = 8;
					int k16 = DataUtil.bits_get(payload, i10, 8);
					i10 += 8;
					for (int i21 = 0; i21 < k16; i21++) {
						Mob class14_1 = this.aClass14Array745[i21];
						int i28 = DataUtil.bits_get(payload, i10, 1);
						i10++;
						if (i28 != 0) {
							int j32 = DataUtil.bits_get(payload, i10, 1);
							i10++;
							if (j32 == 0) {
								int k35 = DataUtil.bits_get(payload, i10, 3);
								i10 += 3;
								int j38 = class14_1.anInt600;
								int i41 = class14_1.waypoint_hist_x[j38];
								int k42 = class14_1.waypoint_hist_y[j38];
								if ((k35 == 2) || (k35 == 1) || (k35 == 3))
									i41 += this.anInt684;
								if ((k35 == 6) || (k35 == 5) || (k35 == 7))
									i41 -= this.anInt684;
								if ((k35 == 4) || (k35 == 3) || (k35 == 5))
									k42 += this.anInt684;
								if ((k35 == 0) || (k35 == 1) || (k35 == 7))
									k42 -= this.anInt684;
								class14_1.anInt598 = k35;
								class14_1.anInt600 = (j38 = (j38 + 1) % 10);
								class14_1.waypoint_hist_x[j38] = i41;
								class14_1.waypoint_hist_y[j38] = k42;
							} else {
								int l35 = DataUtil.bits_get(payload, i10, 4);
								if ((l35 & 0xC) == 12) {
									i10 += 2;
									continue;
								}
								class14_1.anInt598 = DataUtil.bits_get(payload,
										i10, 4);
								i10 += 4;
							}
						}
						this.aClass14Array744[(this.anInt741++)] = class14_1;
					}

					while (i10 + 31 < sz * 8) {
						int i25 = DataUtil.bits_get(payload, i10, 11);
						i10 += 11;
						int j28 = DataUtil.bits_get(payload, i10, 5);
						i10 += 5;
						if (j28 > 15)
							j28 -= 32;
						int k32 = DataUtil.bits_get(payload, i10, 5);
						i10 += 5;
						if (k32 > 15)
							k32 -= 32;
						int i36 = DataUtil.bits_get(payload, i10, 4);
						i10 += 4;
						int k38 = (this.anInt736 + j28) * this.anInt684 + 64;
						int j41 = (this.anInt737 + k32) * this.anInt684 + 64;
						int l42 = DataUtil.bits_get(payload, i10, 9);
						i10 += 9;
						if (l42 >= Config.npc_cnt)
							l42 = 24;
						method79(i25, k38, j41, i36, l42);
					}
					return;
				}
				if (op == 247) {
					int j2 = DataUtil.short_get(payload, 1);
					int j10 = 3;
					for (int l16 = 0; l16 < j2; l16++) {
						int j21 = DataUtil.short_get(payload, j10);
						j10 += 2;
						Mob class14_2 = this.aClass14Array743[j21];
						int k28 = DataUtil.unsign(payload[j10]);
						j10++;
						if (k28 == 1) {
							int l32 = DataUtil.short_get(payload, j10);
							j10 += 2;
							byte byte9 = payload[j10];
							j10++;
							if (class14_2 != null) {
								String s4 = DataUtil.decode_censor(payload,
										j10, byte9, false);
								class14_2.anInt605 = 150;
								class14_2.aString604 = s4;
								if (l32 == this.local_player.idx)
									method77(
											new StringBuilder()
													.append("@yel@")
													.append(Config.npc_name[class14_2.anInt595])
													.append(": ")
													.append(class14_2.aString604)
													.toString(), 5);
							}
							j10 += byte9;
						} else if (k28 == 2) {
							int i33 = DataUtil.unsign(payload[j10]);
							j10++;
							int j36 = DataUtil.unsign(payload[j10]);
							j10++;
							int l38 = DataUtil.unsign(payload[j10]);
							j10++;
							if (class14_2 != null) {
								class14_2.anInt608 = i33;
								class14_2.anInt609 = j36;
								class14_2.anInt610 = l38;
								class14_2.anInt611 = 200;
							}
						}
					}

					return;
				}
				if (op == 246) {
					this.aBoolean901 = true;
					int k2 = DataUtil.unsign(payload[1]);
					this.anInt902 = k2;
					int k10 = 2;
					for (int i17 = 0; i17 < k2; i17++) {
						int k21 = DataUtil.unsign(payload[k10]);
						k10++;
						this.aStringArray903[i17] = new String(payload, k10,
								k21);
						k10 += k21;
					}

					return;
				}
				if (op == 245) {
					this.aBoolean901 = false;
					return;
				}
				if (op == 244) {
					this.world_loading = true;
					this.anInt738 = DataUtil.short_get(payload, 1);
					this.anInt705 = DataUtil.short_get(payload, 3);
					this.anInt706 = DataUtil.short_get(payload, 5);
					this.anInt711 = DataUtil.short_get(payload, 7);
					this.anInt707 = DataUtil.short_get(payload, 9);
					this.anInt706 -= this.anInt711 * this.anInt707;
					return;
				}
				if (op == 243) {
					int l2 = 1;
					for (int l10 = 0; l10 < 16; l10++) {
						this.anIntArray779[l10] = DataUtil
								.unsign(payload[(l2++)]);
					}
					for (int j17 = 0; j17 < 16; j17++) {
						this.anIntArray780[j17] = DataUtil
								.unsign(payload[(l2++)]);
					}
					for (int l21 = 0; l21 < 16; l21++) {
						this.anIntArray781[l21] = DataUtil.int_get(payload, l2);
						l2 += 4;
					}

					this.anInt783 = DataUtil.unsign(payload[(l2++)]);
					return;
				}
				if (op == 242) {
					for (int i3 = 0; i3 < 5; i3++) {
						this.anIntArray782[i3] = DataUtil
								.unsign(payload[(1 + i3)]);
					}
					return;
				}
				if (op == 241) {
					this.anInt920 = 250;
					return;
				}
				if (op == 240) {
					int j3 = (sz - 1) / 4;
					for (int i11 = 0; i11 < j3; i11++) {
						int k17 = this.anInt736
								+ DataUtil.signed_short_get(payload,
										1 + i11 * 4) >> 3;
						int i22 = this.anInt737
								+ DataUtil.signed_short_get(payload,
										3 + i11 * 4) >> 3;
						int j25 = 0;
						for (int l28 = 0; l28 < this.anInt748; l28++) {
							int j33 = (this.anIntArray749[l28] >> 3) - k17;
							int k36 = (this.anIntArray750[l28] >> 3) - i22;
							if ((j33 != 0) || (k36 != 0)) {
								if (l28 != j25) {
									this.anIntArray749[j25] = this.anIntArray749[l28];
									this.anIntArray750[j25] = this.anIntArray750[l28];
									this.anIntArray751[j25] = this.anIntArray751[l28];
									this.anIntArray752[j25] = this.anIntArray752[l28];
								}
								j25++;
							}
						}

						this.anInt748 = j25;
						j25 = 0;
						for (int k33 = 0; k33 < this.anInt754; k33++) {
							int l36 = (this.obj_x[k33] >> 3) - k17;
							int i39 = (this.obj_y[k33] >> 3) - i22;
							if ((l36 != 0) || (i39 != 0)) {
								if (k33 != j25) {
									this.obj_models[j25] = this.obj_models[k33];
									this.obj_models[j25].anInt323 = j25;
									this.obj_x[j25] = this.obj_x[k33];
									this.obj_y[j25] = this.obj_y[k33];
									this.obj_orient[j25] = this.obj_orient[k33];
									this.anIntArray759[j25] = this.anIntArray759[k33];
								}
								j25++;
							} else {
								this.scene.remove(this.obj_models[k33]);
								this.terrain.obj_remove(this.obj_x[k33],
										this.obj_y[k33], this.obj_orient[k33]);
							}
						}

						this.anInt754 = j25;
						j25 = 0;
						for (int i37 = 0; i37 < this.anInt763; i37++) {
							int j39 = (this.wall_x[i37] >> 3) - k17;
							int k41 = (this.wall_y[i37] >> 3) - i22;
							if ((j39 != 0) || (k41 != 0)) {
								if (i37 != j25) {
									this.wall_models[j25] = this.wall_models[i37];
									this.wall_models[j25].anInt323 = (j25 + 10000);
									this.wall_x[j25] = this.wall_x[i37];
									this.wall_y[j25] = this.wall_y[i37];
									this.anIntArray767[j25] = this.anIntArray767[i37];
									this.anIntArray768[j25] = this.anIntArray768[i37];
								}
								j25++;
							} else {
								this.scene.remove(this.wall_models[i37]);
								this.terrain.wall_remove(this.wall_x[i37],
										this.wall_y[i37],
										this.anIntArray767[i37],
										this.anIntArray768[i37]);
							}
						}

						this.anInt763 = j25;
					}

					return;
				}
				if (op == 239) {
					this.aBoolean1008 = true;
					return;
				}
				if (op == 238) {
					int k3 = DataUtil.short_get(payload, 1);
					if (this.local_npcs[k3] != null)
						this.aString862 = this.local_npcs[k3].aString590;
					this.aBoolean861 = true;
					this.aBoolean869 = false;
					this.aBoolean870 = false;
					this.anInt863 = 0;
					this.anInt866 = 0;
					return;
				}
				if (op == 237) {
					this.aBoolean861 = false;
					this.aBoolean874 = false;
					return;
				}
				if (op == 236) {
					this.anInt866 = (payload[1] & 0xFF);
					int l3 = 2;
					for (int j11 = 0; j11 < this.anInt866; j11++) {
						this.anIntArray867[j11] = DataUtil.short_get(payload,
								l3);
						l3 += 2;
						this.anIntArray868[j11] = DataUtil.int_get(payload, l3);
						l3 += 4;
					}

					this.aBoolean869 = false;
					this.aBoolean870 = false;
					return;
				}
				if (op == 235) {
					byte byte0 = payload[1];
					if (byte0 == 1) {
						this.aBoolean869 = true;
						return;
					}
					this.aBoolean869 = false;
					return;
				}

				if (op == 234) {
					this.aBoolean882 = true;
					int i4 = 1;
					int k11 = payload[(i4++)] & 0xFF;
					byte byte4 = payload[(i4++)];
					this.anInt883 = (payload[(i4++)] & 0xFF);
					this.anInt884 = (payload[(i4++)] & 0xFF);
					for (int j22 = 0; j22 < 40; j22++) {
						this.anIntArray885[j22] = -1;
					}
					for (int k25 = 0; k25 < k11; k25++) {
						this.anIntArray885[k25] = DataUtil.short_get(payload,
								i4);
						i4 += 2;
						this.anIntArray886[k25] = DataUtil.short_get(payload,
								i4);
						i4 += 2;
						this.anIntArray887[k25] = payload[(i4++)];
					}

					if (byte4 == 1) {
						int i29 = 39;
						for (int l33 = 0; (l33 < this.anInt772) && (i29 >= k11); l33++) {
							boolean flag2 = false;
							for (int k39 = 0; k39 < 40; k39++) {
								if (this.anIntArray885[k39] != this.anIntArray773[l33])
									continue;
								flag2 = true;
								break;
							}

							if (this.anIntArray773[l33] == 10)
								flag2 = true;
							if (!flag2) {
								this.anIntArray885[i29] = (this.anIntArray773[l33] & 0x7FFF);
								this.anIntArray886[i29] = 0;
								this.anIntArray887[i29] = 0;
								i29--;
							}
						}
					}

					if ((this.anInt888 >= 0)
							&& (this.anInt888 < 40)
							&& (this.anIntArray885[this.anInt888] != this.anInt889)) {
						this.anInt888 = -1;
						this.anInt889 = -2;
						return;
					}
				} else {
					if (op == 233) {
						this.aBoolean882 = false;
						return;
					}
					if (op == 229) {
						byte byte1 = payload[1];
						if (byte1 == 1) {
							this.aBoolean870 = true;
							return;
						}
						this.aBoolean870 = false;
						return;
					}

					if (op == 228) {
						this.auto_angle = (DataUtil.unsign(payload[1]) == 1);
						this.single_button_mouse = (DataUtil.unsign(payload[2]) == 1);
						this.sound_effects = (DataUtil.unsign(payload[3]) == 1);
						return;
					}
					if (op == 227) {
						for (int j4 = 0; j4 < sz - 1; j4++) {
							boolean flag = payload[(j4 + 1)] == 1;
							if ((this.prayer_on[j4]) && (flag))
								method64("prayeron");
							if ((this.prayer_on[j4]) && (!flag))
								method64("prayeroff");
							this.prayer_on[j4] = flag;
						}

						return;
					}
					if (op == 226) {
						for (int k4 = 0; k4 < this.anInt797; k4++) {
							this.aBooleanArray799[k4] = payload[(k4 + 1)] == 1;
						}
						return;
					}
					if (op == 224) {
						this.aBoolean965 = true;
						for (int l4 = 0; l4 < 5; l4++) {
							this.anIntArray974[l4] = l4;
							this.aStringArray975[l4] = new StringBuilder()
									.append("~:")
									.append(this.anIntArray974[l4]).toString();
							this.aClass6_966.set_text(this.anIntArray971[l4],
									"");
							this.aClass6_966
									.set_text(
											this.anIntArray970[l4],
											new StringBuilder()
													.append(l4 + 1)
													.append(": ")
													.append(this.aStringArray1030[this.anIntArray974[l4]])
													.toString());
						}

						return;
					}
					if (op == 222) {
						this.aBoolean890 = true;
						int i5 = 1;
						this.anInt891 = (payload[(i5++)] & 0xFF);
						this.anInt899 = (payload[(i5++)] & 0xFF);
						for (int l11 = 0; l11 < this.anInt891; l11++) {
							this.anIntArray892[l11] = DataUtil.short_get(
									payload, i5);
							i5 += 2;
							this.anIntArray893[l11] = DataUtil.smart_get(
									payload, i5);
							if (this.anIntArray893[l11] >= 128)
								i5 += 4;
							else {
								i5++;
							}
						}
						method80();
						return;
					}
					if (op == 221) {
						this.aBoolean890 = false;
						return;
					}
					if (op == 220) {
						int j5 = payload[1] & 0xFF;
						this.anIntArray781[j5] = DataUtil.int_get(payload, 2);
						return;
					}
					if (op == 219) {
						int k5 = DataUtil.short_get(payload, 1);
						if (this.local_npcs[k5] != null)
							this.aString835 = this.local_npcs[k5].aString590;
						this.aBoolean834 = true;
						this.anInt836 = 0;
						this.anInt839 = 0;
						this.aBoolean842 = false;
						this.aBoolean843 = false;
						this.aBoolean844 = false;
						this.aBoolean845 = false;
						this.aBoolean846 = false;
						this.aBoolean847 = false;
						return;
					}
					if (op == 218) {
						this.aBoolean834 = false;
						this.aBoolean848 = false;
						return;
					}
					if (op == 217) {
						this.aBoolean874 = true;
						this.aBoolean875 = false;
						this.aBoolean861 = false;
						int l5 = 1;
						this.aLong873 = DataUtil.long_get(payload, l5);
						l5 += 8;
						this.anInt879 = (payload[(l5++)] & 0xFF);
						for (int i12 = 0; i12 < this.anInt879; i12++) {
							this.anIntArray880[i12] = DataUtil.short_get(
									payload, l5);
							l5 += 2;
							this.anIntArray881[i12] = DataUtil.int_get(payload,
									l5);
							l5 += 4;
						}

						this.anInt876 = (payload[(l5++)] & 0xFF);
						for (int l17 = 0; l17 < this.anInt876; l17++) {
							this.anIntArray877[l17] = DataUtil.short_get(
									payload, l5);
							l5 += 2;
							this.anIntArray878[l17] = DataUtil.int_get(payload,
									l5);
							l5 += 4;
						}

						return;
					}
					if (op == 216) {
						this.anInt839 = (payload[1] & 0xFF);
						int i6 = 2;
						for (int j12 = 0; j12 < this.anInt839; j12++) {
							this.anIntArray840[j12] = DataUtil.short_get(
									payload, i6);
							i6 += 2;
							this.anIntArray841[j12] = DataUtil.int_get(payload,
									i6);
							i6 += 4;
						}

						this.aBoolean842 = false;
						this.aBoolean843 = false;
						return;
					}
					if (op == 215) {
						if (payload[1] == 1)
							this.aBoolean844 = true;
						else
							this.aBoolean844 = false;
						if (payload[2] == 1)
							this.aBoolean845 = true;
						else
							this.aBoolean845 = false;
						if (payload[3] == 1)
							this.aBoolean846 = true;
						else
							this.aBoolean846 = false;
						if (payload[4] == 1)
							this.aBoolean847 = true;
						else
							this.aBoolean847 = false;
						this.aBoolean842 = false;
						this.aBoolean843 = false;
						return;
					}
					if (op == 214) {
						int j6 = 1;
						int k12 = payload[(j6++)] & 0xFF;
						int i18 = DataUtil.short_get(payload, j6);
						j6 += 2;
						int k22 = DataUtil.smart_get(payload, j6);
						if (k22 >= 128)
							j6 += 4;
						else
							j6++;
						if (k22 == 0) {
							this.anInt891 -= 1;
							for (int l25 = k12; l25 < this.anInt891; l25++) {
								this.anIntArray892[l25] = this.anIntArray892[(l25 + 1)];
								this.anIntArray893[l25] = this.anIntArray893[(l25 + 1)];
							}
						} else {
							this.anIntArray892[k12] = i18;
							this.anIntArray893[k12] = k22;
							if (k12 >= this.anInt891)
								this.anInt891 = (k12 + 1);
						}
						method80();
						return;
					}
					if (op == 213) {
						int k6 = 1;
						int l12 = 1;
						int j18 = payload[(k6++)] & 0xFF;
						int l22 = DataUtil.short_get(payload, k6);
						k6 += 2;
						if (Config.item_stackable[(l22 & 0x7FFF)] == 0) {
							l12 = DataUtil.smart_get(payload, k6);
							if (l12 >= 128)
								k6 += 4;
							else
								k6++;
						}
						this.anIntArray773[j18] = (l22 & 0x7FFF);
						this.anIntArray775[j18] = (l22 / 32768);
						this.anIntArray774[j18] = l12;
						if (j18 >= this.anInt772) {
							this.anInt772 = (j18 + 1);
							return;
						}
					} else {
						if (op == 212) {
							int l6 = payload[1] & 0xFF;
							this.anInt772 -= 1;
							for (int i13 = l6; i13 < this.anInt772; i13++) {
								this.anIntArray773[i13] = this.anIntArray773[(i13 + 1)];
								this.anIntArray774[i13] = this.anIntArray774[(i13 + 1)];
								this.anIntArray775[i13] = this.anIntArray775[(i13 + 1)];
							}

							return;
						}
						if (op == 211) {
							int i7 = 1;
							int j13 = payload[(i7++)] & 0xFF;
							this.anIntArray779[j13] = DataUtil
									.unsign(payload[(i7++)]);
							this.anIntArray780[j13] = DataUtil
									.unsign(payload[(i7++)]);
							this.anIntArray781[j13] = DataUtil.int_get(payload,
									i7);
							i7 += 4;
							return;
						}
						if (op == 210) {
							byte byte2 = payload[1];
							if (byte2 == 1) {
								this.aBoolean842 = true;
								return;
							}
							this.aBoolean842 = false;
							return;
						}

						if (op == 209) {
							byte byte3 = payload[1];
							if (byte3 == 1) {
								this.aBoolean843 = true;
								return;
							}
							this.aBoolean843 = false;
							return;
						}

						if (op == 208) {
							this.aBoolean848 = true;
							this.aBoolean849 = false;
							this.aBoolean834 = false;
							int j7 = 1;
							this.aLong850 = DataUtil.long_get(payload, j7);
							j7 += 8;
							this.anInt854 = (payload[(j7++)] & 0xFF);
							for (int k13 = 0; k13 < this.anInt854; k13++) {
								this.anIntArray855[k13] = DataUtil.short_get(
										payload, j7);
								j7 += 2;
								this.anIntArray856[k13] = DataUtil.int_get(
										payload, j7);
								j7 += 4;
							}

							this.anInt851 = (payload[(j7++)] & 0xFF);
							for (int k18 = 0; k18 < this.anInt851; k18++) {
								this.anIntArray852[k18] = DataUtil.short_get(
										payload, j7);
								j7 += 2;
								this.anIntArray853[k18] = DataUtil.int_get(
										payload, j7);
								j7 += 4;
							}

							this.anInt857 = (payload[(j7++)] & 0xFF);
							this.anInt858 = (payload[(j7++)] & 0xFF);
							this.anInt859 = (payload[(j7++)] & 0xFF);
							this.anInt860 = (payload[(j7++)] & 0xFF);
							return;
						}
						if (op == 207) {
							String s = new String(payload, 1, sz - 1);
							method64(s);
							return;
						}
						if (op == 206) {
							if (this.anInt1025 < 50) {
								int k7 = payload[1] & 0xFF;
								int l13 = payload[2] + this.anInt736;
								int l18 = payload[3] + this.anInt737;
								this.anIntArray1029[this.anInt1025] = k7;
								this.anIntArray1028[this.anInt1025] = 0;
								this.anIntArray1026[this.anInt1025] = l13;
								this.anIntArray1027[this.anInt1025] = l18;
								this.anInt1025 += 1;
								return;
							}
						} else if (op == 205) {
							if (!this.logged_in) {
								this.prev_login = DataUtil.int_get(payload, 1);
								this.anInt914 = DataUtil.int_get(payload, 5);
								this.anInt911 = DataUtil.int_get(payload, 9);
								this.anInt915 = (int) (Math.random() * 6.0D);
								this.aBoolean910 = true;
								this.logged_in = true;
								this.aString912 = null;
								return;
							}
						} else if (op == 204) {
							this.aString917 = new String(payload, 1, sz - 1);
							this.aBoolean916 = true;
						}
					}
				}
			}
			return;
		} catch (RuntimeException runtimeexception) {
			if (this.anInt662 < 3) {
				this.conn.enter(17);
				this.conn.str_put(runtimeexception.toString());
				this.conn.exit();
				this.conn.enter(17);
				this.conn.str_put(new StringBuilder().append("p-type:")
						.append(op).append(" p-size:").append(sz).toString());
				this.conn.exit();
				this.conn.enter(17);
				this.conn.str_put(new StringBuilder().append("rx:")
						.append(this.anInt736).append(" ry:")
						.append(this.anInt737).append(" num3l:")
						.append(this.anInt754).toString());
				this.conn.exit();
				String s1 = "";
				for (int i19 = 0; (i19 < 80) && (i19 < sz); i19++) {
					s1 = new StringBuilder().append(s1).append(payload[i19])
							.append(" ").toString();
				}
				this.conn.enter(17);
				this.conn.str_put(s1);
				this.conn.exit();
				this.anInt662 += 1;
			}
		}
	}

	public void method80() {
		this.anInt894 = this.anInt891;
		for (int i = 0; i < this.anInt891; i++) {
			this.anIntArray895[i] = this.anIntArray892[i];
			this.anIntArray896[i] = this.anIntArray893[i];
		}

		for (int j = 0; (j < this.anInt772) && (this.anInt894 < this.anInt899); j++) {
			int k = this.anIntArray773[j];
			boolean flag = false;
			for (int l = 0; l < this.anInt894; l++) {
				if (this.anIntArray895[l] != k)
					continue;
				flag = true;
				break;
			}

			if (!flag) {
				this.anIntArray895[this.anInt894] = k;
				this.anIntArray896[this.anInt894] = 0;
				this.anInt894 += 1;
			}
		}
	}

	public boolean method81(int i) {
		int j = this.local_player.cur_x / 128;
		int k = this.local_player.cur_y / 128;
		for (int l = 2; l >= 1; l--) {
			if ((i == 1)
					&& (((this.terrain.collision_flags[j][(k - l)] & 0x80) == 128)
							|| ((this.terrain.collision_flags[(j - l)][k] & 0x80) == 128) || ((this.terrain.collision_flags[(j - l)][(k - l)] & 0x80) == 128)))
				return false;
			if ((i == 3)
					&& (((this.terrain.collision_flags[j][(k + l)] & 0x80) == 128)
							|| ((this.terrain.collision_flags[(j - l)][k] & 0x80) == 128) || ((this.terrain.collision_flags[(j - l)][(k + l)] & 0x80) == 128)))
				return false;
			if ((i == 5)
					&& (((this.terrain.collision_flags[j][(k + l)] & 0x80) == 128)
							|| ((this.terrain.collision_flags[(j + l)][k] & 0x80) == 128) || ((this.terrain.collision_flags[(j + l)][(k + l)] & 0x80) == 128)))
				return false;
			if ((i == 7)
					&& (((this.terrain.collision_flags[j][(k - l)] & 0x80) == 128)
							|| ((this.terrain.collision_flags[(j + l)][k] & 0x80) == 128) || ((this.terrain.collision_flags[(j + l)][(k - l)] & 0x80) == 128)))
				return false;
			if ((i == 0)
					&& ((this.terrain.collision_flags[j][(k - l)] & 0x80) == 128))
				return false;
			if ((i == 2)
					&& ((this.terrain.collision_flags[(j - l)][k] & 0x80) == 128))
				return false;
			if ((i == 4)
					&& ((this.terrain.collision_flags[j][(k + l)] & 0x80) == 128))
				return false;
			if ((i == 6)
					&& ((this.terrain.collision_flags[(j + l)][k] & 0x80) == 128)) {
				return false;
			}
		}
		return true;
	}

	public void method82() {
		if (((this.anInt723 & 0x1) == 1) && (method81(this.anInt723)))
			return;
		if (((this.anInt723 & 0x1) == 0) && (method81(this.anInt723))) {
			if (method81(this.anInt723 + 1 & 0x7)) {
				this.anInt723 = (this.anInt723 + 1 & 0x7);
				return;
			}
			if (method81(this.anInt723 + 7 & 0x7))
				this.anInt723 = (this.anInt723 + 7 & 0x7);
			return;
		}
		int[] ai = { 1, -1, 2, -2, 3, -3, 4 };

		for (int i = 0; i < 7; i++) {
			if (!method81(this.anInt723 + ai[i] + 8 & 0x7))
				continue;
			this.anInt723 = (this.anInt723 + ai[i] + 8 & 0x7);
			break;
		}

		if (((this.anInt723 & 0x1) == 0) && (method81(this.anInt723))) {
			if (method81(this.anInt723 + 1 & 0x7)) {
				this.anInt723 = (this.anInt723 + 1 & 0x7);
				return;
			}
			if (method81(this.anInt723 + 7 & 0x7))
				this.anInt723 = (this.anInt723 + 7 & 0x7);
			return;
		}
	}

	public void game_display() {
		if (this.anInt920 != 0) {
			this.screen.darken();
			this.screen.center_text_draw("Oh dear! You are dead...",
					this.wnd_w / 2, this.wnd_h / 2, 7, 16711680);
			message_links_display();
			this.screen.copy(this.graphics, 0, 0);
			return;
		}
		if (this.aBoolean1008) {
			method70();
			return;
		}
		if (this.aBoolean965) {
			method67();
			return;
		}
		if (!this.terrain.aBoolean83)
			return;
		for (int i = 0; i < 64; i++) {
			this.scene.remove(this.terrain.rooves[this.anInt708][i]);
			if (this.anInt708 == 0) {
				this.scene.remove(this.terrain.walls[1][i]);
				this.scene.remove(this.terrain.rooves[1][i]);
				this.scene.remove(this.terrain.walls[2][i]);
				this.scene.remove(this.terrain.rooves[2][i]);
			}
			this.aBoolean720 = true;
			if ((this.anInt708 == 0)
					&& ((this.terrain.collision_flags[(this.local_player.cur_x / 128)][(this.local_player.cur_y / 128)] & 0x80) == 0)) {
				this.scene.add(this.terrain.rooves[this.anInt708][i]);
				if (this.anInt708 == 0) {
					this.scene.add(this.terrain.walls[1][i]);
					this.scene.add(this.terrain.rooves[1][i]);
					this.scene.add(this.terrain.walls[2][i]);
					this.scene.add(this.terrain.rooves[2][i]);
				}
				this.aBoolean720 = false;
			}
		}

		if (this.anInt697 != this.anInt699) {
			this.anInt699 = this.anInt697;
			for (int j = 0; j < this.anInt754; j++) {
				if (this.obj_orient[j] == 51) {
					int i1 = this.obj_x[j];
					int j2 = this.obj_y[j];
					int l3 = i1 - this.local_player.cur_x / 128;
					int j5 = j2 - this.local_player.cur_y / 128;
					byte byte0 = 7;
					if ((i1 >= 0) && (j2 >= 0) && (i1 < 96) && (j2 < 96)
							&& (l3 > -byte0) && (l3 < byte0) && (j5 > -byte0)
							&& (j5 < byte0)) {
						this.scene.remove(this.obj_models[j]);
						String s = new StringBuilder().append("torcha")
								.append(this.anInt697 + 1).toString();
						int i12 = Config.lookup_model(s);
						Model class7 = this.models[i12].copy();
						this.scene.add(class7);
						class7.set_light(true, 48, 48, -50, -10, -50);
						class7.copy_trans(this.obj_models[j]);
						class7.anInt323 = j;
						this.obj_models[j] = class7;
					}
				}
				if (this.obj_orient[j] == 143) {
					int j1 = this.obj_x[j];
					int k2 = this.obj_y[j];
					int i4 = j1 - this.local_player.cur_x / 128;
					int k5 = k2 - this.local_player.cur_y / 128;
					byte byte1 = 7;
					if ((j1 >= 0) && (k2 >= 0) && (j1 < 96) && (k2 < 96)
							&& (i4 > -byte1) && (i4 < byte1) && (k5 > -byte1)
							&& (k5 < byte1)) {
						this.scene.remove(this.obj_models[j]);
						String s1 = new StringBuilder().append("skulltorcha")
								.append(this.anInt697 + 1).toString();
						int j12 = Config.lookup_model(s1);
						Model class7_1 = this.models[j12].copy();
						this.scene.add(class7_1);
						class7_1.set_light(true, 48, 48, -50, -10, -50);
						class7_1.copy_trans(this.obj_models[j]);
						class7_1.anInt323 = j;
						this.obj_models[j] = class7_1;
					}
				}
			}
		}

		if (this.anInt698 != this.anInt700) {
			this.anInt700 = this.anInt698;
			for (int k = 0; k < this.anInt754; k++) {
				if (this.obj_orient[k] == 97) {
					int k1 = this.obj_x[k];
					int l2 = this.obj_y[k];
					int j4 = k1 - this.local_player.cur_x / 128;
					int l5 = l2 - this.local_player.cur_y / 128;
					byte byte2 = 9;
					if ((k1 >= 0) && (l2 >= 0) && (k1 < 96) && (l2 < 96)
							&& (j4 > -byte2) && (j4 < byte2) && (l5 > -byte2)
							&& (l5 < byte2)) {
						this.scene.remove(this.obj_models[k]);
						String s2 = new StringBuilder().append("firea")
								.append(this.anInt698 + 1).toString();
						int k12 = Config.lookup_model(s2);
						Model class7_2 = this.models[k12].copy();
						this.scene.add(class7_2);
						class7_2.set_light(true, 48, 48, -50, -10, -50);
						class7_2.copy_trans(this.obj_models[k]);
						class7_2.anInt323 = k;
						this.obj_models[k] = class7_2;
					}
				}
				if (this.obj_orient[k] == 274) {
					int l1 = this.obj_x[k];
					int i3 = this.obj_y[k];
					int k4 = l1 - this.local_player.cur_x / 128;
					int i6 = i3 - this.local_player.cur_y / 128;
					byte byte3 = 9;
					if ((l1 >= 0) && (i3 >= 0) && (l1 < 96) && (i3 < 96)
							&& (k4 > -byte3) && (k4 < byte3) && (i6 > -byte3)
							&& (i6 < byte3)) {
						this.scene.remove(this.obj_models[k]);
						String s3 = new StringBuilder().append("fireplacea")
								.append(this.anInt698 + 1).toString();
						int l12 = Config.lookup_model(s3);
						Model class7_3 = this.models[l12].copy();
						this.scene.add(class7_3);
						class7_3.set_light(true, 48, 48, -50, -10, -50);
						class7_3.copy_trans(this.obj_models[k]);
						class7_3.anInt323 = k;
						this.obj_models[k] = class7_3;
					}
				}
			}
		}

		this.scene.reduce(this.scene_reduction);
		this.scene_reduction = 0;
		for (int l = 0; l < this.local_player_cnt; l++) {
			Mob player = this.local_players[l];
			if (player.anInt615 != 255) {
				int j3 = player.cur_x;
				int l4 = player.cur_y;
				int j6 = -this.terrain.calc_z(j3, l4);
				int k7 = this.scene.sprite_add(5000 + l, j3, j6, l4, 145, 220,
						l + 10000);
				this.scene_reduction += 1;
				if (player == this.local_player)
					this.scene.set_local_player(k7);
				if (player.anInt597 == 8)
					this.scene.set_trans_x(k7, -30);
				if (player.anInt597 == 9) {
					this.scene.set_trans_x(k7, 30);
				}
			}
		}
		for (int i2 = 0; i2 < this.local_player_cnt; i2++) {
			Mob player = this.local_players[i2];
			if (player.anInt620 > 0) {
				Mob target = null;
				if (player.anInt619 != -1)
					target = this.aClass14Array743[player.anInt619];
				else if (player.anInt618 != -1)
					target = this.local_npcs[player.anInt618];
				if (target != null) {
					int k6 = player.cur_x;
					int l7 = player.cur_y;
					int j10 = -this.terrain.calc_z(k6, l7) - 110;
					int i13 = target.cur_x;
					int l13 = target.cur_y;
					int i14 = -this.terrain.calc_z(i13, l13)
							- Config.anIntArray524[target.anInt595] / 2;
					int j14 = (k6 * player.anInt620 + i13
							* (this.anInt695 - player.anInt620))
							/ this.anInt695;
					int k14 = (j10 * player.anInt620 + i14
							* (this.anInt695 - player.anInt620))
							/ this.anInt695;
					int l14 = (l7 * player.anInt620 + l13
							* (this.anInt695 - player.anInt620))
							/ this.anInt695;
					this.scene.sprite_add(this.proj_sprite_off
							+ player.anInt617, j14, k14, l14, 32, 32, 0);
					this.scene_reduction += 1;
				}
			}
		}

		for (int k3 = 0; k3 < this.anInt741; k3++) {
			Mob class14_3 = this.aClass14Array744[k3];
			int l6 = class14_3.cur_x;
			int i8 = class14_3.cur_y;
			int k10 = -this.terrain.calc_z(l6, i8);
			int j13 = this.scene.sprite_add(20000 + k3, l6, k10, i8,
					Config.anIntArray523[class14_3.anInt595],
					Config.anIntArray524[class14_3.anInt595], k3 + 30000);
			this.scene_reduction += 1;
			if (class14_3.anInt597 == 8)
				this.scene.set_trans_x(j13, -30);
			if (class14_3.anInt597 == 9) {
				this.scene.set_trans_x(j13, 30);
			}
		}
		for (int i5 = 0; i5 < this.anInt748; i5++) {
			int i7 = this.anIntArray749[i5] * this.anInt684 + 64;
			int j8 = this.anIntArray750[i5] * this.anInt684 + 64;
			this.scene.sprite_add(40000 + this.anIntArray751[i5], i7,
					-this.terrain.calc_z(i7, j8) - this.anIntArray752[i5], j8,
					96, 64, i5 + 20000);
			this.scene_reduction += 1;
		}

		for (int j7 = 0; j7 < this.anInt1025; j7++) {
			int k8 = this.anIntArray1026[j7] * this.anInt684 + 64;
			int l10 = this.anIntArray1027[j7] * this.anInt684 + 64;
			int k13 = this.anIntArray1029[j7];
			if (k13 == 0) {
				this.scene.sprite_add(50000 + j7, k8,
						-this.terrain.calc_z(k8, l10), l10, 128, 256,
						j7 + 50000);
				this.scene_reduction += 1;
			}
			if (k13 == 1) {
				this.scene
						.sprite_add(50000 + j7, k8,
								-this.terrain.calc_z(k8, l10), l10, 128, 64,
								j7 + 50000);
				this.scene_reduction += 1;
			}
		}

		this.screen.skip_lines = false;
		this.screen.clear();
		this.screen.skip_lines = this.reduce_lag;
		if (this.anInt708 == 3) {
			int l8 = 40 + (int) (Math.random() * 3.0D);
			int i11 = 40 + (int) (Math.random() * 7.0D);
			this.scene.set_light(l8, i11, -50, -10, -50);
		}
		this.anInt996 = 0;
		this.anInt990 = 0;
		this.anInt1001 = 0;
		if (this.aBoolean801) {
			if ((this.auto_angle) && (!this.aBoolean720)) {
				int i9 = this.anInt723;
				method82();
				if (this.anInt723 != i9) {
					this.local_x = this.local_player.cur_x;
					this.local_y = this.local_player.cur_y;
				}
			}
			this.scene.clip_far_3d = 3000;
			this.scene.clip_far_2d = 3000;
			this.scene.fog_falloff = 1;
			this.scene.fog_dist = 2800;
			this.pitch = (this.anInt723 * 32);
			int x = this.local_x + this.base_x;
			int y = this.local_y + this.base_y;
			this.scene.set_camera(x, -this.terrain.calc_z(x, y), y, 912,
					this.pitch * 4, 0, 2000);
		} else {
			if ((this.auto_angle) && (!this.aBoolean720))
				method82();
			if (!this.reduce_lag) {
				this.scene.clip_far_3d = 2400;
				this.scene.clip_far_2d = 2400;
				this.scene.fog_falloff = 1;
				this.scene.fog_dist = 2300;
			} else {
				this.scene.clip_far_3d = 2200;
				this.scene.clip_far_2d = 2200;
				this.scene.fog_falloff = 1;
				this.scene.fog_dist = 2100;
			}
			int k9 = this.local_x + this.base_x;
			int k11 = this.local_y + this.base_y;
			this.scene.set_camera(k9, -this.terrain.calc_z(k9, k11), k11, 912,
					this.pitch * 4, 0, this.anInt719 * 2);
		}
		this.scene.render();
		method89();
		if (this.anInt701 > 0)
			this.screen.sprite_plot(this.anInt702 - 8, this.anInt703 - 8,
					this.ui_sprite_off + 14 + (24 - this.anInt701) / 6);
		if (this.anInt701 < 0)
			this.screen.sprite_plot(this.anInt702 - 8, this.anInt703 - 8,
					this.ui_sprite_off + 18 + (24 + this.anInt701) / 6);
		if (!this.world_loading) {
			int l9 = 2203 - (this.anInt737 + this.anInt706 + this.anInt710);
			if (this.anInt736 + this.anInt705 + this.anInt709 >= 2640)
				l9 = -50;
			if (l9 > 0) {
				int l11 = 1 + l9 / 6;
				this.screen.sprite_plot(453, this.wnd_h - 56,
						this.ui_sprite_off + 13);
				this.screen.center_text_draw("Wilderness", 465,
						this.wnd_h - 20, 1, 16776960);
				this.screen.center_text_draw(
						new StringBuilder().append("Level: ").append(l11)
								.toString(), 465, this.wnd_h - 7, 1, 16776960);
				if (this.anInt921 == 0)
					this.anInt921 = 2;
			}
			if ((this.anInt921 == 0) && (l9 > -10) && (l9 <= 0))
				this.anInt921 = 1;
		}
		if (this.anInt830 == 0) {
			for (int i10 = 0; i10 < this.anInt831; i10++) {
				if (this.anIntArray833[i10] > 0) {
					String s4 = this.aStringArray832[i10];
					this.screen.text_draw(s4, 7, this.wnd_h - 18 - i10 * 12, 1,
							16776960);
				}
			}
		}
		this.aClass6_825.disable(this.anInt826);
		this.aClass6_825.disable(this.anInt828);
		this.aClass6_825.disable(this.anInt829);
		if (this.anInt830 == 1)
			this.aClass6_825.enable(this.anInt826);
		else if (this.anInt830 == 2)
			this.aClass6_825.enable(this.anInt828);
		else if (this.anInt830 == 3)
			this.aClass6_825.enable(this.anInt829);
		Menu.anInt290 = 2;
		this.aClass6_825.display();
		Menu.anInt290 = 0;
		this.screen.trans_sprite_plot(this.screen.w - 3 - 197, 3,
				this.ui_sprite_off, 128);
		method101();
		this.screen.logged_in = false;
		message_links_display();
		this.screen.copy(this.graphics, 0, 0);
	}

	public void message_links_display() {
		this.screen.sprite_plot(0, this.wnd_h - 4, this.ui_sprite_off + 23);
		int i = Surface.rgb(200, 200, 255);
		if (this.anInt830 == 0)
			i = Surface.rgb(255, 200, 50);
		if (this.anInt821 % 30 > 15)
			i = Surface.rgb(255, 50, 50);
		this.screen.center_text_draw("All messages", 54, this.wnd_h + 6, 0, i);
		i = Surface.rgb(200, 200, 255);
		if (this.anInt830 == 1)
			i = Surface.rgb(255, 200, 50);
		if (this.anInt822 % 30 > 15)
			i = Surface.rgb(255, 50, 50);
		this.screen.center_text_draw("Chat history", 155, this.wnd_h + 6, 0, i);
		i = Surface.rgb(200, 200, 255);
		if (this.anInt830 == 2)
			i = Surface.rgb(255, 200, 50);
		if (this.anInt823 % 30 > 15)
			i = Surface.rgb(255, 50, 50);
		this.screen
				.center_text_draw("Quest history", 255, this.wnd_h + 6, 0, i);
		i = Surface.rgb(200, 200, 255);
		if (this.anInt830 == 3)
			i = Surface.rgb(255, 200, 50);
		if (this.anInt824 % 30 > 15)
			i = Surface.rgb(255, 50, 50);
		this.screen.center_text_draw("Private history", 355, this.wnd_h + 6, 0,
				i);
	}

	public void bubble_plot(int i, int j, int k, int l, int id, int j1, int k1) {
		int l1 = this.anIntArray1029[id];
		int i2 = this.anIntArray1028[id];
		if (l1 == 0) {
			int j2 = 255 + i2 * 5 * 256;
			this.screen.circle_fill(i + k / 2, j + l / 2, 20 + i2 * 2, j2,
					255 - i2 * 5);
		}
		if (l1 == 1) {
			int k2 = 16711680 + i2 * 5 * 256;
			this.screen.circle_fill(i + k / 2, j + l / 2, 10 + i2, k2,
					255 - i2 * 5);
		}
	}

	public void item_plot(int x, int y, int w, int h, int id, int j1, int k1) {
		int sprite = Config.item_sprite[id] + this.item_sprite_off;
		int tint = Config.item_tint[id];
		this.screen.resize_trans_sprite_plot(x, y, w, h, sprite, tint, 0, 0,
				false);
	}

	public void method87(int i, int j, int k, int l, int i1, int j1, int k1) {
		Mob class14 = this.aClass14Array744[i1];
		int l1 = class14.anInt597 + (this.pitch + 16) / 32 & 0x7;
		boolean flag = false;
		int i2 = l1;
		if (i2 == 5) {
			i2 = 3;
			flag = true;
		} else if (i2 == 6) {
			i2 = 2;
			flag = true;
		} else if (i2 == 7) {
			i2 = 1;
			flag = true;
		}
		int j2 = i2
				* 3
				+ this.anIntArray1020[(class14.anInt596
						/ Config.anIntArray525[class14.anInt595] % 4)];
		if (class14.anInt597 == 8) {
			i2 = 5;
			l1 = 2;
			flag = false;
			i -= Config.anIntArray527[class14.anInt595] * k1 / 100;
			j2 = i2
					* 3
					+ this.anIntArray1021[(this.ticks
							/ (Config.anIntArray526[class14.anInt595] - 1) % 8)];
		} else if (class14.anInt597 == 9) {
			i2 = 5;
			l1 = 2;
			flag = true;
			i += Config.anIntArray527[class14.anInt595] * k1 / 100;
			j2 = i2
					* 3
					+ this.anIntArray1022[(this.ticks
							/ Config.anIntArray526[class14.anInt595] % 8)];
		}
		for (int k2 = 0; k2 < 12; k2++) {
			int l2 = this.anIntArrayArray1007[l1][k2];
			int k3 = Config.anIntArrayArray518[class14.anInt595][l2];
			if (k3 >= 0) {
				int i4 = 0;
				int j4 = 0;
				int k4 = j2;
				if ((flag) && (i2 >= 1) && (i2 <= 3)
						&& (Config.anIntArray536[k3] == 1))
					k4 += 15;
				if ((i2 != 5) || (Config.anIntArray535[k3] == 1)) {
					int l4 = k4 + Config.entity_sprite_off[k3];
					i4 = i4 * k / this.screen.sprite_mask_w[l4];
					j4 = j4 * l / this.screen.sprite_mask_h[l4];
					int i5 = k
							* this.screen.sprite_mask_w[l4]
							/ this.screen.sprite_mask_w[Config.entity_sprite_off[k3]];
					i4 -= (i5 - k) / 2;
					int j5 = Config.anIntArray533[k3];
					int k5 = 0;
					if (j5 == 1) {
						j5 = Config.anIntArray519[class14.anInt595];
						k5 = Config.anIntArray522[class14.anInt595];
					} else if (j5 == 2) {
						j5 = Config.anIntArray520[class14.anInt595];
						k5 = Config.anIntArray522[class14.anInt595];
					} else if (j5 == 3) {
						j5 = Config.anIntArray521[class14.anInt595];
						k5 = Config.anIntArray522[class14.anInt595];
					}
					this.screen.resize_trans_sprite_plot(i + i4, j + j4, i5, l,
							l4, j5, k5, j1, flag);
				}
			}
		}

		if (class14.anInt605 > 0) {
			this.anIntArray994[this.anInt990] = (Surface.text_width(
					class14.aString604, 1) / 2);
			if (this.anIntArray994[this.anInt990] > 150)
				this.anIntArray994[this.anInt990] = 150;
			this.anIntArray995[this.anInt990] = (Surface.text_width(
					class14.aString604, 1) / 300 * Surface.height(1));
			this.anIntArray992[this.anInt990] = (i + k / 2);
			this.anIntArray993[this.anInt990] = j;
			this.aStringArray991[(this.anInt990++)] = class14.aString604;
		}
		if ((class14.anInt597 == 8) || (class14.anInt597 == 9)
				|| (class14.anInt611 != 0)) {
			if (class14.anInt611 > 0) {
				int i3 = i;
				if (class14.anInt597 == 8)
					i3 -= 20 * k1 / 100;
				else if (class14.anInt597 == 9)
					i3 += 20 * k1 / 100;
				int l3 = class14.anInt609 * 30 / class14.anInt610;
				this.anIntArray1002[this.anInt1001] = (i3 + k / 2);
				this.anIntArray1003[this.anInt1001] = j;
				this.anIntArray1004[(this.anInt1001++)] = l3;
			}
			if (class14.anInt611 > 150) {
				int j3 = i;
				if (class14.anInt597 == 8)
					j3 -= 10 * k1 / 100;
				else if (class14.anInt597 == 9)
					j3 += 10 * k1 / 100;
				this.screen.sprite_plot(j3 + k / 2 - 12, j + l / 2 - 12,
						this.ui_sprite_off + 12);
				this.screen.center_text_draw(String.valueOf(class14.anInt608),
						j3 + k / 2 - 1, j + l / 2 + 5, 3, 16777215);
			}
		}
	}

	public void method88(int i, int j, int k, int l, int i1, int j1, int k1) {
		Mob class14 = this.local_players[i1];
		if (class14.anInt615 == 255)
			return;
		int l1 = class14.anInt597 + (this.pitch + 16) / 32 & 0x7;
		boolean flag = false;
		int i2 = l1;
		if (i2 == 5) {
			i2 = 3;
			flag = true;
		} else if (i2 == 6) {
			i2 = 2;
			flag = true;
		} else if (i2 == 7) {
			i2 = 1;
			flag = true;
		}
		int j2 = i2 * 3 + this.anIntArray1020[(class14.anInt596 / 6 % 4)];
		if (class14.anInt597 == 8) {
			i2 = 5;
			l1 = 2;
			flag = false;
			i -= 5 * k1 / 100;
			j2 = i2 * 3 + this.anIntArray1021[(this.ticks / 5 % 8)];
		} else if (class14.anInt597 == 9) {
			i2 = 5;
			l1 = 2;
			flag = true;
			i += 5 * k1 / 100;
			j2 = i2 * 3 + this.anIntArray1022[(this.ticks / 6 % 8)];
		}
		for (int k2 = 0; k2 < 12; k2++) {
			int l2 = this.anIntArrayArray1007[l1][k2];
			int l3 = class14.sprites[l2] - 1;
			if (l3 >= 0) {
				int k4 = 0;
				int i5 = 0;
				int j5 = j2;
				if ((flag) && (i2 >= 1) && (i2 <= 3))
					if (Config.anIntArray536[l3] == 1) {
						j5 += 15;
					} else if ((l2 == 4) && (i2 == 1)) {
						k4 = -22;
						i5 = -3;
						j5 = i2
								* 3
								+ this.anIntArray1020[((2 + class14.anInt596 / 6) % 4)];
					} else if ((l2 == 4) && (i2 == 2)) {
						k4 = 0;
						i5 = -8;
						j5 = i2
								* 3
								+ this.anIntArray1020[((2 + class14.anInt596 / 6) % 4)];
					} else if ((l2 == 4) && (i2 == 3)) {
						k4 = 26;
						i5 = -5;
						j5 = i2
								* 3
								+ this.anIntArray1020[((2 + class14.anInt596 / 6) % 4)];
					} else if ((l2 == 3) && (i2 == 1)) {
						k4 = 22;
						i5 = 3;
						j5 = i2
								* 3
								+ this.anIntArray1020[((2 + class14.anInt596 / 6) % 4)];
					} else if ((l2 == 3) && (i2 == 2)) {
						k4 = 0;
						i5 = 8;
						j5 = i2
								* 3
								+ this.anIntArray1020[((2 + class14.anInt596 / 6) % 4)];
					} else if ((l2 == 3) && (i2 == 3)) {
						k4 = -26;
						i5 = 5;
						j5 = i2
								* 3
								+ this.anIntArray1020[((2 + class14.anInt596 / 6) % 4)];
					}
				if ((i2 != 5) || (Config.anIntArray535[l3] == 1)) {
					int k5 = j5 + Config.entity_sprite_off[l3];
					k4 = k4 * k / this.screen.sprite_mask_w[k5];
					i5 = i5 * l / this.screen.sprite_mask_h[k5];
					int l5 = k
							* this.screen.sprite_mask_w[k5]
							/ this.screen.sprite_mask_w[Config.entity_sprite_off[l3]];
					k4 -= (l5 - k) / 2;
					int i6 = Config.anIntArray533[l3];
					int j6 = this.anIntArray1019[class14.anInt616];
					if (i6 == 1)
						i6 = this.anIntArray1018[class14.anInt613];
					else if (i6 == 2)
						i6 = this.anIntArray1017[class14.anInt614];
					else if (i6 == 3)
						i6 = this.anIntArray1017[class14.anInt615];
					this.screen.resize_trans_sprite_plot(i + k4, j + i5, l5, l,
							k5, i6, j6, j1, flag);
				}
			}
		}

		if (class14.anInt605 > 0) {
			this.anIntArray994[this.anInt990] = (Surface.text_width(
					class14.aString604, 1) / 2);
			if (this.anIntArray994[this.anInt990] > 150)
				this.anIntArray994[this.anInt990] = 150;
			this.anIntArray995[this.anInt990] = (Surface.text_width(
					class14.aString604, 1) / 300 * Surface.height(1));
			this.anIntArray992[this.anInt990] = (i + k / 2);
			this.anIntArray993[this.anInt990] = j;
			this.aStringArray991[(this.anInt990++)] = class14.aString604;
		}
		if (class14.anInt607 > 0) {
			this.anIntArray997[this.anInt996] = (i + k / 2);
			this.anIntArray998[this.anInt996] = j;
			this.anIntArray999[this.anInt996] = k1;
			this.anIntArray1000[(this.anInt996++)] = class14.anInt606;
		}
		if ((class14.anInt597 == 8) || (class14.anInt597 == 9)
				|| (class14.anInt611 != 0)) {
			if (class14.anInt611 > 0) {
				int i3 = i;
				if (class14.anInt597 == 8)
					i3 -= 20 * k1 / 100;
				else if (class14.anInt597 == 9)
					i3 += 20 * k1 / 100;
				int i4 = class14.anInt609 * 30 / class14.anInt610;
				this.anIntArray1002[this.anInt1001] = (i3 + k / 2);
				this.anIntArray1003[this.anInt1001] = j;
				this.anIntArray1004[(this.anInt1001++)] = i4;
			}
			if (class14.anInt611 > 150) {
				int j3 = i;
				if (class14.anInt597 == 8)
					j3 -= 10 * k1 / 100;
				else if (class14.anInt597 == 9)
					j3 += 10 * k1 / 100;
				this.screen.sprite_plot(j3 + k / 2 - 12, j + l / 2 - 12,
						this.ui_sprite_off + 11);
				this.screen.center_text_draw(String.valueOf(class14.anInt608),
						j3 + k / 2 - 1, j + l / 2 + 5, 3, 16777215);
			}
		}
		if ((class14.anInt623 == 1) && (class14.anInt607 == 0)) {
			int k3 = j1 + i + k / 2;
			if (class14.anInt597 == 8)
				k3 -= 20 * k1 / 100;
			else if (class14.anInt597 == 9)
				k3 += 20 * k1 / 100;
			int j4 = 16 * k1 / 100;
			int l4 = 16 * k1 / 100;
			this.screen.resize_sprite_plot(k3 - j4 / 2, j - l4 / 2 - 10 * k1
					/ 100, j4, l4, this.ui_sprite_off + 13);
		}
	}

	public void method89() {
		for (int i = 0; i < this.anInt990; i++) {
			int j = Surface.height(1);
			int l = this.anIntArray992[i];
			int k1 = this.anIntArray993[i];
			int j2 = this.anIntArray994[i];
			int i3 = this.anIntArray995[i];
			boolean flag = true;
			while (flag) {
				flag = false;
				for (int i4 = 0; i4 < i; i4++) {
					if ((k1 + i3 > this.anIntArray993[i4] - j)
							&& (k1 - j < this.anIntArray993[i4]
									+ this.anIntArray995[i4])
							&& (l - j2 < this.anIntArray992[i4]
									+ this.anIntArray994[i4])
							&& (l + j2 > this.anIntArray992[i4]
									- this.anIntArray994[i4])
							&& (this.anIntArray993[i4] - j - i3 < k1)) {
						k1 = this.anIntArray993[i4] - j - i3;
						flag = true;
					}
				}
			}
			this.anIntArray993[i] = k1;
			this.screen.multiline_text_draw(this.aStringArray991[i], l, k1, 1,
					16776960, 300);
		}

		for (int k = 0; k < this.anInt996; k++) {
			int i1 = this.anIntArray997[k];
			int l1 = this.anIntArray998[k];
			int k2 = this.anIntArray999[k];
			int j3 = this.anIntArray1000[k];
			int l3 = 39 * k2 / 100;
			int j4 = 27 * k2 / 100;
			int k4 = l1 - j4;
			this.screen.resize_trans_sprite_plot(i1 - l3 / 2, k4, l3, j4,
					this.ui_sprite_off + 9, 85);
			int l4 = 36 * k2 / 100;
			int i5 = 24 * k2 / 100;
			this.screen.resize_trans_sprite_plot(i1 - l4 / 2, k4 + j4 / 2 - i5
					/ 2, l4, i5, Config.item_sprite[j3] + this.item_sprite_off,
					Config.item_tint[j3], 0, 0, false);
		}

		for (int j1 = 0; j1 < this.anInt1001; j1++) {
			int i2 = this.anIntArray1002[j1];
			int l2 = this.anIntArray1003[j1];
			int k3 = this.anIntArray1004[j1];
			this.screen.rect_fill(i2 - 15, l2 - 3, k3, 5, 65280, 192);
			this.screen.rect_fill(i2 - 15 + k3, l2 - 3, 30 - k3, 5, 16711680,
					192);
		}
	}

	public int method90(int i) {
		int j = 0;
		for (int k = 0; k < this.anInt772; k++) {
			if (this.anIntArray773[k] == i)
				if (Config.item_stackable[i] == 1)
					j++;
				else
					j += this.anIntArray774[k];
		}
		return j;
	}

	public boolean method91(int i, int j) {
		if ((i == 31) && ((method92(197)) || (method92(615))))
			return true;
		if ((i == 32) && ((method92(102)) || (method92(616))))
			return true;
		if ((i == 33) && ((method92(101)) || (method92(617))))
			return true;
		if ((i == 34) && ((method92(103)) || (method92(618))))
			return true;
		return method90(i) >= j;
	}

	public boolean method92(int i) {
		for (int j = 0; j < this.anInt772; j++) {
			if ((this.anIntArray773[j] == i) && (this.anIntArray775[j] == 1))
				return true;
		}
		return false;
	}

	public void method93(int i, int j, int k) {
		this.screen.plot(i, j, k);
		this.screen.plot(i - 1, j, k);
		this.screen.plot(i + 1, j, k);
		this.screen.plot(i, j - 1, k);
		this.screen.plot(i, j + 1, k);
	}

	public void method94(int i, int j, int k, int l, boolean flag) {
		method98(i, j, k, l, k, l, false, flag);
	}

	public void method95(int i, int j, int k, int l, boolean flag) {
		if (method98(i, j, k, l, k, l, false, flag)) {
			return;
		}
		method98(i, j, k, l, k, l, true, flag);
	}

	public void method96(int i, int j, int k, int l) {
		int j1;
		int i1;
		if ((k == 0) || (k == 4)) {
			i1 = Config.anIntArray544[l];
			j1 = Config.anIntArray545[l];
		} else {
			j1 = Config.anIntArray544[l];
			i1 = Config.anIntArray545[l];
		}
		if ((Config.anIntArray546[l] == 2) || (Config.anIntArray546[l] == 3)) {
			if (k == 0) {
				i--;
				i1++;
			}
			if (k == 2)
				j1++;
			if (k == 4)
				i1++;
			if (k == 6) {
				j--;
				j1++;
			}
			method98(this.anInt736, this.anInt737, i, j, i + i1 - 1,
					j + j1 - 1, false, true);
			return;
		}
		method98(this.anInt736, this.anInt737, i, j, i + i1 - 1, j + j1 - 1,
				true, true);
	}

	public void method97(int i, int j, int k) {
		if (k == 0) {
			method98(this.anInt736, this.anInt737, i, j - 1, i, j, false, true);
			return;
		}
		if (k == 1) {
			method98(this.anInt736, this.anInt737, i - 1, j, i, j, false, true);
			return;
		}
		method98(this.anInt736, this.anInt737, i, j, i, j, true, true);
	}

	public boolean method98(int i, int j, int k, int l, int i1, int j1,
			boolean flag, boolean flag1) {
		int k1 = this.terrain.create_path(i, j, k, l, i1, j1,
				this.anIntArray670, this.anIntArray671, flag);
		if (k1 == -1)
			return false;
		k1--;
		i = this.anIntArray670[k1];
		j = this.anIntArray671[k1];
		k1--;
		if (flag1)
			this.conn.enter(215);
		else
			this.conn.enter(255);
		this.conn.short_put(i + this.anInt709);
		this.conn.short_put(j + this.anInt710);
		for (int l1 = k1; (l1 >= 0) && (l1 > k1 - 25); l1--) {
			this.conn.byte_put(this.anIntArray670[l1] - i);
			this.conn.byte_put(this.anIntArray671[l1] - j);
		}

		this.conn.exit();
		this.anInt701 = -24;
		this.anInt702 = this.mouse_x;
		this.anInt703 = this.mouse_y;
		return true;
	}

	public boolean method99(int i, int j) {
		if (this.anInt920 != 0) {
			this.terrain.aBoolean83 = false;
			return false;
		}
		this.world_loading = false;
		i += this.anInt705;
		j += this.anInt706;
		if ((this.anInt708 == this.anInt711) && (i > this.anInt712)
				&& (i < this.anInt714) && (j > this.anInt713)
				&& (j < this.anInt715)) {
			this.terrain.aBoolean83 = true;
			return false;
		}
		this.screen.center_text_draw("Loading... Please wait", 256, 192, 1,
				16777215);
		message_links_display();
		this.screen.copy(this.graphics, 0, 0);
		int k = this.anInt709;
		int l = this.anInt710;
		int i1 = (i + 24) / 48;
		int j1 = (j + 24) / 48;
		this.anInt708 = this.anInt711;
		this.anInt709 = (i1 * 48 - 48);
		this.anInt710 = (j1 * 48 - 48);
		this.anInt712 = (i1 * 48 - 32);
		this.anInt713 = (j1 * 48 - 32);
		this.anInt714 = (i1 * 48 + 32);
		this.anInt715 = (j1 * 48 + 32);
		this.terrain.load(i, j, this.anInt708);
		this.anInt709 -= this.anInt705;
		this.anInt710 -= this.anInt706;
		int k1 = this.anInt709 - k;
		int l1 = this.anInt710 - l;
		for (int i2 = 0; i2 < this.anInt754; i2++) {
			this.obj_x[i2] -= k1;
			this.obj_y[i2] -= l1;
			int j2 = this.obj_x[i2];
			int l2 = this.obj_y[i2];
			int k3 = this.obj_orient[i2];
			Model class7 = this.obj_models[i2];
			try {
				int l4 = this.anIntArray759[i2];
				int i6;
				int k5;
				if ((l4 == 0) || (l4 == 4)) {
					k5 = Config.anIntArray544[k3];
					i6 = Config.anIntArray545[k3];
				} else {
					i6 = Config.anIntArray544[k3];
					k5 = Config.anIntArray545[k3];
				}
				int j6 = (j2 + j2 + k5) * this.anInt684 / 2;
				int k6 = (l2 + l2 + i6) * this.anInt684 / 2;
				if ((j2 >= 0) && (l2 >= 0) && (j2 < 96) && (l2 < 96)) {
					this.scene.add(class7);
					class7.position(j6, -this.terrain.calc_z(j6, k6), k6);
					this.terrain.obj_plot(j2, l2, k3);
					if (k3 == 74)
						class7.translate(0, -480, 0);
				}
			} catch (RuntimeException runtimeexception) {
				System.out.println(new StringBuilder().append("Loc Error: ")
						.append(runtimeexception.getMessage()).toString());
				System.out.println(new StringBuilder().append("i:").append(i2)
						.append(" obj:").append(class7).toString());
				runtimeexception.printStackTrace();
			}
		}

		for (int k2 = 0; k2 < this.anInt763; k2++) {
			this.wall_x[k2] -= k1;
			this.wall_y[k2] -= l1;
			int i3 = this.wall_x[k2];
			int l3 = this.wall_y[k2];
			int j4 = this.anIntArray768[k2];
			int i5 = this.anIntArray767[k2];
			try {
				this.terrain.wall_plot(i3, l3, i5, j4);
				Model class7_1 = method100(i3, l3, i5, j4, k2);
				this.wall_models[k2] = class7_1;
			} catch (RuntimeException runtimeexception1) {
				System.out.println(new StringBuilder().append("Bound Error: ")
						.append(runtimeexception1.getMessage()).toString());
				runtimeexception1.printStackTrace();
			}
		}

		for (int j3 = 0; j3 < this.anInt748; j3++) {
			this.anIntArray749[j3] -= k1;
			this.anIntArray750[j3] -= l1;
		}

		for (int i4 = 0; i4 < this.local_player_cnt; i4++) {
			Mob class14 = this.local_players[i4];
			class14.cur_x -= k1 * this.anInt684;
			class14.cur_y -= l1 * this.anInt684;
			for (int j5 = 0; j5 <= class14.anInt600; j5++) {
				class14.waypoint_hist_x[j5] -= k1 * this.anInt684;
				class14.waypoint_hist_y[j5] -= l1 * this.anInt684;
			}

		}

		for (int k4 = 0; k4 < this.anInt741; k4++) {
			Mob class14_1 = this.aClass14Array744[k4];
			class14_1.cur_x -= k1 * this.anInt684;
			class14_1.cur_y -= l1 * this.anInt684;
			for (int l5 = 0; l5 <= class14_1.anInt600; l5++) {
				class14_1.waypoint_hist_x[l5] -= k1 * this.anInt684;
				class14_1.waypoint_hist_y[l5] -= l1 * this.anInt684;
			}

		}

		this.terrain.aBoolean83 = true;
		return true;
	}

	public Model method100(int i, int j, int k, int l, int i1) {
		int j1 = i;
		int k1 = j;
		int l1 = i;
		int i2 = j;
		int j2 = Config.anIntArray554[l];
		int k2 = Config.anIntArray555[l];
		int l2 = Config.anIntArray553[l];
		Model class7 = new Model(4, 1);
		if (k == 0)
			l1 = i + 1;
		if (k == 1)
			i2 = j + 1;
		if (k == 2) {
			j1 = i + 1;
			i2 = j + 1;
		}
		if (k == 3) {
			l1 = i + 1;
			i2 = j + 1;
		}
		j1 *= this.anInt684;
		k1 *= this.anInt684;
		l1 *= this.anInt684;
		i2 *= this.anInt684;
		int i3 = class7.vert_get(j1, -this.terrain.calc_z(j1, k1), k1);
		int j3 = class7.vert_get(j1, -this.terrain.calc_z(j1, k1) - l2, k1);
		int k3 = class7.vert_get(l1, -this.terrain.calc_z(l1, i2) - l2, i2);
		int l3 = class7.vert_get(l1, -this.terrain.calc_z(l1, i2), i2);
		int[] ai = { i3, j3, k3, l3 };

		class7.face_add(4, ai, j2, k2);
		class7.set_light(false, 60, 24, -50, -10, -50);
		if ((i >= 0) && (j >= 0) && (i < 96) && (j < 96))
			this.scene.add(class7);
		class7.anInt323 = (i1 + 10000);
		return class7;
	}

	public void method101() {
		if (this.anInt918 != 0) {
			method106();
		} else if (this.aBoolean910) {
			method104();
		} else if (this.aBoolean916) {
			method105();
		} else if (this.anInt921 == 1) {
			method107();
		} else if ((this.aBoolean890) && (this.anInt919 == 0)) {
			method110();
		} else if ((this.aBoolean882) && (this.anInt919 == 0)) {
			method111();
		} else if (this.aBoolean874) {
			method112();
		} else if (this.aBoolean861) {
			method113();
		} else if (this.aBoolean848) {
			method114();
		} else if (this.aBoolean834) {
			method115();
		} else if (this.anInt906 != 0) {
			method108();
		} else if (this.anInt905 != 0) {
			method109();
		} else {
			if (this.aBoolean901)
				method102();
			if ((this.local_player.anInt597 == 8)
					|| (this.local_player.anInt597 == 9))
				method103();
			method116();
			boolean flag = (!this.aBoolean901) && (!this.aBoolean805);
			if (flag)
				this.anInt810 = 0;
			if ((this.anInt770 == 0) && (flag))
				method123();
			if (this.anInt770 == 1)
				method117(flag);
			if (this.anInt770 == 2)
				method118(flag);
			if (this.anInt770 == 3)
				method119(flag);
			if (this.anInt770 == 4)
				method120(flag);
			if (this.anInt770 == 5)
				method121(flag);
			if (this.anInt770 == 6)
				method122(flag);
			if ((!this.aBoolean805) && (!this.aBoolean901))
				method125();
			if ((this.aBoolean805) && (!this.aBoolean901))
				method124();
		}
		this.anInt668 = 0;
	}

	public void method102() {
		if (this.anInt668 != 0) {
			for (int i = 0; i < this.anInt902; i++) {
				if ((this.mouse_x >= Surface.text_width(
						this.aStringArray903[i], 1))
						|| (this.mouse_y <= i * 12)
						|| (this.mouse_y >= 12 + i * 12))
					continue;
				this.conn.enter(237);
				this.conn.byte_put(i);
				this.conn.exit();
				break;
			}

			this.anInt668 = 0;
			this.aBoolean901 = false;
			return;
		}
		for (int j = 0; j < this.anInt902; j++) {
			int k = 65535;
			if ((this.mouse_x < Surface.text_width(this.aStringArray903[j],
					1))
					&& (this.mouse_y > j * 12)
					&& (this.mouse_y < 12 + j * 12))
				k = 16711680;
			this.screen
					.text_draw(this.aStringArray903[j], 6, 12 + j * 12, 1, k);
		}
	}

	public void method103() {
		byte byte0 = 7;
		byte byte1 = 15;
		char c = '¯';
		if (this.anInt668 != 0) {
			for (int i = 0; i < 5; i++) {
				if ((i <= 0) || (this.mouse_x <= byte0)
						|| (this.mouse_x >= byte0 + c)
						|| (this.mouse_y <= byte1 + i * 20)
						|| (this.mouse_y >= byte1 + i * 20 + 20))
					continue;
				this.anInt904 = (i - 1);
				this.anInt668 = 0;
				this.conn.enter(231);
				this.conn.byte_put(this.anInt904);
				this.conn.exit();
				break;
			}
		}

		for (int j = 0; j < 5; j++) {
			if (j == this.anInt904 + 1)
				this.screen.rect_fill(byte0, byte1 + j * 20, c, 20,
						Surface.rgb(255, 0, 0), 128);
			else
				this.screen.rect_fill(byte0, byte1 + j * 20, c, 20,
						Surface.rgb(190, 190, 190), 128);
			this.screen.line_horiz(byte0, byte1 + j * 20, c, 0);
			this.screen.line_horiz(byte0, byte1 + j * 20 + 20, c, 0);
		}

		this.screen.center_text_draw("Select combat style", byte0 + c / '\002',
				byte1 + 16, 3, 16777215);
		this.screen.center_text_draw("Controlled (+1 of each)", byte0 + c
				/ '\002', byte1 + 36, 3, 0);
		this.screen.center_text_draw("Aggressive (+3 strength)", byte0 + c
				/ '\002', byte1 + 56, 3, 0);
		this.screen.center_text_draw("Accurate   (+3 attack)", byte0 + c
				/ '\002', byte1 + 76, 3, 0);
		this.screen.center_text_draw("Defensive  (+3 defense)", byte0 + c
				/ '\002', byte1 + 96, 3, 0);
	}

	public void method104() {
		char c = '´';
		int i = 167 - c / '\002';
		this.screen.rect_fill(56, 167 - c / '\002', 400, c, 0);
		this.screen.rect_draw(56, 167 - c / '\002', 400, c, 16777215);
		i += 20;
		this.screen.center_text_draw(
				new StringBuilder().append("Welcome to RuneScape ")
						.append(this.aString947).toString(), 256, i, 4,
				16776960);
		i += 30;
		this.screen.center_text_draw(
				new StringBuilder().append("You last logged in ")
						.append(this.prev_login / 1440).append(" days, ")
						.append(this.prev_login / 60 % 24).append(" hours ago")
						.toString(), 256, i, 1, 16777215);
		i += 15;
		if (this.aString912 == null) {
			this.aString912 = DataUtil.addr_decode(this.anInt911);
			try {
				this.aString912 = InetAddress.getByName(this.aString912)
						.getHostName();
			} catch (Exception exception) {
				String s = exception.getMessage();
				int l = s.indexOf("cannot connect to");
				if (l != -1)
					this.aString912 = s.substring(l + 18);
			}
		}
		this.screen.center_text_draw(new StringBuilder().append("from: ")
				.append(this.aString912).toString(), 256, i, 1, 16777215);
		i += 15;
		i += 15;
		if (this.anInt914 != 0) {
			int j = 1 + this.anInt914 / 1440;
			if (j > 14)
				j = 14;
			String s1;
			if (j == 14) {
				s1 = "Earlier today";
			} else {
				if (j == 13)
					s1 = "Yesterday";
				else
					s1 = new StringBuilder().append(14 - j).append(" days ago")
							.toString();
			}
			this.screen.center_text_draw(
					new StringBuilder().append(s1)
							.append(" you requested new recovery questions")
							.toString(), 256, i, 1, 16744448);
			i += 15;
			this.screen.center_text_draw(
					"If you do not remember making this request then", 256, i,
					1, 16744448);
			i += 15;
			this.screen.center_text_draw(
					"cancel it and change your password immediately!", 256, i,
					1, 16744448);
			i += 15;
			i += 15;
			int i1 = 16777215;
			if ((this.mouse_y > i - 12) && (this.mouse_y <= i)
					&& (this.mouse_x > 106) && (this.mouse_x < 406))
				i1 = 16711680;
			this.screen.center_text_draw(
					"No that wasn't me - Cancel the request!", 256, i, 1, i1);
			if ((i1 == 16711680) && (this.anInt668 == 1)) {
				this.conn.enter(196);
				this.conn.exit();
				this.aBoolean910 = false;
			}
			i += 15;
			i1 = 16777215;
			if ((this.mouse_y > i - 12) && (this.mouse_y <= i)
					&& (this.mouse_x > 106) && (this.mouse_x < 406))
				i1 = 16711680;
			this.screen
					.center_text_draw(
							new StringBuilder()
									.append("That's ok, activate the new questions in ")
									.append(j).append(" days time").toString(),
							256, i, 1, i1);
			if ((i1 == 16711680) && (this.anInt668 == 1))
				this.aBoolean910 = false;
		} else {
			i += 7;
			this.screen.center_text_draw("Security tip of the day", 256, i, 1,
					16711680);
			i += 15;
			if (this.anInt915 == 0) {
				this.screen
						.center_text_draw(
								"Don't tell ANYONE your password or recovery questions!",
								256, i, 1, 16777215);
				i += 15;
				this.screen.center_text_draw(
						"Not even people claiming to be Jagex staff.", 256, i,
						1, 16777215);
				i += 15;
			}
			if (this.anInt915 == 1) {
				this.screen
						.center_text_draw(
								"Never enter your password or recovery questions into ANY",
								256, i, 1, 16777215);
				i += 15;
				this.screen
						.center_text_draw(
								"website other than this one - Not even if it looks similar.",
								256, i, 1, 16777215);
				i += 15;
			}
			if (this.anInt915 == 2) {
				this.screen.center_text_draw(
						"Don't use RuneScape cheats, helpers, or automaters.",
						256, i, 1, 16777215);
				i += 15;
				this.screen.center_text_draw(
						"These programs WILL steal your password.", 256, i, 1,
						16777215);
				i += 15;
			}
			if (this.anInt915 == 3) {
				this.screen
						.center_text_draw(
								"Watch out for fake emails, and fake staff. Real staff",
								256, i, 1, 16777215);
				i += 15;
				this.screen
						.center_text_draw(
								"will NEVER ask you for your password or recovery questions!",
								256, i, 1, 16777215);
				i += 15;
			}
			if (this.anInt915 == 4) {
				this.screen
						.center_text_draw(
								"Use a password your friends won't guess. Do NOT use your name!",
								256, i, 1, 16777215);
				i += 15;
				this.screen
						.center_text_draw(
								"Choose a unique password which you haven't used anywhere else",
								256, i, 1, 16777215);
				i += 15;
			}
			if (this.anInt915 == 5) {
				this.screen
						.center_text_draw(
								"If possible only play runescape from your own computer",
								256, i, 1, 16777215);
				i += 15;
				this.screen
						.center_text_draw(
								"Other machines could have been tampered with to steal your pass",
								256, i, 1, 16777215);
				i += 15;
			}
			i += 22;
			int k = 16777215;
			if ((this.mouse_y > i - 12) && (this.mouse_y <= i)
					&& (this.mouse_x > 106) && (this.mouse_x < 406))
				k = 16711680;
			this.screen.center_text_draw("Click here to close window", 256, i,
					1, k);
			if (this.anInt668 == 1) {
				if (k == 16711680)
					this.aBoolean910 = false;
				if (((this.mouse_x < 86) || (this.mouse_x > 426))
						&& ((this.mouse_y < 167 - c / '\002') || (this.mouse_y > 167 + c / '\002')))
					this.aBoolean910 = false;
			}
		}
		this.anInt668 = 0;
	}

	public void method105() {
		char c = 'Ɛ';
		byte byte0 = 100;
		this.screen.rect_fill(256 - c / '\002', 167 - byte0 / 2, c, byte0, 0);
		this.screen.rect_draw(256 - c / '\002', 167 - byte0 / 2, c, byte0,
				16777215);
		this.screen.multiline_text_draw(this.aString917, 256, 137, 1, 16777215,
				c - '(');
		int i = 157 + byte0 / 2;
		int j = 16777215;
		if ((this.mouse_y > i - 12) && (this.mouse_y <= i)
				&& (this.mouse_x > 106) && (this.mouse_x < 406))
			j = 16711680;
		this.screen
				.center_text_draw("Click here to close window", 256, i, 1, j);
		if (this.anInt668 == 1) {
			if (j == 16711680)
				this.aBoolean916 = false;
			if (((this.mouse_x < 256 - c / '\002') || (this.mouse_x > 256 + c / '\002'))
					&& ((this.mouse_y < 167 - byte0 / 2) || (this.mouse_y > 167 + byte0 / 2)))
				this.aBoolean916 = false;
		}
		this.anInt668 = 0;
	}

	public void method106() {
		this.screen.rect_fill(126, 137, 260, 60, 0);
		this.screen.rect_draw(126, 137, 260, 60, 16777215);
		this.screen.center_text_draw("Logging out...", 256, 173, 5, 16777215);
	}

	public void method107() {
		int i = 97;
		this.screen.rect_fill(86, 77, 340, 180, 0);
		this.screen.rect_draw(86, 77, 340, 180, 16777215);
		this.screen.center_text_draw("Warning! Proceed with caution", 256, i,
				4, 16711680);
		i += 26;
		this.screen.center_text_draw(
				"If you go much further north you will enter the", 256, i, 1,
				16777215);
		i += 13;
		this.screen.center_text_draw(
				"wilderness. This a very dangerous area where", 256, i, 1,
				16777215);
		i += 13;
		this.screen.center_text_draw("other players can attack you!", 256, i,
				1, 16777215);
		i += 22;
		this.screen.center_text_draw(
				"The further north you go the more dangerous it", 256, i, 1,
				16777215);
		i += 13;
		this.screen.center_text_draw(
				"becomes, but the more treasure you will find.", 256, i, 1,
				16777215);
		i += 22;
		this.screen.center_text_draw(
				"In the wilderness an indicator at the bottom-right", 256, i,
				1, 16777215);
		i += 13;
		this.screen.center_text_draw(
				"of the screen will show the current level of danger", 256, i,
				1, 16777215);
		i += 22;
		int j = 16777215;
		if ((this.mouse_y > i - 12) && (this.mouse_y <= i)
				&& (this.mouse_x > 181) && (this.mouse_x < 331))
			j = 16711680;
		this.screen
				.center_text_draw("Click here to close window", 256, i, 1, j);
		if (this.anInt668 != 0) {
			if ((this.mouse_y > i - 12) && (this.mouse_y <= i)
					&& (this.mouse_x > 181) && (this.mouse_x < 331))
				this.anInt921 = 2;
			if ((this.mouse_x < 86) || (this.mouse_x > 426)
					|| (this.mouse_y < 77) || (this.mouse_y > 257))
				this.anInt921 = 2;
			this.anInt668 = 0;
		}
	}

	public void method108() {
		if (this.anInt668 != 0) {
			this.anInt668 = 0;
			if ((this.mouse_x < 106) || (this.mouse_y < 150)
					|| (this.mouse_x > 406) || (this.mouse_y > 210)) {
				this.anInt906 = 0;
				return;
			}
		}
		int i = 150;
		this.screen.rect_fill(106, i, 300, 60, 0);
		this.screen.rect_draw(106, i, 300, 60, 16777215);
		i += 22;
		if (this.anInt906 == 6) {
			this.screen.center_text_draw("Please enter your current password",
					256, i, 4, 16777215);
			i += 25;
			String s = "*";
			for (int j = 0; j < this.line_buf_a.length(); j++) {
				s = new StringBuilder().append("X").append(s).toString();
			}
			this.screen.center_text_draw(s, 256, i, 4, 16777215);
			if (this.line_a.length() > 0) {
				this.aString907 = this.line_a;
				this.line_buf_a = "";
				this.line_a = "";
				this.anInt906 = 1;
				return;
			}
		} else if (this.anInt906 == 1) {
			this.screen.center_text_draw("Please enter your new password", 256,
					i, 4, 16777215);
			i += 25;
			String s1 = "*";
			for (int k = 0; k < this.line_buf_a.length(); k++) {
				s1 = new StringBuilder().append("X").append(s1).toString();
			}
			this.screen.center_text_draw(s1, 256, i, 4, 16777215);
			if (this.line_a.length() > 0) {
				this.aString908 = this.line_a;
				this.line_buf_a = "";
				this.line_a = "";
				if (this.aString908.length() >= 5) {
					this.anInt906 = 2;
					return;
				}
				this.anInt906 = 5;
				return;
			}
		} else if (this.anInt906 == 2) {
			this.screen.center_text_draw("Enter password again to confirm",
					256, i, 4, 16777215);
			i += 25;
			String s2 = "*";
			for (int l = 0; l < this.line_buf_a.length(); l++) {
				s2 = new StringBuilder().append("X").append(s2).toString();
			}
			this.screen.center_text_draw(s2, 256, i, 4, 16777215);
			if (this.line_a.length() > 0) {
				if (this.line_a.equalsIgnoreCase(this.aString908)) {
					this.anInt906 = 4;
					method33(this.aString907, this.aString908);
					return;
				}
				this.anInt906 = 3;
				return;
			}
		} else {
			if (this.anInt906 == 3) {
				this.screen.center_text_draw("Passwords do not match!", 256, i,
						4, 16777215);
				i += 25;
				this.screen.center_text_draw("Press any key to close", 256, i,
						4, 16777215);
				return;
			}
			if (this.anInt906 == 4) {
				this.screen.center_text_draw("Ok, your request has been sent",
						256, i, 4, 16777215);
				i += 25;
				this.screen.center_text_draw("Press any key to close", 256, i,
						4, 16777215);
				return;
			}
			if (this.anInt906 == 5) {
				this.screen.center_text_draw("Password must be at", 256, i, 4,
						16777215);
				i += 25;
				this.screen.center_text_draw("least 5 letters long", 256, i, 4,
						16777215);
			}
		}
	}

	public void method109() {
		if (this.anInt668 != 0) {
			this.anInt668 = 0;
			if ((this.anInt905 == 1)
					&& ((this.mouse_x < 106) || (this.mouse_y < 145)
							|| (this.mouse_x > 406) || (this.mouse_y > 215))) {
				this.anInt905 = 0;
				return;
			}
			if ((this.anInt905 == 2)
					&& ((this.mouse_x < 6) || (this.mouse_y < 145)
							|| (this.mouse_x > 506) || (this.mouse_y > 215))) {
				this.anInt905 = 0;
				return;
			}
			if ((this.anInt905 == 3)
					&& ((this.mouse_x < 106) || (this.mouse_y < 145)
							|| (this.mouse_x > 406) || (this.mouse_y > 215))) {
				this.anInt905 = 0;
				return;
			}
			if ((this.mouse_x > 236) && (this.mouse_x < 276)
					&& (this.mouse_y > 193) && (this.mouse_y < 213)) {
				this.anInt905 = 0;
				return;
			}
		}
		int i = 145;
		if (this.anInt905 == 1) {
			this.screen.rect_fill(106, i, 300, 70, 0);
			this.screen.rect_draw(106, i, 300, 70, 16777215);
			i += 20;
			this.screen.center_text_draw("Enter name to add to friends list",
					256, i, 4, 16777215);
			i += 20;
			this.screen.center_text_draw(
					new StringBuilder().append(this.line_buf_a).append("*")
							.toString(), 256, i, 4, 16777215);
			if (this.line_a.length() > 0) {
				String s = this.line_a.trim();
				this.line_buf_a = "";
				this.line_a = "";
				this.anInt905 = 0;
				if ((s.length() > 0)
						&& (DataUtil.encode_name(s) != this.local_player.aLong589))
					method37(s);
			}
		}
		if (this.anInt905 == 2) {
			this.screen.rect_fill(6, i, 500, 70, 0);
			this.screen.rect_draw(6, i, 500, 70, 16777215);
			i += 20;
			this.screen.center_text_draw(
					new StringBuilder().append("Enter message to send to ")
							.append(DataUtil.decode_name(this.aLong793))
							.toString(), 256, i, 4, 16777215);
			i += 20;
			this.screen.center_text_draw(
					new StringBuilder().append(this.line_buf_b).append("*")
							.toString(), 256, i, 4, 16777215);
			if (this.line_b.length() > 0) {
				String s1 = this.line_b;
				this.line_buf_b = "";
				this.line_b = "";
				this.anInt905 = 0;
				int k = DataUtil.encode_censor(s1);
				method39(this.aLong793, DataUtil.coded, k);
				s1 = DataUtil.decode_censor(DataUtil.coded, 0, k, true);
				push_message(new StringBuilder().append("@pri@You tell ")
						.append(DataUtil.decode_name(this.aLong793))
						.append(": ").append(s1).toString());
			}
		}
		if (this.anInt905 == 3) {
			this.screen.rect_fill(106, i, 300, 70, 0);
			this.screen.rect_draw(106, i, 300, 70, 16777215);
			i += 20;
			this.screen.center_text_draw("Enter name to add to ignore list",
					256, i, 4, 16777215);
			i += 20;
			this.screen.center_text_draw(
					new StringBuilder().append(this.line_buf_a).append("*")
							.toString(), 256, i, 4, 16777215);
			if (this.line_a.length() > 0) {
				String s2 = this.line_a.trim();
				this.line_buf_a = "";
				this.line_a = "";
				this.anInt905 = 0;
				if ((s2.length() > 0)
						&& (DataUtil.encode_name(s2) != this.local_player.aLong589))
					method35(s2);
			}
		}
		int j = 16777215;
		if ((this.mouse_x > 236) && (this.mouse_x < 276)
				&& (this.mouse_y > 193) && (this.mouse_y < 213))
			j = 16776960;
		this.screen.center_text_draw("Cancel", 256, 208, 1, j);
	}

	public void method110() {
		char c = 'Ƙ';
		char c1 = 'Ŏ';
		if ((this.anInt900 == 1) && (this.anInt894 <= 48))
			this.anInt900 = 0;
		if ((this.anInt897 >= this.anInt894) || (this.anInt897 < 0))
			this.anInt897 = -1;
		if ((this.anInt897 != -1)
				&& (this.anIntArray895[this.anInt897] != this.anInt898)) {
			this.anInt897 = -1;
			this.anInt898 = -2;
		}
		if (this.anInt668 != 0) {
			this.anInt668 = 0;
			int i = this.mouse_x - (256 - c / '\002');
			int k = this.mouse_y - (170 - c1 / '\002');
			if ((i >= 0) && (k >= 12) && (i < 408) && (k < 280)) {
				int i1 = this.anInt900 * 48;
				for (int l1 = 0; l1 < 6; l1++) {
					for (int k5 = 0; k5 < 8; k5++) {
						int j6 = 7 + k5 * 49;
						int i7 = 28 + l1 * 34;
						if ((i > j6) && (i < j6 + 49) && (k > i7)
								&& (k < i7 + 34) && (i1 < this.anInt894)
								&& (this.anIntArray895[i1] != -1)) {
							this.anInt898 = this.anIntArray895[i1];
							this.anInt897 = i1;
						}
						i1++;
					}

				}

				i = 256 - c / '\002';
				k = 170 - c1 / '\002';
				int l5;
				if (this.anInt897 < 0)
					l5 = -1;
				else
					l5 = this.anIntArray895[this.anInt897];
				if (l5 != -1) {
					int j1 = this.anIntArray896[this.anInt897];
					if ((Config.item_stackable[l5] == 1) && (j1 > 1))
						j1 = 1;
					if ((j1 >= 1) && (this.mouse_x >= i + 220)
							&& (this.mouse_y >= k + 238)
							&& (this.mouse_x < i + 250)
							&& (this.mouse_y <= k + 249)) {
						this.conn.enter(206);
						this.conn.short_put(l5);
						this.conn.short_put(1);
						this.conn.exit();
					}
					if ((j1 >= 5) && (this.mouse_x >= i + 250)
							&& (this.mouse_y >= k + 238)
							&& (this.mouse_x < i + 280)
							&& (this.mouse_y <= k + 249)) {
						this.conn.enter(206);
						this.conn.short_put(l5);
						this.conn.short_put(5);
						this.conn.exit();
					}
					if ((j1 >= 25) && (this.mouse_x >= i + 280)
							&& (this.mouse_y >= k + 238)
							&& (this.mouse_x < i + 305)
							&& (this.mouse_y <= k + 249)) {
						this.conn.enter(206);
						this.conn.short_put(l5);
						this.conn.short_put(25);
						this.conn.exit();
					}
					if ((j1 >= 100) && (this.mouse_x >= i + 305)
							&& (this.mouse_y >= k + 238)
							&& (this.mouse_x < i + 335)
							&& (this.mouse_y <= k + 249)) {
						this.conn.enter(206);
						this.conn.short_put(l5);
						this.conn.short_put(100);
						this.conn.exit();
					}
					if ((j1 >= 500) && (this.mouse_x >= i + 335)
							&& (this.mouse_y >= k + 238)
							&& (this.mouse_x < i + 368)
							&& (this.mouse_y <= k + 249)) {
						this.conn.enter(206);
						this.conn.short_put(l5);
						this.conn.short_put(500);
						this.conn.exit();
					}
					if ((j1 >= 2500) && (this.mouse_x >= i + 370)
							&& (this.mouse_y >= k + 238)
							&& (this.mouse_x < i + 400)
							&& (this.mouse_y <= k + 249)) {
						this.conn.enter(206);
						this.conn.short_put(l5);
						this.conn.short_put(2500);
						this.conn.exit();
					}
					if ((method90(l5) >= 1) && (this.mouse_x >= i + 220)
							&& (this.mouse_y >= k + 263)
							&& (this.mouse_x < i + 250)
							&& (this.mouse_y <= k + 274)) {
						this.conn.enter(205);
						this.conn.short_put(l5);
						this.conn.short_put(1);
						this.conn.exit();
					}
					if ((method90(l5) >= 5) && (this.mouse_x >= i + 250)
							&& (this.mouse_y >= k + 263)
							&& (this.mouse_x < i + 280)
							&& (this.mouse_y <= k + 274)) {
						this.conn.enter(205);
						this.conn.short_put(l5);
						this.conn.short_put(5);
						this.conn.exit();
					}
					if ((method90(l5) >= 25) && (this.mouse_x >= i + 280)
							&& (this.mouse_y >= k + 263)
							&& (this.mouse_x < i + 305)
							&& (this.mouse_y <= k + 274)) {
						this.conn.enter(205);
						this.conn.short_put(l5);
						this.conn.short_put(25);
						this.conn.exit();
					}
					if ((method90(l5) >= 100) && (this.mouse_x >= i + 305)
							&& (this.mouse_y >= k + 263)
							&& (this.mouse_x < i + 335)
							&& (this.mouse_y <= k + 274)) {
						this.conn.enter(205);
						this.conn.short_put(l5);
						this.conn.short_put(100);
						this.conn.exit();
					}
					if ((method90(l5) >= 500) && (this.mouse_x >= i + 335)
							&& (this.mouse_y >= k + 263)
							&& (this.mouse_x < i + 368)
							&& (this.mouse_y <= k + 274)) {
						this.conn.enter(205);
						this.conn.short_put(l5);
						this.conn.short_put(500);
						this.conn.exit();
					}
					if ((method90(l5) >= 2500) && (this.mouse_x >= i + 370)
							&& (this.mouse_y >= k + 263)
							&& (this.mouse_x < i + 400)
							&& (this.mouse_y <= k + 274)) {
						this.conn.enter(205);
						this.conn.short_put(l5);
						this.conn.short_put(2500);
						this.conn.exit();
					}
				}
			} else if ((this.anInt894 > 48) && (i >= 120) && (i <= 190)
					&& (k <= 12)) {
				this.anInt900 = 0;
			} else if ((this.anInt894 > 48) && (i >= 190) && (i <= 260)
					&& (k <= 12)) {
				this.anInt900 = 1;
			} else {
				this.conn.enter(207);
				this.conn.exit();
				this.aBoolean890 = false;
				return;
			}
		}
		int j = 256 - c / '\002';
		int l = 170 - c1 / '\002';
		this.screen.rect_fill(j, l, 408, 12, 192);
		int k1 = 10000536;
		this.screen.rect_fill(j, l + 12, 408, 17, k1, 160);
		this.screen.rect_fill(j, l + 29, 8, 204, k1, 160);
		this.screen.rect_fill(j + 399, l + 29, 9, 204, k1, 160);
		this.screen.rect_fill(j, l + 233, 408, 47, k1, 160);
		this.screen.text_draw("Bank", j + 1, l + 10, 1, 16777215);
		if (this.anInt894 > 48) {
			int i2 = 16777215;
			if (this.anInt900 == 0)
				i2 = 16711680;
			else if ((this.mouse_x > j + 120) && (this.mouse_y >= l)
					&& (this.mouse_x < j + 190) && (this.mouse_y < l + 12))
				i2 = 16776960;
			this.screen.text_draw("<page 1>", j + 120, l + 10, 1, i2);
			i2 = 16777215;
			if (this.anInt900 == 1)
				i2 = 16711680;
			else if ((this.mouse_x > j + 190) && (this.mouse_y >= l)
					&& (this.mouse_x < j + 260) && (this.mouse_y < l + 12))
				i2 = 16776960;
			this.screen.text_draw("<page 2>", j + 200, l + 10, 1, i2);
		}
		int j2 = 16777215;
		if ((this.mouse_x > j + 320) && (this.mouse_y >= l)
				&& (this.mouse_x < j + 408) && (this.mouse_y < l + 12))
			j2 = 16711680;
		this.screen.precede_text_draw("Close window", j + 406, l + 10, 1, j2);
		this.screen.text_draw("Number in bank in green", j + 7, l + 24, 1,
				65280);
		this.screen.text_draw("Number held in blue", j + 289, l + 24, 1, 65535);
		int i6 = 13684944;
		int k6 = this.anInt900 * 48;
		for (int j7 = 0; j7 < 6; j7++) {
			for (int k7 = 0; k7 < 8; k7++) {
				int i8 = j + 7 + k7 * 49;
				int j8 = l + 28 + j7 * 34;
				if (this.anInt897 == k6)
					this.screen.rect_fill(i8, j8, 49, 34, 16711680, 160);
				else
					this.screen.rect_fill(i8, j8, 49, 34, i6, 160);
				this.screen.rect_draw(i8, j8, 50, 35, 0);
				if ((k6 < this.anInt894) && (this.anIntArray895[k6] != -1)) {
					this.screen
							.resize_trans_sprite_plot(
									i8,
									j8,
									48,
									32,
									this.item_sprite_off
											+ Config.item_sprite[this.anIntArray895[k6]],
									Config.item_tint[this.anIntArray895[k6]],
									0, 0, false);
					this.screen.text_draw(
							String.valueOf(this.anIntArray896[k6]), i8 + 1,
							j8 + 10, 1, 65280);
					this.screen.precede_text_draw(
							String.valueOf(method90(this.anIntArray895[k6])),
							i8 + 47, j8 + 29, 1, 65535);
				}
				k6++;
			}

		}

		this.screen.line_horiz(j + 5, l + 256, 398, 0);
		if (this.anInt897 == -1) {
			this.screen.center_text_draw(
					"Select an object to withdraw or deposit", j + 204,
					l + 248, 3, 16776960);
			return;
		}
		int l7;
		if (this.anInt897 < 0)
			l7 = -1;
		else
			l7 = this.anIntArray895[this.anInt897];
		if (l7 != -1) {
			int l6 = this.anIntArray896[this.anInt897];
			if ((Config.item_stackable[l7] == 1) && (l6 > 1))
				l6 = 1;
			if (l6 > 0) {
				this.screen.text_draw(new StringBuilder().append("Withdraw ")
						.append(Config.item_name[l7]).toString(), j + 2,
						l + 248, 1, 16777215);
				int k2 = 16777215;
				if ((this.mouse_x >= j + 220) && (this.mouse_y >= l + 238)
						&& (this.mouse_x < j + 250)
						&& (this.mouse_y <= l + 249))
					k2 = 16711680;
				this.screen.text_draw("One", j + 222, l + 248, 1, k2);
				if (l6 >= 5) {
					int l2 = 16777215;
					if ((this.mouse_x >= j + 250) && (this.mouse_y >= l + 238)
							&& (this.mouse_x < j + 280)
							&& (this.mouse_y <= l + 249))
						l2 = 16711680;
					this.screen.text_draw("Five", j + 252, l + 248, 1, l2);
				}
				if (l6 >= 25) {
					int i3 = 16777215;
					if ((this.mouse_x >= j + 280) && (this.mouse_y >= l + 238)
							&& (this.mouse_x < j + 305)
							&& (this.mouse_y <= l + 249))
						i3 = 16711680;
					this.screen.text_draw("25", j + 282, l + 248, 1, i3);
				}
				if (l6 >= 100) {
					int j3 = 16777215;
					if ((this.mouse_x >= j + 305) && (this.mouse_y >= l + 238)
							&& (this.mouse_x < j + 335)
							&& (this.mouse_y <= l + 249))
						j3 = 16711680;
					this.screen.text_draw("100", j + 307, l + 248, 1, j3);
				}
				if (l6 >= 500) {
					int k3 = 16777215;
					if ((this.mouse_x >= j + 335) && (this.mouse_y >= l + 238)
							&& (this.mouse_x < j + 368)
							&& (this.mouse_y <= l + 249))
						k3 = 16711680;
					this.screen.text_draw("500", j + 337, l + 248, 1, k3);
				}
				if (l6 >= 2500) {
					int l3 = 16777215;
					if ((this.mouse_x >= j + 370) && (this.mouse_y >= l + 238)
							&& (this.mouse_x < j + 400)
							&& (this.mouse_y <= l + 249))
						l3 = 16711680;
					this.screen.text_draw("2500", j + 370, l + 248, 1, l3);
				}
			}
			if (method90(l7) > 0) {
				this.screen.text_draw(new StringBuilder().append("Deposit ")
						.append(Config.item_name[l7]).toString(), j + 2,
						l + 273, 1, 16777215);
				int i4 = 16777215;
				if ((this.mouse_x >= j + 220) && (this.mouse_y >= l + 263)
						&& (this.mouse_x < j + 250)
						&& (this.mouse_y <= l + 274))
					i4 = 16711680;
				this.screen.text_draw("One", j + 222, l + 273, 1, i4);
				if (method90(l7) >= 5) {
					int j4 = 16777215;
					if ((this.mouse_x >= j + 250) && (this.mouse_y >= l + 263)
							&& (this.mouse_x < j + 280)
							&& (this.mouse_y <= l + 274))
						j4 = 16711680;
					this.screen.text_draw("Five", j + 252, l + 273, 1, j4);
				}
				if (method90(l7) >= 25) {
					int k4 = 16777215;
					if ((this.mouse_x >= j + 280) && (this.mouse_y >= l + 263)
							&& (this.mouse_x < j + 305)
							&& (this.mouse_y <= l + 274))
						k4 = 16711680;
					this.screen.text_draw("25", j + 282, l + 273, 1, k4);
				}
				if (method90(l7) >= 100) {
					int l4 = 16777215;
					if ((this.mouse_x >= j + 305) && (this.mouse_y >= l + 263)
							&& (this.mouse_x < j + 335)
							&& (this.mouse_y <= l + 274))
						l4 = 16711680;
					this.screen.text_draw("100", j + 307, l + 273, 1, l4);
				}
				if (method90(l7) >= 500) {
					int i5 = 16777215;
					if ((this.mouse_x >= j + 335) && (this.mouse_y >= l + 263)
							&& (this.mouse_x < j + 368)
							&& (this.mouse_y <= l + 274))
						i5 = 16711680;
					this.screen.text_draw("500", j + 337, l + 273, 1, i5);
				}
				if (method90(l7) >= 2500) {
					int j5 = 16777215;
					if ((this.mouse_x >= j + 370) && (this.mouse_y >= l + 263)
							&& (this.mouse_x < j + 400)
							&& (this.mouse_y <= l + 274))
						j5 = 16711680;
					this.screen.text_draw("2500", j + 370, l + 273, 1, j5);
				}
			}
		}
	}

	public void method111() {
		if (this.anInt668 != 0) {
			this.anInt668 = 0;
			int i = this.mouse_x - 52;
			int j = this.mouse_y - 44;
			if ((i >= 0) && (j >= 12) && (i < 408) && (j < 246)) {
				int k = 0;
				for (int i1 = 0; i1 < 5; i1++) {
					for (int i2 = 0; i2 < 8; i2++) {
						int l2 = 7 + i2 * 49;
						int l3 = 28 + i1 * 34;
						if ((i > l2) && (i < l2 + 49) && (j > l3)
								&& (j < l3 + 34)
								&& (this.anIntArray885[k] != -1)) {
							this.anInt888 = k;
							this.anInt889 = this.anIntArray885[k];
						}
						k++;
					}

				}

				if (this.anInt888 >= 0) {
					int j2 = this.anIntArray885[this.anInt888];
					if (j2 != -1) {
						if ((this.anIntArray886[this.anInt888] > 0)
								&& (i > 298) && (j >= 204) && (i < 408)
								&& (j <= 215)) {
							int i3 = this.anInt884
									+ this.anIntArray887[this.anInt888];
							if (i3 < 10)
								i3 = 10;
							int i4 = i3 * Config.item_val[j2] / 100;
							this.conn.enter(217);
							this.conn
									.short_put(this.anIntArray885[this.anInt888]);
							this.conn.int_put(i4);
							this.conn.exit();
						}
						if ((method90(j2) > 0) && (i > 2) && (j >= 229)
								&& (i < 112) && (j <= 240)) {
							int j3 = this.anInt883
									+ this.anIntArray887[this.anInt888];
							if (j3 < 10)
								j3 = 10;
							int j4 = j3 * Config.item_val[j2] / 100;
							this.conn.enter(216);
							this.conn
									.short_put(this.anIntArray885[this.anInt888]);
							this.conn.int_put(j4);
							this.conn.exit();
						}
					}
				}
			} else {
				this.conn.enter(218);
				this.conn.exit();
				this.aBoolean882 = false;
				return;
			}
		}
		byte byte0 = 52;
		byte byte1 = 44;
		this.screen.rect_fill(byte0, byte1, 408, 12, 192);
		int l = 10000536;
		this.screen.rect_fill(byte0, byte1 + 12, 408, 17, l, 160);
		this.screen.rect_fill(byte0, byte1 + 29, 8, 170, l, 160);
		this.screen.rect_fill(byte0 + 399, byte1 + 29, 9, 170, l, 160);
		this.screen.rect_fill(byte0, byte1 + 199, 408, 47, l, 160);
		this.screen.text_draw("Buying and selling items", byte0 + 1,
				byte1 + 10, 1, 16777215);
		int j1 = 16777215;
		if ((this.mouse_x > byte0 + 320) && (this.mouse_y >= byte1)
				&& (this.mouse_x < byte0 + 408) && (this.mouse_y < byte1 + 12))
			j1 = 16711680;
		this.screen.precede_text_draw("Close window", byte0 + 406, byte1 + 10,
				1, j1);
		this.screen.text_draw("Shops stock in green", byte0 + 2, byte1 + 24, 1,
				65280);
		this.screen.text_draw("Number you own in blue", byte0 + 135,
				byte1 + 24, 1, 65535);
		this.screen.text_draw(new StringBuilder().append("Your money: ")
				.append(method90(10)).append("gp").toString(), byte0 + 280,
				byte1 + 24, 1, 16776960);
		int k2 = 13684944;
		int k3 = 0;
		for (int k4 = 0; k4 < 5; k4++) {
			for (int l4 = 0; l4 < 8; l4++) {
				int j5 = byte0 + 7 + l4 * 49;
				int i6 = byte1 + 28 + k4 * 34;
				if (this.anInt888 == k3)
					this.screen.rect_fill(j5, i6, 49, 34, 16711680, 160);
				else
					this.screen.rect_fill(j5, i6, 49, 34, k2, 160);
				this.screen.rect_draw(j5, i6, 50, 35, 0);
				if (this.anIntArray885[k3] != -1) {
					this.screen
							.resize_trans_sprite_plot(
									j5,
									i6,
									48,
									32,
									this.item_sprite_off
											+ Config.item_sprite[this.anIntArray885[k3]],
									Config.item_tint[this.anIntArray885[k3]],
									0, 0, false);
					this.screen.text_draw(
							String.valueOf(this.anIntArray886[k3]), j5 + 1,
							i6 + 10, 1, 65280);
					this.screen.precede_text_draw(
							String.valueOf(method90(this.anIntArray885[k3])),
							j5 + 47, i6 + 10, 1, 65535);
				}
				k3++;
			}

		}

		this.screen.line_horiz(byte0 + 5, byte1 + 222, 398, 0);
		if (this.anInt888 == -1) {
			this.screen.center_text_draw("Select an object to buy or sell",
					byte0 + 204, byte1 + 214, 3, 16776960);
			return;
		}
		int i5 = this.anIntArray885[this.anInt888];
		if (i5 != -1) {
			if (this.anIntArray886[this.anInt888] > 0) {
				int k5 = this.anInt884 + this.anIntArray887[this.anInt888];
				if (k5 < 10)
					k5 = 10;
				int j6 = k5 * Config.item_val[i5] / 100;
				this.screen.text_draw(new StringBuilder().append("Buy a new ")
						.append(Config.item_name[i5]).append(" for ")
						.append(j6).append("gp").toString(), byte0 + 2,
						byte1 + 214, 1, 16776960);
				int k1 = 16777215;
				if ((this.mouse_x > byte0 + 298)
						&& (this.mouse_y >= byte1 + 204)
						&& (this.mouse_x < byte0 + 408)
						&& (this.mouse_y <= byte1 + 215))
					k1 = 16711680;
				this.screen.precede_text_draw("Click here to buy", byte0 + 405,
						byte1 + 214, 3, k1);
			} else {
				this.screen.center_text_draw(
						"This item is not currently available to buy",
						byte0 + 204, byte1 + 214, 3, 16776960);
			}
			if (method90(i5) > 0) {
				int l5 = this.anInt883 + this.anIntArray887[this.anInt888];
				if (l5 < 10)
					l5 = 10;
				int k6 = l5 * Config.item_val[i5] / 100;
				this.screen.precede_text_draw(
						new StringBuilder().append("Sell your ")
								.append(Config.item_name[i5]).append(" for ")
								.append(k6).append("gp").toString(),
						byte0 + 405, byte1 + 239, 1, 16776960);
				int l1 = 16777215;
				if ((this.mouse_x > byte0 + 2) && (this.mouse_y >= byte1 + 229)
						&& (this.mouse_x < byte0 + 112)
						&& (this.mouse_y <= byte1 + 240))
					l1 = 16711680;
				this.screen.text_draw("Click here to sell", byte0 + 2,
						byte1 + 239, 3, l1);
				return;
			}
			this.screen.center_text_draw(
					"You do not have any of this item to sell", byte0 + 204,
					byte1 + 239, 3, 16776960);
		}
	}

	public void method112() {
		byte byte0 = 22;
		byte byte1 = 36;
		this.screen.rect_fill(byte0, byte1, 468, 16, 192);
		int i = 10000536;
		this.screen.rect_fill(byte0, byte1 + 16, 468, 246, i, 160);
		this.screen
				.center_text_draw(
						new StringBuilder()
								.append("Please confirm your trade with @yel@")
								.append(DataUtil.decode_name(this.aLong873))
								.toString(), byte0 + 234, byte1 + 12, 1,
						16777215);
		this.screen.center_text_draw("You are about to give:", byte0 + 117,
				byte1 + 30, 1, 16776960);
		for (int j = 0; j < this.anInt863; j++) {
			String s = Config.item_name[this.anIntArray864[j]];
			if (Config.item_stackable[this.anIntArray864[j]] == 0)
				s = new StringBuilder().append(s).append(" (")
						.append(this.anIntArray865[j]).append(")").toString();
			this.screen.center_text_draw(s, byte0 + 117, byte1 + 42 + j * 12,
					1, 16777215);
		}

		if (this.anInt863 == 0)
			this.screen.center_text_draw("Nothing!", byte0 + 117, byte1 + 42,
					1, 16777215);
		this.screen.center_text_draw("In return you will receive:",
				byte0 + 351, byte1 + 30, 1, 16776960);
		for (int k = 0; k < this.anInt866; k++) {
			String s1 = Config.item_name[this.anIntArray867[k]];
			if (Config.item_stackable[this.anIntArray867[k]] == 0)
				s1 = new StringBuilder().append(s1).append(" (")
						.append(this.anIntArray868[k]).append(")").toString();
			this.screen.center_text_draw(s1, byte0 + 351, byte1 + 42 + k * 12,
					1, 16777215);
		}

		if (this.anInt866 == 0)
			this.screen.center_text_draw("Nothing!", byte0 + 351, byte1 + 42,
					1, 16777215);
		this.screen.center_text_draw("Are you sure you want to do this?",
				byte0 + 234, byte1 + 200, 4, 65535);
		this.screen.center_text_draw(
				"There is NO WAY to reverse a trade if you change your mind.",
				byte0 + 234, byte1 + 215, 1, 16777215);
		this.screen.center_text_draw(
				"Remember that not all players are trustworthy", byte0 + 234,
				byte1 + 230, 1, 16777215);
		if (!this.aBoolean875) {
			this.screen.sprite_plot(byte0 + 118 - 35, byte1 + 238,
					this.ui_sprite_off + 25);
			this.screen.sprite_plot(byte0 + 352 - 35, byte1 + 238,
					this.ui_sprite_off + 26);
		} else {
			this.screen.center_text_draw("Waiting for other player...",
					byte0 + 234, byte1 + 250, 1, 16776960);
		}
		if (this.anInt668 == 1) {
			if ((this.mouse_x < byte0) || (this.mouse_y < byte1)
					|| (this.mouse_x > byte0 + 468)
					|| (this.mouse_y > byte1 + 262)) {
				this.aBoolean874 = false;
				this.conn.enter(233);
				this.conn.exit();
			}
			if ((this.mouse_x >= byte0 + 118 - 35)
					&& (this.mouse_x <= byte0 + 118 + 70)
					&& (this.mouse_y >= byte1 + 238)
					&& (this.mouse_y <= byte1 + 238 + 21)) {
				this.aBoolean875 = true;
				this.conn.enter(202);
				this.conn.exit();
			}
			if ((this.mouse_x >= byte0 + 352 - 35)
					&& (this.mouse_x <= byte0 + 353 + 70)
					&& (this.mouse_y >= byte1 + 238)
					&& (this.mouse_y <= byte1 + 238 + 21)) {
				this.aBoolean874 = false;
				this.conn.enter(233);
				this.conn.exit();
			}
			this.anInt668 = 0;
		}
	}

	public void method113() {
		if ((this.anInt668 != 0) && (this.anInt872 == 0))
			this.anInt872 = 1;
		if (this.anInt872 > 0) {
			int i = this.mouse_x - 22;
			int j = this.mouse_y - 36;
			if ((i >= 0) && (j >= 0) && (i < 468) && (j < 262)) {
				if ((i > 216) && (j > 30) && (i < 462) && (j < 235)) {
					int k = (i - 217) / 49 + (j - 31) / 34 * 5;
					if ((k >= 0) && (k < this.anInt772)) {
						boolean flag = false;
						int l1 = 0;
						int k2 = this.anIntArray773[k];
						for (int k3 = 0; k3 < this.anInt863; k3++) {
							if (this.anIntArray864[k3] == k2) {
								if (Config.item_stackable[k2] == 0) {
									for (int i4 = 0; i4 < this.anInt872; i4++) {
										if (this.anIntArray865[k3] < this.anIntArray774[k])
											this.anIntArray865[k3] += 1;
										flag = true;
									}
								} else
									l1++;
							}
						}
						if (method90(k2) <= l1)
							flag = true;
						if (Config.item_tradable[k2] == 1) {
							method77(
									"This object cannot be traded with other players",
									3);
							flag = true;
						}
						if ((!flag) && (this.anInt863 < 12)) {
							this.anIntArray864[this.anInt863] = k2;
							this.anIntArray865[this.anInt863] = 1;
							this.anInt863 += 1;
							flag = true;
						}
						if (flag) {
							this.conn.enter(234);
							this.conn.byte_put(this.anInt863);
							for (int j4 = 0; j4 < this.anInt863; j4++) {
								this.conn.short_put(this.anIntArray864[j4]);
								this.conn.int_put(this.anIntArray865[j4]);
							}

							this.conn.exit();
							this.aBoolean869 = false;
							this.aBoolean870 = false;
						}
					}
				}
				if ((i > 8) && (j > 30) && (i < 205) && (j < 133)) {
					int l = (i - 9) / 49 + (j - 31) / 34 * 4;
					if ((l >= 0) && (l < this.anInt863)) {
						int j1 = this.anIntArray864[l];
						for (int i2 = 0; i2 < this.anInt872; i2++) {
							if ((Config.item_stackable[j1] == 0)
									&& (this.anIntArray865[l] > 1)) {
								this.anIntArray865[l] -= 1;
							} else {
								this.anInt863 -= 1;
								this.anInt871 = 0;
								for (int l2 = l; l2 < this.anInt863; l2++) {
									this.anIntArray864[l2] = this.anIntArray864[(l2 + 1)];
									this.anIntArray865[l2] = this.anIntArray865[(l2 + 1)];
								}

								break;
							}
						}
						this.conn.enter(234);
						this.conn.byte_put(this.anInt863);
						for (int i3 = 0; i3 < this.anInt863; i3++) {
							this.conn.short_put(this.anIntArray864[i3]);
							this.conn.int_put(this.anIntArray865[i3]);
						}

						this.conn.exit();
						this.aBoolean869 = false;
						this.aBoolean870 = false;
					}
				}
				if ((i >= 217) && (j >= 238) && (i <= 286) && (j <= 259)) {
					this.aBoolean870 = true;
					this.conn.enter(232);
					this.conn.exit();
				}
				if ((i >= 394) && (j >= 238) && (i < 463) && (j < 259)) {
					this.aBoolean861 = false;
					this.conn.enter(233);
					this.conn.exit();
				}
			} else if (this.anInt668 != 0) {
				this.aBoolean861 = false;
				this.conn.enter(233);
				this.conn.exit();
			}
			this.anInt668 = 0;
			this.anInt872 = 0;
		}
		if (!this.aBoolean861)
			return;
		byte byte0 = 22;
		byte byte1 = 36;
		this.screen.rect_fill(byte0, byte1, 468, 12, 192);
		int i1 = 10000536;
		this.screen.rect_fill(byte0, byte1 + 12, 468, 18, i1, 160);
		this.screen.rect_fill(byte0, byte1 + 30, 8, 248, i1, 160);
		this.screen.rect_fill(byte0 + 205, byte1 + 30, 11, 248, i1, 160);
		this.screen.rect_fill(byte0 + 462, byte1 + 30, 6, 248, i1, 160);
		this.screen.rect_fill(byte0 + 8, byte1 + 133, 197, 22, i1, 160);
		this.screen.rect_fill(byte0 + 8, byte1 + 258, 197, 20, i1, 160);
		this.screen.rect_fill(byte0 + 216, byte1 + 235, 246, 43, i1, 160);
		int k1 = 13684944;
		this.screen.rect_fill(byte0 + 8, byte1 + 30, 197, 103, k1, 160);
		this.screen.rect_fill(byte0 + 8, byte1 + 155, 197, 103, k1, 160);
		this.screen.rect_fill(byte0 + 216, byte1 + 30, 246, 205, k1, 160);
		for (int j2 = 0; j2 < 4; j2++) {
			this.screen.line_horiz(byte0 + 8, byte1 + 30 + j2 * 34, 197, 0);
		}
		for (int j3 = 0; j3 < 4; j3++) {
			this.screen.line_horiz(byte0 + 8, byte1 + 155 + j3 * 34, 197, 0);
		}
		for (int l3 = 0; l3 < 7; l3++) {
			this.screen.line_horiz(byte0 + 216, byte1 + 30 + l3 * 34, 246, 0);
		}
		for (int k4 = 0; k4 < 6; k4++) {
			if (k4 < 5)
				this.screen.line_vert(byte0 + 8 + k4 * 49, byte1 + 30, 103, 0);
			if (k4 < 5)
				this.screen.line_vert(byte0 + 8 + k4 * 49, byte1 + 155, 103, 0);
			this.screen.line_vert(byte0 + 216 + k4 * 49, byte1 + 30, 205, 0);
		}

		this.screen.text_draw(new StringBuilder().append("Trading with: ")
				.append(this.aString862).toString(), byte0 + 1, byte1 + 10, 1,
				16777215);
		this.screen.text_draw("Your Offer", byte0 + 9, byte1 + 27, 4, 16777215);
		this.screen.text_draw("Opponent's Offer", byte0 + 9, byte1 + 152, 4,
				16777215);
		this.screen.text_draw("Your Inventory", byte0 + 216, byte1 + 27, 4,
				16777215);
		if (!this.aBoolean870)
			this.screen.sprite_plot(byte0 + 217, byte1 + 238,
					this.ui_sprite_off + 25);
		this.screen.sprite_plot(byte0 + 394, byte1 + 238,
				this.ui_sprite_off + 26);
		if (this.aBoolean869) {
			this.screen.center_text_draw("Other player", byte0 + 341,
					byte1 + 246, 1, 16777215);
			this.screen.center_text_draw("has accepted", byte0 + 341,
					byte1 + 256, 1, 16777215);
		}
		if (this.aBoolean870) {
			this.screen.center_text_draw("Waiting for", byte0 + 217 + 35,
					byte1 + 246, 1, 16777215);
			this.screen.center_text_draw("other player", byte0 + 217 + 35,
					byte1 + 256, 1, 16777215);
		}
		for (int l4 = 0; l4 < this.anInt772; l4++) {
			int i5 = 217 + byte0 + l4 % 5 * 49;
			int k5 = 31 + byte1 + l4 / 5 * 34;
			this.screen.resize_trans_sprite_plot(i5, k5, 48, 32,
					this.item_sprite_off
							+ Config.item_sprite[this.anIntArray773[l4]],
					Config.item_tint[this.anIntArray773[l4]], 0, 0, false);
			if (Config.item_stackable[this.anIntArray773[l4]] == 0) {
				this.screen.text_draw(String.valueOf(this.anIntArray774[l4]),
						i5 + 1, k5 + 10, 1, 16776960);
			}
		}
		for (int j5 = 0; j5 < this.anInt863; j5++) {
			int l5 = 9 + byte0 + j5 % 4 * 49;
			int j6 = 31 + byte1 + j5 / 4 * 34;
			this.screen.resize_trans_sprite_plot(l5, j6, 48, 32,
					this.item_sprite_off
							+ Config.item_sprite[this.anIntArray864[j5]],
					Config.item_tint[this.anIntArray864[j5]], 0, 0, false);
			if (Config.item_stackable[this.anIntArray864[j5]] == 0)
				this.screen.text_draw(String.valueOf(this.anIntArray865[j5]),
						l5 + 1, j6 + 10, 1, 16776960);
			if ((this.mouse_x > l5) && (this.mouse_x < l5 + 48)
					&& (this.mouse_y > j6) && (this.mouse_y < j6 + 32)) {
				this.screen
						.text_draw(
								new StringBuilder()
										.append(Config.item_name[this.anIntArray864[j5]])
										.append(": @whi@")
										.append(Config.item_desc[this.anIntArray864[j5]])
										.toString(), byte0 + 8, byte1 + 273, 1,
								16776960);
			}
		}
		for (int i6 = 0; i6 < this.anInt866; i6++) {
			int k6 = 9 + byte0 + i6 % 4 * 49;
			int l6 = 156 + byte1 + i6 / 4 * 34;
			this.screen.resize_trans_sprite_plot(k6, l6, 48, 32,
					this.item_sprite_off
							+ Config.item_sprite[this.anIntArray867[i6]],
					Config.item_tint[this.anIntArray867[i6]], 0, 0, false);
			if (Config.item_stackable[this.anIntArray867[i6]] == 0)
				this.screen.text_draw(String.valueOf(this.anIntArray868[i6]),
						k6 + 1, l6 + 10, 1, 16776960);
			if ((this.mouse_x > k6) && (this.mouse_x < k6 + 48)
					&& (this.mouse_y > l6) && (this.mouse_y < l6 + 32))
				this.screen
						.text_draw(
								new StringBuilder()
										.append(Config.item_name[this.anIntArray867[i6]])
										.append(": @whi@")
										.append(Config.item_desc[this.anIntArray867[i6]])
										.toString(), byte0 + 8, byte1 + 273, 1,
								16776960);
		}
	}

	public void method114() {
		byte byte0 = 22;
		byte byte1 = 36;
		this.screen.rect_fill(byte0, byte1, 468, 16, 192);
		int i = 10000536;
		this.screen.rect_fill(byte0, byte1 + 16, 468, 246, i, 160);
		this.screen
				.center_text_draw(
						new StringBuilder()
								.append("Please confirm your duel with @yel@")
								.append(DataUtil.decode_name(this.aLong850))
								.toString(), byte0 + 234, byte1 + 12, 1,
						16777215);
		this.screen.center_text_draw("Your stake:", byte0 + 117, byte1 + 30, 1,
				16776960);
		for (int j = 0; j < this.anInt836; j++) {
			String s = Config.item_name[this.anIntArray837[j]];
			if (Config.item_stackable[this.anIntArray837[j]] == 0)
				s = new StringBuilder().append(s).append(" (")
						.append(this.anIntArray838[j]).append(")").toString();
			this.screen.center_text_draw(s, byte0 + 117, byte1 + 42 + j * 12,
					1, 16777215);
		}

		if (this.anInt836 == 0)
			this.screen.center_text_draw("Nothing!", byte0 + 117, byte1 + 42,
					1, 16777215);
		this.screen.center_text_draw("Your opponent's stake:", byte0 + 351,
				byte1 + 30, 1, 16776960);
		for (int k = 0; k < this.anInt839; k++) {
			String s1 = Config.item_name[this.anIntArray840[k]];
			if (Config.item_stackable[this.anIntArray840[k]] == 0)
				s1 = new StringBuilder().append(s1).append(" (")
						.append(this.anIntArray841[k]).append(")").toString();
			this.screen.center_text_draw(s1, byte0 + 351, byte1 + 42 + k * 12,
					1, 16777215);
		}

		if (this.anInt839 == 0)
			this.screen.center_text_draw("Nothing!", byte0 + 351, byte1 + 42,
					1, 16777215);
		if (this.anInt857 == 0)
			this.screen.center_text_draw("You can retreat from this duel",
					byte0 + 234, byte1 + 180, 1, 65280);
		else
			this.screen.center_text_draw("No retreat is possible!",
					byte0 + 234, byte1 + 180, 1, 16711680);
		if (this.anInt858 == 0)
			this.screen.center_text_draw("Magic may be used", byte0 + 234,
					byte1 + 192, 1, 65280);
		else
			this.screen.center_text_draw("Magic cannot be used", byte0 + 234,
					byte1 + 192, 1, 16711680);
		if (this.anInt859 == 0)
			this.screen.center_text_draw("Prayer may be used", byte0 + 234,
					byte1 + 204, 1, 65280);
		else
			this.screen.center_text_draw("Prayer cannot be used", byte0 + 234,
					byte1 + 204, 1, 16711680);
		if (this.anInt860 == 0)
			this.screen.center_text_draw("Weapons may be used", byte0 + 234,
					byte1 + 216, 1, 65280);
		else
			this.screen.center_text_draw("Weapons cannot be used", byte0 + 234,
					byte1 + 216, 1, 16711680);
		this.screen.center_text_draw(
				"If you are sure click 'Accept' to begin the duel",
				byte0 + 234, byte1 + 230, 1, 16777215);
		if (!this.aBoolean849) {
			this.screen.sprite_plot(byte0 + 118 - 35, byte1 + 238,
					this.ui_sprite_off + 25);
			this.screen.sprite_plot(byte0 + 352 - 35, byte1 + 238,
					this.ui_sprite_off + 26);
		} else {
			this.screen.center_text_draw("Waiting for other player...",
					byte0 + 234, byte1 + 250, 1, 16776960);
		}
		if (this.anInt668 == 1) {
			if ((this.mouse_x < byte0) || (this.mouse_y < byte1)
					|| (this.mouse_x > byte0 + 468)
					|| (this.mouse_y > byte1 + 262)) {
				this.aBoolean848 = false;
				this.conn.enter(233);
				this.conn.exit();
			}
			if ((this.mouse_x >= byte0 + 118 - 35)
					&& (this.mouse_x <= byte0 + 118 + 70)
					&& (this.mouse_y >= byte1 + 238)
					&& (this.mouse_y <= byte1 + 238 + 21)) {
				this.aBoolean849 = true;
				this.conn.enter(198);
				this.conn.exit();
			}
			if ((this.mouse_x >= byte0 + 352 - 35)
					&& (this.mouse_x <= byte0 + 353 + 70)
					&& (this.mouse_y >= byte1 + 238)
					&& (this.mouse_y <= byte1 + 238 + 21)) {
				this.aBoolean848 = false;
				this.conn.enter(203);
				this.conn.exit();
			}
			this.anInt668 = 0;
		}
	}

	public void method115() {
		if ((this.anInt668 != 0) && (this.anInt872 == 0))
			this.anInt872 = 1;
		if (this.anInt872 > 0) {
			int i = this.mouse_x - 22;
			int j = this.mouse_y - 36;
			if ((i >= 0) && (j >= 0) && (i < 468) && (j < 262)) {
				if ((i > 216) && (j > 30) && (i < 462) && (j < 235)) {
					int k = (i - 217) / 49 + (j - 31) / 34 * 5;
					if ((k >= 0) && (k < this.anInt772)) {
						boolean flag1 = false;
						int l1 = 0;
						int k2 = this.anIntArray773[k];
						for (int k3 = 0; k3 < this.anInt836; k3++) {
							if (this.anIntArray837[k3] == k2) {
								if (Config.item_stackable[k2] == 0) {
									for (int i4 = 0; i4 < this.anInt872; i4++) {
										if (this.anIntArray838[k3] < this.anIntArray774[k])
											this.anIntArray838[k3] += 1;
										flag1 = true;
									}
								} else
									l1++;
							}
						}
						if (method90(k2) <= l1)
							flag1 = true;
						if (Config.item_tradable[k2] == 1) {
							method77(
									"This object cannot be added to a duel offer",
									3);
							flag1 = true;
						}
						if ((!flag1) && (this.anInt836 < 8)) {
							this.anIntArray837[this.anInt836] = k2;
							this.anIntArray838[this.anInt836] = 1;
							this.anInt836 += 1;
							flag1 = true;
						}
						if (flag1) {
							this.conn.enter(201);
							this.conn.byte_put(this.anInt836);
							for (int j4 = 0; j4 < this.anInt836; j4++) {
								this.conn.short_put(this.anIntArray837[j4]);
								this.conn.int_put(this.anIntArray838[j4]);
							}

							this.conn.exit();
							this.aBoolean842 = false;
							this.aBoolean843 = false;
						}
					}
				}
				if ((i > 8) && (j > 30) && (i < 205) && (j < 129)) {
					int l = (i - 9) / 49 + (j - 31) / 34 * 4;
					if ((l >= 0) && (l < this.anInt836)) {
						int j1 = this.anIntArray837[l];
						for (int i2 = 0; i2 < this.anInt872; i2++) {
							if ((Config.item_stackable[j1] == 0)
									&& (this.anIntArray838[l] > 1)) {
								this.anIntArray838[l] -= 1;
							} else {
								this.anInt836 -= 1;
								this.anInt871 = 0;
								for (int l2 = l; l2 < this.anInt836; l2++) {
									this.anIntArray837[l2] = this.anIntArray837[(l2 + 1)];
									this.anIntArray838[l2] = this.anIntArray838[(l2 + 1)];
								}

								break;
							}
						}
						this.conn.enter(201);
						this.conn.byte_put(this.anInt836);
						for (int i3 = 0; i3 < this.anInt836; i3++) {
							this.conn.short_put(this.anIntArray837[i3]);
							this.conn.int_put(this.anIntArray838[i3]);
						}

						this.conn.exit();
						this.aBoolean842 = false;
						this.aBoolean843 = false;
					}
				}
				boolean flag = false;
				if ((i >= 93) && (j >= 221) && (i <= 104) && (j <= 232)) {
					this.aBoolean844 = (!this.aBoolean844);
					flag = true;
				}
				if ((i >= 93) && (j >= 240) && (i <= 104) && (j <= 251)) {
					this.aBoolean845 = (!this.aBoolean845);
					flag = true;
				}
				if ((i >= 191) && (j >= 221) && (i <= 202) && (j <= 232)) {
					this.aBoolean846 = (!this.aBoolean846);
					flag = true;
				}
				if ((i >= 191) && (j >= 240) && (i <= 202) && (j <= 251)) {
					this.aBoolean847 = (!this.aBoolean847);
					flag = true;
				}
				if (flag) {
					this.conn.enter(200);
					this.conn.byte_put(this.aBoolean844 ? 1 : 0);
					this.conn.byte_put(this.aBoolean845 ? 1 : 0);
					this.conn.byte_put(this.aBoolean846 ? 1 : 0);
					this.conn.byte_put(this.aBoolean847 ? 1 : 0);
					this.conn.exit();
					this.aBoolean842 = false;
					this.aBoolean843 = false;
				}
				if ((i >= 217) && (j >= 238) && (i <= 286) && (j <= 259)) {
					this.aBoolean843 = true;
					this.conn.enter(199);
					this.conn.exit();
				}
				if ((i >= 394) && (j >= 238) && (i < 463) && (j < 259)) {
					this.aBoolean834 = false;
					this.conn.enter(203);
					this.conn.exit();
				}
			} else if (this.anInt668 != 0) {
				this.aBoolean834 = false;
				this.conn.enter(203);
				this.conn.exit();
			}
			this.anInt668 = 0;
			this.anInt872 = 0;
		}
		if (!this.aBoolean834)
			return;
		byte byte0 = 22;
		byte byte1 = 36;
		this.screen.rect_fill(byte0, byte1, 468, 12, 13175581);
		int i1 = 10000536;
		this.screen.rect_fill(byte0, byte1 + 12, 468, 18, i1, 160);
		this.screen.rect_fill(byte0, byte1 + 30, 8, 248, i1, 160);
		this.screen.rect_fill(byte0 + 205, byte1 + 30, 11, 248, i1, 160);
		this.screen.rect_fill(byte0 + 462, byte1 + 30, 6, 248, i1, 160);
		this.screen.rect_fill(byte0 + 8, byte1 + 99, 197, 24, i1, 160);
		this.screen.rect_fill(byte0 + 8, byte1 + 192, 197, 23, i1, 160);
		this.screen.rect_fill(byte0 + 8, byte1 + 258, 197, 20, i1, 160);
		this.screen.rect_fill(byte0 + 216, byte1 + 235, 246, 43, i1, 160);
		int k1 = 13684944;
		this.screen.rect_fill(byte0 + 8, byte1 + 30, 197, 69, k1, 160);
		this.screen.rect_fill(byte0 + 8, byte1 + 123, 197, 69, k1, 160);
		this.screen.rect_fill(byte0 + 8, byte1 + 215, 197, 43, k1, 160);
		this.screen.rect_fill(byte0 + 216, byte1 + 30, 246, 205, k1, 160);
		for (int j2 = 0; j2 < 3; j2++) {
			this.screen.line_horiz(byte0 + 8, byte1 + 30 + j2 * 34, 197, 0);
		}
		for (int j3 = 0; j3 < 3; j3++) {
			this.screen.line_horiz(byte0 + 8, byte1 + 123 + j3 * 34, 197, 0);
		}
		for (int l3 = 0; l3 < 7; l3++) {
			this.screen.line_horiz(byte0 + 216, byte1 + 30 + l3 * 34, 246, 0);
		}
		for (int k4 = 0; k4 < 6; k4++) {
			if (k4 < 5)
				this.screen.line_vert(byte0 + 8 + k4 * 49, byte1 + 30, 69, 0);
			if (k4 < 5)
				this.screen.line_vert(byte0 + 8 + k4 * 49, byte1 + 123, 69, 0);
			this.screen.line_vert(byte0 + 216 + k4 * 49, byte1 + 30, 205, 0);
		}

		this.screen.line_horiz(byte0 + 8, byte1 + 215, 197, 0);
		this.screen.line_horiz(byte0 + 8, byte1 + 257, 197, 0);
		this.screen.line_vert(byte0 + 8, byte1 + 215, 43, 0);
		this.screen.line_vert(byte0 + 204, byte1 + 215, 43, 0);
		this.screen.text_draw(
				new StringBuilder().append("Preparing to duel with: ")
						.append(this.aString835).toString(), byte0 + 1,
				byte1 + 10, 1, 16777215);
		this.screen.text_draw("Your Stake", byte0 + 9, byte1 + 27, 4, 16777215);
		this.screen.text_draw("Opponent's Stake", byte0 + 9, byte1 + 120, 4,
				16777215);
		this.screen.text_draw("Duel Options", byte0 + 9, byte1 + 212, 4,
				16777215);
		this.screen.text_draw("Your Inventory", byte0 + 216, byte1 + 27, 4,
				16777215);
		this.screen.text_draw("No retreating", byte0 + 8 + 1, byte1 + 215 + 16,
				3, 16776960);
		this.screen.text_draw("No magic", byte0 + 8 + 1, byte1 + 215 + 35, 3,
				16776960);
		this.screen.text_draw("No prayer", byte0 + 8 + 102, byte1 + 215 + 16,
				3, 16776960);
		this.screen.text_draw("No weapons", byte0 + 8 + 102, byte1 + 215 + 35,
				3, 16776960);
		this.screen.rect_draw(byte0 + 93, byte1 + 215 + 6, 11, 11, 16776960);
		if (this.aBoolean844)
			this.screen.rect_fill(byte0 + 95, byte1 + 215 + 8, 7, 7, 16776960);
		this.screen.rect_draw(byte0 + 93, byte1 + 215 + 25, 11, 11, 16776960);
		if (this.aBoolean845)
			this.screen.rect_fill(byte0 + 95, byte1 + 215 + 27, 7, 7, 16776960);
		this.screen.rect_draw(byte0 + 191, byte1 + 215 + 6, 11, 11, 16776960);
		if (this.aBoolean846)
			this.screen.rect_fill(byte0 + 193, byte1 + 215 + 8, 7, 7, 16776960);
		this.screen.rect_draw(byte0 + 191, byte1 + 215 + 25, 11, 11, 16776960);
		if (this.aBoolean847)
			this.screen
					.rect_fill(byte0 + 193, byte1 + 215 + 27, 7, 7, 16776960);
		if (!this.aBoolean843)
			this.screen.sprite_plot(byte0 + 217, byte1 + 238,
					this.ui_sprite_off + 25);
		this.screen.sprite_plot(byte0 + 394, byte1 + 238,
				this.ui_sprite_off + 26);
		if (this.aBoolean842) {
			this.screen.center_text_draw("Other player", byte0 + 341,
					byte1 + 246, 1, 16777215);
			this.screen.center_text_draw("has accepted", byte0 + 341,
					byte1 + 256, 1, 16777215);
		}
		if (this.aBoolean843) {
			this.screen.center_text_draw("Waiting for", byte0 + 217 + 35,
					byte1 + 246, 1, 16777215);
			this.screen.center_text_draw("other player", byte0 + 217 + 35,
					byte1 + 256, 1, 16777215);
		}
		for (int l4 = 0; l4 < this.anInt772; l4++) {
			int i5 = 217 + byte0 + l4 % 5 * 49;
			int k5 = 31 + byte1 + l4 / 5 * 34;
			this.screen.resize_trans_sprite_plot(i5, k5, 48, 32,
					this.item_sprite_off
							+ Config.item_sprite[this.anIntArray773[l4]],
					Config.item_tint[this.anIntArray773[l4]], 0, 0, false);
			if (Config.item_stackable[this.anIntArray773[l4]] == 0) {
				this.screen.text_draw(String.valueOf(this.anIntArray774[l4]),
						i5 + 1, k5 + 10, 1, 16776960);
			}
		}
		for (int j5 = 0; j5 < this.anInt836; j5++) {
			int l5 = 9 + byte0 + j5 % 4 * 49;
			int j6 = 31 + byte1 + j5 / 4 * 34;
			this.screen.resize_trans_sprite_plot(l5, j6, 48, 32,
					this.item_sprite_off
							+ Config.item_sprite[this.anIntArray837[j5]],
					Config.item_tint[this.anIntArray837[j5]], 0, 0, false);
			if (Config.item_stackable[this.anIntArray837[j5]] == 0)
				this.screen.text_draw(String.valueOf(this.anIntArray838[j5]),
						l5 + 1, j6 + 10, 1, 16776960);
			if ((this.mouse_x > l5) && (this.mouse_x < l5 + 48)
					&& (this.mouse_y > j6) && (this.mouse_y < j6 + 32)) {
				this.screen
						.text_draw(
								new StringBuilder()
										.append(Config.item_name[this.anIntArray837[j5]])
										.append(": @whi@")
										.append(Config.item_desc[this.anIntArray837[j5]])
										.toString(), byte0 + 8, byte1 + 273, 1,
								16776960);
			}
		}
		for (int i6 = 0; i6 < this.anInt839; i6++) {
			int k6 = 9 + byte0 + i6 % 4 * 49;
			int l6 = 124 + byte1 + i6 / 4 * 34;
			this.screen.resize_trans_sprite_plot(k6, l6, 48, 32,
					this.item_sprite_off
							+ Config.item_sprite[this.anIntArray840[i6]],
					Config.item_tint[this.anIntArray840[i6]], 0, 0, false);
			if (Config.item_stackable[this.anIntArray840[i6]] == 0)
				this.screen.text_draw(String.valueOf(this.anIntArray841[i6]),
						k6 + 1, l6 + 10, 1, 16776960);
			if ((this.mouse_x > k6) && (this.mouse_x < k6 + 48)
					&& (this.mouse_y > l6) && (this.mouse_y < l6 + 32))
				this.screen
						.text_draw(
								new StringBuilder()
										.append(Config.item_name[this.anIntArray840[i6]])
										.append(": @whi@")
										.append(Config.item_desc[this.anIntArray840[i6]])
										.toString(), byte0 + 8, byte1 + 273, 1,
								16776960);
		}
	}

	public void method116() {
		if ((this.anInt770 == 0) && (this.mouse_x >= this.screen.w - 35)
				&& (this.mouse_y >= 3) && (this.mouse_x < this.screen.w - 3)
				&& (this.mouse_y < 35))
			this.anInt770 = 1;
		if ((this.anInt770 == 0) && (this.mouse_x >= this.screen.w - 35 - 33)
				&& (this.mouse_y >= 3)
				&& (this.mouse_x < this.screen.w - 3 - 33)
				&& (this.mouse_y < 35))
			this.anInt770 = 2;
		if ((this.anInt770 == 0) && (this.mouse_x >= this.screen.w - 35 - 66)
				&& (this.mouse_y >= 3)
				&& (this.mouse_x < this.screen.w - 3 - 66)
				&& (this.mouse_y < 35))
			this.anInt770 = 3;
		if ((this.anInt770 == 0) && (this.mouse_x >= this.screen.w - 35 - 99)
				&& (this.mouse_y >= 3)
				&& (this.mouse_x < this.screen.w - 3 - 99)
				&& (this.mouse_y < 35))
			this.anInt770 = 4;
		if ((this.anInt770 == 0) && (this.mouse_x >= this.screen.w - 35 - 132)
				&& (this.mouse_y >= 3)
				&& (this.mouse_x < this.screen.w - 3 - 132)
				&& (this.mouse_y < 35))
			this.anInt770 = 5;
		if ((this.anInt770 == 0) && (this.mouse_x >= this.screen.w - 35 - 165)
				&& (this.mouse_y >= 3)
				&& (this.mouse_x < this.screen.w - 3 - 165)
				&& (this.mouse_y < 35))
			this.anInt770 = 6;
		if ((this.anInt770 != 0) && (this.mouse_x >= this.screen.w - 35)
				&& (this.mouse_y >= 3) && (this.mouse_x < this.screen.w - 3)
				&& (this.mouse_y < 26))
			this.anInt770 = 1;
		if ((this.anInt770 != 0) && (this.mouse_x >= this.screen.w - 35 - 33)
				&& (this.mouse_y >= 3)
				&& (this.mouse_x < this.screen.w - 3 - 33)
				&& (this.mouse_y < 26))
			this.anInt770 = 2;
		if ((this.anInt770 != 0) && (this.mouse_x >= this.screen.w - 35 - 66)
				&& (this.mouse_y >= 3)
				&& (this.mouse_x < this.screen.w - 3 - 66)
				&& (this.mouse_y < 26))
			this.anInt770 = 3;
		if ((this.anInt770 != 0) && (this.mouse_x >= this.screen.w - 35 - 99)
				&& (this.mouse_y >= 3)
				&& (this.mouse_x < this.screen.w - 3 - 99)
				&& (this.mouse_y < 26))
			this.anInt770 = 4;
		if ((this.anInt770 != 0) && (this.mouse_x >= this.screen.w - 35 - 132)
				&& (this.mouse_y >= 3)
				&& (this.mouse_x < this.screen.w - 3 - 132)
				&& (this.mouse_y < 26))
			this.anInt770 = 5;
		if ((this.anInt770 != 0) && (this.mouse_x >= this.screen.w - 35 - 165)
				&& (this.mouse_y >= 3)
				&& (this.mouse_x < this.screen.w - 3 - 165)
				&& (this.mouse_y < 26))
			this.anInt770 = 6;
		if ((this.anInt770 == 1)
				&& ((this.mouse_x < this.screen.w - 248) || (this.mouse_y > 36 + this.anInt771 / 5 * 34)))
			this.anInt770 = 0;
		if ((this.anInt770 == 3)
				&& ((this.mouse_x < this.screen.w - 199) || (this.mouse_y > 294)))
			this.anInt770 = 0;
		if (((this.anInt770 == 2) || (this.anInt770 == 4) || (this.anInt770 == 5))
				&& ((this.mouse_x < this.screen.w - 199) || (this.mouse_y > 240)))
			this.anInt770 = 0;
		if ((this.anInt770 == 6)
				&& ((this.mouse_x < this.screen.w - 199) || (this.mouse_y > 311)))
			this.anInt770 = 0;
	}

	public void method117(boolean flag) {
		int i = this.screen.w - 248;
		this.screen.sprite_plot(i, 3, this.ui_sprite_off + 1);
		for (int j = 0; j < this.anInt771; j++) {
			int k = i + j % 5 * 49;
			int i1 = 36 + j / 5 * 34;
			if ((j < this.anInt772) && (this.anIntArray775[j] == 1))
				this.screen.rect_fill(k, i1, 49, 34, 16711680, 128);
			else
				this.screen.rect_fill(k, i1, 49, 34,
						Surface.rgb(181, 181, 181), 128);
			if (j < this.anInt772) {
				this.screen.resize_trans_sprite_plot(k, i1, 48, 32,
						this.item_sprite_off
								+ Config.item_sprite[this.anIntArray773[j]],
						Config.item_tint[this.anIntArray773[j]], 0, 0, false);
				if (Config.item_stackable[this.anIntArray773[j]] == 0) {
					this.screen.text_draw(
							String.valueOf(this.anIntArray774[j]), k + 1,
							i1 + 10, 1, 16776960);
				}
			}
		}
		for (int l = 1; l <= 4; l++) {
			this.screen.line_vert(i + l * 49, 36, this.anInt771 / 5 * 34, 0);
		}
		for (int j1 = 1; j1 <= this.anInt771 / 5 - 1; j1++) {
			this.screen.line_horiz(i, 36 + j1 * 34, 245, 0);
		}
		if (!flag)
			return;
		i = this.mouse_x - (this.screen.w - 248);
		int k1 = this.mouse_y - 36;
		if ((i >= 0) && (k1 >= 0) && (i < 248) && (k1 < this.anInt771 / 5 * 34)) {
			int l1 = i / 49 + k1 / 34 * 5;
			if (l1 < this.anInt772) {
				int i2 = this.anIntArray773[l1];
				if (this.anInt789 >= 0) {
					if (Config.anIntArray571[this.anInt789] == 3) {
						this.aStringArray813[this.anInt810] = new StringBuilder()
								.append("Cast ")
								.append(Config.aStringArray567[this.anInt789])
								.append(" on").toString();
						this.aStringArray812[this.anInt810] = new StringBuilder()
								.append("@lre@").append(Config.item_name[i2])
								.toString();
						this.anIntArray814[this.anInt810] = 600;
						this.anIntArray817[this.anInt810] = l1;
						this.anIntArray818[this.anInt810] = this.anInt789;
						this.anInt810 += 1;
						return;
					}
				} else {
					if (this.anInt776 >= 0) {
						this.aStringArray813[this.anInt810] = new StringBuilder()
								.append("Use ").append(this.aString777)
								.append(" with").toString();
						this.aStringArray812[this.anInt810] = new StringBuilder()
								.append("@lre@").append(Config.item_name[i2])
								.toString();
						this.anIntArray814[this.anInt810] = 610;
						this.anIntArray817[this.anInt810] = l1;
						this.anIntArray818[this.anInt810] = this.anInt776;
						this.anInt810 += 1;
						return;
					}
					if (this.anIntArray775[l1] == 1) {
						this.aStringArray813[this.anInt810] = "Remove";
						this.aStringArray812[this.anInt810] = new StringBuilder()
								.append("@lre@").append(Config.item_name[i2])
								.toString();
						this.anIntArray814[this.anInt810] = 620;
						this.anIntArray817[this.anInt810] = l1;
						this.anInt810 += 1;
					} else if (Config.item_equipable[i2] != 0) {
						if ((Config.item_equipable[i2] & 0x18) != 0)
							this.aStringArray813[this.anInt810] = "Wield";
						else
							this.aStringArray813[this.anInt810] = "Wear";
						this.aStringArray812[this.anInt810] = new StringBuilder()
								.append("@lre@").append(Config.item_name[i2])
								.toString();
						this.anIntArray814[this.anInt810] = 630;
						this.anIntArray817[this.anInt810] = l1;
						this.anInt810 += 1;
					}
					if (!Config.item_actions[i2].equals("")) {
						this.aStringArray813[this.anInt810] = Config.item_actions[i2];
						this.aStringArray812[this.anInt810] = new StringBuilder()
								.append("@lre@").append(Config.item_name[i2])
								.toString();
						this.anIntArray814[this.anInt810] = 640;
						this.anIntArray817[this.anInt810] = l1;
						this.anInt810 += 1;
					}
					this.aStringArray813[this.anInt810] = "Use";
					this.aStringArray812[this.anInt810] = new StringBuilder()
							.append("@lre@").append(Config.item_name[i2])
							.toString();
					this.anIntArray814[this.anInt810] = 650;
					this.anIntArray817[this.anInt810] = l1;
					this.anInt810 += 1;
					this.aStringArray813[this.anInt810] = "Drop";
					this.aStringArray812[this.anInt810] = new StringBuilder()
							.append("@lre@").append(Config.item_name[i2])
							.toString();
					this.anIntArray814[this.anInt810] = 660;
					this.anIntArray817[this.anInt810] = l1;
					this.anInt810 += 1;
					this.aStringArray813[this.anInt810] = "Examine";
					this.aStringArray812[this.anInt810] = new StringBuilder()
							.append("@lre@").append(Config.item_name[i2])
							.toString();
					this.anIntArray814[this.anInt810] = 3600;
					this.anIntArray817[this.anInt810] = i2;
					this.anInt810 += 1;
				}
			}
		}
	}

	public void method118(boolean flag) {
		int i = this.screen.w - 199;
		char c = '';
		char c2 = '';
		this.screen.sprite_plot(i - 49, 3, this.ui_sprite_off + 2);
		i += 40;
		this.screen.rect_fill(i, 36, c, c2, 0);
		this.screen.set_rect(i, 36, i + c, '$' + c2);
		char c4 = 'À';
		int k = (this.local_player.cur_x - 6040) * 3 * c4 / 2048;
		int i2 = (this.local_player.cur_y - 6040) * 3 * c4 / 2048;
		int k3 = Scene.sin2048_cache[(1024 - this.pitch * 4 & 0x3FF)];
		int i4 = Scene.sin2048_cache[((1024 - this.pitch * 4 & 0x3FF) + 1024)];
		int k4 = i2 * k3 + k * i4 >> 18;
		i2 = i2 * i4 - k * k3 >> 18;
		k = k4;
		this.screen.rotate_sprite_plot(i + c / '\002' - k, 36 + c2 / '\002'
				+ i2, this.ui_sprite_off - 1, this.pitch + 64 & 0xFF, c4);
		for (int i6 = 0; i6 < this.anInt754; i6++) {
			int l = (this.obj_x[i6] * this.anInt684 + 64 - this.local_player.cur_x)
					* 3 * c4 / 2048;
			int j2 = (this.obj_y[i6] * this.anInt684 + 64 - this.local_player.cur_y)
					* 3 * c4 / 2048;
			int l4 = j2 * k3 + l * i4 >> 18;
			j2 = j2 * i4 - l * k3 >> 18;
			l = l4;
			method93(i + c / '\002' + l, 36 + c2 / '\002' - j2, 65535);
		}

		for (int j6 = 0; j6 < this.anInt748; j6++) {
			int i1 = (this.anIntArray749[j6] * this.anInt684 + 64 - this.local_player.cur_x)
					* 3 * c4 / 2048;
			int k2 = (this.anIntArray750[j6] * this.anInt684 + 64 - this.local_player.cur_y)
					* 3 * c4 / 2048;
			int i5 = k2 * k3 + i1 * i4 >> 18;
			k2 = k2 * i4 - i1 * k3 >> 18;
			i1 = i5;
			method93(i + c / '\002' + i1, 36 + c2 / '\002' - k2, 16711680);
		}

		for (int k6 = 0; k6 < this.anInt741; k6++) {
			Mob class14 = this.aClass14Array744[k6];
			int j1 = (class14.cur_x - this.local_player.cur_x) * 3 * c4 / 2048;
			int l2 = (class14.cur_y - this.local_player.cur_y) * 3 * c4 / 2048;
			int j5 = l2 * k3 + j1 * i4 >> 18;
			l2 = l2 * i4 - j1 * k3 >> 18;
			j1 = j5;
			method93(i + c / '\002' + j1, 36 + c2 / '\002' - l2, 16776960);
		}

		for (int l6 = 0; l6 < this.local_player_cnt; l6++) {
			Mob class14_1 = this.local_players[l6];
			int k1 = (class14_1.cur_x - this.local_player.cur_x) * 3 * c4
					/ 2048;
			int i3 = (class14_1.cur_y - this.local_player.cur_y) * 3 * c4
					/ 2048;
			int k5 = i3 * k3 + k1 * i4 >> 18;
			i3 = i3 * i4 - k1 * k3 >> 18;
			k1 = k5;
			int j7 = 16777215;
			for (int k7 = 0; k7 < this.friend_cnt; k7++) {
				if ((class14_1.aLong589 != this.friends[k7])
						|| (this.friend_nodes[k7] != 10))
					continue;
				j7 = 65280;
				break;
			}

			method93(i + c / '\002' + k1, 36 + c2 / '\002' - i3, j7);
		}

		this.screen.circle_fill(i + c / '\002', 36 + c2 / '\002', 2, 16777215,
				255);
		this.screen.rotate_sprite_plot(i + 19, 55, this.ui_sprite_off + 24,
				this.pitch + 128 & 0xFF, 128);
		this.screen.set_rect(0, 0, this.wnd_w, this.wnd_h + 12);
		if (!flag)
			return;
		i = this.mouse_x - (this.screen.w - 199);
		int i7 = this.mouse_y - 36;
		if ((i >= 40) && (i7 >= 0) && (i < 196) && (i7 < 152)) {
			char c1 = '';
			char c3 = '';
			char c5 = 'À';
			int j = this.screen.w - 199;
			j += 40;
			int l1 = (this.mouse_x - (j + c1 / '\002')) * 16384 / ('\003' * c5);
			int j3 = (this.mouse_y - (36 + c3 / '\002')) * 16384
					/ ('\003' * c5);
			int l3 = Scene.sin2048_cache[(1024 - this.pitch * 4 & 0x3FF)];
			int j4 = Scene.sin2048_cache[((1024 - this.pitch * 4 & 0x3FF) + 1024)];
			int l5 = j3 * l3 + l1 * j4 >> 15;
			j3 = j3 * j4 - l1 * l3 >> 15;
			l1 = l5;
			l1 += this.local_player.cur_x;
			j3 = this.local_player.cur_y - j3;
			if (this.anInt668 == 1)
				method94(this.anInt736, this.anInt737, l1 / 128, j3 / 128,
						false);
			this.anInt668 = 0;
		}
	}

	public void method119(boolean flag) {
		int i = this.screen.w - 199;
		int j = 36;
		this.screen.sprite_plot(i - 49, 3, this.ui_sprite_off + 3);
		char c = 'Ä';
		char c1 = 'ú';
		int l;
		int k = l = Surface.rgb(160, 160, 160);
		if (this.anInt796 == 0)
			k = Surface.rgb(220, 220, 220);
		else
			l = Surface.rgb(220, 220, 220);
		this.screen.rect_fill(i, j, c / '\002', 24, k, 128);
		this.screen.rect_fill(i + c / '\002', j, c / '\002', 24, l, 128);
		this.screen.rect_fill(i, j + 24, c, c1 - '\030',
				Surface.rgb(220, 220, 220), 128);
		this.screen.line_horiz(i, j + 24, c, 0);
		this.screen.line_vert(i + c / '\002', j, 24, 0);
		this.screen.center_text_draw("Stats", i + c / '\004', j + 16, 4, 0);
		this.screen.center_text_draw("Quests", i + c / '\004' + c / '\002',
				j + 16, 4, 0);
		if (this.anInt796 == 0) {
			int i1 = 72;
			int k1 = -1;
			this.screen.text_draw("Skills", i + 5, i1, 3, 16776960);
			i1 += 13;
			for (int l1 = 0; l1 < 8; l1++) {
				int i2 = 16777215;
				if ((this.mouse_x > i + 3) && (this.mouse_y >= i1 - 11)
						&& (this.mouse_y < i1 + 2) && (this.mouse_x < i + 90)) {
					i2 = 16711680;
					k1 = l1;
				}
				this.screen.text_draw(
						new StringBuilder().append(this.skill_names[l1])
								.append(":@yel@")
								.append(this.anIntArray779[l1]).append("/")
								.append(this.anIntArray780[l1]).toString(),
						i + 5, i1, 1, i2);
				i2 = 16777215;
				if ((this.mouse_x >= i + 90) && (this.mouse_y >= i1 - 13 - 11)
						&& (this.mouse_y < i1 - 13 + 2)
						&& (this.mouse_x < i + 196)) {
					i2 = 16711680;
					k1 = l1 + 8;
				}
				this.screen
						.text_draw(
								new StringBuilder()
										.append(this.skill_names[(l1 + 8)])
										.append(":@yel@")
										.append(this.anIntArray779[(l1 + 8)])
										.append("/")
										.append(this.anIntArray780[(l1 + 8)])
										.toString(), i + c / '\002' - 8,
								i1 - 13, 1, i2);
				i1 += 13;
			}

			this.screen.text_draw(
					new StringBuilder().append("Quest Points:@yel@")
							.append(this.anInt783).toString(), i + c / '\002'
							- 8, i1 - 13, 1, 16777215);
			i1 += 8;
			this.screen.text_draw("Equipment Status", i + 5, i1, 3, 16776960);
			i1 += 12;
			for (int j2 = 0; j2 < 3; j2++) {
				this.screen.text_draw(
						new StringBuilder().append(this.aStringArray785[j2])
								.append(":@yel@")
								.append(this.anIntArray782[j2]).toString(),
						i + 5, i1, 1, 16777215);
				if (j2 < 2)
					this.screen.text_draw(
							new StringBuilder()
									.append(this.aStringArray785[(j2 + 3)])
									.append(":@yel@")
									.append(this.anIntArray782[(j2 + 3)])
									.toString(), i + c / '\002' + 25, i1, 1,
							16777215);
				i1 += 13;
			}

			i1 += 6;
			this.screen.line_horiz(i, i1 - 15, c, 0);
			if (k1 != -1) {
				this.screen.text_draw(
						new StringBuilder().append(this.skill_names[k1])
								.append(" skill").toString(), i + 5, i1, 1,
						16776960);
				i1 += 12;
				int k2 = this.anIntArray778[0];
				for (int i3 = 0; i3 < 98; i3++) {
					if (this.anIntArray781[k1] >= this.anIntArray778[i3])
						k2 = this.anIntArray778[(i3 + 1)];
				}
				this.screen.text_draw(new StringBuilder().append("Total xp: ")
						.append(this.anIntArray781[k1] / 4).toString(), i + 5,
						i1, 1, 16777215);
				i1 += 12;
				this.screen.text_draw(
						new StringBuilder().append("Next level at: ")
								.append(k2 / 4).toString(), i + 5, i1, 1,
						16777215);
			} else {
				this.screen.text_draw("Overall levels", i + 5, i1, 1, 16776960);
				i1 += 12;
				int l2 = 0;
				for (int j3 = 0; j3 < 16; j3++) {
					l2 += this.anIntArray780[j3];
				}
				this.screen.text_draw(
						new StringBuilder().append("Skill total: ").append(l2)
								.toString(), i + 5, i1, 1, 16777215);
				i1 += 12;
				this.screen.text_draw(
						new StringBuilder().append("Combat level: ")
								.append(this.local_player.anInt612).toString(),
						i + 5, i1, 1, 16777215);
				i1 += 12;
			}
		}
		if (this.anInt796 == 1) {
			this.aClass6_794.method323(this.anInt795);
			this.aClass6_794.method325(this.anInt795, 0,
					"@whi@Quest-list (green=completed)");
			for (int j1 = 0; j1 < this.anInt797; j1++) {
				this.aClass6_794
						.method325(
								this.anInt795,
								j1 + 1,
								new StringBuilder()
										.append(this.aBooleanArray799[j1] ? "@gre@" : "@red@")
										.append(this.quest_names[j1])
										.toString());
			}
			this.aClass6_794.display();
		}
		if (!flag)
			return;
		i = this.mouse_x - (this.screen.w - 199);
		j = this.mouse_y - 36;
		if ((i >= 0) && (j >= 0) && (i < c) && (j < c1)) {
			if (this.anInt796 == 1)
				this.aClass6_794.consume(i + (this.screen.w - 199), j + 36,
						this.click_state, this.mouse_state);
			if ((j <= 24) && (this.anInt668 == 1)) {
				if (i < 98) {
					this.anInt796 = 0;
					return;
				}
				if (i > 98)
					this.anInt796 = 1;
			}
		}
	}

	public void method120(boolean flag) {
		int i = this.screen.w - 199;
		int j = 36;
		this.screen.sprite_plot(i - 49, 3, this.ui_sprite_off + 4);
		char c = 'Ä';
		char c1 = '¶';
		int l;
		int k = l = Surface.rgb(160, 160, 160);
		if (this.anInt788 == 0)
			k = Surface.rgb(220, 220, 220);
		else
			l = Surface.rgb(220, 220, 220);
		this.screen.rect_fill(i, j, c / '\002', 24, k, 128);
		this.screen.rect_fill(i + c / '\002', j, c / '\002', 24, l, 128);
		this.screen
				.rect_fill(i, j + 24, c, 90, Surface.rgb(220, 220, 220), 128);
		this.screen.rect_fill(i, j + 24 + 90, c, c1 - 'Z' - 24,
				Surface.rgb(160, 160, 160), 128);
		this.screen.line_horiz(i, j + 24, c, 0);
		this.screen.line_vert(i + c / '\002', j, 24, 0);
		this.screen.line_horiz(i, j + 113, c, 0);
		this.screen.center_text_draw("Magic", i + c / '\004', j + 16, 4, 0);
		this.screen.center_text_draw("Prayers", i + c / '\004' + c / '\002',
				j + 16, 4, 0);
		if (this.anInt788 == 0) {
			this.aClass6_786.method323(this.anInt787);
			int i1 = 0;
			for (int i2 = 0; i2 < Config.anInt566; i2++) {
				String s = "@yel@";
				for (int l3 = 0; l3 < Config.anIntArray570[i2]; l3++) {
					int k4 = Config.anIntArrayArray572[i2][l3];
					if (method91(k4, Config.anIntArrayArray573[i2][l3]))
						continue;
					s = "@whi@";
					break;
				}

				int l4 = this.anIntArray779[6];
				if (Config.anIntArray569[i2] > l4)
					s = "@bla@";
				this.aClass6_786.method325(
						this.anInt787,
						i1++,
						new StringBuilder().append(s).append("Level ")
								.append(Config.anIntArray569[i2]).append(": ")
								.append(Config.aStringArray567[i2]).toString());
			}

			this.aClass6_786.display();
			int i3 = this.aClass6_786.method333(this.anInt787);
			if (i3 != -1) {
				this.screen.text_draw(
						new StringBuilder().append("Level ")
								.append(Config.anIntArray569[i3]).append(": ")
								.append(Config.aStringArray567[i3]).toString(),
						i + 2, j + 124, 1, 16776960);
				this.screen.text_draw(Config.aStringArray568[i3], i + 2,
						j + 136, 0, 16777215);
				for (int i4 = 0; i4 < Config.anIntArray570[i3]; i4++) {
					int i5 = Config.anIntArrayArray572[i3][i4];
					this.screen.sprite_plot(i + 2 + i4 * 44, j + 150,
							this.item_sprite_off + Config.item_sprite[i5]);
					int j5 = method90(i5);
					int k5 = Config.anIntArrayArray573[i3][i4];
					String s2 = "@red@";
					if (method91(i5, k5))
						s2 = "@gre@";
					this.screen.text_draw(new StringBuilder().append(s2)
							.append(j5).append("/").append(k5).toString(), i
							+ 2 + i4 * 44, j + 150, 1, 16777215);
				}
			} else {
				this.screen.text_draw("Point at a spell for a description",
						i + 2, j + 124, 1, 0);
			}
		}
		if (this.anInt788 == 1) {
			this.aClass6_786.method323(this.anInt787);
			int j1 = 0;
			for (int j2 = 0; j2 < Config.anInt574; j2++) {
				String s1 = "@whi@";
				if (Config.anIntArray577[j2] > this.anIntArray780[5])
					s1 = "@bla@";
				if (this.prayer_on[j2])
					s1 = "@gre@";
				this.aClass6_786.method325(
						this.anInt787,
						j1++,
						new StringBuilder().append(s1).append("Level ")
								.append(Config.anIntArray577[j2]).append(": ")
								.append(Config.aStringArray575[j2]).toString());
			}

			this.aClass6_786.display();
			int j3 = this.aClass6_786.method333(this.anInt787);
			if (j3 != -1) {
				this.screen.center_text_draw(
						new StringBuilder().append("Level ")
								.append(Config.anIntArray577[j3]).append(": ")
								.append(Config.aStringArray575[j3]).toString(),
						i + c / '\002', j + 130, 1, 16776960);
				this.screen.center_text_draw(Config.aStringArray576[j3], i + c
						/ '\002', j + 145, 0, 16777215);
				this.screen.center_text_draw(
						new StringBuilder().append("Drain rate: ")
								.append(Config.anIntArray578[j3]).toString(), i
								+ c / '\002', j + 160, 1, 0);
			} else {
				this.screen.text_draw("Point at a prayer for a description",
						i + 2, j + 124, 1, 0);
			}
		}
		if (!flag)
			return;
		i = this.mouse_x - (this.screen.w - 199);
		j = this.mouse_y - 36;
		if ((i >= 0) && (j >= 0) && (i < 196) && (j < 182)) {
			this.aClass6_786.consume(i + (this.screen.w - 199), j + 36,
					this.click_state, this.mouse_state);
			if ((j <= 24) && (this.anInt668 == 1))
				if ((i < 98) && (this.anInt788 == 1)) {
					this.anInt788 = 0;
					this.aClass6_786.method324(this.anInt787);
				} else if ((i > 98) && (this.anInt788 == 0)) {
					this.anInt788 = 1;
					this.aClass6_786.method324(this.anInt787);
				}
			if ((this.anInt668 == 1) && (this.anInt788 == 0)) {
				int k1 = this.aClass6_786.method333(this.anInt787);
				if (k1 != -1) {
					int k2 = this.anIntArray779[6];
					if (Config.anIntArray569[k1] > k2) {
						method77(
								"Your magic ability is not high enough for this spell",
								3);
					} else {
						int k3 = 0;
						for (; k3 < Config.anIntArray570[k1]; k3++) {
							int j4 = Config.anIntArrayArray572[k1][k3];
							if (method91(j4, Config.anIntArrayArray573[k1][k3]))
								continue;
							method77(
									"You don't have all the reagents you need for this spell",
									3);
							k3 = -1;
							break;
						}

						if (k3 == Config.anIntArray570[k1]) {
							this.anInt789 = k1;
							this.anInt776 = -1;
						}
					}
				}
			}
			if ((this.anInt668 == 1) && (this.anInt788 == 1)) {
				int l1 = this.aClass6_786.method333(this.anInt787);
				if (l1 != -1) {
					int l2 = this.anIntArray780[5];
					if (Config.anIntArray577[l1] > l2) {
						method77(
								"Your prayer ability is not high enough for this prayer",
								3);
					} else if (this.anIntArray779[5] == 0) {
						method77(
								"You have run out of prayer points. Return to a church to recharge",
								3);
					} else if (this.prayer_on[l1]) {
						this.conn.enter(211);
						this.conn.byte_put(l1);
						this.conn.exit();
						this.prayer_on[l1] = false;
						method64("prayeroff");
					} else {
						this.conn.enter(212);
						this.conn.byte_put(l1);
						this.conn.exit();
						this.prayer_on[l1] = true;
						method64("prayeron");
					}
				}
			}
			this.anInt668 = 0;
		}
	}

	public void method121(boolean flag) {
		int i = this.screen.w - 199;
		int j = 36;
		this.screen.sprite_plot(i - 49, 3, this.ui_sprite_off + 5);
		char c = 'Ä';
		char c1 = '¶';
		int l;
		int k = l = Surface.rgb(160, 160, 160);
		if (this.anInt792 == 0)
			k = Surface.rgb(220, 220, 220);
		else
			l = Surface.rgb(220, 220, 220);
		this.screen.rect_fill(i, j, c / '\002', 24, k, 128);
		this.screen.rect_fill(i + c / '\002', j, c / '\002', 24, l, 128);
		this.screen.rect_fill(i, j + 24, c, c1 - '\030',
				Surface.rgb(220, 220, 220), 128);
		this.screen.line_horiz(i, j + 24, c, 0);
		this.screen.line_vert(i + c / '\002', j, 24, 0);
		this.screen.line_horiz(i, j + c1 - 16, c, 0);
		this.screen.center_text_draw("Friends", i + c / '\004', j + 16, 4, 0);
		this.screen.center_text_draw("Ignore", i + c / '\004' + c / '\002',
				j + 16, 4, 0);
		this.aClass6_790.method323(this.anInt791);
		if (this.anInt792 == 0) {
			for (int i1 = 0; i1 < this.friend_cnt; i1++) {
				String s;
				if (this.friend_nodes[i1] == 10) {
					s = "@gre@";
				} else {
					if (this.friend_nodes[i1] > 0)
						s = "@yel@";
					else
						s = "@red@";
				}
				this.aClass6_790.method325(
						this.anInt791,
						i1,
						new StringBuilder().append(s)
								.append(DataUtil.decode_name(this.friends[i1]))
								.append("~439~@whi@Remove         WWWWWWWWWW")
								.toString());
			}
		}

		if (this.anInt792 == 1) {
			for (int j1 = 0; j1 < this.ignore_cnt; j1++) {
				this.aClass6_790.method325(
						this.anInt791,
						j1,
						new StringBuilder().append("@yel@")
								.append(DataUtil.decode_name(this.ignores[j1]))
								.append("~439~@whi@Remove         WWWWWWWWWW")
								.toString());
			}
		}
		this.aClass6_790.display();
		if (this.anInt792 == 0) {
			int k1 = this.aClass6_790.method333(this.anInt791);
			if ((k1 >= 0) && (this.mouse_x < 489)) {
				if (this.mouse_x > 429)
					this.screen.center_text_draw(
							new StringBuilder()
									.append("Click to remove ")
									.append(DataUtil
											.decode_name(this.friends[k1]))
									.toString(), i + c / '\002', j + 35, 1,
							16777215);
				else if (this.friend_nodes[k1] == 10)
					this.screen.center_text_draw(
							new StringBuilder()
									.append("Click to message ")
									.append(DataUtil
											.decode_name(this.friends[k1]))
									.toString(), i + c / '\002', j + 35, 1,
							16777215);
				else if (this.friend_nodes[k1] > 0)
					this.screen.center_text_draw(
							new StringBuilder()
									.append(DataUtil
											.decode_name(this.friends[k1]))
									.append(" is on world ")
									.append(this.friend_nodes[k1]).toString(),
							i + c / '\002', j + 35, 1, 16777215);
				else
					this.screen.center_text_draw(
							new StringBuilder()
									.append(DataUtil
											.decode_name(this.friends[k1]))
									.append(" is offline").toString(), i + c
									/ '\002', j + 35, 1, 16777215);
			} else
				this.screen.center_text_draw("Click a name to send a message",
						i + c / '\002', j + 35, 1, 16777215);
			int k2;
			if ((this.mouse_x > i) && (this.mouse_x < i + c)
					&& (this.mouse_y > j + c1 - 16) && (this.mouse_y < j + c1))
				k2 = 16776960;
			else
				k2 = 16777215;
			this.screen.center_text_draw("Click here to add a friend", i + c
					/ '\002', j + c1 - 3, 1, k2);
		}
		if (this.anInt792 == 1) {
			int l1 = this.aClass6_790.method333(this.anInt791);
			if ((l1 >= 0) && (this.mouse_x < 489) && (this.mouse_x > 429)) {
				if (this.mouse_x > 429)
					this.screen.center_text_draw(
							new StringBuilder()
									.append("Click to remove ")
									.append(DataUtil
											.decode_name(this.ignores[l1]))
									.toString(), i + c / '\002', j + 35, 1,
							16777215);
			} else
				this.screen.center_text_draw("Blocking messages from:", i + c
						/ '\002', j + 35, 1, 16777215);
			int l2;
			if ((this.mouse_x > i) && (this.mouse_x < i + c)
					&& (this.mouse_y > j + c1 - 16) && (this.mouse_y < j + c1))
				l2 = 16776960;
			else
				l2 = 16777215;
			this.screen.center_text_draw("Click here to add a name", i + c
					/ '\002', j + c1 - 3, 1, l2);
		}
		if (!flag)
			return;
		i = this.mouse_x - (this.screen.w - 199);
		j = this.mouse_y - 36;
		if ((i >= 0) && (j >= 0) && (i < 196) && (j < 182)) {
			this.aClass6_790.consume(i + (this.screen.w - 199), j + 36,
					this.click_state, this.mouse_state);
			if ((j <= 24) && (this.anInt668 == 1))
				if ((i < 98) && (this.anInt792 == 1)) {
					this.anInt792 = 0;
					this.aClass6_790.method324(this.anInt791);
				} else if ((i > 98) && (this.anInt792 == 0)) {
					this.anInt792 = 1;
					this.aClass6_790.method324(this.anInt791);
				}
			if ((this.anInt668 == 1) && (this.anInt792 == 0)) {
				int i2 = this.aClass6_790.method333(this.anInt791);
				if ((i2 >= 0) && (this.mouse_x < 489))
					if (this.mouse_x > 429) {
						friend_remove(this.friends[i2]);
					} else if (this.friend_nodes[i2] != 0) {
						this.anInt905 = 2;
						this.aLong793 = this.friends[i2];
						this.line_buf_b = "";
						this.line_b = "";
					}
			}
			if ((this.anInt668 == 1) && (this.anInt792 == 1)) {
				int j2 = this.aClass6_790.method333(this.anInt791);
				if ((j2 >= 0) && (this.mouse_x < 489) && (this.mouse_x > 429))
					method36(this.ignores[j2]);
			}
			if ((j > 166) && (this.anInt668 == 1) && (this.anInt792 == 0)) {
				this.anInt905 = 1;
				this.line_buf_a = "";
				this.line_a = "";
			}
			if ((j > 166) && (this.anInt668 == 1) && (this.anInt792 == 1)) {
				this.anInt905 = 3;
				this.line_buf_a = "";
				this.line_a = "";
			}
			this.anInt668 = 0;
		}
	}

	public void method122(boolean flag) {
		int x = this.screen.w - 199;
		int j = 36;
		this.screen.sprite_plot(x - 49, 3, this.ui_sprite_off + 6);
		char c = 'Ä';
		this.screen.rect_fill(x, 36, c, 65, Surface.rgb(181, 181, 181), 160);
		this.screen.rect_fill(x, 101, c, 65, Surface.rgb(201, 201, 201), 160);
		this.screen.rect_fill(x, 166, c, 95, Surface.rgb(181, 181, 181), 160);
		this.screen.rect_fill(x, 261, c, 40, Surface.rgb(201, 201, 201), 160);
		int k = x + 3;
		int y = j + 15;
		this.screen.text_draw("Game options - click to toggle", k, y, 1, 0);
		y += 15;
		if (this.auto_angle)
			this.screen.text_draw("Camera angle mode - @gre@Auto", k, y, 1,
					16777215);
		else
			this.screen.text_draw("Camera angle mode - @red@Manual", k, y, 1,
					16777215);
		y += 15;
		if (this.single_button_mouse)
			this.screen
					.text_draw("Mouse buttons - @red@One", k, y, 1, 16777215);
		else
			this.screen
					.text_draw("Mouse buttons - @gre@Two", k, y, 1, 16777215);
		y += 15;
		if (this.is_members)
			if (this.sound_effects)
				this.screen.text_draw("Sound effects - @red@off", k, y, 1,
						16777215);
			else
				this.screen.text_draw("Sound effects - @gre@on", k, y, 1,
						16777215);
		y += 15;
		y += 5;
		this.screen.text_draw("Security settings", k, y, 1, 0);
		y += 15;
		int color = 16777215;
		if ((this.mouse_x > k) && (this.mouse_x < k + c)
				&& (this.mouse_y > y - 12) && (this.mouse_y < y + 4))
			color = 16776960;
		this.screen.text_draw("Change password", k, y, 1, color);
		y += 15;
		color = 16777215;
		if ((this.mouse_x > k) && (this.mouse_x < k + c)
				&& (this.mouse_y > y - 12) && (this.mouse_y < y + 4))
			color = 16776960;
		this.screen.text_draw("Change recovery questions", k, y, 1, color);
		y += 15;
		y += 15;
		y += 5;
		this.screen.text_draw("Privacy settings. Will be applied to", x + 3, y,
				1, 0);
		y += 15;
		this.screen.text_draw("all people not on your friends list", x + 3, y,
				1, 0);
		y += 15;
		if (this.pub_block == 0)
			this.screen.text_draw("Block chat messages: @red@<off>", x + 3, y,
					1, 16777215);
		else
			this.screen.text_draw("Block chat messages: @gre@<on>", x + 3, y,
					1, 16777215);
		y += 15;
		if (this.priv_block == 0)
			this.screen.text_draw("Block private messages: @red@<off>", x + 3,
					y, 1, 16777215);
		else
			this.screen.text_draw("Block private messages: @gre@<on>", x + 3,
					y, 1, 16777215);
		y += 15;
		if (this.trade_block == 0)
			this.screen.text_draw("Block trade requests: @red@<off>", x + 3, y,
					1, 16777215);
		else
			this.screen.text_draw("Block trade requests: @gre@<on>", x + 3, y,
					1, 16777215);
		y += 15;
		if (this.is_members)
			if (this.duel_block == 0)
				this.screen.text_draw("Block duel requests: @red@<off>", x + 3,
						y, 1, 16777215);
			else
				this.screen.text_draw("Block duel requests: @gre@<on>", x + 3,
						y, 1, 16777215);
		y += 15;
		y += 5;
		this.screen.text_draw("Always logout when you finish", k, y, 1, 0);
		y += 15;
		color = 16777215;
		if ((this.mouse_x > k) && (this.mouse_x < k + c)
				&& (this.mouse_y > y - 12) && (this.mouse_y < y + 4))
			color = 16776960;
		this.screen.text_draw("Click here to logout", x + 3, y, 1, color);
		if (!flag)
			return;
		x = this.mouse_x - (this.screen.w - 199);
		j = this.mouse_y - 36;
		if ((x >= 0) && (j >= 0) && (x < 196) && (j < 265)) {
			int l1 = this.screen.w - 199;
			byte byte0 = 36;
			char c1 = 'Ä';
			int l = l1 + 3;
			int j1 = byte0 + 30;
			if ((this.mouse_x > l) && (this.mouse_x < l + c1)
					&& (this.mouse_y > j1 - 12) && (this.mouse_y < j1 + 4)
					&& (this.anInt668 == 1)) {
				this.auto_angle = (!this.auto_angle);
				this.conn.enter(213);
				this.conn.byte_put(0);
				this.conn.byte_put(this.auto_angle ? 1 : 0);
				this.conn.exit();
			}
			j1 += 15;
			if ((this.mouse_x > l) && (this.mouse_x < l + c1)
					&& (this.mouse_y > j1 - 12) && (this.mouse_y < j1 + 4)
					&& (this.anInt668 == 1)) {
				this.single_button_mouse = (!this.single_button_mouse);
				this.conn.enter(213);
				this.conn.byte_put(2);
				this.conn.byte_put(this.single_button_mouse ? 1 : 0);
				this.conn.exit();
			}
			j1 += 15;
			if ((this.is_members) && (this.mouse_x > l)
					&& (this.mouse_x < l + c1) && (this.mouse_y > j1 - 12)
					&& (this.mouse_y < j1 + 4) && (this.anInt668 == 1)) {
				this.sound_effects = (!this.sound_effects);
				this.conn.enter(213);
				this.conn.byte_put(3);
				this.conn.byte_put(this.sound_effects ? 1 : 0);
				this.conn.exit();
			}
			j1 += 15;
			j1 += 20;
			if ((this.mouse_x > l) && (this.mouse_x < l + c1)
					&& (this.mouse_y > j1 - 12) && (this.mouse_y < j1 + 4)
					&& (this.anInt668 == 1)) {
				this.anInt906 = 6;
				this.line_buf_a = "";
				this.line_a = "";
			}
			j1 += 15;
			if ((this.mouse_x > l) && (this.mouse_x < l + c1)
					&& (this.mouse_y > j1 - 12) && (this.mouse_y < j1 + 4)
					&& (this.anInt668 == 1)) {
				this.conn.enter(197);
				this.conn.exit();
			}
			j1 += 15;
			j1 += 15;
			boolean flag1 = false;
			j1 += 35;
			if ((this.mouse_x > l) && (this.mouse_x < l + c1)
					&& (this.mouse_y > j1 - 12) && (this.mouse_y < j1 + 4)
					&& (this.anInt668 == 1)) {
				this.pub_block = (1 - this.pub_block);
				flag1 = true;
			}
			j1 += 15;
			if ((this.mouse_x > l) && (this.mouse_x < l + c1)
					&& (this.mouse_y > j1 - 12) && (this.mouse_y < j1 + 4)
					&& (this.anInt668 == 1)) {
				this.priv_block = (1 - this.priv_block);
				flag1 = true;
			}
			j1 += 15;
			if ((this.mouse_x > l) && (this.mouse_x < l + c1)
					&& (this.mouse_y > j1 - 12) && (this.mouse_y < j1 + 4)
					&& (this.anInt668 == 1)) {
				this.trade_block = (1 - this.trade_block);
				flag1 = true;
			}
			j1 += 15;
			if ((this.is_members) && (this.mouse_x > l)
					&& (this.mouse_x < l + c1) && (this.mouse_y > j1 - 12)
					&& (this.mouse_y < j1 + 4) && (this.anInt668 == 1)) {
				this.duel_block = (1 - this.duel_block);
				flag1 = true;
			}
			j1 += 15;
			if (flag1)
				method34(this.pub_block, this.priv_block, this.trade_block,
						this.duel_block);
			j1 += 20;
			if ((this.mouse_x > l) && (this.mouse_x < l + c1)
					&& (this.mouse_y > j1 - 12) && (this.mouse_y < j1 + 4)
					&& (this.anInt668 == 1))
				logout();
			this.anInt668 = 0;
		}
	}

	public void method123() {
		int i = -1;
		for (int j = 0; j < this.anInt754; j++) {
			this.aBooleanArray761[j] = false;
		}
		for (int k = 0; k < this.anInt763; k++) {
			this.aBooleanArray769[k] = false;
		}
		int l = this.scene.method199();
		Model[] aclass7 = this.scene.picked_models();
		int[] ai = this.scene.picked_faces();
		for (int i1 = 0; (i1 < l) && (this.anInt810 <= 200); i1++) {
			int j1 = ai[i1];
			Model class7 = aclass7[i1];
			if ((class7.face_pick_tag[j1] <= 65535)
					|| ((class7.face_pick_tag[j1] >= 200000) && (class7.face_pick_tag[j1] <= 300000))) {
				if (class7 == this.scene.view) {
					int l1 = class7.face_pick_tag[j1] % 10000;
					int k2 = class7.face_pick_tag[j1] / 10000;
					if (k2 == 1) {
						String s = "";
						int j3 = 0;
						if ((this.local_player.anInt612 > 0)
								&& (this.local_players[l1].anInt612 > 0))
							j3 = this.local_player.anInt612
									- this.local_players[l1].anInt612;
						if (j3 < 0)
							s = "@or1@";
						if (j3 < -3)
							s = "@or2@";
						if (j3 < -6)
							s = "@or3@";
						if (j3 < -9)
							s = "@red@";
						if (j3 > 0)
							s = "@gr1@";
						if (j3 > 3)
							s = "@gr2@";
						if (j3 > 6)
							s = "@gr3@";
						if (j3 > 9)
							s = "@gre@";
						s = new StringBuilder().append(" ").append(s)
								.append("(level-")
								.append(this.local_players[l1].anInt612)
								.append(")").toString();
						if (this.anInt789 >= 0) {
							if ((Config.anIntArray571[this.anInt789] == 1)
									|| ((Config.anIntArray571[this.anInt789] == 2) && (this.anInt737 < 2203))) {
								this.aStringArray813[this.anInt810] = new StringBuilder()
										.append("Cast ")
										.append(Config.aStringArray567[this.anInt789])
										.append(" on").toString();
								this.aStringArray812[this.anInt810] = new StringBuilder()
										.append("@whi@")
										.append(this.local_players[l1].aString590)
										.toString();
								this.anIntArray814[this.anInt810] = 800;
								this.anIntArray815[this.anInt810] = this.local_players[l1].cur_x;
								this.anIntArray816[this.anInt810] = this.local_players[l1].cur_y;
								this.anIntArray817[this.anInt810] = this.local_players[l1].idx;
								this.anIntArray818[this.anInt810] = this.anInt789;
								this.anInt810 += 1;
							}
						} else if (this.anInt776 >= 0) {
							this.aStringArray813[this.anInt810] = new StringBuilder()
									.append("Use ").append(this.aString777)
									.append(" with").toString();
							this.aStringArray812[this.anInt810] = new StringBuilder()
									.append("@whi@")
									.append(this.local_players[l1].aString590)
									.toString();
							this.anIntArray814[this.anInt810] = 810;
							this.anIntArray815[this.anInt810] = this.local_players[l1].cur_x;
							this.anIntArray816[this.anInt810] = this.local_players[l1].cur_y;
							this.anIntArray817[this.anInt810] = this.local_players[l1].idx;
							this.anIntArray818[this.anInt810] = this.anInt776;
							this.anInt810 += 1;
						} else {
							if ((this.anInt737 + this.anInt706 + this.anInt710 < 2203)
									&& ((this.local_players[l1].cur_y - 64)
											/ this.anInt684 + this.anInt706
											+ this.anInt710 < 2203)) {
								this.aStringArray813[this.anInt810] = "Attack";
								this.aStringArray812[this.anInt810] = new StringBuilder()
										.append("@whi@")
										.append(this.local_players[l1].aString590)
										.append(s).toString();
								if ((j3 >= 0) && (j3 < 5))
									this.anIntArray814[this.anInt810] = 805;
								else
									this.anIntArray814[this.anInt810] = 2805;
								this.anIntArray815[this.anInt810] = this.local_players[l1].cur_x;
								this.anIntArray816[this.anInt810] = this.local_players[l1].cur_y;
								this.anIntArray817[this.anInt810] = this.local_players[l1].idx;
								this.anInt810 += 1;
							} else if (this.is_members) {
								this.aStringArray813[this.anInt810] = "Duel with";
								this.aStringArray812[this.anInt810] = new StringBuilder()
										.append("@whi@")
										.append(this.local_players[l1].aString590)
										.append(s).toString();
								this.anIntArray815[this.anInt810] = this.local_players[l1].cur_x;
								this.anIntArray816[this.anInt810] = this.local_players[l1].cur_y;
								this.anIntArray814[this.anInt810] = 2806;
								this.anIntArray817[this.anInt810] = this.local_players[l1].idx;
								this.anInt810 += 1;
							}
							this.aStringArray813[this.anInt810] = "Trade with";
							this.aStringArray812[this.anInt810] = new StringBuilder()
									.append("@whi@")
									.append(this.local_players[l1].aString590)
									.toString();
							this.anIntArray814[this.anInt810] = 2810;
							this.anIntArray817[this.anInt810] = this.local_players[l1].idx;
							this.anInt810 += 1;
							this.aStringArray813[this.anInt810] = "Follow";
							this.aStringArray812[this.anInt810] = new StringBuilder()
									.append("@whi@")
									.append(this.local_players[l1].aString590)
									.toString();
							this.anIntArray814[this.anInt810] = 2820;
							this.anIntArray817[this.anInt810] = this.local_players[l1].idx;
							this.anInt810 += 1;
						}
					} else if (k2 == 2) {
						if (this.anInt789 >= 0) {
							if (Config.anIntArray571[this.anInt789] == 3) {
								this.aStringArray813[this.anInt810] = new StringBuilder()
										.append("Cast ")
										.append(Config.aStringArray567[this.anInt789])
										.append(" on").toString();
								this.aStringArray812[this.anInt810] = new StringBuilder()
										.append("@lre@")
										.append(Config.item_name[this.anIntArray751[l1]])
										.toString();
								this.anIntArray814[this.anInt810] = 200;
								this.anIntArray815[this.anInt810] = this.anIntArray749[l1];
								this.anIntArray816[this.anInt810] = this.anIntArray750[l1];
								this.anIntArray817[this.anInt810] = this.anIntArray751[l1];
								this.anIntArray818[this.anInt810] = this.anInt789;
								this.anInt810 += 1;
							}
						} else if (this.anInt776 >= 0) {
							this.aStringArray813[this.anInt810] = new StringBuilder()
									.append("Use ").append(this.aString777)
									.append(" with").toString();
							this.aStringArray812[this.anInt810] = new StringBuilder()
									.append("@lre@")
									.append(Config.item_name[this.anIntArray751[l1]])
									.toString();
							this.anIntArray814[this.anInt810] = 210;
							this.anIntArray815[this.anInt810] = this.anIntArray749[l1];
							this.anIntArray816[this.anInt810] = this.anIntArray750[l1];
							this.anIntArray817[this.anInt810] = this.anIntArray751[l1];
							this.anIntArray818[this.anInt810] = this.anInt776;
							this.anInt810 += 1;
						} else {
							this.aStringArray813[this.anInt810] = "Take";
							this.aStringArray812[this.anInt810] = new StringBuilder()
									.append("@lre@")
									.append(Config.item_name[this.anIntArray751[l1]])
									.toString();
							this.anIntArray814[this.anInt810] = 220;
							this.anIntArray815[this.anInt810] = this.anIntArray749[l1];
							this.anIntArray816[this.anInt810] = this.anIntArray750[l1];
							this.anIntArray817[this.anInt810] = this.anIntArray751[l1];
							this.anInt810 += 1;
							this.aStringArray813[this.anInt810] = "Examine";
							this.aStringArray812[this.anInt810] = new StringBuilder()
									.append("@lre@")
									.append(Config.item_name[this.anIntArray751[l1]])
									.toString();
							this.anIntArray814[this.anInt810] = 3200;
							this.anIntArray817[this.anInt810] = this.anIntArray751[l1];
							this.anInt810 += 1;
						}
					} else if (k2 == 3) {
						String s1 = "";
						int k3 = -1;
						int l3 = this.aClass14Array744[l1].anInt595;
						if (Config.npc_attackable[l3] > 0) {
							int i4 = (Config.npc_atk_lvl[l3]
									+ Config.npc_hp_lvl[l3]
									+ Config.npc_def_lvl[l3] + Config.npc_str_lvl[l3]) / 4;
							int j4 = (this.anIntArray780[0]
									+ this.anIntArray780[1]
									+ this.anIntArray780[2]
									+ this.anIntArray780[3] + 27) / 4;
							k3 = j4 - i4;
							s1 = "@yel@";
							if (k3 < 0)
								s1 = "@or1@";
							if (k3 < -3)
								s1 = "@or2@";
							if (k3 < -6)
								s1 = "@or3@";
							if (k3 < -9)
								s1 = "@red@";
							if (k3 > 0)
								s1 = "@gr1@";
							if (k3 > 3)
								s1 = "@gr2@";
							if (k3 > 6)
								s1 = "@gr3@";
							if (k3 > 9)
								s1 = "@gre@";
							s1 = new StringBuilder().append(" ").append(s1)
									.append("(level-").append(i4).append(")")
									.toString();
						}
						if (this.anInt789 >= 0) {
							if (Config.anIntArray571[this.anInt789] == 2) {
								this.aStringArray813[this.anInt810] = new StringBuilder()
										.append("Cast ")
										.append(Config.aStringArray567[this.anInt789])
										.append(" on").toString();
								this.aStringArray812[this.anInt810] = new StringBuilder()
										.append("@yel@")
										.append(Config.npc_name[this.aClass14Array744[l1].anInt595])
										.toString();
								this.anIntArray814[this.anInt810] = 700;
								this.anIntArray815[this.anInt810] = this.aClass14Array744[l1].cur_x;
								this.anIntArray816[this.anInt810] = this.aClass14Array744[l1].cur_y;
								this.anIntArray817[this.anInt810] = this.aClass14Array744[l1].idx;
								this.anIntArray818[this.anInt810] = this.anInt789;
								this.anInt810 += 1;
							}
						} else if (this.anInt776 >= 0) {
							this.aStringArray813[this.anInt810] = new StringBuilder()
									.append("Use ").append(this.aString777)
									.append(" with").toString();
							this.aStringArray812[this.anInt810] = new StringBuilder()
									.append("@yel@")
									.append(Config.npc_name[this.aClass14Array744[l1].anInt595])
									.toString();
							this.anIntArray814[this.anInt810] = 710;
							this.anIntArray815[this.anInt810] = this.aClass14Array744[l1].cur_x;
							this.anIntArray816[this.anInt810] = this.aClass14Array744[l1].cur_y;
							this.anIntArray817[this.anInt810] = this.aClass14Array744[l1].idx;
							this.anIntArray818[this.anInt810] = this.anInt776;
							this.anInt810 += 1;
						} else {
							if (Config.npc_attackable[l3] > 0) {
								this.aStringArray813[this.anInt810] = "Attack";
								this.aStringArray812[this.anInt810] = new StringBuilder()
										.append("@yel@")
										.append(Config.npc_name[this.aClass14Array744[l1].anInt595])
										.append(s1).toString();
								if (k3 >= 0)
									this.anIntArray814[this.anInt810] = 715;
								else
									this.anIntArray814[this.anInt810] = 2715;
								this.anIntArray815[this.anInt810] = this.aClass14Array744[l1].cur_x;
								this.anIntArray816[this.anInt810] = this.aClass14Array744[l1].cur_y;
								this.anIntArray817[this.anInt810] = this.aClass14Array744[l1].idx;
								this.anInt810 += 1;
							}
							this.aStringArray813[this.anInt810] = "Talk-to";
							this.aStringArray812[this.anInt810] = new StringBuilder()
									.append("@yel@")
									.append(Config.npc_name[this.aClass14Array744[l1].anInt595])
									.toString();
							this.anIntArray814[this.anInt810] = 720;
							this.anIntArray815[this.anInt810] = this.aClass14Array744[l1].cur_x;
							this.anIntArray816[this.anInt810] = this.aClass14Array744[l1].cur_y;
							this.anIntArray817[this.anInt810] = this.aClass14Array744[l1].idx;
							this.anInt810 += 1;
							this.aStringArray813[this.anInt810] = "Examine";
							this.aStringArray812[this.anInt810] = new StringBuilder()
									.append("@yel@")
									.append(Config.npc_name[this.aClass14Array744[l1].anInt595])
									.toString();
							this.anIntArray814[this.anInt810] = 3700;
							this.anIntArray817[this.anInt810] = this.aClass14Array744[l1].anInt595;
							this.anInt810 += 1;
						}
					}
				} else if ((class7 != null) && (class7.anInt323 >= 10000)) {
					int i2 = class7.anInt323 - 10000;
					int l2 = this.anIntArray768[i2];
					if (this.aBooleanArray769[i2]) {
						if (this.anInt789 >= 0) {
							if (Config.anIntArray571[this.anInt789] == 4) {
								this.aStringArray813[this.anInt810] = new StringBuilder()
										.append("Cast ")
										.append(Config.aStringArray567[this.anInt789])
										.append(" on").toString();
								this.aStringArray812[this.anInt810] = new StringBuilder()
										.append("@cya@")
										.append(Config.aStringArray549[l2])
										.toString();
								this.anIntArray814[this.anInt810] = 300;
								this.anIntArray815[this.anInt810] = this.wall_x[i2];
								this.anIntArray816[this.anInt810] = this.wall_y[i2];
								this.anIntArray817[this.anInt810] = this.anIntArray767[i2];
								this.anIntArray818[this.anInt810] = this.anInt789;
								this.anInt810 += 1;
							}
						} else if (this.anInt776 >= 0) {
							this.aStringArray813[this.anInt810] = new StringBuilder()
									.append("Use ").append(this.aString777)
									.append(" with").toString();
							this.aStringArray812[this.anInt810] = new StringBuilder()
									.append("@cya@")
									.append(Config.aStringArray549[l2])
									.toString();
							this.anIntArray814[this.anInt810] = 310;
							this.anIntArray815[this.anInt810] = this.wall_x[i2];
							this.anIntArray816[this.anInt810] = this.wall_y[i2];
							this.anIntArray817[this.anInt810] = this.anIntArray767[i2];
							this.anIntArray818[this.anInt810] = this.anInt776;
							this.anInt810 += 1;
						} else {
							if (!Config.aStringArray551[l2]
									.equalsIgnoreCase("WalkTo")) {
								this.aStringArray813[this.anInt810] = Config.aStringArray551[l2];
								this.aStringArray812[this.anInt810] = new StringBuilder()
										.append("@cya@")
										.append(Config.aStringArray549[l2])
										.toString();
								this.anIntArray814[this.anInt810] = 320;
								this.anIntArray815[this.anInt810] = this.wall_x[i2];
								this.anIntArray816[this.anInt810] = this.wall_y[i2];
								this.anIntArray817[this.anInt810] = this.anIntArray767[i2];
								this.anInt810 += 1;
							}
							if (!Config.aStringArray552[l2]
									.equalsIgnoreCase("Examine")) {
								this.aStringArray813[this.anInt810] = Config.aStringArray552[l2];
								this.aStringArray812[this.anInt810] = new StringBuilder()
										.append("@cya@")
										.append(Config.aStringArray549[l2])
										.toString();
								this.anIntArray814[this.anInt810] = 2300;
								this.anIntArray815[this.anInt810] = this.wall_x[i2];
								this.anIntArray816[this.anInt810] = this.wall_y[i2];
								this.anIntArray817[this.anInt810] = this.anIntArray767[i2];
								this.anInt810 += 1;
							}
							this.aStringArray813[this.anInt810] = "Examine";
							this.aStringArray812[this.anInt810] = new StringBuilder()
									.append("@cya@")
									.append(Config.aStringArray549[l2])
									.toString();
							this.anIntArray814[this.anInt810] = 3300;
							this.anIntArray817[this.anInt810] = l2;
							this.anInt810 += 1;
						}
						this.aBooleanArray769[i2] = true;
					}
				} else if ((class7 != null) && (class7.anInt323 >= 0)) {
					int j2 = class7.anInt323;
					int i3 = this.obj_orient[j2];
					if (this.aBooleanArray761[j2]) {
						if (this.anInt789 >= 0) {
							if (Config.anIntArray571[this.anInt789] == 5) {
								this.aStringArray813[this.anInt810] = new StringBuilder()
										.append("Cast ")
										.append(Config.aStringArray567[this.anInt789])
										.append(" on").toString();
								this.aStringArray812[this.anInt810] = new StringBuilder()
										.append("@cya@")
										.append(Config.aStringArray539[i3])
										.toString();
								this.anIntArray814[this.anInt810] = 400;
								this.anIntArray815[this.anInt810] = this.obj_x[j2];
								this.anIntArray816[this.anInt810] = this.obj_y[j2];
								this.anIntArray817[this.anInt810] = this.anIntArray759[j2];
								this.anIntArray818[this.anInt810] = this.obj_orient[j2];
								this.anIntArray819[this.anInt810] = this.anInt789;
								this.anInt810 += 1;
							}
						} else if (this.anInt776 >= 0) {
							this.aStringArray813[this.anInt810] = new StringBuilder()
									.append("Use ").append(this.aString777)
									.append(" with").toString();
							this.aStringArray812[this.anInt810] = new StringBuilder()
									.append("@cya@")
									.append(Config.aStringArray539[i3])
									.toString();
							this.anIntArray814[this.anInt810] = 410;
							this.anIntArray815[this.anInt810] = this.obj_x[j2];
							this.anIntArray816[this.anInt810] = this.obj_y[j2];
							this.anIntArray817[this.anInt810] = this.anIntArray759[j2];
							this.anIntArray818[this.anInt810] = this.obj_orient[j2];
							this.anIntArray819[this.anInt810] = this.anInt776;
							this.anInt810 += 1;
						} else {
							if (!Config.aStringArray541[i3]
									.equalsIgnoreCase("WalkTo")) {
								this.aStringArray813[this.anInt810] = Config.aStringArray541[i3];
								this.aStringArray812[this.anInt810] = new StringBuilder()
										.append("@cya@")
										.append(Config.aStringArray539[i3])
										.toString();
								this.anIntArray814[this.anInt810] = 420;
								this.anIntArray815[this.anInt810] = this.obj_x[j2];
								this.anIntArray816[this.anInt810] = this.obj_y[j2];
								this.anIntArray817[this.anInt810] = this.anIntArray759[j2];
								this.anIntArray818[this.anInt810] = this.obj_orient[j2];
								this.anInt810 += 1;
							}
							if (!Config.aStringArray542[i3]
									.equalsIgnoreCase("Examine")) {
								this.aStringArray813[this.anInt810] = Config.aStringArray542[i3];
								this.aStringArray812[this.anInt810] = new StringBuilder()
										.append("@cya@")
										.append(Config.aStringArray539[i3])
										.toString();
								this.anIntArray814[this.anInt810] = 2400;
								this.anIntArray815[this.anInt810] = this.obj_x[j2];
								this.anIntArray816[this.anInt810] = this.obj_y[j2];
								this.anIntArray817[this.anInt810] = this.anIntArray759[j2];
								this.anIntArray818[this.anInt810] = this.obj_orient[j2];
								this.anInt810 += 1;
							}
							this.aStringArray813[this.anInt810] = "Examine";
							this.aStringArray812[this.anInt810] = new StringBuilder()
									.append("@cya@")
									.append(Config.aStringArray539[i3])
									.toString();
							this.anIntArray814[this.anInt810] = 3400;
							this.anIntArray817[this.anInt810] = i3;
							this.anInt810 += 1;
						}
						this.aBooleanArray761[j2] = true;
					}
				} else {
					if (j1 >= 0)
						j1 = class7.face_pick_tag[j1] - 200000;
					if (j1 >= 0)
						i = j1;
				}
			}
		}
		if ((this.anInt789 >= 0) && (Config.anIntArray571[this.anInt789] <= 1)) {
			this.aStringArray813[this.anInt810] = new StringBuilder()
					.append("Cast ")
					.append(Config.aStringArray567[this.anInt789])
					.append(" on self").toString();
			this.aStringArray812[this.anInt810] = "";
			this.anIntArray814[this.anInt810] = 1000;
			this.anIntArray817[this.anInt810] = this.anInt789;
			this.anInt810 += 1;
		}
		if (i != -1) {
			int k1 = i;
			if (this.anInt789 >= 0) {
				if (Config.anIntArray571[this.anInt789] == 6) {
					this.aStringArray813[this.anInt810] = new StringBuilder()
							.append("Cast ")
							.append(Config.aStringArray567[this.anInt789])
							.append(" on ground").toString();
					this.aStringArray812[this.anInt810] = "";
					this.anIntArray814[this.anInt810] = 900;
					this.anIntArray815[this.anInt810] = this.terrain.anIntArray78[k1];
					this.anIntArray816[this.anInt810] = this.terrain.anIntArray79[k1];
					this.anIntArray817[this.anInt810] = this.anInt789;
					this.anInt810 += 1;
					return;
				}
			} else if (this.anInt776 < 0) {
				this.aStringArray813[this.anInt810] = "Walk here";
				this.aStringArray812[this.anInt810] = "";
				this.anIntArray814[this.anInt810] = 920;
				this.anIntArray815[this.anInt810] = this.terrain.anIntArray78[k1];
				this.anIntArray816[this.anInt810] = this.terrain.anIntArray79[k1];
				this.anInt810 += 1;
			}
		}
	}

	public void method124() {
		if (this.anInt668 != 0) {
			for (int i = 0; i < this.anInt810; i++) {
				int k = this.anInt806 + 2;
				int i1 = this.anInt807 + 27 + i * 15;
				if ((this.mouse_x <= k - 2) || (this.mouse_y <= i1 - 12)
						|| (this.mouse_y >= i1 + 4)
						|| (this.mouse_x >= k - 3 + this.anInt808))
					continue;
				method126(this.anIntArray820[i]);
				break;
			}

			this.anInt668 = 0;
			this.aBoolean805 = false;
			return;
		}
		if ((this.mouse_x < this.anInt806 - 10)
				|| (this.mouse_y < this.anInt807 - 10)
				|| (this.mouse_x > this.anInt806 + this.anInt808 + 10)
				|| (this.mouse_y > this.anInt807 + this.anInt809 + 10)) {
			this.aBoolean805 = false;
			return;
		}
		this.screen.rect_fill(this.anInt806, this.anInt807, this.anInt808,
				this.anInt809, 13684944, 160);
		this.screen.text_draw("Choose option", this.anInt806 + 2,
				this.anInt807 + 12, 1, 65535);
		for (int j = 0; j < this.anInt810; j++) {
			int l = this.anInt806 + 2;
			int j1 = this.anInt807 + 27 + j * 15;
			int k1 = 16777215;
			if ((this.mouse_x > l - 2) && (this.mouse_y > j1 - 12)
					&& (this.mouse_y < j1 + 4)
					&& (this.mouse_x < l - 3 + this.anInt808))
				k1 = 16776960;
			this.screen
					.text_draw(
							new StringBuilder()
									.append(this.aStringArray813[this.anIntArray820[j]])
									.append(" ")
									.append(this.aStringArray812[this.anIntArray820[j]])
									.toString(), l, j1, 1, k1);
		}
	}

	public void method125() {
		if ((this.anInt789 >= 0) || (this.anInt776 >= 0)) {
			this.aStringArray813[this.anInt810] = "Cancel";
			this.aStringArray812[this.anInt810] = "";
			this.anIntArray814[this.anInt810] = 4000;
			this.anInt810 += 1;
		}
		for (int i = 0; i < this.anInt810; i++) {
			this.anIntArray820[i] = i;
		}
		for (boolean flag = false; !flag;) {
			flag = true;
			for (int j = 0; j < this.anInt810 - 1; j++) {
				int l = this.anIntArray820[j];
				int j1 = this.anIntArray820[(j + 1)];
				if (this.anIntArray814[l] > this.anIntArray814[j1]) {
					this.anIntArray820[j] = j1;
					this.anIntArray820[(j + 1)] = l;
					flag = false;
				}
			}

		}

		if (this.anInt810 > 20)
			this.anInt810 = 20;
		if (this.anInt810 > 0) {
			int k = -1;
			for (int i1 = 0; i1 < this.anInt810; i1++) {
				if ((this.aStringArray812[this.anIntArray820[i1]] == null)
						|| (this.aStringArray812[this.anIntArray820[i1]]
								.length() <= 0))
					continue;
				k = i1;
				break;
			}

			String s = null;
			if (((this.anInt776 >= 0) || (this.anInt789 >= 0))
					&& (this.anInt810 == 1))
				s = "Choose a target";
			else if (((this.anInt776 >= 0) || (this.anInt789 >= 0))
					&& (this.anInt810 > 1))
				s = new StringBuilder().append("@whi@")
						.append(this.aStringArray813[this.anIntArray820[0]])
						.append(" ")
						.append(this.aStringArray812[this.anIntArray820[0]])
						.toString();
			else if (k != -1)
				s = new StringBuilder()
						.append(this.aStringArray812[this.anIntArray820[k]])
						.append(": @whi@")
						.append(this.aStringArray813[this.anIntArray820[0]])
						.toString();
			if ((this.anInt810 == 2) && (s != null))
				s = new StringBuilder().append(s)
						.append("@whi@ / 1 more option").toString();
			if ((this.anInt810 > 2) && (s != null))
				s = new StringBuilder().append(s).append("@whi@ / ")
						.append(this.anInt810 - 1).append(" more options")
						.toString();
			if (s != null)
				this.screen.text_draw(s, 6, 14, 1, 16776960);
			if (((!this.single_button_mouse) && (this.anInt668 == 1))
					|| ((this.single_button_mouse) && (this.anInt668 == 1) && (this.anInt810 == 1))) {
				method126(this.anIntArray820[0]);
				this.anInt668 = 0;
				return;
			}
			if (((!this.single_button_mouse) && (this.anInt668 == 2))
					|| ((this.single_button_mouse) && (this.anInt668 == 1))) {
				this.anInt809 = ((this.anInt810 + 1) * 15);
				this.anInt808 = (Surface.text_width("Choose option", 1) + 5);
				for (int k1 = 0; k1 < this.anInt810; k1++) {
					int l1 = Surface.text_width(
							new StringBuilder()
									.append(this.aStringArray813[k1])
									.append(" ")
									.append(this.aStringArray812[k1])
									.toString(), 1) + 5;
					if (l1 > this.anInt808) {
						this.anInt808 = l1;
					}
				}
				this.anInt806 = (this.mouse_x - this.anInt808 / 2);
				this.anInt807 = (this.mouse_y - 7);
				this.aBoolean805 = true;
				if (this.anInt806 < 0)
					this.anInt806 = 0;
				if (this.anInt807 < 0)
					this.anInt807 = 0;
				if (this.anInt806 + this.anInt808 > 510)
					this.anInt806 = (510 - this.anInt808);
				if (this.anInt807 + this.anInt809 > 315)
					this.anInt807 = (315 - this.anInt809);
				this.anInt668 = 0;
			}
		}
	}

	public void method126(int i) {
		int j = this.anIntArray815[i];
		int k = this.anIntArray816[i];
		int l = this.anIntArray817[i];
		int i1 = this.anIntArray818[i];
		int j1 = this.anIntArray819[i];
		int k1 = this.anIntArray814[i];
		if (k1 == 200) {
			method95(this.anInt736, this.anInt737, j, k, true);
			this.conn.enter(224);
			this.conn.short_put(j + this.anInt709);
			this.conn.short_put(k + this.anInt710);
			this.conn.short_put(l);
			this.conn.short_put(i1);
			this.conn.exit();
			this.anInt789 = -1;
		}
		if (k1 == 210) {
			method95(this.anInt736, this.anInt737, j, k, true);
			this.conn.enter(250);
			this.conn.short_put(j + this.anInt709);
			this.conn.short_put(k + this.anInt710);
			this.conn.short_put(l);
			this.conn.short_put(i1);
			this.conn.exit();
			this.anInt776 = -1;
		}
		if (k1 == 220) {
			method95(this.anInt736, this.anInt737, j, k, true);
			this.conn.enter(252);
			this.conn.short_put(j + this.anInt709);
			this.conn.short_put(k + this.anInt710);
			this.conn.short_put(l);
			this.conn.exit();
		}
		if (k1 == 3200)
			method77(Config.item_desc[l], 3);
		if (k1 == 300) {
			method97(j, k, l);
			this.conn.enter(223);
			this.conn.short_put(j + this.anInt709);
			this.conn.short_put(k + this.anInt710);
			this.conn.byte_put(l);
			this.conn.short_put(i1);
			this.conn.exit();
			this.anInt789 = -1;
		}
		if (k1 == 310) {
			method97(j, k, l);
			this.conn.enter(239);
			this.conn.short_put(j + this.anInt709);
			this.conn.short_put(k + this.anInt710);
			this.conn.byte_put(l);
			this.conn.short_put(i1);
			this.conn.exit();
			this.anInt776 = -1;
		}
		if (k1 == 320) {
			method97(j, k, l);
			this.conn.enter(238);
			this.conn.short_put(j + this.anInt709);
			this.conn.short_put(k + this.anInt710);
			this.conn.byte_put(l);
			this.conn.exit();
		}
		if (k1 == 2300) {
			method97(j, k, l);
			this.conn.enter(229);
			this.conn.short_put(j + this.anInt709);
			this.conn.short_put(k + this.anInt710);
			this.conn.byte_put(l);
			this.conn.exit();
		}
		if (k1 == 3300)
			method77(Config.aStringArray550[l], 3);
		if (k1 == 400) {
			method96(j, k, l, i1);
			this.conn.enter(222);
			this.conn.short_put(j + this.anInt709);
			this.conn.short_put(k + this.anInt710);
			this.conn.short_put(j1);
			this.conn.exit();
			this.anInt789 = -1;
		}
		if (k1 == 410) {
			method96(j, k, l, i1);
			this.conn.enter(241);
			this.conn.short_put(j + this.anInt709);
			this.conn.short_put(k + this.anInt710);
			this.conn.short_put(j1);
			this.conn.exit();
			this.anInt776 = -1;
		}
		if (k1 == 420) {
			method96(j, k, l, i1);
			this.conn.enter(242);
			this.conn.short_put(j + this.anInt709);
			this.conn.short_put(k + this.anInt710);
			this.conn.exit();
		}
		if (k1 == 2400) {
			method96(j, k, l, i1);
			this.conn.enter(230);
			this.conn.short_put(j + this.anInt709);
			this.conn.short_put(k + this.anInt710);
			this.conn.exit();
		}
		if (k1 == 3400)
			method77(Config.aStringArray540[l], 3);
		if (k1 == 600) {
			this.conn.enter(220);
			this.conn.short_put(l);
			this.conn.short_put(i1);
			this.conn.exit();
			this.anInt789 = -1;
		}
		if (k1 == 610) {
			this.conn.enter(240);
			this.conn.short_put(l);
			this.conn.short_put(i1);
			this.conn.exit();
			this.anInt776 = -1;
		}
		if (k1 == 620) {
			this.conn.enter(248);
			this.conn.short_put(l);
			this.conn.exit();
		}
		if (k1 == 630) {
			this.conn.enter(249);
			this.conn.short_put(l);
			this.conn.exit();
		}
		if (k1 == 640) {
			this.conn.enter(246);
			this.conn.short_put(l);
			this.conn.exit();
		}
		if (k1 == 650) {
			this.anInt776 = l;
			this.anInt770 = 0;
			this.aString777 = Config.item_name[this.anIntArray773[this.anInt776]];
		}
		if (k1 == 660) {
			this.conn.enter(251);
			this.conn.short_put(l);
			this.conn.exit();
			this.anInt776 = -1;
			this.anInt770 = 0;
			method77(
					new StringBuilder().append("Dropping ")
							.append(Config.item_name[this.anIntArray773[l]])
							.toString(), 4);
		}
		if (k1 == 3600)
			method77(Config.item_desc[l], 3);
		if (k1 == 700) {
			int l1 = (j - 64) / this.anInt684;
			int k3 = (k - 64) / this.anInt684;
			method94(this.anInt736, this.anInt737, l1, k3, true);
			this.conn.enter(225);
			this.conn.short_put(l);
			this.conn.short_put(i1);
			this.conn.exit();
			this.anInt789 = -1;
		}
		if (k1 == 710) {
			int i2 = (j - 64) / this.anInt684;
			int l3 = (k - 64) / this.anInt684;
			method94(this.anInt736, this.anInt737, i2, l3, true);
			this.conn.enter(243);
			this.conn.short_put(l);
			this.conn.short_put(i1);
			this.conn.exit();
			this.anInt776 = -1;
		}
		if (k1 == 720) {
			int j2 = (j - 64) / this.anInt684;
			int i4 = (k - 64) / this.anInt684;
			method94(this.anInt736, this.anInt737, j2, i4, true);
			this.conn.enter(245);
			this.conn.short_put(l);
			this.conn.exit();
		}
		if ((k1 == 715) || (k1 == 2715)) {
			int k2 = (j - 64) / this.anInt684;
			int j4 = (k - 64) / this.anInt684;
			method94(this.anInt736, this.anInt737, k2, j4, true);
			this.conn.enter(244);
			this.conn.short_put(l);
			this.conn.exit();
		}
		if (k1 == 3700)
			method77(Config.npc_actions[l], 3);
		if (k1 == 800) {
			int l2 = (j - 64) / this.anInt684;
			int k4 = (k - 64) / this.anInt684;
			method94(this.anInt736, this.anInt737, l2, k4, true);
			this.conn.enter(226);
			this.conn.short_put(l);
			this.conn.short_put(i1);
			this.conn.exit();
			this.anInt789 = -1;
		}
		if (k1 == 810) {
			int i3 = (j - 64) / this.anInt684;
			int l4 = (k - 64) / this.anInt684;
			method94(this.anInt736, this.anInt737, i3, l4, true);
			this.conn.enter(219);
			this.conn.short_put(l);
			this.conn.short_put(i1);
			this.conn.exit();
			this.anInt776 = -1;
		}
		if ((k1 == 805) || (k1 == 2805)) {
			int j3 = (j - 64) / this.anInt684;
			int i5 = (k - 64) / this.anInt684;
			method94(this.anInt736, this.anInt737, j3, i5, true);
			this.conn.enter(228);
			this.conn.short_put(l);
			this.conn.exit();
		}
		if (k1 == 2806) {
			this.conn.enter(204);
			this.conn.short_put(l);
			this.conn.exit();
		}
		if (k1 == 2810) {
			this.conn.enter(235);
			this.conn.short_put(l);
			this.conn.exit();
		}
		if (k1 == 2820) {
			this.conn.enter(214);
			this.conn.short_put(l);
			this.conn.exit();
		}
		if (k1 == 900) {
			method94(this.anInt736, this.anInt737, j, k, true);
			this.conn.enter(221);
			this.conn.short_put(j + this.anInt709);
			this.conn.short_put(k + this.anInt710);
			this.conn.short_put(l);
			this.conn.exit();
			this.anInt789 = -1;
		}
		if (k1 == 920) {
			method94(this.anInt736, this.anInt737, j, k, false);
			if (this.anInt701 == -24)
				this.anInt701 = 24;
		}
		if (k1 == 1000) {
			this.conn.enter(227);
			this.conn.short_put(l);
			this.conn.exit();
			this.anInt789 = -1;
		}
		if (k1 == 4000) {
			this.anInt776 = -1;
			this.anInt789 = -1;
		}
	}

	public Client() {
		this.is_members = false;
		this.RSA_EXP = new BigInteger("18439792161837834709");
		this.RSA_MOD = new BigInteger("192956484481579778191558061814292671521");
		this.disallowed = false;
		this.outofmem = false;
		this.normal_user = true;
		this.anInt667 = 12345678;
		this.anInt669 = 8000;
		this.anIntArray670 = new int[this.anInt669];
		this.anIntArray671 = new int[this.anInt669];
		this.anIntArray673 = new int[8192];
		this.anIntArray674 = new int[8192];
		this.anInt676 = 2;
		this.anInt678 = 2;
		this.anInt684 = 128;
		this.wnd_w = 512;
		this.wnd_h = 334;
		this.view_dist = 9;
		this.anInt695 = 40;
		this.anInt699 = -1;
		this.anInt700 = -1;
		this.anInt708 = -1;
		this.anInt711 = -1;
		this.anInt719 = 550;
		this.aBoolean720 = false;
		this.anInt723 = 1;
		this.pitch = 128;
		this.anInt727 = 4000;
		this.anInt728 = 500;
		this.local_npcs = new Mob[this.anInt727];
		this.local_players = new Mob[this.anInt728];
		this.aClass14Array734 = new Mob[this.anInt728];
		this.local_player = new Mob();
		this.anInt738 = -1;
		this.anInt739 = 1500;
		this.anInt740 = 500;
		this.aClass14Array743 = new Mob[this.anInt739];
		this.aClass14Array744 = new Mob[this.anInt740];
		this.aClass14Array745 = new Mob[this.anInt740];
		this.anIntArray746 = new int[500];
		this.anInt747 = 500;
		this.anIntArray749 = new int[this.anInt747];
		this.anIntArray750 = new int[this.anInt747];
		this.anIntArray751 = new int[this.anInt747];
		this.anIntArray752 = new int[this.anInt747];
		this.anInt753 = 1500;
		this.obj_models = new Model[this.anInt753];
		this.obj_x = new int[this.anInt753];
		this.obj_y = new int[this.anInt753];
		this.obj_orient = new int[this.anInt753];
		this.anIntArray759 = new int[this.anInt753];
		this.models = new Model['È'];
		this.aBooleanArray761 = new boolean[this.anInt753];
		this.anInt762 = 500;
		this.wall_models = new Model[this.anInt762];
		this.wall_x = new int[this.anInt762];
		this.wall_y = new int[this.anInt762];
		this.anIntArray767 = new int[this.anInt762];
		this.anIntArray768 = new int[this.anInt762];
		this.aBooleanArray769 = new boolean[this.anInt762];
		this.anInt771 = 30;
		this.anIntArray773 = new int[35];
		this.anIntArray774 = new int[35];
		this.anIntArray775 = new int[35];
		this.anInt776 = -1;
		this.aString777 = "";
		this.anIntArray778 = new int[99];
		this.anIntArray779 = new int[16];
		this.anIntArray780 = new int[16];
		this.anIntArray781 = new int[16];
		this.anIntArray782 = new int[5];
		this.anInt789 = -1;
		this.anInt797 = 22;
		this.aBooleanArray799 = new boolean[this.anInt797];
		this.prayer_on = new boolean[50];
		this.aBoolean801 = false;
		this.auto_angle = true;
		this.single_button_mouse = false;
		this.sound_effects = false;
		this.aBoolean805 = false;
		this.anInt811 = 250;
		this.aStringArray812 = new String[this.anInt811];
		this.aStringArray813 = new String[this.anInt811];
		this.anIntArray814 = new int[this.anInt811];
		this.anIntArray815 = new int[this.anInt811];
		this.anIntArray816 = new int[this.anInt811];
		this.anIntArray817 = new int[this.anInt811];
		this.anIntArray818 = new int[this.anInt811];
		this.anIntArray819 = new int[this.anInt811];
		this.anIntArray820 = new int[this.anInt811];
		this.anInt831 = 5;
		this.aStringArray832 = new String[this.anInt831];
		this.anIntArray833 = new int[this.anInt831];
		this.aBoolean834 = false;
		this.aString835 = "";
		this.anIntArray837 = new int[8];
		this.anIntArray838 = new int[8];
		this.anIntArray840 = new int[8];
		this.anIntArray841 = new int[8];
		this.aBoolean842 = false;
		this.aBoolean843 = false;
		this.aBoolean844 = false;
		this.aBoolean845 = false;
		this.aBoolean846 = false;
		this.aBoolean847 = false;
		this.aBoolean848 = false;
		this.aBoolean849 = false;
		this.anIntArray852 = new int[8];
		this.anIntArray853 = new int[8];
		this.anIntArray855 = new int[8];
		this.anIntArray856 = new int[8];
		this.aBoolean861 = false;
		this.aString862 = "";
		this.anIntArray864 = new int[14];
		this.anIntArray865 = new int[14];
		this.anIntArray867 = new int[14];
		this.anIntArray868 = new int[14];
		this.aBoolean869 = false;
		this.aBoolean870 = false;
		this.aBoolean874 = false;
		this.aBoolean875 = false;
		this.anIntArray877 = new int[14];
		this.anIntArray878 = new int[14];
		this.anIntArray880 = new int[14];
		this.anIntArray881 = new int[14];
		this.aBoolean882 = false;
		this.anIntArray885 = new int[256];
		this.anIntArray886 = new int[256];
		this.anIntArray887 = new int[256];
		this.anInt888 = -1;
		this.anInt889 = -2;
		this.aBoolean890 = false;
		this.anIntArray892 = new int[256];
		this.anIntArray893 = new int[256];
		this.anIntArray895 = new int[256];
		this.anIntArray896 = new int[256];
		this.anInt897 = -1;
		this.anInt898 = -2;
		this.anInt899 = 48;
		this.aBoolean901 = false;
		this.aStringArray903 = new String[5];
		this.aString907 = "";
		this.aString908 = "";
		this.logged_in = false;
		this.aBoolean910 = false;
		this.aBoolean916 = false;
		this.aString917 = "";
		this.world_loading = false;
		this.aString945 = "";
		this.aString946 = "";
		this.aString947 = "";
		this.aString948 = "";
		this.aBoolean965 = false;
		this.anInt969 = -1;
		this.anIntArray970 = new int[5];
		this.anIntArray971 = new int[5];
		this.anIntArray972 = new int[5];
		this.anIntArray973 = new int[5];
		this.aStringArray975 = new String[5];
		this.aBoolean976 = false;
		this.anIntArray988 = new int[5];
		this.anIntArray989 = new int[5];
		this.aStringArray991 = new String[50];
		this.anIntArray992 = new int[50];
		this.anIntArray993 = new int[50];
		this.anIntArray994 = new int[50];
		this.anIntArray995 = new int[50];
		this.anIntArray997 = new int[50];
		this.anIntArray998 = new int[50];
		this.anIntArray999 = new int[50];
		this.anIntArray1000 = new int[50];
		this.anIntArray1002 = new int[50];
		this.anIntArray1003 = new int[50];
		this.anIntArray1004 = new int[50];
		this.aBoolean1008 = false;
		this.anInt1010 = 1;
		this.anInt1011 = 2;
		this.anInt1012 = 2;
		this.anInt1013 = 8;
		this.anInt1014 = 14;
		this.anInt1016 = 1;
		this.anIntArray1026 = new int[50];
		this.anIntArray1027 = new int[50];
		this.anIntArray1028 = new int[50];
		this.anIntArray1029 = new int[50];
	}

	@Override
	public void preload_draw() {
		//nothing
	}
}