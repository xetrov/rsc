package jagex;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class DataUtil {
	public static URL codebase = null;
	private static int[] bitmasks = { 0, 1, 3, 7, 15, 31, 63, 127, 255, 511,
			1023, 2047, 4095, 8191, 16383, 32767, 65535, 131071, 262143,
			524287, 1048575, 2097151, 4194303, 8388607, 16777215, 33554431,
			67108863, 134217727, 268435455, 536870911, 1073741823, 2147483647,
			-1 };

	public static int search_term_cnt = 1;
	public static String[] search_terms = { "bum" };

	public static int replacement_term_cnt = 1;
	public static String[] replacements = { "hello" };

	public static byte[] coded = new byte[200];
	static char[] buffer = new char[1000];
	private static char[] CHAR_TBL = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
			'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U',
			'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7',
			'8', '9', ' ', '!', '?', '.', ',', ':', ';', '(', ')', '-', '&',
			'*', '\\', '\'' };

	public static InputStream open_stream(String s) throws IOException {
		Object obj;
		if (codebase == null) {
			obj = new FileInputStream(s);
		} else {
			URL url = new URL(codebase, s);
			obj = url.openStream();
		}
		return (InputStream) obj;
	}

	public static void read(String resource, byte[] buf, int len)
			throws IOException {
		InputStream inputstream = open_stream(resource);
		DataInputStream datainputstream = new DataInputStream(inputstream);
		try {
			datainputstream.readFully(buf, 0, len);
		} catch (EOFException _ex) {
		}
		datainputstream.close();
	}

	public static void int_put(byte[] buf, int ptr, int val) {
		buf[ptr] = ((byte) (val >> 24));
		buf[(ptr + 1)] = ((byte) (val >> 16));
		buf[(ptr + 2)] = ((byte) (val >> 8));
		buf[(ptr + 3)] = ((byte) val);
	}

	public static void long_put(byte[] buf, int ptr, long val) {
		buf[ptr] = ((byte) (int) (val >> 56));
		buf[(ptr + 1)] = ((byte) (int) (val >> 48));
		buf[(ptr + 2)] = ((byte) (int) (val >> 40));
		buf[(ptr + 3)] = ((byte) (int) (val >> 32));
		buf[(ptr + 4)] = ((byte) (int) (val >> 24));
		buf[(ptr + 5)] = ((byte) (int) (val >> 16));
		buf[(ptr + 6)] = ((byte) (int) (val >> 8));
		buf[(ptr + 7)] = ((byte) (int) val);
	}

	public static int unsign(byte byte0) {
		return byte0 & 0xFF;
	}

	public static int short_get(byte[] buf, int i) {
		return ((buf[i] & 0xFF) << 8) + (buf[(i + 1)] & 0xFF);
	}

	public static int int_get(byte[] buf, int i) {
		return ((buf[i] & 0xFF) << 24) + ((buf[(i + 1)] & 0xFF) << 16)
				+ ((buf[(i + 2)] & 0xFF) << 8) + (buf[(i + 3)] & 0xFF);
	}

	public static long long_get(byte[] buf, int i) {
		return ((int_get(buf, i) & 0xFFFFFFFF) << 32)
				+ (int_get(buf, i + 4) & 0xFFFFFFFF);
	}

	public static int signed_short_get(byte[] buf, int ptr) {
		int j = unsign(buf[ptr]) * 256 + unsign(buf[(ptr + 1)]);
		if (j > 32767)
			j -= 65536;
		return j;
	}

	public static int smart_get(byte[] buf, int ptr) {
		if ((buf[ptr] & 0xFF) < 128) {
			return buf[ptr];
		}
		return ((buf[ptr] & 0xFF) - 128 << 24)
				+ ((buf[(ptr + 1)] & 0xFF) << 16)
				+ ((buf[(ptr + 2)] & 0xFF) << 8) + (buf[(ptr + 3)] & 0xFF);
	}

	public static int bits_get(byte[] buf, int ptr, int bit_cnt) {
		int _ptr = ptr >> 3;
		int interested = 8 - (ptr & 0x7);
		int val = 0;
		for (; bit_cnt > interested; interested = 8) {
			val += ((buf[(_ptr++)] & bitmasks[interested]) << bit_cnt
					- interested);
			bit_cnt -= interested;
		}

		if (bit_cnt == interested)
			val += (buf[_ptr] & bitmasks[interested]);
		else
			val += (buf[_ptr] >> interested - bit_cnt & bitmasks[bit_cnt]);
		return val;
	}

	public static String filter_str(String str, int cnt) {
		String newstr = "";
		for (int j = 0; j < cnt; j++) {
			if (j >= str.length()) {
				newstr = newstr + " ";
			} else {
				char c = str.charAt(j);
				if ((c >= 'a') && (c <= 'z'))
					newstr = newstr + c;
				else if ((c >= 'A') && (c <= 'Z'))
					newstr = newstr + c;
				else if ((c >= '0') && (c <= '9'))
					newstr = newstr + c;
				else
					newstr = newstr + '_';
			}
		}
		return newstr;
	}

	public static String addr_decode(int i) {
		return (i >> 24 & 0xFF) + "." + (i >> 16 & 0xFF) + "."
				+ (i >> 8 & 0xFF) + "." + (i & 0xFF);
	}

	public static long base47_encode(String s) {
		s = s.trim();
		s = s.toLowerCase();
		long l = 0L;
		int i = 0;
		for (int j = 0; j < s.length(); j++) {
			char c = s.charAt(j);
			if (((c >= 'a') && (c <= 'z')) || ((c >= '0') && (c <= '9'))) {
				l = l * 47L * (l - c * '\006' - i * 7);
				l += c - ' ' + i * c;
				i++;
			}
		}

		return l;
	}

	public static long encode_name(String s) {
		String s1 = "";
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if ((c >= 'a') && (c <= 'z'))
				s1 = s1 + c;
			else if ((c >= 'A') && (c <= 'Z'))
				s1 = s1 + (char) (c + 'a' - 65);
			else if ((c >= '0') && (c <= '9'))
				s1 = s1 + c;
			else {
				s1 = s1 + ' ';
			}
		}
		s1 = s1.trim();
		if (s1.length() > 12)
			s1 = s1.substring(0, 12);
		long l = 0L;
		for (int j = 0; j < s1.length(); j++) {
			char c1 = s1.charAt(j);
			l *= 37L;
			if ((c1 >= 'a') && (c1 <= 'z'))
				l += '\001' + c1 - 97;
			else if ((c1 >= '0') && (c1 <= '9')) {
				l += '\033' + c1 - 48;
			}
		}
		return l;
	}

	public static String decode_name(long l) {
		if (l < 0L)
			return "invalid_name";
		String s = "";
		while (l != 0L) {
			int i = (int) (l % 37L);
			l /= 37L;
			if (i == 0)
				s = " " + s;
			else if (i < 27) {
				if (l % 37L == 0L)
					s = (char) (i + 65 - 1) + s;
				else
					s = (char) (i + 97 - 1) + s;
			} else
				s = (char) (i + 48 - 27) + s;
		}

		return s;
	}

	public static byte[] load(String name) throws IOException {
		int attempts = 0;
		int decmp_len = 0;
		int len = 0;
		byte[] buf = null;
		while (attempts < 2)
			try {
				if (attempts == 1)
					name = name.toUpperCase();
				InputStream stream = open_stream(name);
				DataInputStream _stream = new DataInputStream(stream);
				byte[] header = new byte[6];
				_stream.readFully(header, 0, 6);
				decmp_len = ((header[0] & 0xFF) << 16)
						+ ((header[1] & 0xFF) << 8) + (header[2] & 0xFF);
				len = ((header[3] & 0xFF) << 16) + ((header[4] & 0xFF) << 8)
						+ (header[5] & 0xFF);
				int off = 0;
				buf = new byte[len];
				int step;
				for (; off < len; off += step) {
					step = len - off;
					if (step > 1000)
						step = 1000;
					_stream.readFully(buf, off, step);
				}

				attempts = 2;
				_stream.close();
			} catch (IOException _ex) {
				attempts++;
			}
		if (len != decmp_len) {
			byte[] abyte1 = new byte[decmp_len];
			Bzip2.decompress(abyte1, decmp_len, buf, len, 0);
			return abyte1;
		}
		return buf;
	}

	public static int offset_entry(String entry, byte[] arc) {
		int cnt = short_get(arc, 0);
		int name = 0;
		entry = entry.toUpperCase();
		for (int k = 0; k < entry.length(); k++) {
			name = name * 61 + entry.charAt(k) - 32;
		}
		int ptr = 2 + cnt * 10;
		for (int i1 = 0; i1 < cnt; i1++) {
			int _name = (arc[(i1 * 10 + 2)] & 0xFF) * 16777216
					+ (arc[(i1 * 10 + 3)] & 0xFF) * 65536
					+ (arc[(i1 * 10 + 4)] & 0xFF) * 256
					+ (arc[(i1 * 10 + 5)] & 0xFF);
			int _decmp_len = (arc[(i1 * 10 + 9)] & 0xFF) * 65536
					+ (arc[(i1 * 10 + 10)] & 0xFF) * 256
					+ (arc[(i1 * 10 + 11)] & 0xFF);
			if (_name == name)
				return ptr;
			ptr += _decmp_len;
		}

		return 0;
	}

	public static int length_entry(String entry, byte[] arc) {
		int cnt = short_get(arc, 0);
		int name = 0;
		entry = entry.toUpperCase();
		for (int k = 0; k < entry.length(); k++) {
			name = name * 61 + entry.charAt(k) - 32;
		}
		for (int i1 = 0; i1 < cnt; i1++) {
			int _name = (arc[(i1 * 10 + 2)] & 0xFF) * 16777216
					+ (arc[(i1 * 10 + 3)] & 0xFF) * 65536
					+ (arc[(i1 * 10 + 4)] & 0xFF) * 256
					+ (arc[(i1 * 10 + 5)] & 0xFF);
			int _decmp_len = (arc[(i1 * 10 + 6)] & 0xFF) * 65536
					+ (arc[(i1 * 10 + 7)] & 0xFF) * 256
					+ (arc[(i1 * 10 + 8)] & 0xFF);
			if (_name == name)
				return _decmp_len;
		}

		return 0;
	}

	public static byte[] entry_extract(String entry, int off, byte[] arc) {
		return entry_extract(entry, off, arc, null);
	}

	public static String decode(int val, int radix, int offset, String charset,
			int buf_sz) {
		if (val < 0) {
			return "NaN";
		}
		int i = buf_sz - 1;
		byte[] buf = new byte[buf_sz];
		for (; i >= 0; i--) {
			int c = val % radix + offset;
			if (c < offset)
				break;
			while (charset.indexOf(c) < 0) {
				c += radix;
			}
			buf[i] = ((byte) c);
			val -= c;
		}

		return new String(buf, i, buf_sz - i);
	}

	public static byte[] entry_extract(String s, int off, byte[] arc, byte[] out) {
		int cnt = (arc[0] & 0xFF) * 256 + (arc[1] & 0xFF);
		int name = 0;
		s = s.toUpperCase();
		for (int l = 0; l < s.length(); l++) {
			name = name * 61 + s.charAt(l) - 32;
		}
		int i1 = 2 + cnt * 10;
		for (int j1 = 0; j1 < cnt; j1++) {
			int _name = (arc[(j1 * 10 + 2)] & 0xFF) * 16777216
					+ (arc[(j1 * 10 + 3)] & 0xFF) * 65536
					+ (arc[(j1 * 10 + 4)] & 0xFF) * 256
					+ (arc[(j1 * 10 + 5)] & 0xFF);
			int _decmp_len = (arc[(j1 * 10 + 6)] & 0xFF) * 65536
					+ (arc[(j1 * 10 + 7)] & 0xFF) * 256
					+ (arc[(j1 * 10 + 8)] & 0xFF);
			int _len = (arc[(j1 * 10 + 9)] & 0xFF) * 65536
					+ (arc[(j1 * 10 + 10)] & 0xFF) * 256
					+ (arc[(j1 * 10 + 11)] & 0xFF);
			if (_name == name) {
				if (out == null)
					out = new byte[_decmp_len + off];
				if (_decmp_len != _len)
					Bzip2.decompress(out, _decmp_len, arc, _len, i1);
				else {
					for (int j2 = 0; j2 < _decmp_len; j2++) {
						out[j2] = arc[(i1 + j2)];
					}
				}
				return out;
			}
			i1 += _len;
		}

		return null;
	}

	public static int encode_censor(String s) {
		int i = 0;
		try {
			if (s.length() > 80)
				s = s.substring(0, 80);
			s = s.toLowerCase() + " ";
			if (s.startsWith("@red@")) {
				coded[(i++)] = -1;
				coded[(i++)] = 0;
				s = s.substring(5);
			}
			if (s.startsWith("@gre@")) {
				coded[(i++)] = -1;
				coded[(i++)] = 1;
				s = s.substring(5);
			}
			if (s.startsWith("@blu@")) {
				coded[(i++)] = -1;
				coded[(i++)] = 2;
				s = s.substring(5);
			}
			if (s.startsWith("@cya@")) {
				coded[(i++)] = -1;
				coded[(i++)] = 3;
				s = s.substring(5);
			}
			if (s.startsWith("@ran@")) {
				coded[(i++)] = -1;
				coded[(i++)] = 4;
				s = s.substring(5);
			}
			if (s.startsWith("@whi@")) {
				coded[(i++)] = -1;
				coded[(i++)] = 5;
				s = s.substring(5);
			}
			if (s.startsWith("@bla@")) {
				coded[(i++)] = -1;
				coded[(i++)] = 6;
				s = s.substring(5);
			}
			if (s.startsWith("@ora@")) {
				coded[(i++)] = -1;
				coded[(i++)] = 7;
				s = s.substring(5);
			}
			if (s.startsWith("@yel@")) {
				coded[(i++)] = -1;
				coded[(i++)] = 8;
				s = s.substring(5);
			}
			if (s.startsWith("@mag@")) {
				coded[(i++)] = -1;
				coded[(i++)] = 9;
				s = s.substring(5);
			}
			String s1 = "";
			for (int j = 0; j < s.length(); j++) {
				char c = s.charAt(j);
				if (((c >= 'a') && (c <= 'z')) || ((c >= '0') && (c <= '9'))
						|| (c == '\'')) {
					s1 = s1 + c;
				} else {
					int k = encode(c);
					if (s1.length() > 0) {
						for (int l = 0; l < replacement_term_cnt; l++) {
							if (s1.equals(replacements[l])) {
								if ((k == 36) && (l < 165)) {
									coded[(i++)] = ((byte) (l + 90));
									k = -1;
								} else if (k == 36) {
									coded[(i++)] = ((byte) (l / 256 + 50));
									coded[(i++)] = ((byte) (l & 0xFF));
									k = -1;
								} else {
									coded[(i++)] = ((byte) (l / 256 + 70));
									coded[(i++)] = ((byte) (l & 0xFF));
								}
								s1 = "";
								break;
							}
						}
					}
					for (int i1 = 0; i1 < s1.length(); i1++) {
						coded[(i++)] = ((byte) encode(s1.charAt(i1)));
					}
					s1 = "";
					if ((k != -1) && (j < s.length() - 1))
						coded[(i++)] = ((byte) k);
				}
			}
		} catch (Exception _ex) {
		}
		return i;
	}

	private static int encode(char c) {
		if ((c >= 'a') && (c <= 'z'))
			return c - 'a';
		if ((c >= '0') && (c <= '9'))
			return c + '\032' - 48;
		if (c == ' ')
			return 36;
		if (c == '!')
			return 37;
		if (c == '?')
			return 38;
		if (c == '.')
			return 39;
		if (c == ',')
			return 40;
		if (c == ':')
			return 41;
		if (c == ';')
			return 42;
		if (c == '(')
			return 43;
		if (c == ')')
			return 44;
		if (c == '-')
			return 45;
		if (c == '&')
			return 46;
		if (c == '*')
			return 47;
		if (c == '\\')
			return 48;
		return c != '\'' ? 36 : 49;
	}

	public static String decode_censor(byte[] abyte0, int i, int j, boolean flag) {
		try {
			String s = "";
			String s1 = "";
			for (int k = i; k < i + j; k++) {
				int l = abyte0[k] & 0xFF;
				if (l < 50) {
					s = s + CHAR_TBL[l];
				} else if (l < 70) {
					k++;
					s = s + replacements[((l - 50) * 256 + (abyte0[k] & 0xFF))]
							+ " ";
				} else if (l < 90) {
					k++;
					s = s + replacements[((l - 70) * 256 + (abyte0[k] & 0xFF))];
				} else if (l < 255) {
					s = s + replacements[(l - 90)] + " ";
				} else {
					k++;
					int i1 = abyte0[k] & 0xFF;
					if (i1 == 0)
						s1 = "@red@";
					if (i1 == 1)
						s1 = "@gre@";
					if (i1 == 2)
						s1 = "@blu@";
					if (i1 == 3)
						s1 = "@cya@";
					if (i1 == 4)
						s1 = "@ran@";
					if (i1 == 5)
						s1 = "@whi@";
					if (i1 == 6)
						s1 = "@bla@";
					if (i1 == 7)
						s1 = "@ora@";
					if (i1 == 8)
						s1 = "@yel@";
					if (i1 == 9) {
						s1 = "@mag@";
					}
				}
			}
			if (flag) {
				for (int j1 = 0; j1 < 2; j1++) {
					String s3 = s;
					s = transform(s);
					if (s.equals(s3)) {
						break;
					}
				}
			}
			if (s.length() > 80)
				s = s.substring(0, 80);
			s = s.toLowerCase();
			String s2 = s1;
			boolean flag1 = true;
			for (int k1 = 0; k1 < s.length(); k1++) {
				char c = s.charAt(k1);
				if ((c >= 'a') && (c <= 'z') && (flag1)) {
					flag1 = false;
					c = (char) (c + 'A' - 97);
				}
				if ((c == '.') || (c == '!') || (c == '?'))
					flag1 = true;
				s2 = s2 + c;
			}

			return s2;
		} catch (Exception _ex) {
		}
		return "eep!";
	}

	private static String transform(String input) {
		try {
			int len = input.length();
			input.toLowerCase().getChars(0, len, buffer, 0);
			for (int i = 0; i < len; i++) {
				char c = buffer[i];
				for (int search = 0; search < search_term_cnt; search++) {
					String term = search_terms[search];
					char last = term.charAt(0);
					if (similar(last, c, 0)) {
						int deepness = 1;
						int sensitivity = term.length();
						char current = term.charAt(1);
						int break_point = 0;
						if (sensitivity >= 6)
							break_point = 1;
						for (int ii = i + 1; ii < len; ii++) {
							char c3 = buffer[ii];
							if (similar(current, c3, sensitivity)) {
								deepness++;
								if (deepness >= sensitivity) {
									boolean alpha = false;
									for (int idx = i; idx <= ii; idx++) {
										if ((input.charAt(idx) >= 'A')
												&& (input.charAt(idx) <= 'Z')) {
											alpha = true;
											break;
										}
									}
									if (!alpha)
										break;
									String s2 = "";
									for (int idx = 0; idx < input.length(); idx++) {
										char c4 = input.charAt(idx);
										if ((idx >= i) && (idx <= ii)
												&& (c4 != ' ')
												&& ((c4 < 'a') || (c4 > 'z')))
											s2 = s2 + "*";
										else
											s2 = s2 + c4;
									}
									input = s2;
									break;
								}

								last = current;
								current = term.charAt(deepness);
							} else if (!_similar(last, c3, sensitivity)) {
								break_point--;
								if (break_point < 0) {
									break;
								}
							}
						}
					}
				}
			}
			return input;
		} catch (Exception _ex) {
		}
		return "wibble!";
	}

	private static boolean similar(char c, char c1, int sensitivity) {
		if (c == c1)
			return true;
		if ((c == 'i')
				&& ((c1 == 'y') || (c1 == '1') || (c1 == '!') || (c1 == ':') || (c1 == ';')))
			return true;
		if ((c == 's') && ((c1 == '5') || (c1 == 'z')))
			return true;
		if ((c == 'e') && (c1 == '3'))
			return true;
		if ((c == 'a') && (c1 == '4'))
			return true;
		if ((c == 'o') && ((c1 == '0') || (c1 == '*')))
			return true;
		if ((c == 'u') && (c1 == 'v'))
			return true;
		if ((c == 'c') && ((c1 == '(') || (c1 == 'k')))
			return true;
		if ((c == 'k') && ((c1 == '(') || (c1 == 'c')))
			return true;
		if ((c == 'w') && (c1 == 'v'))
			return true;
		return (sensitivity >= 4) && (c == 'i') && (c1 == 'l');
	}

	private static boolean _similar(char c, char c1, int sensitivity) {
		if (c == c1)
			return true;
		if ((c1 < 'a') || ((c1 > 'u') && (c1 != 'y')))
			return true;
		if ((c == 'i') && (c1 == 'y'))
			return true;
		if ((c == 'c') && (c1 == 'k'))
			return true;
		if ((c == 'k') && (c1 == 'c'))
			return true;
		return (sensitivity >= 5)
				&& ((c == 'a') || (c == 'e') || (c == 'i') || (c == 'o')
						|| (c == 'u') || (c == 'y'))
				&& ((c1 == 'a') || (c1 == 'e') || (c1 == 'i') || (c1 == 'o')
						|| (c1 == 'u') || (c1 == 'y'));
	}
}