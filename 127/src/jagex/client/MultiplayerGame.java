package jagex.client;

import jagex.DataUtil;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.io.IOException;
import java.math.BigInteger;

@SuppressWarnings("serial")
public abstract class MultiplayerGame extends Game {
	public static String[] response_status_text;
	public static int version = 1;
	public static int time_out;
	public String host;
	public int port_off;
	String username;
	String password;
	public Connection conn;
	byte[] frame_buf;
	int attempts_left;
	long last_tick;
	public int friend_cnt;
	public long[] friends;
	public int[] friend_nodes;
	public int ignore_cnt;
	public long[] ignores;
	public int pub_block;
	public int priv_block;
	public int trade_block;
	public int duel_block;
	public BigInteger exp;
	public BigInteger mod;
	public int sess_id;
	public int anInt647;

	public void set_key(BigInteger exp, BigInteger mod) {
		this.exp = exp;
		this.mod = mod;
	}

	public int seed() {
		try {
			String s = getParameter("ranseed");
			String s1 = s.substring(0, 10);
			int i = Integer.parseInt(s1);
			if (i == 987654321) {
				byte[] abyte0 = new byte[4];
				DataUtil.read("uid.dat", abyte0, 4);
				i = DataUtil.int_get(abyte0, 0);
			}
			return i;
		} catch (Exception _ex) {
		}
		return 0;
	}

	public void login(String user, String pass, boolean reconnect) {
		if (this.anInt647 > 0) {
			status_text(response_status_text[6], response_status_text[7]);
			try {
				Thread.sleep(2000L);
			} catch (Exception _ex) {
			}
			status_text(response_status_text[8], response_status_text[9]);
			return;
		}
		try {
			this.username = user;
			user = DataUtil.filter_str(user, 20);
			this.password = pass;
			pass = DataUtil.filter_str(pass, 20);
			if (user.trim().length() == 0) {
				status_text(response_status_text[0], response_status_text[1]);
				return;
			}
			if (reconnect)
				method28(response_status_text[2], response_status_text[3]);
			else
				status_text(response_status_text[6], response_status_text[7]);
			if (is_applet())
				this.conn = new Connection(this.host, this, this.port_off);
			else
				this.conn = new Connection(this.host, null, this.port_off);
			this.conn.time_out = time_out;
			int i = this.conn.int_read();
			this.sess_id = i;
			System.out.println("Session id: " + i);
			if (reconnect)
				this.conn.enter(19);
			else
				this.conn.enter(0);
			this.conn.short_put(version);
			this.conn.long_put(DataUtil.encode_name(user));
			this.conn.enc_cred_put(pass, i, this.exp, this.mod);
			this.conn.int_put(seed());
			this.conn.send();
			this.conn.read();
			int rsp = this.conn.read();
			System.out.println("Login response: " + rsp);
			if (rsp == 0) {
				this.attempts_left = 0;
				method44();
				return;
			}
			if (rsp == 1) {
				this.attempts_left = 0;
				method43();
				return;
			}
			if (reconnect) {
				user = "";
				pass = "";
				method45();
				return;
			}
			if (rsp == 3) {
				status_text(response_status_text[10], response_status_text[11]);
				return;
			}
			if (rsp == 4) {
				status_text(response_status_text[4], response_status_text[5]);
				return;
			}
			if (rsp == 5) {
				status_text(response_status_text[16], response_status_text[17]);
				return;
			}
			if (rsp == 6) {
				status_text(response_status_text[18], response_status_text[19]);
				return;
			}
			if (rsp == 7) {
				status_text(response_status_text[20], response_status_text[21]);
				return;
			}
			if (rsp == 11) {
				status_text(response_status_text[22], response_status_text[23]);
				return;
			}
			if (rsp == 12) {
				status_text(response_status_text[24], response_status_text[25]);
				return;
			}
			if (rsp == 13) {
				status_text(response_status_text[14], response_status_text[15]);
				return;
			}
			if (rsp == 14) {
				status_text(response_status_text[8], response_status_text[9]);
				this.anInt647 = 1500;
				return;
			}
			if (rsp == 15) {
				status_text(response_status_text[26], response_status_text[27]);
				return;
			}
			if (rsp == 16) {
				status_text(response_status_text[28], response_status_text[29]);
				return;
			}
			status_text(response_status_text[12], response_status_text[13]);
			return;
		} catch (Exception _ex) {
			System.out.println(String.valueOf(_ex));

			if (this.attempts_left > 0) {
				try {
					Thread.sleep(5000L);
				} catch (Exception _) { }
				this.attempts_left -= 1;
				login(this.username, this.password, reconnect);
			}
			if (reconnect) {
				this.username = "";
				this.password = "";
				method45();
			} else {
				status_text(response_status_text[12], response_status_text[13]);
			}
		}
	}

	public void method26() {
		if (this.conn != null)
			try {
				this.conn.enter(1);
				this.conn.send();
			} catch (IOException _ex) {
			}
		this.username = "";
		this.password = "";
		method45();
	}

	public void on_drop() {
		System.out.println("Lost connection");
		this.attempts_left = 10;
		login(this.username, this.password, true);
	}

	public void method28(String s, String s1) {
		Graphics g = getGraphics();
		Font font = new Font("Helvetica", 1, 15);
		int i = width();
		int j = height();
		g.setColor(Color.black);
		g.fillRect(i / 2 - 140, j / 2 - 25, 280, 50);
		g.setColor(Color.white);
		g.drawRect(i / 2 - 140, j / 2 - 25, 280, 50);
		text_draw(g, s, font, i / 2, j / 2 - 10);
		text_draw(g, s1, font, i / 2, j / 2 + 10);
	}

	public void register(String s, String pass) {
		if (this.anInt647 > 0) {
			status_text(response_status_text[6], response_status_text[7]);
			try {
				Thread.sleep(2000L);
			} catch (Exception _ex) {
			}
			status_text(response_status_text[8], response_status_text[9]);
			return;
		}
		try {
			s = DataUtil.filter_str(s, 20);
			pass = DataUtil.filter_str(pass, 20);
			status_text(response_status_text[6], response_status_text[7]);
			if (is_applet())
				this.conn = new Connection(this.host, this, this.port_off);
			else
				this.conn = new Connection(this.host, null, this.port_off);
			int id = this.conn.int_read();
			this.sess_id = id;
			System.out.println("Session id: " + id);
			this.conn.enter(2);
			this.conn.short_put(version);
			this.conn.long_put(DataUtil.encode_name(s));
			this.conn.enc_cred_put(pass, id, this.exp, this.mod);
			this.conn.int_put(seed());
			this.conn.send();
			this.conn.read();
			int j = this.conn.read();
			this.conn.close();
			System.out.println("Newplayer response: " + j);
			if (j == 2) {
				method47();
				return;
			}
			if (j == 3) {
				status_text(response_status_text[14], response_status_text[15]);
				return;
			}
			if (j == 4) {
				status_text(response_status_text[4], response_status_text[5]);
				return;
			}
			if (j == 5) {
				status_text(response_status_text[16], response_status_text[17]);
				return;
			}
			if (j == 6) {
				status_text(response_status_text[18], response_status_text[19]);
				return;
			}
			if (j == 7) {
				status_text(response_status_text[20], response_status_text[21]);
				return;
			}
			if (j == 11) {
				status_text(response_status_text[22], response_status_text[23]);
				return;
			}
			if (j == 12) {
				status_text(response_status_text[24], response_status_text[25]);
				return;
			}
			if (j == 13) {
				status_text(response_status_text[14], response_status_text[15]);
				return;
			}
			if (j == 14) {
				status_text(response_status_text[8], response_status_text[9]);
				this.anInt647 = 1500;
				return;
			}
			if (j == 15) {
				status_text(response_status_text[26], response_status_text[27]);
				return;
			}
			if (j == 16) {
				status_text(response_status_text[28], response_status_text[29]);
				return;
			}
			status_text(response_status_text[12], response_status_text[13]);
			return;
		} catch (Exception exception) {
			System.out.println(String.valueOf(exception));

			status_text(response_status_text[12], response_status_text[13]);
		}
	}

	public void method30() {
		long l = System.currentTimeMillis();
		if (this.conn.in_frame())
			this.last_tick = l;
		if (l - this.last_tick > 5000L) {
			this.last_tick = l;
			this.conn.enter(5);
			this.conn.exit();
		}
		try {
			this.conn.flush(20);
		} catch (IOException _ex) {
			on_drop();
			return;
		}
		if (!method50())
			return;
		int i = this.conn.frame_read(this.frame_buf);
		if (i > 0)
			handle_frame(this.frame_buf[0] & 0xFF, i);
	}

	public void handle_frame(int op, int sz) {
		if (op == 8) {
			String s = new String(this.frame_buf, 1, sz - 1);
			push_message(s);
		}
		if (op == 9)
			method26();
		if (op == 10) {
			method46();
			return;
		}
		if (op == 23) {
			this.friend_cnt = DataUtil.unsign(this.frame_buf[1]);
			for (int k = 0; k < this.friend_cnt; k++) {
				this.friends[k] = DataUtil.long_get(this.frame_buf, 2 + k * 9);
				this.friend_nodes[k] = DataUtil
						.unsign(this.frame_buf[(10 + k * 9)]);
			}

			friend_order();
			return;
		}
		if (op == 24) {
			long l = DataUtil.long_get(this.frame_buf, 1);
			int node = this.frame_buf[9] & 0xFF;
			for (int k1 = 0; k1 < this.friend_cnt; k1++) {
				if (this.friends[k1] == l) {
					if ((this.friend_nodes[k1] == 0) && (node != 0))
						push_message("@pri@" + DataUtil.decode_name(l)
								+ " has logged in");
					if ((this.friend_nodes[k1] != 0) && (node == 0))
						push_message("@pri@" + DataUtil.decode_name(l)
								+ " has logged out");
					this.friend_nodes[k1] = node;
					sz = 0;
					friend_order();
					return;
				}
			}
			this.friends[this.friend_cnt] = l;
			this.friend_nodes[this.friend_cnt] = node;
			this.friend_cnt += 1;
			push_message("@pri@" + DataUtil.decode_name(l)
					+ " has been added to your friends list");
			friend_order();
			return;
		}
		if (op == 26) {
			this.ignore_cnt = DataUtil.unsign(this.frame_buf[1]);
			for (int i1 = 0; i1 < this.ignore_cnt; i1++) {
				this.ignores[i1] = DataUtil
						.long_get(this.frame_buf, 2 + i1 * 8);
			}
			return;
		}
		if (op == 27) {
			this.pub_block = this.frame_buf[1];
			this.priv_block = this.frame_buf[2];
			this.trade_block = this.frame_buf[3];
			this.duel_block = this.frame_buf[4];
			return;
		}
		if (op == 28) {
			long l1 = DataUtil.long_get(this.frame_buf, 1);
			String s1 = DataUtil.decode_censor(this.frame_buf, 9, sz - 9, true);
			push_message("@pri@" + DataUtil.decode_name(l1) + ": tells you "
					+ s1);
			return;
		}
		handle_frame(op, sz, this.frame_buf);
	}

	public void friend_order() {
		boolean flag = true;
		while (flag) {
			flag = false;
			for (int i = 0; i < this.friend_cnt - 1; i++)
				if (this.friend_nodes[i] < this.friend_nodes[(i + 1)]) {
					int j = this.friend_nodes[i];
					this.friend_nodes[i] = this.friend_nodes[(i + 1)];
					this.friend_nodes[(i + 1)] = j;
					long l = this.friends[i];
					this.friends[i] = this.friends[(i + 1)];
					this.friends[(i + 1)] = l;
					flag = true;
				}
		}
	}

	public void method33(String s, String s1) {
		s = DataUtil.filter_str(s, 20);
		s1 = DataUtil.filter_str(s1, 20);
		this.conn.enter(25);
		this.conn.enc_cred_put(s + s1, this.sess_id, this.exp, this.mod);
		this.conn.exit();
	}

	public void method34(int i, int j, int k, int l) {
		this.conn.enter(31);
		this.conn.byte_put(i);
		this.conn.byte_put(j);
		this.conn.byte_put(k);
		this.conn.byte_put(l);
		this.conn.exit();
	}

	public void method35(String s) {
		long l = DataUtil.encode_name(s);
		this.conn.enter(29);
		this.conn.long_put(l);
		this.conn.exit();
		for (int i = 0; i < this.ignore_cnt; i++) {
			if (this.ignores[i] == l)
				return;
		}
		if (this.ignore_cnt >= 50) {
			return;
		}
		this.ignores[(this.ignore_cnt++)] = l;
	}

	public void method36(long l) {
		this.conn.enter(30);
		this.conn.long_put(l);
		this.conn.exit();
		for (int i = 0; i < this.ignore_cnt; i++)
			if (this.ignores[i] == l) {
				this.ignore_cnt -= 1;
				for (int j = i; j < this.ignore_cnt; j++) {
					this.ignores[j] = this.ignores[(j + 1)];
				}
				return;
			}
	}

	public void method37(String s) {
		this.conn.enter(26);
		this.conn.long_put(DataUtil.encode_name(s));
		this.conn.exit();
	}

	public void friend_remove(long l) {
		this.conn.enter(27);
		this.conn.long_put(l);
		this.conn.exit();
		for (int i = 0; i < this.friend_cnt; i++) {
			if (this.friends[i] != l)
				continue;
			this.friend_cnt -= 1;
			for (int j = i; j < this.friend_cnt; j++) {
				this.friends[j] = this.friends[(j + 1)];
				this.friend_nodes[j] = this.friend_nodes[(j + 1)];
			}

			break;
		}

		push_message("@pri@" + DataUtil.decode_name(l)
				+ " has been removed from your friends list");
	}

	public void method39(long l, byte[] abyte0, int i) {
		this.conn.enter(28);
		this.conn.long_put(l);
		this.conn.bytes_put(abyte0, 0, i);
		this.conn.exit();
	}

	public void method40(byte[] abyte0, int i) {
		this.conn.enter(3);
		this.conn.bytes_put(abyte0, 0, i);
		this.conn.exit();
	}

	public void method41(String s) {
		this.conn.enter(7);
		this.conn.str_put(s);
		this.conn.exit();
	}

	public void status_text(String s, String s1) {
	}

	public void method43() {
	}

	public void method44() {
	}

	public void method45() {
	}

	public void method46() {
	}

	public void method47() {
	}

	public void handle_frame(int i, int j, byte[] abyte0) {
	}

	public void push_message(String s) {
	}

	public boolean method50() {
		return true;
	}

	public MultiplayerGame() {
		this.host = "127.0.0.1";
		this.port_off = 43594;
		this.username = "";
		this.password = "";
		this.frame_buf = new byte[5000];
		this.friends = new long[100];
		this.friend_nodes = new int[100];
		this.ignores = new long[50];
	}

	static {
		response_status_text = new String[50];
		response_status_text[0] = "You must enter both a username";
		response_status_text[1] = "and a password - Please try again";
		response_status_text[2] = "Connection lost! Please wait...";
		response_status_text[3] = "Attempting to re-establish";
		response_status_text[4] = "That username is already in use.";
		response_status_text[5] = "Wait 60 seconds then retry";
		response_status_text[6] = "Please wait...";
		response_status_text[7] = "Connecting to server";
		response_status_text[8] = "Sorry! The server is currently full.";
		response_status_text[9] = "Please try again later";
		response_status_text[10] = "Invalid username or password.";
		response_status_text[11] = "Try again, or create a new account";
		response_status_text[12] = "Sorry! Unable to connect to server.";
		response_status_text[13] = "Check your internet settings";
		response_status_text[14] = "Username already taken.";
		response_status_text[15] = "Please choose another username";
		response_status_text[16] = "The client has been updated.";
		response_status_text[17] = "Please reload this page";
		response_status_text[18] = "You may only use 1 character at once.";
		response_status_text[19] = "Your ip-address is already in use";
		response_status_text[20] = "Login attempts exceeded!";
		response_status_text[21] = "Please try again in 5 minutes";
		response_status_text[22] = "Account has been temporarily disabled";
		response_status_text[23] = "for cheating or abuse";
		response_status_text[24] = "Account has been permanently disabled";
		response_status_text[25] = "for cheating or abuse";
		response_status_text[26] = "You need a members account";
		response_status_text[27] = "to login to this server";
		response_status_text[28] = "Please login to a members server";
		response_status_text[29] = "to access member-only features";
	}
}