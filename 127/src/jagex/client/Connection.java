package jagex.client;

import jagex.Buffer;

import java.applet.Applet;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

public class Connection extends Buffer implements Runnable {
	private InputStream input;
	private OutputStream output;
	private Socket socket;
	private boolean closed;
	private byte[] write_buf;
	private int last_write_ptr;
	private int write_ptr;
	private Thread writer;
	private boolean writer_stop;

	public Connection(String host, Applet app, int port) throws IOException {
		closed = false;
		writer_stop = true;
		if (app != null)
			socket = new Socket(InetAddress.getByName(app.getCodeBase().getHost()), port);
		else
			socket = new Socket(InetAddress.getByName(host), port);
		socket.setSoTimeout(30000);
		socket.setTcpNoDelay(true);
		input = socket.getInputStream();
		output = socket.getOutputStream();
	}

	public void close() {
		super.close();
		closed = true;
		try {
			if (input != null)
				input.close();
			if (output != null)
				output.close();
			if (socket != null)
				socket.close();
		} catch (IOException _ex) {
			System.out.println("Error closing stream");
		}
		if (writer != null) {
			writer_stop = true;
			synchronized (this) {
				notify();
			}
			writer = null;
		}
		write_buf = null;
	}

	public int read() throws IOException {
		if (closed) {
			return 0;
		}
		return input.read();
	}

	public int available() throws IOException {
		if (closed) {
			return 0;
		}
		return input.available();
	}

	public void read(byte[] out, int off, int len) throws IOException {
		if (closed)
			return;
		int k = 0;
		int l;
		for (; k < len; k += l)
			if ((l = input.read(out, k + off, len - k)) <= 0)
				throw new IOException("EOF");
	}

	public void write(byte[] data, int len, int off) throws IOException {
		if (closed)
			return;
		if (write_buf == null)
			write_buf = new byte[5000];
		synchronized (this) {
			for (int k = 0; k < off; k++) {
				write_buf[write_ptr] = data[(k + len)];
				write_ptr = ((write_ptr + 1) % 5000);
				if (write_ptr == (last_write_ptr + 4900) % 5000) {
					throw new IOException("buffer overflow");
				}
			}
			if (writer == null) {
				writer_stop = false;
				writer = new Thread(this);
				writer.setDaemon(true);
				writer.setPriority(4);
				writer.start();
			}
			notify();
		}
	}

	public void run() {
		while ((writer != null) && (!writer_stop)) {
			int off;
			int written;
			synchronized (this) {
				if (write_ptr == last_write_ptr)
					try {
						wait();
					} catch (InterruptedException _ex) {
					}
				if ((writer == null) || (writer_stop))
					return;
				off = last_write_ptr;
				if (write_ptr >= last_write_ptr)
					written = write_ptr - last_write_ptr;
				else
					written = 5000 - last_write_ptr;
			}
			if (written > 0) {
				try {
					output.write(write_buf, off, written);
				} catch (IOException ioexception) {
					error = true;
					err_str = ("Twriter:" + ioexception);
				}
				last_write_ptr = ((last_write_ptr + written) % 5000);
				try {
					if (write_ptr == last_write_ptr)
						output.flush();
				} catch (IOException ioexception1) {
					error = true;
					err_str = ("Twriter:" + ioexception1);
				}
			}
		}
	}
}