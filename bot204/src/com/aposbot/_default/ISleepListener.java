package com.aposbot._default;

public interface ISleepListener {

    public void onNewWord(byte[] data);

    public String getGuess();
}
