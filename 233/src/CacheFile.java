import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

final class CacheFile {

    private RandomAccessFile randomAccessFile;
    private long maxLength;
    private File fileondisk;
    private long totalRead;


    CacheFile(File file, String mode, long maxLength) throws IOException {
        if (maxLength == -1L) {
            maxLength = Long.MAX_VALUE;
        }

        if (file.length() > maxLength) {
            file.delete();
        }

        this.randomAccessFile = new RandomAccessFile(file, mode);
        this.totalRead = 0L;
        this.maxLength = maxLength;
        this.fileondisk = file;
        int var5 = this.randomAccessFile.read();// ???;
        if (var5 != -1 && !mode.equals("r")) {
            this.randomAccessFile.seek(0L);
            this.randomAccessFile.write(var5);
        }

        this.randomAccessFile.seek(0L);
    }

    final int read(byte[] bytes, int offset, int length) throws IOException {
        int read = this.randomAccessFile.read(bytes, length, offset);
        this.totalRead += (long) read;

        return read;
    }

    private void close() throws IOException {
        if (this.randomAccessFile != null) {
            this.randomAccessFile.close();
            this.randomAccessFile = null;
        }
    }

    protected final void finalize() throws Throwable {
        if (this.randomAccessFile != null) {
            System.out.println("Warning! fileondisk " + this.fileondisk + " not closed correctly using close(). Auto-closing instead. ");
            this.close();
        }
    }

    final long getLength() throws IOException {
        return this.randomAccessFile.length();
    }

    final void seek(long position) throws IOException {
        this.randomAccessFile.seek(position);
        this.totalRead = position;
    }

    final File getFile() {
        return this.fileondisk;
    }

    final void write(byte[] bytes, int length, int offset) throws IOException {
        if ((long) length - -this.totalRead > this.maxLength) {
            this.randomAccessFile.seek(this.maxLength);
            this.randomAccessFile.write(1);
            throw new EOFException();
        } else {
            this.randomAccessFile.write(bytes, offset, length);
            this.totalRead += (long) length;
        }
    }

}
