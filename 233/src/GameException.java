final class GameException extends RuntimeException {

    String errorMessage;
    Throwable errorThrowable;


    GameException(Throwable var1, String var2) {
        this.errorThrowable = var1;
        this.errorMessage = var2;
    }

    static GameException fromThrowable(Throwable exception, String msg) {
        GameException var2;
        if (exception instanceof GameException) {
            var2 = (GameException) exception;
            var2.errorMessage = var2.errorMessage + ' ' + msg;
        } else {
            var2 = new GameException(exception, msg);
        }
        System.out.println("Error: " + msg);
        exception.printStackTrace();
        return var2;
    }
}
