import java.io.File;
import java.io.RandomAccessFile;
import java.util.Hashtable;

public class CacheContainerManager {

    private static boolean aBoolean1080 = false;
    private static String homeDir;
    private static String cacheGameName;
    private static Hashtable cacheFiles = new Hashtable(16);
    private static int clientStage;


    public static void initialize(String var0, int var1) {
        cacheGameName = var0;
        clientStage = var1;

        try {
            homeDir = System.getProperty("user.home");
            if (homeDir != null) {
                homeDir = homeDir + "/";
            }
        } catch (Exception var4) {
            ;
        }

        aBoolean1080 = true;
        if (homeDir == null) {
            homeDir = "~/";
        }
    }

    public static File getFile(String filename, String subdir, int clientStage, int var3) {
        if (!aBoolean1080) {
            throw new RuntimeException("");
        } else {
            File var4 = (File) cacheFiles.get(filename);
            if (var4 != null) {
                return var4;
            } else {
                String[] mainCacheDirs = new String[]{"c:/rscache/", "/rscache/", "c:/windows/", "c:/winnt/", "c:/", homeDir, "/tmp/", ""};
                String[] gameCacheDirs = new String[]{".jagex_cache_" + clientStage, ".file_store_" + clientStage};

                for (int var7 = var3; var7 < 2; ++var7) {
                    for (int curGameCacheDir = 0; curGameCacheDir < gameCacheDirs.length; ++curGameCacheDir) {
                        for (int curMainCacheDir = 0; curMainCacheDir < mainCacheDirs.length; ++curMainCacheDir) {
                            String filepath = mainCacheDirs[curMainCacheDir] + gameCacheDirs[curGameCacheDir] + "/" + (subdir != null ? subdir + "/" : "") + filename;
                            RandomAccessFile randomAccessFile = null;

                            try {
                                File file = new File(filepath);
                                if (var7 != 0 || file.exists()) {
                                    String var13 = mainCacheDirs[curMainCacheDir];
                                    if (var7 != 1 || var13.length() <= 0 || (new File(var13)).exists()) {
                                        (new File(mainCacheDirs[curMainCacheDir] + gameCacheDirs[curGameCacheDir])).mkdir();
                                        if (subdir != null) {
                                            (new File(mainCacheDirs[curMainCacheDir] + gameCacheDirs[curGameCacheDir] + "/" + subdir)).mkdir();
                                        }

                                        randomAccessFile = new RandomAccessFile(file, "rw");
                                        int var14 = randomAccessFile.read();// todo ???
                                        randomAccessFile.seek(0L);
                                        randomAccessFile.write(var14);
                                        randomAccessFile.seek(0L);
                                        randomAccessFile.close();
                                        cacheFiles.put(filename, file);
                                        return file;
                                    }
                                }
                            } catch (Exception var16) {
                                try {
                                    if (randomAccessFile != null) {
                                        randomAccessFile.close();
                                        randomAccessFile = null;
                                    }
                                } catch (Exception var15) {
                                    ;
                                }
                            }
                        }
                    }
                }

                throw new RuntimeException();
            }
        }
    }

    public static File getFile(String var0, int var1) {
        return var1 != 0 ? null : getFile(var0, cacheGameName, clientStage, 0);
    }

}
