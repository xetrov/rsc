abstract class BufferBase_Sub2 extends BufferBase {

    BufferBase_Sub4 aClass8_Sub4_1634;
    int anInt1635;
    BufferBase_Sub2 aClass8_Sub2_1636;
    volatile boolean aBoolean1637 = true;


    abstract void method164(int[] var1, int var2, int var3);

    abstract BufferBase_Sub2 method165();

    final void method166(int[] var1, int var2, int var3) {
        if (this.aBoolean1637) {
            this.method164(var1, var2, var3);
        } else {
            this.method170(var3);
        }
    }

    abstract int method167();

    int method168() {
        return 255;
    }

    abstract BufferBase_Sub2 method169();

    abstract void method170(int var1);

}
