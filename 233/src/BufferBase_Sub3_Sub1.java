final class BufferBase_Sub3_Sub1 extends BufferBase_Sub3 {

    static int anInt1741;
    private int bitmaskOffset;


    BufferBase_Sub3_Sub1(int var1) {
        super(var1);
    }

    static String method234(int var0) {
        return (var0 >> 24 & 255) + "." + ((16747258 & var0) >> 16) + "." + (255 & var0 >> 8) + "." + (255 & var0);
    }

    final void updateOffset() {
        super.offset = (this.bitmaskOffset - -7) / 8;
    }

    final int getBitmaskOffset() {
        return this.bitmaskOffset;
    }

    final int getBitMask(int var1) {
        int var3 = this.bitmaskOffset >> 3;
        int var4 = 8 - (7 & this.bitmaskOffset);
        this.bitmaskOffset += var1;

        int var5;
        for (var5 = 0; var4 < var1; var4 = 8) {
            var5 += (BufferBase_Sub1.anIntArray486[var4] & super.buffer[var3++]) << -var4 + var1;
            var1 -= var4;
        }

        if (var1 == var4) {
            var5 += super.buffer[var3] & BufferBase_Sub1.anIntArray486[var4];
        } else {
            var5 += super.buffer[var3] >> var4 - var1 & BufferBase_Sub1.anIntArray486[var1];
        }

        return var5;
    }

    final void setBitmaskOffset() {
        this.bitmaskOffset = 8 * super.offset;
    }

}
