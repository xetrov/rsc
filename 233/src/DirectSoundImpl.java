import com.ms.com.ComFailException;
import com.ms.com._Guid;
import com.ms.directX.DSBufferDesc;
import com.ms.directX.DSCursors;
import com.ms.directX.DirectSoundBuffer;
import com.ms.directX.WaveFormatEx;

import java.awt.*;

public final class DirectSoundImpl implements DirectSound {

    private WaveFormatEx aWaveFormatEx1138 = new WaveFormatEx();
    private DSCursors[] aDSCursorsArray1139 = new DSCursors[2];
    private int anInt1140;
    private DSBufferDesc[] aDSBufferDescArray1141 = new DSBufferDesc[2];
    private int[] anIntArray1142 = new int[2];
    private int anInt1143;
    private com.ms.directX.DirectSound aDirectSound1144 = new com.ms.directX.DirectSound();
    private DirectSoundBuffer[] aDirectSoundBufferArray1145 = new DirectSoundBuffer[2];
    private int[] anIntArray1146 = new int[2];
    private int[] anIntArray1147;
    private int anInt1148;
    private boolean[] aBooleanArray1149 = new boolean[2];
    private byte[][] aByteArrayArray1150 = new byte[2][];


    public DirectSoundImpl() throws Exception {
        for (int var1 = 0; var1 < 2; ++var1) {
            this.aDSBufferDescArray1141[var1] = new DSBufferDesc();
        }

        for (int var2 = 0; var2 < 2; ++var2) {
            this.aDSCursorsArray1139[var2] = new DSCursors();
        }

    }

    public final int method5(int var1, int var2) {
        if (var2 != -6108) {
            return -2;
        } else if (!this.aBooleanArray1149[var1]) {
            return 0;
        } else {
            this.aDirectSoundBufferArray1145[var1].getCurrentPosition(this.aDSCursorsArray1139[var1]);
            int var3 = this.aDSCursorsArray1139[var1].write / this.anInt1148;
            int var4 = '\uffff' & -var3 + this.anIntArray1146[var1];
            if (var4 > this.anIntArray1142[var1]) {
                for (int var5 = -this.anIntArray1146[var1] + var3 & '\uffff'; var5 > 0; var5 -= 256) {
                    this.method2(var1, this.anIntArray1147);
                }

                var4 = '\uffff' & this.anIntArray1146[var1] - var3;
            }

            return var4;
        }
    }

    public final void method2(int var1, int[] var2) {
        int var3 = var2.length;
        if (var3 != 256 * this.anInt1140) {
            throw new IllegalArgumentException();
        } else {
            int var4 = this.anIntArray1146[var1] * this.anInt1148;

            for (int var5 = 0; var5 < var3; ++var5) {
                int var6 = var2[var5];
                if ((var6 + 8388608 & -16777216) != 0) {
                    var6 = 8388607 ^ var6 >> 31;
                }

                this.aByteArrayArray1150[var1][var4 + var5 * 2] = (byte) (var6 >> 8);
                this.aByteArrayArray1150[var1][var4 + var5 * 2 + 1] = (byte) (var6 >> 16);
            }

            this.aDirectSoundBufferArray1145[var1].writeBuffer(var4, var3 * 2, this.aByteArrayArray1150[var1], 0);
            this.anIntArray1146[var1] = this.anIntArray1146[var1] + var3 / this.anInt1140 & '\uffff';
            if (!this.aBooleanArray1149[var1]) {
                this.aDirectSoundBufferArray1145[var1].play(1);
                this.aBooleanArray1149[var1] = true;
            }

        }
    }

    public final void method1(Component var1, int var2, int var3, boolean var4) throws Exception {
        if (this.anInt1143 == 0) {
            if (var3 >= 8000 && var3 <= 48000) {
                if (var2 < 124) {
                    this.method5(58, 49);
                }

                this.anInt1148 = var4 ? 4 : 2;
                this.anInt1140 = !var4 ? 1 : 2;
                this.anIntArray1147 = new int[this.anInt1140 * 256];
                this.aDirectSound1144.initialize((_Guid) null);
                this.aDirectSound1144.setCooperativeLevel(var1, 2);

                for (int var5 = 0; var5 < 2; ++var5) {
                    this.aDSBufferDescArray1141[var5].flags = 16384;
                }

                this.aWaveFormatEx1138.bitsPerSample = 16;
                this.aWaveFormatEx1138.formatTag = 1;
                this.aWaveFormatEx1138.channels = this.anInt1140;
                this.aWaveFormatEx1138.avgBytesPerSec = this.anInt1148 * var3;
                this.anInt1143 = var3;
                this.aWaveFormatEx1138.blockAlign = this.anInt1148;
                this.aWaveFormatEx1138.samplesPerSec = var3;
            } else {
                throw new IllegalArgumentException();
            }
        }
    }

    public final void method3(byte var1, int var2) {
        if (var1 <= 74) {
            this.method5(47, -108);
        }

        if (this.aDirectSoundBufferArray1145[var2] != null) {
            try {
                this.aDirectSoundBufferArray1145[var2].stop();
            } catch (ComFailException var4) {
                ;
            }

            this.aDirectSoundBufferArray1145[var2] = null;
        }
    }

    public final void method4(int var1, int var2, int var3) throws Exception {
        if (this.anInt1143 != 0 && this.aDirectSoundBufferArray1145[var2] == null) {
            if (var1 == -23386) {
                int var4 = this.anInt1148 * 65536;
                if (this.aByteArrayArray1150[var2] == null || this.aByteArrayArray1150[var2].length != var4) {
                    this.aByteArrayArray1150[var2] = new byte[var4];
                    this.aDSBufferDescArray1141[var2].bufferBytes = var4;
                }

                this.aDirectSoundBufferArray1145[var2] = this.aDirectSound1144.createSoundBuffer(this.aDSBufferDescArray1141[var2], this.aWaveFormatEx1138);
                this.aBooleanArray1149[var2] = false;
                this.anIntArray1146[var2] = 0;
                this.anIntArray1142[var2] = var3;
            }
        } else {
            throw new IllegalStateException();
        }
    }
}
