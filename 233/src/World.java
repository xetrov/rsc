import java.io.IOException;

final class World {

    static int anInt273;
    static String[] aStringArray237 = new String[]{"Type the number of items to sell and press enter"};
    int baseMediaSprite = 750;
    GameModel[][] aGameModelArrayArray236 = new GameModel[4][64];
    int[] anIntArray244 = new int[18432];
    GameModel[][] aGameModelArrayArray246 = new GameModel[4][64];
    byte[] mapPack;
    byte[] memberLandscapePack;
    int[] anIntArray266 = new int[18432];
    boolean playerAlive = false;
    byte[] memberMapPack;
    byte[] landscapePack;
    int[][] anIntArrayArray276 = new int[96][96];
    private GameModel[] aGameModelArray223 = new GameModel[64];
    private byte[][] wallsNorthsouth = new byte[4][2304];
    private byte[][] wallsEastwest = new byte[4][2304];
    private byte[][] wallsRoof = new byte[4][2304];
    private Surface aSurface_233;
    private Scene aScene_234;
    private int[] anIntArray235 = new int[256];
    private byte[][] terrainHeight = new byte[4][2304];
    private int[][] wallsDiagonal = new int[4][2304];
    private int[][] anIntArrayArray249 = new int[96][96];
    private byte[][] tileDecoration = new byte[4][2304];
    private boolean aBoolean257 = true;
    private byte[][] tileDirection = new byte[4][2304];
    private GameModel aGameModel_261;
    private int[][] anIntArrayArray270 = new int[96][96];
    private boolean aBoolean275 = false;
    private byte[][] terrainColour = new byte[4][2304];


    World(Scene var1, Surface var2) {
        this.aSurface_233 = var2;
        this.aScene_234 = var1;

        for (int var3 = 0; var3 < 64; ++var3) {
            this.anIntArray235[var3] = Scene.method307(-(4 * var3) + 255, -114, 255 - var3 * 4, -((int) ((double) var3 * 1.75D)) + 255);
        }

        for (int var4 = 0; var4 < 64; ++var4) {
            this.anIntArray235[var4 + 64] = Scene.method307(0, -124, var4 * 3, 144);
        }

        for (int var5 = 0; var5 < 64; ++var5) {
            this.anIntArray235[128 + var5] = Scene.method307(0, -108, 192 - (int) ((double) var5 * 1.5D), 144 - (int) ((double) var5 * 1.5D));
        }

        for (int var6 = 0; var6 < 64; ++var6) {
            this.anIntArray235[192 + var6] = Scene.method307(0, -112, -((int) (1.5D * (double) var6)) + 96, 48 + (int) (1.5D * (double) var6));
        }
    }

    static boolean method282(char var0, byte var1) {
        if (var1 != -115) {
            aStringArray237 = null;
        }

        return var0 >= 48 && var0 <= 57 || var0 >= 65 && var0 <= 90 || var0 >= 97 && var0 <= 122;
    }

    private void loadSection(int y, int var2, int x, boolean var4, int plane) {
        if (!var4) {
            this.method266(-25, 33, -56);
        }

        String mapname = "m" + plane + x / 10 + x % 10 + y / 10 + y % 10;

        try {
            int var8;
            int var9;
            int var10;
            int var11;
            int var12;
            int var13;
            int var14;
            int var15;
            int var17;
            int var16;
            byte[] buff;
            if (this.landscapePack != null) {
                buff = Utility.loadData(this.landscapePack, 0, mapname + ".hei");
                if (buff == null && this.memberLandscapePack != null) {
                    buff = Utility.loadData(this.memberLandscapePack, 0, mapname + ".hei");
                }

                if (buff != null && buff.length > 0) {
                    var8 = 0;
                    var9 = 0;
                    var10 = 0;

                    while (var10 < 2304) {
                        var11 = buff[var8++] & 255;
                        if (var11 < 128) {
                            var9 = var11;
                            this.terrainHeight[var2][var10++] = (byte) var11;
                        }

                        if (var11 >= 128) {
                            for (var12 = 0; var12 < -128 + var11; ++var12) {
                                this.terrainHeight[var2][var10++] = (byte) var9;
                            }
                        }
                    }

                    var9 = 64;

                    for (var11 = 0; var11 < 48; ++var11) {
                        for (var12 = 0; var12 < 48; ++var12) {
                            var9 = 127 & this.terrainHeight[var2][48 * var12 - -var11] - -var9;
                            this.terrainHeight[var2][48 * var12 - -var11] = (byte) (var9 * 2);
                        }
                    }

                    var9 = 0;
                    var12 = 0;

                    while (var12 < 2304) {
                        var13 = 255 & buff[var8++];
                        if (var13 < 128) {
                            var9 = var13;
                            this.terrainColour[var2][var12++] = (byte) var13;
                        }

                        if (var13 >= 128) {
                            for (var14 = 0; var14 < (var13 - 128); ++var14) {
                                this.terrainColour[var2][var12++] = (byte) var9;
                            }
                        }
                    }

                    var9 = 35;

                    for (var13 = 0; var13 < 48; ++var13) {
                        for (var14 = 0; var14 < 48; ++var14) {
                            var9 = 127 & this.terrainColour[var2][48 * var14 - -var13] - -var9;
                            this.terrainColour[var2][48 * var14 + var13] = (byte) (2 * var9);
                        }
                    }
                } else {
                    for (var8 = 0; var8 < 2304; ++var8) {
                        this.terrainHeight[var2][var8] = 0;
                        this.terrainColour[var2][var8] = 0;
                    }
                }

                buff = Utility.loadData(this.mapPack, 0, mapname + ".dat");
                if (buff == null && this.memberMapPack != null) {
                    buff = Utility.loadData(this.memberMapPack, 0, mapname + ".dat");
                }

                if (buff == null || buff.length == 0) {
                    throw new IOException();
                }

                var8 = 0;

                for (var9 = 0; var9 < 2304; ++var9) {
                    this.wallsNorthsouth[var2][var9] = buff[var8++];
                }

                for (var10 = 0; var10 < 2304; ++var10) {
                    this.wallsEastwest[var2][var10] = buff[var8++];
                }

                for (var11 = 0; var11 < 2304; ++var11) {
                    this.wallsDiagonal[var2][var11] = Utility.bitwiseAnd(255, buff[var8++]);
                }

                for (var12 = 0; var12 < 2304; ++var12) {
                    var13 = buff[var8++] & 255;
                    if (var13 > 0) {
                        this.wallsDiagonal[var2][var12] = 12000 + var13;
                    }
                }

                var13 = 0;

                while (var13 < 2304) {
                    var14 = 255 & buff[var8++];
                    if (var14 >= 128) {
                        for (var15 = 0; var14 - 128 > var15; ++var15) {
                            this.wallsRoof[var2][var13++] = 0;
                        }
                    } else {
                        this.wallsRoof[var2][var13++] = (byte) var14;
                    }
                }

                var14 = 0;
                var15 = 0;

                while (var15 < 2304) {
                    var16 = buff[var8++] & 255;
                    if (var16 >= 128) {
                        for (var17 = 0; var17 < -128 + var16; ++var17) {
                            this.tileDecoration[var2][var15++] = (byte) var14;
                        }
                    } else {
                        this.tileDecoration[var2][var15++] = (byte) var16;
                        var14 = var16;
                    }
                }

                var16 = 0;

                int var18;
                while (var16 < 2304) {
                    var17 = 255 & buff[var8++];
                    if (var17 >= 128) {
                        for (var18 = 0; (var17 - 128) > var18; ++var18) {
                            this.tileDirection[var2][var16++] = 0;
                        }
                    } else {
                        this.tileDirection[var2][var16++] = (byte) var17;
                    }
                }

                buff = Utility.loadData(this.mapPack, 0, mapname + ".loc");
                if (buff != null && buff.length > 0) {
                    var8 = 0;
                    var17 = 0;

                    while (var17 < 2304) {
                        var18 = 255 & buff[var8++];
                        if (var18 >= 128) {
                            var17 += -128 + var18;
                        } else {
                            this.wallsDiagonal[var2][var17++] = var18 - -48000;
                        }
                    }

                    return;
                }
            } else {
                buff = new byte[20736];
                Panel.method543((byte) 126, "../gamedata/maps/" + mapname + ".jm", 20736, buff);
                var8 = 0;
                var9 = 0;

                for (var10 = 0; var10 < 2304; ++var10) {
                    var8 = 255 & var8 - -buff[var9++];
                    this.terrainHeight[var2][var10] = (byte) var8;
                }

                var8 = 0;

                for (var11 = 0; var11 < 2304; ++var11) {
                    var8 = 255 & buff[var9++] + var8;
                    this.terrainColour[var2][var11] = (byte) var8;
                }

                for (var12 = 0; var12 < 2304; ++var12) {
                    this.wallsNorthsouth[var2][var12] = buff[var9++];
                }

                for (var13 = 0; var13 < 2304; ++var13) {
                    this.wallsEastwest[var2][var13] = buff[var9++];
                }

                for (var14 = 0; var14 < 2304; ++var14) {
                    this.wallsDiagonal[var2][var14] = Utility.bitwiseAnd(255, buff[var9]) * 256 + Utility.bitwiseAnd(buff[1 + var9], 255);
                    var9 += 2;
                }

                for (var15 = 0; var15 < 2304; ++var15) {
                    this.wallsRoof[var2][var15] = buff[var9++];
                }

                for (var16 = 0; var16 < 2304; ++var16) {
                    this.tileDecoration[var2][var16] = buff[var9++];
                }

                for (var17 = 0; var17 < 2304; ++var17) {
                    this.tileDirection[var2][var17] = buff[var9++];
                }
            }

        } catch (IOException var19) {
            for (int var7 = 0; var7 < 2304; ++var7) {
                this.terrainHeight[var2][var7] = 0;
                this.terrainColour[var2][var7] = 0;
                this.wallsNorthsouth[var2][var7] = 0;
                this.wallsEastwest[var2][var7] = 0;
                this.wallsDiagonal[var2][var7] = 0;
                this.wallsRoof[var2][var7] = 0;
                this.tileDecoration[var2][var7] = 0;
                if (plane == 0) {
                    this.tileDecoration[var2][var7] = -6;
                }

                if (plane == 3) {
                    this.tileDecoration[var2][var7] = 8;
                }

                this.tileDirection[var2][var7] = 0;
            }

        }
        ;
    }

    final void method251(GameModel[] var1, boolean var2) {
        if (var2) {
            this.anIntArrayArray249 = null;
        }

        for (int var3 = 0; var3 < 94; ++var3) {
            for (int var4 = 0; var4 < 94; ++var4) {
                if (this.getWallDiagonal(var4, var3, 0) > '\ubb80' && this.getWallDiagonal(var4, var3, 0) < 60000) {
                    int var5 = -48001 + this.getWallDiagonal(var4, var3, 0);
                    int var6 = this.getTileDirection(var3, true, var4);
                    int var7;
                    int var8;
                    if (var6 != 0 && var6 != 4) {
                        var8 = GameData.objectWidth[var5];
                        var7 = GameData.objectHeight[var5];
                    } else {
                        var7 = GameData.objectWidth[var5];
                        var8 = GameData.objectHeight[var5];
                    }

                    this.method274(var5, (byte) -113, var3, var4);
                    GameModel var9 = var1[GameData.objectModelIndex[var5]].method569(true, (byte) -126, false, false, false);
                    int var10 = 128 * (var7 + var3 + var3) / 2;
                    int var11 = 128 * (var4 - -var4 + var8) / 2;
                    var9.translate(-this.getElevation(var10, (byte) -56, var11), var11, (byte) 61, var10);
                    var9.method558(0, this.getTileDirection(var3, true, var4) * 32, 0, false);
                    this.aScene_234.method325(var9, (byte) 6);
                    var9.method567(-10, true, -50, 48, -50, 48);
                    if (var7 > 1 || var8 > 1) {
                        for (int var12 = var3; var12 < (var3 + var7); ++var12) {
                            for (int var13 = var4; (var8 + var4) > var13; ++var13) {
                                if ((var12 > var3 || var13 > var4) && var5 == this.getWallDiagonal(var13, var12, 0) + -48001) {
                                    var10 = var12;
                                    var11 = var13;
                                    byte var14 = 0;
                                    if (var12 >= 48 && var13 < 48) {
                                        var10 = var12 - 48;
                                        var14 = 1;
                                    } else if (var12 < 48 && var13 >= 48) {
                                        var14 = 2;
                                        var11 = var13 - 48;
                                    } else if (var12 >= 48 && var13 >= 48) {
                                        var14 = 3;
                                        var10 = var12 - 48;
                                        var11 = var13 - 48;
                                    }

                                    this.wallsDiagonal[var14][var11 + var10 * 48] = 0;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private int method252(int var1, boolean var2, int var3) {
        if (var3 >= 0 && var3 < 96 && var1 >= 0 && var1 < 96) {
            if (var2) {
                return 25;
            } else {
                byte var4 = 0;
                if (var3 >= 48 && var1 < 48) {
                    var3 -= 48;
                    var4 = 1;
                } else if (var3 < 48 && var1 >= 48) {
                    var1 -= 48;
                    var4 = 2;
                } else if (var3 >= 48 && var1 >= 48) {
                    var1 -= 48;
                    var3 -= 48;
                    var4 = 3;
                }

                return this.wallsEastwest[var4][var1 + var3 * 48] & 255;
            }
        } else {
            return 0;
        }
    }

    private void method253(int var1) {
        if (this.aBoolean257) {
            this.aScene_234.method323(true);
        }

        if (var1 == 2689) {
            for (int var2 = 0; var2 < 64; ++var2) {
                this.aGameModelArray223[var2] = null;

                for (int var3 = 0; var3 < 4; ++var3) {
                    this.aGameModelArrayArray246[var3][var2] = null;
                }

                for (int var4 = 0; var4 < 4; ++var4) {
                    this.aGameModelArrayArray236[var4][var2] = null;
                }
            }

            System.gc();
        }
    }

    private void method254(int var1, int var2, byte var3, boolean var4, int var5) {
        int var6 = (24 + var2) / 48;
        int var7 = (24 + var1) / 48;
        this.loadSection(var7 - 1, 0, -1 + var6, true, var5);
        this.loadSection(-1 + var7, 1, var6, true, var5);
        this.loadSection(var7, 2, -1 + var6, true, var5);
        this.loadSection(var7, 3, var6, true, var5);
        this.method260((byte) 85);
        if (var3 == 94) {
            if (this.aGameModel_261 == null) {
                this.aGameModel_261 = new GameModel(18688, 18688, true, true, false, false, true);
            }

            int var8;
            int var9;
            int var10;
            int var11;
            int var12;
            int var13;
            int var14;
            int var15;
            int var16;
            int var19;
            int var18;
            int var21;
            int var44;
            int var45;
            if (var4) {
                this.aSurface_233.blackScreen((byte) 127);

                for (var8 = 0; var8 < 96; ++var8) {
                    for (var9 = 0; var9 < 96; ++var9) {
                        this.anIntArrayArray276[var8][var9] = 0;
                    }
                }

                GameModel var42 = this.aGameModel_261;
                var42.method580((byte) 112);

                for (var10 = 0; var10 < 96; ++var10) {
                    for (var11 = 0; var11 < 96; ++var11) {
                        var12 = -this.getTerrainHeight(var11, (byte) 66, var10);
                        if (this.getTileDecoration(var10, var11, var5, var3 ^ 42) > 0 && GameData.tileType[this.getTileDecoration(var10, var11, var5, 84) - 1] == 4) {
                            var12 = 0;
                        }

                        if (this.getTileDecoration(-1 + var10, var11, var5, 87) > 0 && GameData.tileType[-1 + this.getTileDecoration(var10 - 1, var11, var5, 123)] == 4) {
                            var12 = 0;
                        }

                        if (this.getTileDecoration(var10, var11 - 1, var5, 107) > 0 && GameData.tileType[-1 + this.getTileDecoration(var10, -1 + var11, var5, 123)] == 4) {
                            var12 = 0;
                        }

                        if (this.getTileDecoration(-1 + var10, -1 + var11, var5, 105) > 0 && GameData.tileType[this.getTileDecoration(var10 + -1, var11 - 1, var5, var3 + 31) + -1] == 4) {
                            var12 = 0;
                        }

                        var13 = var42.method561(128 * var10, (byte) 89, var12, 128 * var11);
                        var14 = -5 + (int) (Math.random() * 10.0D);
                        var42.method560(var14, var13, true);
                    }
                }

                int[] var43;
                for (var11 = 0; var11 < 95; ++var11) {
                    for (var12 = 0; var12 < 95; ++var12) {
                        var13 = this.method276(var12, var11, -5098);
                        var14 = this.anIntArray235[var13];
                        var15 = var14;
                        var16 = var14;
                        byte var17 = 0;
                        if (var5 == 1 || var5 == 2) {
                            var15 = 12345678;
                            var16 = 12345678;
                            var14 = 12345678;
                        }

                        if (this.getTileDecoration(var11, var12, var5, 87) > 0) {
                            var18 = this.getTileDecoration(var11, var12, var5, 101);
                            var13 = GameData.tileType[var18 - 1];
                            var19 = this.method262(-14928, var5, var11, var12);
                            var14 = var15 = GameData.tileDecoration[var18 - 1];
                            if (var13 == 4) {
                                var15 = 1;
                                var14 = 1;
                                if (var18 == 12) {
                                    var15 = 31;
                                    var14 = 31;
                                }
                            }

                            if (var13 == 5) {
                                if (this.getWallDiagonal(var12, var11, 0) > 0 && this.getWallDiagonal(var12, var11, 0) < 24000) {
                                    if (this.getTileDecorationSprite(var5, 23470, var12, -1 + var11, var16) != 12345678 && this.getTileDecorationSprite(var5, var3 ^ 23536, var12 - 1, var11, var16) != 12345678) {
                                        var14 = this.getTileDecorationSprite(var5, 23470, var12, -1 + var11, var16);
                                        var17 = 0;
                                    } else if (this.getTileDecorationSprite(var5, 23470, var12, 1 + var11, var16) != 12345678 && this.getTileDecorationSprite(var5, var3 + 23376, var12 - -1, var11, var16) != 12345678) {
                                        var15 = this.getTileDecorationSprite(var5, 23470, var12, var11 - -1, var16);
                                        var17 = 0;
                                    } else if (this.getTileDecorationSprite(var5, 23470, var12, 1 + var11, var16) != 12345678 && this.getTileDecorationSprite(var5, 23470, -1 + var12, var11, var16) != 12345678) {
                                        var15 = this.getTileDecorationSprite(var5, 23470, var12, 1 + var11, var16);
                                        var17 = 1;
                                    } else if (this.getTileDecorationSprite(var5, 23470, var12, -1 + var11, var16) != 12345678 && this.getTileDecorationSprite(var5, 23470, var12 + 1, var11, var16) != 12345678) {
                                        var14 = this.getTileDecorationSprite(var5, 23470, var12, var11 - 1, var16);
                                        var17 = 1;
                                    }
                                }
                            } else if (var13 != 2 || this.getWallDiagonal(var12, var11, 0) > 0 && this.getWallDiagonal(var12, var11, var3 ^ 94) < 24000) {
                                if (this.method262(var3 + -15022, var5, -1 + var11, var12) != var19 && this.method262(-14928, var5, var11, var12 + -1) != var19) {
                                    var14 = var16;
                                    var17 = 0;
                                } else if (var19 != this.method262(var3 ^ -14866, var5, 1 + var11, var12) && var19 != this.method262(-14928, var5, var11, var12 - -1)) {
                                    var17 = 0;
                                    var15 = var16;
                                } else if (this.method262(-14928, var5, var11 + 1, var12) != var19 && var19 != this.method262(-14928, var5, var11, -1 + var12)) {
                                    var17 = 1;
                                    var15 = var16;
                                } else if (this.method262(-14928, var5, var11 + -1, var12) != var19 && var19 != this.method262(-14928, var5, var11, var12 - -1)) {
                                    var17 = 1;
                                    var14 = var16;
                                }
                            }

                            if (GameData.tileAdjacent[-1 + var18] != 0) {
                                this.anIntArrayArray276[var11][var12] = Utility.bitwiseOr(this.anIntArrayArray276[var11][var12], 64);
                            }

                            if (GameData.tileType[var18 - 1] == 2) {
                                this.anIntArrayArray276[var11][var12] = Utility.bitwiseOr(this.anIntArrayArray276[var11][var12], 128);
                            }
                        }

                        this.method283(var12, var17, (byte) 127, var15, var11, var14);
                        var18 = this.getTerrainHeight(1 + var12, (byte) 66, 1 + var11) + -this.getTerrainHeight(var12, (byte) 66, var11 - -1) + this.getTerrainHeight(1 + var12, (byte) 66, var11) + -this.getTerrainHeight(var12, (byte) 66, var11);
                        if (var14 == var15 && var18 == 0) {
                            if (var14 != 12345678) {
                                var43 = new int[]{96 + (var12 - -(96 * var11)), 96 * var11 + var12, 1 + 96 * var11 + var12, 1 + var12 - -(var11 * 96) + 96};
                                var45 = var42.method549(4, var14, (byte) -110, var43, 12345678);
                                this.anIntArray244[var45] = var11;
                                this.anIntArray266[var45] = var12;
                                var42.faceTag[var45] = 200000 - -var45;
                            }
                        } else {
                            var43 = new int[3];
                            int[] var20 = new int[3];
                            if (var17 != 0) {
                                if (var14 != 12345678) {
                                    var43[1] = 96 + 96 * var11 + var12 + 1;
                                    var43[0] = 1 + var12 + var11 * 96;
                                    var43[2] = var11 * 96 + var12;
                                    var21 = var42.method549(3, var14, (byte) -117, var43, 12345678);
                                    this.anIntArray244[var21] = var11;
                                    this.anIntArray266[var21] = var12;
                                    var42.faceTag[var21] = 200000 + var21;
                                }

                                if (var15 != 12345678) {
                                    var20[1] = var12 + 96 * var11;
                                    var20[2] = 1 + var12 - (-(96 * var11) + -96);
                                    var20[0] = 96 * var11 + var12 - -96;
                                    var21 = var42.method549(3, var15, (byte) -121, var20, 12345678);
                                    this.anIntArray244[var21] = var11;
                                    this.anIntArray266[var21] = var12;
                                    var42.faceTag[var21] = 200000 + var21;
                                }
                            } else {
                                if (var14 != 12345678) {
                                    var43[0] = 96 + 96 * var11 + var12;
                                    var43[1] = var12 - -(96 * var11);
                                    var43[2] = var12 - (-(var11 * 96) + -1);
                                    var21 = var42.method549(3, var14, (byte) -96, var43, 12345678);
                                    this.anIntArray244[var21] = var11;
                                    this.anIntArray266[var21] = var12;
                                    var42.faceTag[var21] = 200000 - -var21;
                                }

                                if (var15 != 12345678) {
                                    var20[1] = 1 + var12 + var11 * 96 + 96;
                                    var20[2] = 96 * var11 + var12 + 96;
                                    var20[0] = 1 + 96 * var11 + var12;
                                    var21 = var42.method549(3, var15, (byte) -121, var20, 12345678);
                                    this.anIntArray244[var21] = var11;
                                    this.anIntArray266[var21] = var12;
                                    var42.faceTag[var21] = var21 + 200000;
                                }
                            }
                        }
                    }
                }

                for (var12 = 1; var12 < 95; ++var12) {
                    for (var13 = 1; var13 < 95; ++var13) {
                        if (this.getTileDecoration(var12, var13, var5, 90) > 0 && GameData.tileType[this.getTileDecoration(var12, var13, var5, var3 + -12) + -1] == 4) {
                            var14 = GameData.tileDecoration[this.getTileDecoration(var12, var13, var5, 92) + -1];
                            var15 = var42.method561(128 * var12, (byte) 121, -this.getTerrainHeight(var13, (byte) 66, var12), 128 * var13);
                            var16 = var42.method561(var12 * 128 - -128, (byte) 101, -this.getTerrainHeight(var13, (byte) 66, 1 + var12), var13 * 128);
                            var44 = var42.method561((1 + var12) * 128, (byte) 105, -this.getTerrainHeight(var13 - -1, (byte) 66, 1 + var12), 128 * (1 + var13));
                            var18 = var42.method561(128 * var12, (byte) 93, -this.getTerrainHeight(var13 - -1, (byte) 66, var12), var13 * 128 - -128);
                            var43 = new int[]{var15, var16, var44, var18};
                            var45 = var42.method549(4, 12345678, (byte) -105, var43, var14);
                            this.anIntArray244[var45] = var12;
                            this.anIntArray266[var45] = var13;
                            var42.faceTag[var45] = 200000 - -var45;
                            this.method283(var13, 0, (byte) -92, var14, var12, var14);
                        } else if (this.getTileDecoration(var12, var13, var5, 82) == 0 || GameData.tileType[this.getTileDecoration(var12, var13, var5, 112) + -1] != 3) {
                            if (this.getTileDecoration(var12, var13 + 1, var5, 97) > 0 && GameData.tileType[-1 + this.getTileDecoration(var12, var13 + 1, var5, var3 ^ 2)] == 4) {
                                var14 = GameData.tileDecoration[-1 + this.getTileDecoration(var12, 1 + var13, var5, 99)];
                                var15 = var42.method561(var12 * 128, (byte) 104, -this.getTerrainHeight(var13, (byte) 66, var12), 128 * var13);
                                var16 = var42.method561((1 + var12) * 128, (byte) 83, -this.getTerrainHeight(var13, (byte) 66, var12 + 1), var13 * 128);
                                var44 = var42.method561(128 * (var12 + 1), (byte) 91, -this.getTerrainHeight(1 + var13, (byte) 66, 1 + var12), var13 * 128 - -128);
                                var18 = var42.method561(128 * var12, (byte) 112, -this.getTerrainHeight(1 + var13, (byte) 66, var12), 128 * (var13 - -1));
                                var43 = new int[]{var15, var16, var44, var18};
                                var45 = var42.method549(4, 12345678, (byte) -108, var43, var14);
                                this.anIntArray244[var45] = var12;
                                this.anIntArray266[var45] = var13;
                                var42.faceTag[var45] = var45 + 200000;
                                this.method283(var13, 0, (byte) -111, var14, var12, var14);
                            }

                            if (this.getTileDecoration(var12, -1 + var13, var5, 98) > 0 && GameData.tileType[this.getTileDecoration(var12, -1 + var13, var5, 95) + -1] == 4) {
                                var14 = GameData.tileDecoration[this.getTileDecoration(var12, var13 + -1, var5, var3 ^ 32) - 1];
                                var15 = var42.method561(128 * var12, (byte) 117, -this.getTerrainHeight(var13, (byte) 66, var12), 128 * var13);
                                var16 = var42.method561((1 + var12) * 128, (byte) 103, -this.getTerrainHeight(var13, (byte) 66, 1 + var12), 128 * var13);
                                var44 = var42.method561(128 + 128 * var12, (byte) 95, -this.getTerrainHeight(1 + var13, (byte) 66, 1 + var12), (var13 - -1) * 128);
                                var18 = var42.method561(var12 * 128, (byte) 120, -this.getTerrainHeight(1 + var13, (byte) 66, var12), (1 + var13) * 128);
                                var43 = new int[]{var15, var16, var44, var18};
                                var45 = var42.method549(4, 12345678, (byte) -113, var43, var14);
                                this.anIntArray244[var45] = var12;
                                this.anIntArray266[var45] = var13;
                                var42.faceTag[var45] = 200000 + var45;
                                this.method283(var13, 0, (byte) 116, var14, var12, var14);
                            }

                            if (this.getTileDecoration(var12 - -1, var13, var5, 105) > 0 && GameData.tileType[-1 + this.getTileDecoration(var12 + 1, var13, var5, 124)] == 4) {
                                var14 = GameData.tileDecoration[-1 + this.getTileDecoration(1 + var12, var13, var5, var3 ^ 19)];
                                var15 = var42.method561(128 * var12, (byte) 96, -this.getTerrainHeight(var13, (byte) 66, var12), 128 * var13);
                                var16 = var42.method561(128 * (1 + var12), (byte) 123, -this.getTerrainHeight(var13, (byte) 66, var12 - -1), 128 * var13);
                                var44 = var42.method561(128 + 128 * var12, (byte) 88, -this.getTerrainHeight(var13 - -1, (byte) 66, var12 + 1), var13 * 128 - -128);
                                var18 = var42.method561(128 * var12, (byte) 107, -this.getTerrainHeight(var13 + 1, (byte) 66, var12), 128 + 128 * var13);
                                var43 = new int[]{var15, var16, var44, var18};
                                var45 = var42.method549(4, 12345678, (byte) -98, var43, var14);
                                this.anIntArray244[var45] = var12;
                                this.anIntArray266[var45] = var13;
                                var42.faceTag[var45] = 200000 + var45;
                                this.method283(var13, 0, (byte) -96, var14, var12, var14);
                            }

                            if (this.getTileDecoration(-1 + var12, var13, var5, 99) > 0 && GameData.tileType[this.getTileDecoration(-1 + var12, var13, var5, 100) - 1] == 4) {
                                var14 = GameData.tileDecoration[-1 + this.getTileDecoration(var12 + -1, var13, var5, var3 + -10)];
                                var15 = var42.method561(var12 * 128, (byte) 109, -this.getTerrainHeight(var13, (byte) 66, var12), var13 * 128);
                                var16 = var42.method561((1 + var12) * 128, (byte) 86, -this.getTerrainHeight(var13, (byte) 66, 1 + var12), var13 * 128);
                                var44 = var42.method561(128 * var12 - -128, (byte) 121, -this.getTerrainHeight(var13 + 1, (byte) 66, 1 + var12), var13 * 128 - -128);
                                var18 = var42.method561(var12 * 128, (byte) 90, -this.getTerrainHeight(1 + var13, (byte) 66, var12), 128 * (var13 - -1));
                                var43 = new int[]{var15, var16, var44, var18};
                                var45 = var42.method549(4, 12345678, (byte) -108, var43, var14);
                                this.anIntArray244[var45] = var12;
                                this.anIntArray266[var45] = var13;
                                var42.faceTag[var45] = var45 + 200000;
                                this.method283(var13, 0, (byte) 74, var14, var12, var14);
                            }
                        }
                    }
                }

                var42.setLight(-10, 40, true, (byte) -68, 48, -50, -50);
                this.aGameModelArray223 = this.aGameModel_261.method557(0, 0, 233, 1536, false, 0, 64, 8, 1536);

                for (var13 = 0; var13 < 64; ++var13) {
                    this.aScene_234.method325(this.aGameModelArray223[var13], (byte) 6);
                }

                for (var14 = 0; var14 < 96; ++var14) {
                    for (var15 = 0; var15 < 96; ++var15) {
                        this.anIntArrayArray249[var14][var15] = this.getTerrainHeight(var15, (byte) 66, var14);
                    }
                }
            }

            this.aGameModel_261.method580((byte) 107);
            var8 = 6316128;

            for (var9 = 0; var9 < 95; ++var9) {
                for (var10 = 0; var10 < 95; ++var10) {
                    var11 = this.method252(var10, false, var9);
                    if (var11 > 0 && (GameData.wallObjectInvisible[-1 + var11] == 0 || this.aBoolean275)) {
                        this.method281(var9, 5080, var10, 1 + var9, var11 + -1, this.aGameModel_261, var10);
                        if (var4 && GameData.wallObjectAdjacent[var11 + -1] != 0) {
                            this.anIntArrayArray276[var9][var10] = Utility.bitwiseOr(this.anIntArrayArray276[var9][var10], 1);
                            if (var10 > 0) {
                                this.method256(4, var10 + -1, (byte) 91, var9);
                            }
                        }

                        if (var4) {
                            this.aSurface_233.drawLineHoriz(var9 * 3, 3 * var10, 3, var8);
                        }
                    }

                    var11 = this.getWallsNorthSouth(var9, var10, (byte) 86);
                    if (var11 > 0 && (GameData.wallObjectInvisible[-1 + var11] == 0 || this.aBoolean275)) {
                        this.method281(var9, 5080, var10, var9, var11 + -1, this.aGameModel_261, 1 + var10);
                        if (var4 && GameData.wallObjectAdjacent[var11 - 1] != 0) {
                            this.anIntArrayArray276[var9][var10] = Utility.bitwiseOr(this.anIntArrayArray276[var9][var10], 2);
                            if (var9 > 0) {
                                this.method256(8, var10, (byte) 90, var9 + -1);
                            }
                        }

                        if (var4) {
                            this.aSurface_233.drawLineVert(3 * var9, var10 * 3, 3, var8);
                        }
                    }

                    var11 = this.getWallDiagonal(var10, var9, var3 + -94);
                    if (var11 > 0 && var11 < 12000 && (GameData.wallObjectInvisible[var11 - 1] == 0 || this.aBoolean275)) {
                        this.method281(var9, 5080, var10, 1 + var9, -1 + var11, this.aGameModel_261, var10 - -1);
                        if (var4 && GameData.wallObjectAdjacent[var11 - 1] != 0) {
                            this.anIntArrayArray276[var9][var10] = Utility.bitwiseOr(this.anIntArrayArray276[var9][var10], 32);
                        }

                        if (var4) {
                            this.aSurface_233.method401((byte) 118, var10 * 3, 3 * var9, var8);
                            this.aSurface_233.method401((byte) 118, 1 + var10 * 3, 1 + 3 * var9, var8);
                            this.aSurface_233.method401((byte) 118, 3 * var10 + 2, 2 + var9 * 3, var8);
                        }
                    }

                    if (var11 > 12000 && var11 < 24000 && (GameData.wallObjectInvisible[var11 - 12001] == 0 || this.aBoolean275)) {
                        this.method281(1 + var9, var3 + 4986, var10, var9, -12001 + var11, this.aGameModel_261, 1 + var10);
                        if (var4 && GameData.wallObjectAdjacent[-12001 + var11] != 0) {
                            this.anIntArrayArray276[var9][var10] = Utility.bitwiseOr(this.anIntArrayArray276[var9][var10], 16);
                        }

                        if (var4) {
                            this.aSurface_233.method401((byte) 118, var10 * 3, 3 * var9 + 2, var8);
                            this.aSurface_233.method401((byte) 118, 3 * var10 + 1, 1 + 3 * var9, var8);
                            this.aSurface_233.method401((byte) 118, 2 + 3 * var10, 3 * var9, var8);
                        }
                    }
                }
            }

            if (var4) {
                this.aSurface_233.method424(285, 285, 0, 0, -1 + this.baseMediaSprite, 0);
            }

            this.aGameModel_261.setLight(-10, 60, false, (byte) -85, 24, -50, -50);
            this.aGameModelArrayArray246[var5] = this.aGameModel_261.method557(0, 0, 338, 1536, true, 0, 64, 8, 1536);

            for (var10 = 0; var10 < 64; ++var10) {
                this.aScene_234.method325(this.aGameModelArrayArray246[var5][var10], (byte) 6);
            }

            for (var11 = 0; var11 < 95; ++var11) {
                for (var12 = 0; var12 < 95; ++var12) {
                    var13 = this.method252(var12, false, var11);
                    if (var13 > 0) {
                        this.method280(var12, -1 + var13, 1 + var11, (byte) 23, var12, var11);
                    }

                    var13 = this.getWallsNorthSouth(var11, var12, (byte) 68);
                    if (var13 > 0) {
                        this.method280(1 + var12, var13 + -1, var11, (byte) 23, var12, var11);
                    }

                    var13 = this.getWallDiagonal(var12, var11, 0);
                    if (var13 > 0 && var13 < 12000) {
                        this.method280(1 + var12, -1 + var13, 1 + var11, (byte) 23, var12, var11);
                    }

                    if (var13 > 12000 && var13 < 24000) {
                        this.method280(1 + var12, -12001 + var13, var11, (byte) 23, var12, 1 + var11);
                    }
                }
            }

            int var23;
            int var25;
            int var24;
            int var27;
            int var26;
            for (var12 = 1; var12 < 95; ++var12) {
                for (var13 = 1; var13 < 95; ++var13) {
                    var14 = this.method284(var12, -12345679, var13);
                    if (var14 > 0) {
                        var44 = var12 + 1;
                        var19 = var12 + 1;
                        var45 = 1 + var13;
                        int var22 = 1 + var13;
                        var23 = 0;
                        var24 = this.anIntArrayArray249[var12][var13];
                        var25 = this.anIntArrayArray249[var44][var13];
                        var26 = this.anIntArrayArray249[var19][var45];
                        var27 = this.anIntArrayArray249[var12][var22];
                        if (var26 > 80000) {
                            var26 -= 80000;
                        }

                        if (var25 > 80000) {
                            var25 -= 80000;
                        }

                        if (var24 > 80000) {
                            var24 -= 80000;
                        }

                        if (var27 > 80000) {
                            var27 -= 80000;
                        }

                        if (var24 > var23) {
                            var23 = var24;
                        }

                        if (var23 < var25) {
                            var23 = var25;
                        }

                        if (var26 > var23) {
                            var23 = var26;
                        }

                        if (var23 < var27) {
                            var23 = var27;
                        }

                        if (var23 >= 80000) {
                            var23 -= 80000;
                        }

                        if (var24 < 80000) {
                            this.anIntArrayArray249[var12][var13] = var23;
                        } else {
                            this.anIntArrayArray249[var12][var13] -= 80000;
                        }

                        if (var25 < 80000) {
                            this.anIntArrayArray249[var44][var13] = var23;
                        } else {
                            this.anIntArrayArray249[var44][var13] -= 80000;
                        }

                        if (var26 < 80000) {
                            this.anIntArrayArray249[var19][var45] = var23;
                        } else {
                            this.anIntArrayArray249[var19][var45] -= 80000;
                        }

                        if (var27 >= 80000) {
                            this.anIntArrayArray249[var12][var22] -= 80000;
                        } else {
                            this.anIntArrayArray249[var12][var22] = var23;
                        }
                    }
                }
            }

            this.aGameModel_261.method580((byte) 125);

            for (var13 = 1; var13 < 95; ++var13) {
                for (var14 = 1; var14 < 95; ++var14) {
                    var15 = this.method284(var13, -12345679, var14);
                    if (var15 > 0) {
                        var18 = var13 - -1;
                        var45 = var13 + 1;
                        var21 = var14 + 1;
                        var23 = 1 + var14;
                        var24 = var13 * 128;
                        var25 = var14 * 128;
                        var26 = var24 - -128;
                        var27 = var25 + 128;
                        int var28 = var24;
                        int var29 = var25;
                        int var30 = var26;
                        int var31 = var27;
                        int var32 = this.anIntArrayArray249[var13][var14];
                        int var33 = this.anIntArrayArray249[var18][var14];
                        int var34 = this.anIntArrayArray249[var45][var21];
                        int var35 = this.anIntArrayArray249[var13][var23];
                        int var36 = GameData.roofHeight[-1 + var15];
                        if (this.method266(var14, var13, var3 + -95) && var32 < 80000) {
                            var32 += 80000 + var36;
                            this.anIntArrayArray249[var13][var14] = var32;
                        }

                        if (this.method266(var14, var18, -1) && var33 < 80000) {
                            var33 += var36 - -80000;
                            this.anIntArrayArray249[var18][var14] = var33;
                        }

                        if (this.method266(var21, var45, var3 + -95) && var34 < 80000) {
                            var34 += var36 + 80000;
                            this.anIntArrayArray249[var45][var21] = var34;
                        }

                        if (var33 >= 80000) {
                            var33 -= 80000;
                        }

                        if (var32 >= 80000) {
                            var32 -= 80000;
                        }

                        if (this.method266(var23, var13, -1) && var35 < 80000) {
                            var35 += 80000 + var36;
                            this.anIntArrayArray249[var13][var23] = var35;
                        }

                        if (var34 >= 80000) {
                            var34 -= 80000;
                        }

                        if (var35 >= 80000) {
                            var35 -= 80000;
                        }

                        byte var37 = 16;
                        if (!this.method258(-1, var13 + -1, var14)) {
                            var24 -= var37;
                        }

                        if (!this.method258(var3 ^ -95, 1 + var13, var14)) {
                            var24 += var37;
                        }

                        if (!this.method258(-1, var13, -1 + var14)) {
                            var25 -= var37;
                        }

                        if (!this.method258(-1, var13, 1 + var14)) {
                            var25 += var37;
                        }

                        if (!this.method258(var3 ^ -95, var18 - 1, var14)) {
                            var26 -= var37;
                        }

                        if (!this.method258(-1, 1 + var18, var14)) {
                            var26 += var37;
                        }

                        if (!this.method258(-1, var18, -1 + var14)) {
                            var29 = var25 - var37;
                        }

                        if (!this.method258(-1, var18, var14 - -1)) {
                            var29 += var37;
                        }

                        if (!this.method258(-1, -1 + var45, var21)) {
                            var30 = var26 - var37;
                        }

                        if (!this.method258(-1, var45 + 1, var21)) {
                            var30 += var37;
                        }

                        if (!this.method258(-1, var45, var21 - 1)) {
                            var27 -= var37;
                        }

                        if (!this.method258(-1, var45, 1 + var21)) {
                            var27 += var37;
                        }

                        if (!this.method258(-1, -1 + var13, var23)) {
                            var28 = var24 - var37;
                        }

                        if (!this.method258(var3 + -95, var13 - -1, var23)) {
                            var28 += var37;
                        }

                        if (!this.method258(-1, var13, var23 + -1)) {
                            var31 = var27 - var37;
                        }

                        if (!this.method258(var3 ^ -95, var13, 1 + var23)) {
                            var31 += var37;
                        }

                        var32 = -var32;
                        var34 = -var34;
                        var35 = -var35;
                        var33 = -var33;
                        var15 = GameData.roofNumVertice[var15 - 1];
                        int[] var46;
                        if (this.getWallDiagonal(var14, var13, 0) > 12000 && this.getWallDiagonal(var14, var13, 0) < 24000 && this.method284(-1 + var13, -12345679, var14 + -1) == 0) {
                            var46 = new int[]{this.aGameModel_261.method561(var30, (byte) 94, var34, var27), this.aGameModel_261.method561(var28, (byte) 108, var35, var31), this.aGameModel_261.method561(var26, (byte) 125, var33, var29)};
                            this.aGameModel_261.method549(3, 12345678, (byte) -106, var46, var15);
                        } else if (this.getWallDiagonal(var14, var13, var3 + -94) > 12000 && this.getWallDiagonal(var14, var13, 0) < 24000 && this.method284(1 + var13, var3 + -12345773, var14 - -1) == 0) {
                            var46 = new int[]{this.aGameModel_261.method561(var24, (byte) 98, var32, var25), this.aGameModel_261.method561(var26, (byte) 104, var33, var29), this.aGameModel_261.method561(var28, (byte) 120, var35, var31)};
                            this.aGameModel_261.method549(3, 12345678, (byte) -126, var46, var15);
                        } else if (this.getWallDiagonal(var14, var13, 0) > 0 && this.getWallDiagonal(var14, var13, var3 ^ 94) < 12000 && this.method284(1 + var13, var3 ^ -12345617, var14 - 1) == 0) {
                            var46 = new int[]{this.aGameModel_261.method561(var28, (byte) 123, var35, var31), this.aGameModel_261.method561(var24, (byte) 103, var32, var25), this.aGameModel_261.method561(var30, (byte) 111, var34, var27)};
                            this.aGameModel_261.method549(3, 12345678, (byte) -107, var46, var15);
                        } else if (this.getWallDiagonal(var14, var13, 0) > 0 && this.getWallDiagonal(var14, var13, var3 ^ 94) < 12000 && this.method284(var13 + -1, -12345679, var14 + 1) == 0) {
                            var46 = new int[]{this.aGameModel_261.method561(var26, (byte) 83, var33, var29), this.aGameModel_261.method561(var30, (byte) 125, var34, var27), this.aGameModel_261.method561(var24, (byte) 116, var32, var25)};
                            this.aGameModel_261.method549(3, 12345678, (byte) -104, var46, var15);
                        } else if (var33 == var32 && var34 == var35) {
                            var46 = new int[]{this.aGameModel_261.method561(var24, (byte) 105, var32, var25), this.aGameModel_261.method561(var26, (byte) 120, var33, var29), this.aGameModel_261.method561(var30, (byte) 89, var34, var27), this.aGameModel_261.method561(var28, (byte) 120, var35, var31)};
                            this.aGameModel_261.method549(4, 12345678, (byte) -111, var46, var15);
                        } else if (var35 == var32 && var34 == var33) {
                            var46 = new int[]{this.aGameModel_261.method561(var28, (byte) 95, var35, var31), this.aGameModel_261.method561(var24, (byte) 111, var32, var25), this.aGameModel_261.method561(var26, (byte) 87, var33, var29), this.aGameModel_261.method561(var30, (byte) 124, var34, var27)};
                            this.aGameModel_261.method549(4, 12345678, (byte) -120, var46, var15);
                        } else {
                            boolean var38 = true;
                            if (this.method284(var13 + -1, -12345679, var14 - 1) > 0) {
                                var38 = false;
                            }

                            if (this.method284(var13 + 1, -12345679, 1 + var14) > 0) {
                                var38 = false;
                            }

                            int[] var39;
                            int[] var40;
                            if (!var38) {
                                var39 = new int[]{this.aGameModel_261.method561(var26, (byte) 121, var33, var29), this.aGameModel_261.method561(var30, (byte) 95, var34, var27), this.aGameModel_261.method561(var24, (byte) 115, var32, var25)};
                                this.aGameModel_261.method549(3, 12345678, (byte) -114, var39, var15);
                                var40 = new int[]{this.aGameModel_261.method561(var28, (byte) 83, var35, var31), this.aGameModel_261.method561(var24, (byte) 113, var32, var25), this.aGameModel_261.method561(var30, (byte) 108, var34, var27)};
                                this.aGameModel_261.method549(3, 12345678, (byte) -107, var40, var15);
                            } else {
                                var39 = new int[]{this.aGameModel_261.method561(var24, (byte) 104, var32, var25), this.aGameModel_261.method561(var26, (byte) 92, var33, var29), this.aGameModel_261.method561(var28, (byte) 92, var35, var31)};
                                this.aGameModel_261.method549(3, 12345678, (byte) -105, var39, var15);
                                var40 = new int[]{this.aGameModel_261.method561(var30, (byte) 92, var34, var27), this.aGameModel_261.method561(var28, (byte) 119, var35, var31), this.aGameModel_261.method561(var26, (byte) 90, var33, var29)};
                                this.aGameModel_261.method549(3, 12345678, (byte) -109, var40, var15);
                            }
                        }
                    }
                }
            }

            this.aGameModel_261.setLight(-10, 50, true, (byte) -44, 50, -50, -50);
            this.aGameModelArrayArray236[var5] = this.aGameModel_261.method557(0, 0, 169, 1536, true, 0, 64, 8, 1536);

            for (var14 = 0; var14 < 64; ++var14) {
                this.aScene_234.method325(this.aGameModelArrayArray236[var5][var14], (byte) 6);
            }

            if (this.aGameModelArrayArray236[var5][0] == null) {
                throw new RuntimeException("null roof!");
            } else {
                for (var15 = 0; var15 < 96; ++var15) {
                    for (var16 = 0; var16 < 96; ++var16) {
                        if (this.anIntArrayArray249[var15][var16] >= 80000) {
                            this.anIntArrayArray249[var15][var16] -= 80000;
                        }
                    }
                }

            }
        }
        ;
    }

    private int getWallDiagonal(int var1, int var2, int var3) {
        if (var2 >= 0 && var2 < 96 && var1 >= 0 && var1 < 96) {
            int var4 = var3;
            if (var2 >= 48 && var1 < 48) {
                var4 = 1;
                var2 -= 48;
            } else if (var2 < 48 && var1 >= 48) {
                var4 = 2;
                var1 -= 48;
            } else if (var2 >= 48 && var1 >= 48) {
                var1 -= 48;
                var4 = 3;
                var2 -= 48;
            }

            return this.wallsDiagonal[var4][48 * var2 - -var1];
        } else {
            return 0;
        }
    }

    private void method256(int var1, int var2, byte var3, int var4) {
        this.anIntArrayArray276[var4][var2] = Utility.bitwiseOr(this.anIntArrayArray276[var4][var2], var1);
        if (var3 < 79) {
            this.aBoolean257 = false;
        }
    }

    final int getTileDirection(int var1, boolean var2, int var3) {
        if (var1 >= 0 && var1 < 96 && var3 >= 0 && var3 < 96) {
            byte var4 = 0;
            if (!var2) {
                this.aBoolean257 = false;
            }

            if (var1 >= 48 && var3 < 48) {
                var1 -= 48;
                var4 = 1;
            } else if (var1 < 48 && var3 >= 48) {
                var4 = 2;
                var3 -= 48;
            } else if (var1 >= 48 && var3 >= 48) {
                var4 = 3;
                var3 -= 48;
                var1 -= 48;
            }

            return this.tileDirection[var4][48 * var1 + var3];
        } else {
            return 0;
        }
    }

    private boolean method258(int var1, int var2, int var3) {
        return var1 > ~this.method284(var2, -12345679, var3) || this.method284(-1 + var2, -12345679, var3) > 0 || this.method284(-1 + var2, -12345679, -1 + var3) > 0 || this.method284(var2, -12345679, var3 + -1) > 0;
    }

    private int getTerrainHeight(int var1, byte var2, int var3) {
        if (var2 != 66) {
            this.memberLandscapePack = null;
        }

        if (var3 >= 0 && var3 < 96 && var1 >= 0 && var1 < 96) {
            byte var4 = 0;
            if (var3 >= 48 && var1 < 48) {
                var3 -= 48;
                var4 = 1;
            } else if (var3 < 48 && var1 >= 48) {
                var4 = 2;
                var1 -= 48;
            } else if (var3 >= 48 && var1 >= 48) {
                var1 -= 48;
                var4 = 3;
                var3 -= 48;
            }

            return 3 * (255 & this.terrainHeight[var4][48 * var3 - -var1]);
        } else {
            return 0;
        }
    }

    private void method260(byte var1) {
        for (int var2 = 0; var2 < 96; ++var2) {
            for (int var3 = 0; var3 < 96; ++var3) {
                if (this.getTileDecoration(var2, var3, 0, 122) == 250) {
                    if (var2 == 47 && this.getTileDecoration(var2 + 1, var3, 0, var1 + 36) != 250 && this.getTileDecoration(var2 - -1, var3, 0, 101) != 2) {
                        this.method278(var2, (byte) 126, 9, var3);
                    } else if (var3 == 47 && this.getTileDecoration(var2, 1 + var3, 0, var1 ^ 25) != 250 && this.getTileDecoration(var2, var3 + 1, 0, 116) != 2) {
                        this.method278(var2, (byte) 126, 9, var3);
                    } else {
                        this.method278(var2, (byte) 127, 2, var3);
                    }
                }
            }
        }

        if (var1 != 85) {
            this.wallsDiagonal = null;
        }
    }

    private void method261(int var1, int var2, int var3, int var4) {
        int var5 = var3 / 12;
        int var6 = var4 / 12;
        if (var2 != 2) {
            this.wallsRoof = null;
        }

        int var7 = (-1 + var3) / 12;
        this.method271(var3, var5, var1, var4, var6, 8);
        int var8 = (var4 - 1) / 12;
        if (var5 != var7) {
            this.method271(var3, var7, var1, var4, var6, 8);
        }

        if (var8 != var6) {
            this.method271(var3, var5, var1, var4, var8, 8);
        }

        if (var5 != var7 && var8 != var6) {
            this.method271(var3, var7, var1, var4, var8, 8);
        }
        ;
    }

    private int method262(int var1, int var2, int var3, int var4) {
        if (var1 != -14928) {
            this.removeWallObject(-84, -121, 106, 111, 64);
        }

        int var5 = this.getTileDecoration(var3, var4, var2, 102);
        if (var5 == 0) {
            return -1;
        } else {
            int var6 = GameData.tileType[var5 - 1];
            return var6 == 2 ? 1 : 0;
        }
    }

    final int method263(int[] var1, int var2, int var3, int var4, boolean var5, int[] var6, int var7, int var8, int var9, int var10) {
        int var12;
        for (int var11 = 0; var11 < 96; ++var11) {
            for (var12 = 0; var12 < 96; ++var12) {
                this.anIntArrayArray270[var11][var12] = 0;
            }
        }

        byte var21 = 0;
        int var13 = 0;
        int var14 = var9;
        this.anIntArrayArray270[var9][var8] = 99;
        int var15 = var8;
        var6[var21] = var9;
        var12 = var21 + 1;
        var1[var21] = var8;
        int var16 = var6.length;
        if (var10 != 3) {
            this.method281(122, 116, 127, -106, 26, (GameModel) null, 11);
        }

        boolean var17 = false;

        while (var12 != var13) {
            var15 = var1[var13];
            var14 = var6[var13];
            var13 = (var13 + 1) % var16;
            if (var14 >= var7 && var4 >= var14 && var3 <= var15 && var15 <= var2) {
                var17 = true;
                break;
            }

            if (var5) {
                if (var14 > 0 && var7 <= -1 + var14 && (-1 + var14) <= var4 && var15 >= var3 && var2 >= var15 && (8 & this.anIntArrayArray276[-1 + var14][var15]) == 0) {
                    var17 = true;
                    break;
                }

                if (var14 < 95 && var7 <= var14 - -1 && var4 >= (var14 + 1) && var15 >= var3 && var15 <= var2 && (2 & this.anIntArrayArray276[1 + var14][var15]) == 0) {
                    var17 = true;
                    break;
                }

                if (var15 > 0 && var14 >= var7 && var14 <= var4 && var3 <= -1 + var15 && -1 + var15 <= var2 && (4 & this.anIntArrayArray276[var14][-1 + var15]) == 0) {
                    var17 = true;
                    break;
                }

                if (var15 < 95 && var14 >= var7 && var4 >= var14 && var3 <= (var15 - -1) && var2 >= (1 + var15) && (1 & this.anIntArrayArray276[var14][var15 - -1]) == 0) {
                    var17 = true;
                    break;
                }
            }

            if (var14 > 0 && this.anIntArrayArray270[var14 - 1][var15] == 0 && (120 & this.anIntArrayArray276[var14 - 1][var15]) == 0) {
                var6[var12] = var14 + -1;
                var1[var12] = var15;
                var12 = (var12 + 1) % var16;
                this.anIntArrayArray270[var14 + -1][var15] = 2;
            }

            if (var14 < 95 && this.anIntArrayArray270[var14 + 1][var15] == 0 && (this.anIntArrayArray276[1 + var14][var15] & 114) == 0) {
                var6[var12] = 1 + var14;
                var1[var12] = var15;
                this.anIntArrayArray270[1 + var14][var15] = 8;
                var12 = (var12 - -1) % var16;
            }

            if (var15 > 0 && this.anIntArrayArray270[var14][var15 - 1] == 0 && (116 & this.anIntArrayArray276[var14][-1 + var15]) == 0) {
                var6[var12] = var14;
                var1[var12] = var15 + -1;
                var12 = (var12 - -1) % var16;
                this.anIntArrayArray270[var14][var15 + -1] = 1;
            }

            if (var15 < 95 && this.anIntArrayArray270[var14][1 + var15] == 0 && (this.anIntArrayArray276[var14][var15 + 1] & 113) == 0) {
                var6[var12] = var14;
                var1[var12] = 1 + var15;
                var12 = (var12 + 1) % var16;
                this.anIntArrayArray270[var14][1 + var15] = 4;
            }

            if (var14 > 0 && var15 > 0 && (this.anIntArrayArray276[var14][-1 + var15] & 116) == 0 && (120 & this.anIntArrayArray276[var14 + -1][var15]) == 0 && (124 & this.anIntArrayArray276[-1 + var14][-1 + var15]) == 0 && this.anIntArrayArray270[var14 - 1][-1 + var15] == 0) {
                var6[var12] = var14 + -1;
                var1[var12] = var15 - 1;
                this.anIntArrayArray270[var14 - 1][-1 + var15] = 3;
                var12 = (var12 + 1) % var16;
            }

            if (var14 < 95 && var15 > 0 && (116 & this.anIntArrayArray276[var14][var15 - 1]) == 0 && (this.anIntArrayArray276[var14 - -1][var15] & 114) == 0 && (this.anIntArrayArray276[var14 - -1][-1 + var15] & 118) == 0 && this.anIntArrayArray270[var14 + 1][-1 + var15] == 0) {
                var6[var12] = 1 + var14;
                var1[var12] = var15 - 1;
                this.anIntArrayArray270[1 + var14][-1 + var15] = 9;
                var12 = (1 + var12) % var16;
            }

            if (var14 > 0 && var15 < 95 && (113 & this.anIntArrayArray276[var14][1 + var15]) == 0 && (120 & this.anIntArrayArray276[var14 - 1][var15]) == 0 && (121 & this.anIntArrayArray276[-1 + var14][1 + var15]) == 0 && this.anIntArrayArray270[var14 + -1][1 + var15] == 0) {
                var6[var12] = var14 - 1;
                var1[var12] = 1 + var15;
                var12 = (var12 + 1) % var16;
                this.anIntArrayArray270[-1 + var14][1 + var15] = 6;
            }

            if (var14 < 95 && var15 < 95 && (this.anIntArrayArray276[var14][var15 - -1] & 113) == 0 && (this.anIntArrayArray276[var14 - -1][var15] & 114) == 0 && (this.anIntArrayArray276[1 + var14][1 + var15] & 115) == 0 && this.anIntArrayArray270[1 + var14][var15 + 1] == 0) {
                var6[var12] = 1 + var14;
                var1[var12] = var15 + 1;
                this.anIntArrayArray270[var14 + 1][var15 - -1] = 12;
                var12 = (var12 + 1) % var16;
            }
        }

        if (!var17) {
            return -1;
        } else {
            byte var22 = 0;
            var6[var22] = var14;
            var13 = var22 + 1;
            var1[var22] = var15;

            int var18;
            for (int var19 = var18 = this.anIntArrayArray270[var14][var15]; var9 != var14 || var15 != var8; var19 = this.anIntArrayArray270[var14][var15]) {
                if (var18 != var19) {
                    var6[var13] = var14;
                    var18 = var19;
                    var1[var13++] = var15;
                }

                if ((var19 & 2) != 0) {
                    ++var14;
                } else if ((var19 & 8) != 0) {
                    --var14;
                }

                if ((var19 & 1) == 0) {
                    if ((var19 & 4) != 0) {
                        --var15;
                    }
                } else {
                    ++var15;
                }
            }

            return var13;
        }
    }

    private int getWallsNorthSouth(int var1, int var2, byte var3) {
        if (var1 >= 0 && var1 < 96 && var2 >= 0 && var2 < 96) {
            byte var4 = 0;
            if (var1 >= 48 && var2 < 48) {
                var1 -= 48;
                var4 = 1;
            } else if (var1 < 48 && var2 >= 48) {
                var2 -= 48;
                var4 = 2;
            } else if (var1 >= 48 && var2 >= 48) {
                var1 -= 48;
                var2 -= 48;
                var4 = 3;
            }

            return var3 < 15 ? 16 : 255 & this.wallsNorthsouth[var4][48 * var1 + var2];
        } else {
            return 0;
        }
    }

    private boolean method266(int var1, int var2, int var3) {
        return this.method284(var2, -12345679, var1) > 0 && this.method284(-1 + var2, -12345679, var1) > 0 && this.method284(var2 - 1, -12345679, var1 - 1) > 0 && this.method284(var2, -12345679, -1 + var1) > 0 ? true : (var3 != -1 ? false : false);
    }

    private int getTileDecoration(int var1, int var2, int var3, int var4) {
        if (var4 <= 75) {
            return 57;
        } else if (var1 >= 0 && var1 < 96 && var2 >= 0 && var2 < 96) {
            byte var5 = 0;
            if (var1 >= 48 && var2 < 48) {
                var5 = 1;
                var1 -= 48;
            } else if (var1 < 48 && var2 >= 48) {
                var2 -= 48;
                var5 = 2;
            } else if (var1 >= 48 && var2 >= 48) {
                var1 -= 48;
                var5 = 3;
                var2 -= 48;
            }

            return 255 & this.tileDecoration[var5][48 * var1 + var2];
        } else {
            return 0;
        }
    }

    private int getTileDecorationSprite(int var1, int var2, int var3, int var4, int var5) {
        if (var2 != 23470) {
            return 85;
        } else {
            int var6 = this.getTileDecoration(var4, var3, var1, 88);
            return var6 == 0 ? var5 : GameData.tileDecoration[var6 + -1];
        }
    }

    final void method269(int var1, int var2, int var3, int var4) {
        this.method253(var3 ^ 7713);
        if (var3 != 5280) {
            this.terrainColour = null;
        }

        int var5 = (24 + var1) / 48;
        this.method254(var2, var1, (byte) 94, true, var4);
        int var6 = (24 + var2) / 48;
        if (var4 == 0) {
            this.method254(var2, var1, (byte) 94, false, 1);
            this.method254(var2, var1, (byte) 94, false, 2);
            this.loadSection(var6 + -1, 0, -1 + var5, true, var4);
            this.loadSection(var6 + -1, 1, var5, true, var4);
            this.loadSection(var6, 2, -1 + var5, true, var4);
            this.loadSection(var6, 3, var5, true, var4);
            this.method260((byte) 85);
        }
    }

    private void method270(int var1, byte var2, int var3, int var4, int var5) {
        if (var2 <= -124) {
            if (var3 >= 1 && var1 >= 1 && var4 + var3 < 96 && var1 - -var5 < 96) {
                for (int var6 = var3; var4 + var3 >= var6; ++var6) {
                    for (int var7 = var1; var7 <= var1 - -var5; ++var7) {
                        if ((99 & this.method272(var6, false, var7)) == 0 && (this.method272(-1 + var6, false, var7) & 89) == 0 && (86 & this.method272(var6, false, var7 - 1)) == 0 && (108 & this.method272(var6 + -1, false, var7 + -1)) == 0) {
                            this.method261(0, 2, var6, var7);
                        } else {
                            this.method261(35, 2, var6, var7);
                        }
                    }
                }

            }
        }
    }

    private void method271(int var1, int var2, int var3, int var4, int var5, int var6) {
        GameModel var7 = this.aGameModelArray223[var2 - -(var6 * var5)];

        for (int var8 = 0; var8 < var7.numVertices; ++var8) {
            if (var7.vertexX[var8] == (128 * var1) && var7.vertexZ[var8] == 128 * var4) {
                var7.method560(var3, var8, true);
                return;
            }
        }
    }

    private int method272(int var1, boolean var2, int var3) {
        if (var2) {
            this.baseMediaSprite = -4;
        }

        return var1 >= 0 && var3 >= 0 && var1 < 96 && var3 < 96 ? this.anIntArrayArray276[var1][var3] : 0;
    }

    final void removeWallObject(int var1, int var2, int var3, int var4, int var5) {
        if (var3 >= 0 && var2 >= 0 && var3 < 95 && var2 < 95) {
            if (GameData.wallObjectAdjacent[var5] == 1) {
                if (var4 != 0) {
                    if (var4 == 1) {
                        this.anIntArrayArray276[var3][var2] = Utility.bitwiseAnd(this.anIntArrayArray276[var3][var2], '\ufffd');
                        if (var3 > 0) {
                            this.method285('\uffff', var2, 8, -1 + var3);
                        }
                    } else if (var4 == 2) {
                        this.anIntArrayArray276[var3][var2] = Utility.bitwiseAnd(this.anIntArrayArray276[var3][var2], '\uffef');
                    } else if (var4 == 3) {
                        this.anIntArrayArray276[var3][var2] = Utility.bitwiseAnd(this.anIntArrayArray276[var3][var2], '\uffdf');
                    }
                } else {
                    this.anIntArrayArray276[var3][var2] = Utility.bitwiseAnd(this.anIntArrayArray276[var3][var2], '\ufffe');
                    if (var2 > 0) {
                        this.method285('\uffff', var2 - 1, 4, var3);
                    }
                }

                this.method270(var2, (byte) -126, var3, 1, 1);
            }

            int var6 = 22 / ((-62 - var1) / 62);
        }
    }

    final void method274(int var1, byte var2, int var3, int var4) {
        if (var3 >= 0 && var4 >= 0 && var3 < 95 && var4 < 95) {
            if (var2 == -113) {
                if (GameData.objectType[var1] == 1 || GameData.objectType[var1] == 2) {
                    int var5 = this.getTileDirection(var3, true, var4);
                    int var6;
                    int var7;
                    if (var5 != 0 && var5 != 4) {
                        var6 = GameData.objectWidth[var1];
                        var7 = GameData.objectHeight[var1];
                    } else {
                        var6 = GameData.objectHeight[var1];
                        var7 = GameData.objectWidth[var1];
                    }

                    for (int var8 = var3; var8 < var3 + var7; ++var8) {
                        for (int var9 = var4; var9 < var6 + var4; ++var9) {
                            if (GameData.objectType[var1] == 1) {
                                this.anIntArrayArray276[var8][var9] = Utility.bitwiseOr(this.anIntArrayArray276[var8][var9], 64);
                            } else if (var5 == 0) {
                                this.anIntArrayArray276[var8][var9] = Utility.bitwiseOr(this.anIntArrayArray276[var8][var9], 2);
                                if (var8 > 0) {
                                    this.method256(8, var9, (byte) 112, var8 + -1);
                                }
                            } else if (var5 != 2) {
                                if (var5 == 4) {
                                    this.anIntArrayArray276[var8][var9] = Utility.bitwiseOr(this.anIntArrayArray276[var8][var9], 8);
                                    if (var8 < 95) {
                                        this.method256(2, var9, (byte) 108, var8 - -1);
                                    }
                                } else if (var5 == 6) {
                                    this.anIntArrayArray276[var8][var9] = Utility.bitwiseOr(this.anIntArrayArray276[var8][var9], 1);
                                    if (var9 > 0) {
                                        this.method256(4, -1 + var9, (byte) 102, var8);
                                    }
                                }
                            } else {
                                this.anIntArrayArray276[var8][var9] = Utility.bitwiseOr(this.anIntArrayArray276[var8][var9], 4);
                                if (var9 < 95) {
                                    this.method256(1, 1 + var9, (byte) 83, var8);
                                }
                            }
                        }
                    }

                    this.method270(var4, (byte) -125, var3, var7, var6);
                }

            }
        }
    }

    final int getElevation(int var1, byte var2, int var3) {
        int var4 = var1 >> 7;
        int var5 = var3 >> 7;
        int var6 = 127 & var1;
        int var7 = var3 & 127;
        if (var2 != -56) {
            this.anIntArray266 = null;
        }

        if (var4 >= 0 && var5 >= 0 && var4 < 95 && var5 < 95) {
            int var8;
            int var9;
            int var10;
            if (var6 > (-var7 + 128)) {
                var8 = this.getTerrainHeight(var5 - -1, (byte) 66, var4 - -1);
                var9 = this.getTerrainHeight(1 + var5, (byte) 66, var4) + -var8;
                var6 = -var6 + 128;
                var10 = this.getTerrainHeight(var5, (byte) 66, 1 + var4) - var8;
                var7 = -var7 + 128;
            } else {
                var8 = this.getTerrainHeight(var5, (byte) 66, var4);
                var9 = -var8 + this.getTerrainHeight(var5, (byte) 66, var4 - -1);
                var10 = this.getTerrainHeight(var5 + 1, (byte) 66, var4) + -var8;
            }

            int var11 = var10 * var7 / 128 + var8 - -(var9 * var6 / 128);
            return var11;
        } else {
            return 0;
        }
    }

    private int method276(int var1, int var2, int var3) {
        if (var2 >= 0 && var2 < 96 && var1 >= 0 && var1 < 96) {
            if (var3 != -5098) {
                return -92;
            } else {
                byte var4 = 0;
                if (var2 >= 48 && var1 < 48) {
                    var2 -= 48;
                    var4 = 1;
                } else if (var2 < 48 && var1 >= 48) {
                    var4 = 2;
                    var1 -= 48;
                } else if (var2 >= 48 && var1 >= 48) {
                    var2 -= 48;
                    var4 = 3;
                    var1 -= 48;
                }

                return 255 & this.terrainColour[var4][var1 + var2 * 48];
            }
        } else {
            return 0;
        }
    }

    final void removeObject(int var1, int var2, int var3, int var4) {
        if (var2 != -2106) {
            this.method283(-116, 60, (byte) -77, -42, 33, 95);
        }

        if (var1 >= 0 && var4 >= 0 && var1 < 95 && var4 < 95) {
            if (GameData.objectType[var3] == 1 || GameData.objectType[var3] == 2) {
                int var5 = this.getTileDirection(var1, true, var4);
                int var6;
                int var7;
                if (var5 != 0 && var5 != 4) {
                    var6 = GameData.objectHeight[var3];
                    var7 = GameData.objectWidth[var3];
                } else {
                    var6 = GameData.objectWidth[var3];
                    var7 = GameData.objectHeight[var3];
                }

                for (int var8 = var1; var8 < (var1 + var6); ++var8) {
                    for (int var9 = var4; var7 + var4 > var9; ++var9) {
                        if (GameData.objectType[var3] != 1) {
                            if (var5 != 0) {
                                if (var5 == 2) {
                                    this.anIntArrayArray276[var8][var9] = Utility.bitwiseAnd(this.anIntArrayArray276[var8][var9], '\ufffb');
                                    if (var9 < 95) {
                                        this.method285('\uffff', var9 + 1, 1, var8);
                                    }
                                } else if (var5 != 4) {
                                    if (var5 == 6) {
                                        this.anIntArrayArray276[var8][var9] = Utility.bitwiseAnd(this.anIntArrayArray276[var8][var9], '\ufffe');
                                        if (var9 > 0) {
                                            this.method285('\uffff', var9 - 1, 4, var8);
                                        }
                                    }
                                } else {
                                    this.anIntArrayArray276[var8][var9] = Utility.bitwiseAnd(this.anIntArrayArray276[var8][var9], '\ufff7');
                                    if (var8 < 95) {
                                        this.method285('\uffff', var9, 2, var8 + 1);
                                    }
                                }
                            } else {
                                this.anIntArrayArray276[var8][var9] = Utility.bitwiseAnd(this.anIntArrayArray276[var8][var9], '\ufffd');
                                if (var8 > 0) {
                                    this.method285(var2 ^ -63431, var9, 8, var8 - 1);
                                }
                            }
                        } else {
                            this.anIntArrayArray276[var8][var9] = Utility.bitwiseAnd(this.anIntArrayArray276[var8][var9], '\uffbf');
                        }
                    }
                }

                this.method270(var4, (byte) -125, var1, var6, var7);
            }

        }
    }

    private void method278(int var1, byte var2, int var3, int var4) {
        if (var1 >= 0 && var1 < 96 && var4 >= 0 && var4 < 96) {
            byte var5 = 0;
            int var6 = 36 / ((var2 - 78) / 47);
            if (var1 >= 48 && var4 < 48) {
                var1 -= 48;
                var5 = 1;
            } else if (var1 < 48 && var4 >= 48) {
                var4 -= 48;
                var5 = 2;
            } else if (var1 >= 48 && var4 >= 48) {
                var1 -= 48;
                var4 -= 48;
                var5 = 3;
            }

            this.tileDecoration[var5][var1 * 48 + var4] = (byte) var3;
        }
    }

    final void setObjectAdjacency(int var1, int var2, int var3, int var4, int var5) {
        if (var4 > -34) {
            this.method263((int[]) null, 43, -110, -22, false, (int[]) null, -11, -13, -56, -113);
        }

        if (var3 >= 0 && var2 >= 0 && var3 < 95 && var2 < 95) {
            if (GameData.wallObjectAdjacent[var1] == 1) {
                if (var5 == 0) {
                    this.anIntArrayArray276[var3][var2] = Utility.bitwiseOr(this.anIntArrayArray276[var3][var2], 1);
                    if (var2 > 0) {
                        this.method256(4, -1 + var2, (byte) 89, var3);
                    }
                } else if (var5 != 1) {
                    if (var5 == 2) {
                        this.anIntArrayArray276[var3][var2] = Utility.bitwiseOr(this.anIntArrayArray276[var3][var2], 16);
                    } else if (var5 == 3) {
                        this.anIntArrayArray276[var3][var2] = Utility.bitwiseOr(this.anIntArrayArray276[var3][var2], 32);
                    }
                } else {
                    this.anIntArrayArray276[var3][var2] = Utility.bitwiseOr(this.anIntArrayArray276[var3][var2], 2);
                    if (var3 > 0) {
                        this.method256(8, var2, (byte) 106, var3 - 1);
                    }
                }

                this.method270(var2, (byte) -128, var3, 1, 1);
            }
        }
    }

    private void method280(int var1, int var2, int var3, byte var4, int var5, int var6) {
        if (var4 == 23) {
            int var7 = GameData.wallObjectHeight[var2];
            if (this.anIntArrayArray249[var6][var5] < 80000) {
                this.anIntArrayArray249[var6][var5] += var7 + 80000;
            }

            if (this.anIntArrayArray249[var3][var1] < 80000) {
                this.anIntArrayArray249[var3][var1] += var7 + 80000;
            }
        }
    }

    private void method281(int var1, int var2, int var3, int var4, int var5, GameModel var6, int var7) {
        this.method261(40, 2, var1, var3);
        this.method261(40, var2 + -5078, var4, var7);
        int var8 = GameData.wallObjectHeight[var5];
        int var9 = GameData.wallObjectTextureFront[var5];
        int var10 = GameData.wallObjectTextureBack[var5];
        int var11 = var1 * 128;
        int var12 = var3 * 128;
        int var13 = var4 * 128;
        int var14 = var7 * 128;
        int var15 = var6.method561(var11, (byte) 111, -this.anIntArrayArray249[var1][var3], var12);
        if (var2 != 5080) {
            this.memberLandscapePack = null;
        }

        int var16 = var6.method561(var11, (byte) 121, -var8 + -this.anIntArrayArray249[var1][var3], var12);
        int var17 = var6.method561(var13, (byte) 81, -var8 + -this.anIntArrayArray249[var4][var7], var14);
        int var18 = var6.method561(var13, (byte) 115, -this.anIntArrayArray249[var4][var7], var14);
        int[] var19 = new int[]{var15, var16, var17, var18};
        int var20 = var6.method549(4, var10, (byte) -118, var19, var9);
        if (GameData.wallObjectInvisible[var5] == 5) {
            var6.faceTag[var20] = var5 + 30000;
        } else {
            var6.faceTag[var20] = 0;
        }
    }

    private void method283(int var1, int var2, byte var3, int var4, int var5, int var6) {
        int var7 = 3 * var5;
        int var8 = 3 * var1;
        int var9 = -101 / ((-34 - var3) / 52);
        int var10 = this.aScene_234.method299(var6, -18255);
        int var11 = this.aScene_234.method299(var4, -18255);
        var10 = 8355711 & var10 >> 1;
        var11 = 8355711 & var11 >> 1;
        if (var2 != 0) {
            if (var2 == 1) {
                this.aSurface_233.drawLineHoriz(var7, var8, 3, var11);
                this.aSurface_233.drawLineHoriz(1 + var7, 1 + var8, 2, var11);
                this.aSurface_233.drawLineHoriz(2 + var7, 2 + var8, 1, var11);
                this.aSurface_233.drawLineHoriz(var7, 1 + var8, 1, var10);
                this.aSurface_233.drawLineHoriz(var7, var8 - -2, 2, var10);
                return;
            }
        } else {
            this.aSurface_233.drawLineHoriz(var7, var8, 3, var10);
            this.aSurface_233.drawLineHoriz(var7, var8 + 1, 2, var10);
            this.aSurface_233.drawLineHoriz(var7, 2 + var8, 1, var10);
            this.aSurface_233.drawLineHoriz(2 + var7, var8 - -1, 1, var11);
            this.aSurface_233.drawLineHoriz(1 + var7, 2 + var8, 2, var11);
        }
        ;
    }

    private int method284(int var1, int var2, int var3) {
        if (var1 >= 0 && var1 < 96 && var3 >= 0 && var3 < 96) {
            if (var2 != -12345679) {
                return -111;
            } else {
                byte var4 = 0;
                if (var1 >= 48 && var3 < 48) {
                    var4 = 1;
                    var1 -= 48;
                } else if (var1 < 48 && var3 >= 48) {
                    var4 = 2;
                    var3 -= 48;
                } else if (var1 >= 48 && var3 >= 48) {
                    var4 = 3;
                    var1 -= 48;
                    var3 -= 48;
                }

                return this.wallsRoof[var4][48 * var1 + var3];
            }
        } else {
            return 0;
        }
    }

    private void method285(int var1, int var2, int var3, int var4) {
        this.anIntArrayArray276[var4][var2] = Utility.bitwiseAnd(this.anIntArrayArray276[var4][var2], var1 - var3);
    }

}
