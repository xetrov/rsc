import java.awt.*;
import java.awt.image.BufferedImage;

final class GameCursor {

    private Robot robot = new Robot();
    private Component component;


    GameCursor() throws Exception {
    }

    final void mouseMove(int var1, int var2) {
        System.out.println("mousemove");
        this.robot.mouseMove(var1, var2);
    }

    final void setCursor(Component var1, int[] rgbArray, int width, int height, Point hotSpot) {
        System.out.println("setcursor");
        if (rgbArray == null) {
            var1.setCursor((Cursor) null);
        } else {
            BufferedImage cursor = new BufferedImage(width, height, 2);
            cursor.setRGB(0, 0, width, height, rgbArray, 0, width);
            var1.setCursor(var1.getToolkit().createCustomCursor(cursor, hotSpot, (String) null));
        }

    }

    final void setComponent(Component var1, boolean var2) {
        System.out.println("setcomponent");
        if (!var2) {
            if (var1 == null) {
                throw new NullPointerException();
            }
        } else {
            var1 = null;
        }

        if (this.component != var1) {
            if (this.component != null) {
                this.component.setCursor((Cursor) null);
                this.component = null;
            }

            if (null != var1) {
                var1.setCursor(var1.getToolkit().createCustomCursor(new BufferedImage(1, 1, 2), new Point(0, 0), (String) null));
                this.component = var1;
            }

        }
    }
}
