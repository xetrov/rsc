import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;

final class Panel {

    static boolean drawBackgroundArrow = true;
    static int anInt809 = 176;
    static int textListEntryHeightMod = 0;
    private static int anInt867 = 114;
    int[] controlListEntryCount;
    int[] controlFlashText;
    private Surface surface;
    private int colourRoundedBoxMid;
    private int colourRoundedBoxOut;
    private int colourBoxLeftNRight;
    private String[][] controListEntriesExtra2;
    private int controlCount = 0;
    private int colourScrollbarTop;
    private boolean[] controlMaskText;
    private int[] controlTextSize;
    private int colourScrollbarHandleMid;
    private int colourScrollbarHandleRight;
    private int mouseLastButtonDown = 0;
    private int colourBoxTopNBottom;
    private int[] controlInputMaxLen;
    private int colourRoundedBoxIn;
    private int[][] controlCrowns;
    private int[] controlType;
    private int mouseY = 0;
    private int mouseButtonDown = 0;
    private int[] controlListEntryMouseOver;
    private int[] controlX;
    private int colourScrollbarBottom;
    private String[][] controlListEntries;
    private int colourBoxLeftNRight2;
    private String[][] controListEntriesExtra1;
    private int anInt878 = 0;
    private int[] controlWidth;
    private boolean[] controlClicked;
    private int focusControlIndex = -1;
    private int[] controlListEntryMouseButtonDown;
    private int colourScrollbarHandleLeft;
    private String[] controlText;
    private int colourBoxTopNBottom2;
    private int[] controlY;
    private boolean[] controlListScrollbarHandleDragged;
    private int[] controlHeight;
    private boolean aBoolean904 = true;
    private boolean[] controlUseAlternativeColour;
    private int mouseX = 0;
    private boolean[] controlShown;


    Panel(Surface var1, int var2) {
        this.controlCrowns = new int[var2][];
        this.controlListEntryMouseOver = new int[var2];
        this.controlHeight = new int[var2];
        this.controlListEntryMouseButtonDown = new int[var2];
        this.controListEntriesExtra2 = new String[var2][];
        this.controlClicked = new boolean[var2];
        this.controlY = new int[var2];
        this.controlListScrollbarHandleDragged = new boolean[var2];
        this.controlListEntryCount = new int[var2];
        this.surface = var1;
        this.controlType = new int[var2];
        this.controlX = new int[var2];
        this.controlMaskText = new boolean[var2];
        this.controlListEntries = new String[var2][];
        this.controListEntriesExtra1 = new String[var2][];
        this.controlWidth = new int[var2];
        this.controlInputMaxLen = new int[var2];
        this.controlTextSize = new int[var2];
        this.controlFlashText = new int[var2];
        this.controlShown = new boolean[var2];
        this.controlText = new String[var2];
        this.controlUseAlternativeColour = new boolean[var2];
        this.colourScrollbarTop = this.rgb2longMod(176, 114, -12, 114);
        this.colourScrollbarBottom = this.rgb2longMod(62, 14, -12, 14);
        this.colourScrollbarHandleLeft = this.rgb2longMod(232, 208, -12, 200);
        this.colourScrollbarHandleMid = this.rgb2longMod(184, 129, -12, 96);
        this.colourScrollbarHandleRight = this.rgb2longMod(115, 95, -12, 53);
        this.colourRoundedBoxOut = this.rgb2longMod(171, 142, -12, 117);
        this.colourRoundedBoxMid = this.rgb2longMod(158, 122, -12, 98);
        this.colourRoundedBoxIn = this.rgb2longMod(136, 100, -12, 86);
        this.colourBoxTopNBottom = this.rgb2longMod(179, 146, -12, 135);
        this.colourBoxTopNBottom2 = this.rgb2longMod(151, 112, -12, 97);
        this.colourBoxLeftNRight2 = this.rgb2longMod(136, 102, -12, 88);
        this.colourBoxLeftNRight = this.rgb2longMod(120, 93, -12, 84);
    }

    static void method543(byte var0, String var1, int var2, byte[] var3) throws IOException {
        if (var0 < 95) {
            GameData.spellCount = 16;
        }

        InputStream var4 = Utility.method545(var1, -32341);
        DataInputStream var5 = new DataInputStream(var4);

        try {
            var5.readFully(var3, 0, var2);
        } catch (EOFException var6) {
            ;
        }

        var5.close();
    }

    private void drawListContainer(int var1, int var2, int var3, int var4, int var5, int var6, int var7) {
        int var8 = -12 + var7 + var3;
        this.surface.drawBoxEdge(var8, var2, 12, var5, 0);
        this.surface.drawSprite((byte) 61, var2 + 1, client.baseSpriteStart, 1 + var8);
        this.surface.drawSprite((byte) 61, var5 + var2 - 12, client.baseSpriteStart + 1, 1 + var8);
        this.surface.drawLineHoriz(var8, 13 + var2, var6, 0);
        this.surface.drawLineHoriz(var8, var5 + var2 - 13, 12, 0);
        this.surface.drawGradient(1 + var8, var2 + 14, 11, var5 - 27, this.colourScrollbarTop, this.colourScrollbarBottom);
        this.surface.drawBox(3 + var8, var2 + var4 - -14, 7, var1, this.colourScrollbarHandleMid);
        this.surface.drawLineVert(var8 + 2, 14 + var4 + var2, var1, this.colourScrollbarHandleLeft);
        this.surface.drawLineVert(var8 + 2 - -8, var2 + (var4 - -14), var1, this.colourScrollbarHandleRight);
    }

    private void drawTextListInteractive(int var1, int var2, int var3, int var4, int var5, byte var6, int x, int var8, int font, String[] lines, int[] crowns) {
        int var12 = var1 / this.surface.textHeight(font, true);
        int var13;
        int y;
        int var15;
        int color;
        if (var3 <= var12) {
            var2 = 0;
            this.controlFlashText[var4] = 0;
        } else {
            var13 = var8 + x - 12;
            y = var12 * (var1 + -27) / var3;
            if (y < 6) {
                y = 6;
            }

            int var10000 = var2 * (-y + -27 + var1) / (-var12 + var3);
            if (this.mouseButtonDown == 1 && var13 <= this.mouseX && 12 + var13 >= this.mouseX) {
                if (var5 < this.mouseY && this.mouseY < 12 + var5 && var2 > 0) {
                    --var2;
                }

                if ((-12 + var5 + var1) < this.mouseY && this.mouseY < (var1 + var5) && var2 < -var12 + var3) {
                    ++var2;
                }

                this.controlFlashText[var4] = var2;
            }

            if (this.mouseButtonDown == 1 && (var13 <= this.mouseX && var13 - -12 >= this.mouseX || this.mouseX >= (var13 + -12) && this.mouseX <= (24 + var13) && this.controlListScrollbarHandleDragged[var4])) {
                if (this.mouseY > 12 + var5 && this.mouseY < (-12 + var1 + var5)) {
                    this.controlListScrollbarHandleDragged[var4] = true;
                    color = -12 + -var5 + this.mouseY - y / 2;
                    var2 = color * var3 / (var1 - 24);
                    if (var2 < 0) {
                        var2 = 0;
                    }

                    if ((var3 + -var12) < var2) {
                        var2 = -var12 + var3;
                    }

                    this.controlFlashText[var4] = var2;
                }
            } else {
                this.controlListScrollbarHandleDragged[var4] = false;
            }

            var15 = var2 * (var1 - (27 + y)) / (-var12 + var3);
            this.drawListContainer(y, var5, x, var15, var1, 12, var8);
        }

        this.controlListEntryMouseOver[var4] = -1;
        var13 = var1 - this.surface.textHeight(font, true) * var12;
        if (var6 <= 43) {
            this.show(-68, (byte) -36);
        }

        y = 5 * this.surface.textHeight(font, true) / 6 + var5 + var13 / 2;

        for (var15 = var2; var3 > var15; ++var15) {
            if (!this.controlUseAlternativeColour[var4]) {
                color = 0;
            } else {
                color = 16777215;
            }

            if ((x - -2) <= this.mouseX && this.mouseX <= this.surface.textWidth(font, -127, lines[var15]) + (x - -2) && (this.mouseY + -2) <= y && -2 + this.mouseY > y - this.surface.textHeight(font, true)) {
                if (!this.controlUseAlternativeColour[var4]) {
                    color = 16777215;
                } else {
                    color = 8421504;
                }

                this.controlListEntryMouseOver[var4] = var15;
                if (this.mouseLastButtonDown == 1) {
                    this.controlListEntryMouseButtonDown[var4] = var15;
                    this.controlClicked[var4] = true;
                }
            }

            if (var15 == this.controlListEntryMouseButtonDown[var4] && this.aBoolean904) {
                color = 16711680;
            }

            this.surface.drawstring(lines[var15], 2 + x, y, font, color, crowns[var15]);
            y += this.surface.textHeight(font, true);
            if (var1 + var5 <= y) {
                return;
            }
        }
        ;
    }

    final void method506(int var1, int var2) {
        if (var1 != -22757) {
            this.controlHeight = null;
        }

        this.controlFlashText[var2] = 0;
        this.controlListEntryMouseOver[var2] = -1;
    }

    final String getListEntryExtra1(int var1, byte var2, int var3) {
        if (var2 != -93) {
            this.controlTextSize = null;
        }

        return this.controListEntriesExtra1[var1][var3];
    }

    final int method508(int var1, int var2, byte var3, int var4, int var5) {
        this.controlType[this.controlCount] = 11;
        if (var3 != -2) {
            this.controlY = null;
        }

        this.controlShown[this.controlCount] = true;
        this.controlClicked[this.controlCount] = false;
        this.controlX[this.controlCount] = -(var5 / 2) + var2;
        this.controlY[this.controlCount] = var1 + -(var4 / 2);
        this.controlWidth[this.controlCount] = var5;
        this.controlHeight[this.controlCount] = var4;
        return this.controlCount++;
    }

    private void method509(int var1, int var2, int var3, int var4, int var5, int var6) {
        this.surface.drawBox(var5, var4, var2, var6, 16777215);
        if (var3 == 14) {
            this.surface.drawLineHoriz(var5, var4, var2, this.colourBoxTopNBottom);
            this.surface.drawLineVert(var5, var4, var6, this.colourBoxTopNBottom);
            this.surface.drawLineHoriz(var5, var4 - (-var6 - -1), var2, this.colourBoxLeftNRight);
            this.surface.drawLineVert(var2 + (var5 - 1), var4, var6, this.colourBoxLeftNRight);
            if (this.controlListEntryMouseButtonDown[var1] == 1) {
                for (int var7 = 0; var7 < var6; ++var7) {
                    this.surface.drawLineHoriz(var5 - -var7, var7 + var4, 1, 0);
                    this.surface.drawLineHoriz(-1 + var2 + var5 + -var7, var4 - -var7, 1, 0);
                }

            }
        }
    }

    final void hide(int var1, int var2) {
        this.controlShown[var1] = false;
        if (var2 != -12) {
            this.addTextListInteractive(68, -119, -124, -40, 56, 66, false, true);
        }
    }

    final void drawPanel(int var1) {
        int id = 0;
        if (var1 > 41) {
            for (; this.controlCount > id; ++id) {
                if (this.controlShown[id]) {
                    if (this.controlType[id] != 0) {
                        if (this.controlType[id] != 1) {
                            if (this.controlType[id] != 2) {
                                if (this.controlType[id] != 3) {
                                    if (this.controlType[id] == 4) {
                                        this.drawTextList(id, this.controlX[id], this.controlY[id], this.controlWidth[id], this.controlHeight[id], this.controlTextSize[id], this.controlListEntries[id], this.controlListEntryCount[id], this.controlFlashText[id], this.controlCrowns[id]);
                                    } else if (this.controlType[id] != 5 && this.controlType[id] != 6) {
                                        if (this.controlType[id] == 7) {
                                            this.method527(this.controlTextSize[id], this.controlY[id], id, this.controlX[id], false, this.controlListEntries[id]);
                                        } else if (this.controlType[id] != 8) {
                                            if (this.controlType[id] != 9) {
                                                if (this.controlType[id] != 11) {
                                                    if (this.controlType[id] != 12) {
                                                        if (this.controlType[id] == 14) {
                                                            this.method509(id, this.controlWidth[id], 14, this.controlY[id], this.controlX[id], this.controlHeight[id]);
                                                        }
                                                    } else {
                                                        this.method534(this.controlTextSize[id], (byte) -12, this.controlX[id], this.controlY[id]);
                                                    }
                                                } else {
                                                    this.method514(this.controlY[id], this.controlHeight[id], (byte) 35, this.controlWidth[id], this.controlX[id]);
                                                }
                                            } else {
                                                this.drawTextListInteractive(this.controlHeight[id], this.controlFlashText[id], this.controlListEntryCount[id], id, this.controlY[id], (byte) 120, this.controlX[id], this.controlWidth[id], this.controlTextSize[id], this.controlListEntries[id], this.controlCrowns[id]);
                                            }
                                        } else {
                                            this.method536(this.controlY[id], this.controlX[id], this.controlTextSize[id], this.controlListEntries[id], (byte) -15, id);
                                        }
                                    } else {
                                        this.drawTextInput(this.controlX[id], this.controlY[id], this.controlWidth[id], this.controlHeight[id], this.controlText[id], id, this.controlTextSize[id]);
                                    }
                                } else {
                                    this.drawLineHoriz(this.controlX[id], this.controlY[id], this.controlWidth[id]);
                                }
                            } else {
                                this.drawBox(this.controlX[id], this.controlY[id], this.controlWidth[id], this.controlHeight[id], false);
                            }
                        } else {
                            this.drawText(id, this.controlX[id] + -(this.surface.textWidth(this.controlTextSize[id], -127, this.controlText[id]) / 2), this.controlY[id], this.controlText[id], this.controlTextSize[id], 0);
                        }
                    } else {
                        this.drawText(id, this.controlX[id], this.controlY[id], this.controlText[id], this.controlTextSize[id], 0);
                    }
                }
            }

            this.mouseLastButtonDown = 0;
        }
    }

    private void drawText(int control, int x, int y, String text, int textSize, int crown) {
        int y2 = y + this.surface.textHeight(textSize, true) / 3;
        this.drawstring(control, x, y2, text, textSize, crown);
    }

    final boolean isClicked(int var1, int var2) {
        if (var1 != 3) {
            return true;
        } else {
            if (this.controlShown[var2] && this.controlClicked[var2]) {
                this.controlClicked[var2] = false;
                return true;
            } else {
                return false;
            }
        }
    }

    private void method514(int var1, int var2, byte var3, int var4, int var5) {
        if (var3 != 35) {
            this.controlHeight = null;
        }

        this.surface.drawBox(var5, var1, var4, var2, 0);
        this.surface.drawBoxEdge(var5, var1, var4, var2, this.colourRoundedBoxOut);
        this.surface.drawBoxEdge(1 + var5, var1 + 1, var4 - 2, var2 + -2, this.colourRoundedBoxMid);
        this.surface.drawBoxEdge(2 + var5, var1 + 2, var4 - 4, -4 + var2, this.colourRoundedBoxIn);
        this.surface.drawSprite((byte) 61, var1, client.baseSpriteStart + 2, var5);
        this.surface.drawSprite((byte) 61, var1, 3 - -client.baseSpriteStart, var4 + var5 + -7);
        this.surface.drawSprite((byte) 61, var2 + var1 + -7, client.baseSpriteStart + 4, var5);
        this.surface.drawSprite((byte) 61, -7 + var2 + var1, 5 + client.baseSpriteStart, -7 + var4 + var5);
    }

    final void show(int var1, byte var2) {
        if (var2 <= -57) {
            this.controlShown[var1] = true;
        }
    }

    final int addButton(int var1, int var2, int var3, int var4, int var5) {
        this.controlType[this.controlCount] = 10;
        this.controlShown[this.controlCount] = true;
        this.controlClicked[this.controlCount] = false;
        this.controlX[this.controlCount] = -(var1 / 2) + var4;
        this.controlY[this.controlCount] = -(var2 / 2) + var3;
        this.controlWidth[this.controlCount] = var1;
        this.controlHeight[this.controlCount] = var2;
        if (var5 < 122) {
            this.getListEntryExtra1(81, (byte) -42, 92);
        }

        return this.controlCount++;
    }

    final String getListEntryExtra2(int var1, int var2, int var3) {
        if (var1 >= -71) {
            this.drawText(-118, 92, 29, (String) null, 100, -52);
        }

        return this.controListEntriesExtra2[var2][var3];
    }

    final int addButtonBackground(int var1, int var2, int var3, int var4, byte var5) {
        this.controlType[this.controlCount] = 2;
        this.controlShown[this.controlCount] = true;
        this.controlClicked[this.controlCount] = false;
        if (var5 != -8) {
            this.getListEntryExtra1(78, (byte) 93, -111);
        }

        this.controlX[this.controlCount] = var1 + -(var2 / 2);
        this.controlY[this.controlCount] = -(var4 / 2) + var3;
        this.controlWidth[this.controlCount] = var2;
        this.controlHeight[this.controlCount] = var4;
        return this.controlCount++;
    }

    final int addText(boolean var1, boolean var2, int var3, int var4, int var5, String var6) {
        this.controlType[this.controlCount] = 1;
        this.controlShown[this.controlCount] = true;
        this.controlClicked[this.controlCount] = var2;
        this.controlTextSize[this.controlCount] = var3;
        this.controlUseAlternativeColour[this.controlCount] = var1;
        this.controlX[this.controlCount] = var5;
        this.controlY[this.controlCount] = var4;
        this.controlText[this.controlCount] = var6;
        return this.controlCount++;
    }

    final void removeListEntry(int control, String text, boolean flash, String extra1, String extra2, int crown) {
        int id = this.controlListEntryCount[control]++;
        if (this.controlInputMaxLen[control] <= id) {
            --id;
            --this.controlListEntryCount[control];

            for (int var9 = 0; id > var9; ++var9) {
                this.controlListEntries[control][var9] = this.controlListEntries[control][var9 - -1];
                this.controlCrowns[control][var9] = this.controlCrowns[control][var9 + 1];
                this.controListEntriesExtra1[control][var9] = this.controListEntriesExtra1[control][1 + var9];
                this.controListEntriesExtra2[control][var9] = this.controListEntriesExtra2[control][1 + var9];
            }
        }

        this.controlListEntries[control][id] = text;
        this.controlCrowns[control][id] = crown;
        this.controListEntriesExtra1[control][id] = extra1;
        this.controListEntriesExtra2[control][id] = extra2;
        if (flash) {
            this.controlFlashText[control] = 999999;
        }
        ;
    }

    final void setFocus(byte var1, int var2) {
        int var3 = -7 % ((21 - var1) / 48);
        this.focusControlIndex = var2;
    }

    private void drawBox(int x, int y, int width, int height, boolean var1) {
        this.surface.setBounds(x, y, x + width, height + y);
        this.surface.drawGradient(x, y, width, height, this.colourBoxLeftNRight, this.colourBoxTopNBottom);
        if (drawBackgroundArrow) {
            for (int var6 = x + -(63 & y); x - -width > var6; var6 += 128) {
                for (int var7 = -(y & 31) + y; height + y > var7; var7 += 128) {
                    this.surface.drawSpriteAlpha(var6, var7, 6 + client.baseSpriteStart, 128);
                }
            }
        }

        this.surface.drawLineHoriz(x, y, width, this.colourBoxTopNBottom);
        this.surface.drawLineHoriz(1 + x, 1 + y, width + -2, this.colourBoxTopNBottom);
        this.surface.drawLineHoriz(x - -2, 2 + y, width - 4, this.colourBoxTopNBottom2);
        this.surface.drawLineVert(x, y, height, this.colourBoxTopNBottom);
        this.surface.drawLineVert(1 + x, y - -1, height + -2, this.colourBoxTopNBottom);
        this.surface.drawLineVert(2 + x, y - -2, height + -4, this.colourBoxTopNBottom2);
        this.surface.drawLineHoriz(x, -1 + y + height, width, this.colourBoxLeftNRight);
        this.surface.drawLineHoriz(1 + x, height + (y - 2), width - 2, this.colourBoxLeftNRight);
        if (!var1) {
            this.surface.drawLineHoriz(2 + x, y - -height + -3, width - 4, this.colourBoxLeftNRight2);
            this.surface.drawLineVert(x - -width - 1, y, height, this.colourBoxLeftNRight);
            this.surface.drawLineVert(width + (x - 2), y - -1, height - 2, this.colourBoxLeftNRight);
            this.surface.drawLineVert(x + width + -3, y - -2, height - 4, this.colourBoxLeftNRight2);
            this.surface.resetBounds((byte) 10);
        }
    }

    final String method523(int var1, int var2) {
        if (var1 != -17054) {
            this.addButtonBackground(-103, -83, 101, -54, (byte) -51);
        }

        return this.controlText[var2] == null ? "null" : this.controlText[var2];
    }

    final void handleMouse(int var1, int var2, int var3, int var4, int var5) {
        if (var2 != 0) {
            this.mouseLastButtonDown = var2;
        }

        this.mouseX = var3;
        this.mouseY = var5;
        this.mouseButtonDown = var4;
        int var6;
        if (var2 == 1) {
            for (var6 = 0; var6 < this.controlCount; ++var6) {
                if (this.controlShown[var6] && this.controlType[var6] == 10 && this.controlX[var6] <= this.mouseX && this.controlY[var6] <= this.mouseY && (this.controlWidth[var6] + this.controlX[var6]) >= this.mouseX && this.mouseY <= this.controlHeight[var6] + this.controlY[var6]) {
                    this.controlClicked[var6] = true;
                }

                if (this.controlShown[var6] && this.controlType[var6] == 14 && this.controlX[var6] <= this.mouseX && this.mouseY >= this.controlY[var6] && this.mouseX <= (this.controlX[var6] - -this.controlWidth[var6]) && this.controlY[var6] - -this.controlHeight[var6] >= this.mouseY) {
                    this.controlListEntryMouseButtonDown[var6] = 1 - this.controlListEntryMouseButtonDown[var6];
                }
            }
        }

        if (var4 == 1) {
            ++this.anInt878;
        } else {
            this.anInt878 = 0;
        }

        if (var2 == 1 || this.anInt878 > 20) {
            for (var6 = 0; this.controlCount > var6; ++var6) {
                if (this.controlShown[var6] && this.controlType[var6] == 15 && this.controlX[var6] <= this.mouseX && this.mouseY >= this.controlY[var6] && this.controlX[var6] + this.controlWidth[var6] >= this.mouseX && this.mouseY <= this.controlHeight[var6] + this.controlY[var6]) {
                    this.controlClicked[var6] = true;
                }
            }

            this.anInt878 -= 5;
        }

        if (var1 >= -73) {
            this.colourRoundedBoxIn = -112;
        }
    }

    private void drawLineHoriz(int x, int y, int width) {
        this.surface.drawLineHoriz(x, y, width, 0);
    }

    final int setFocus(int var1) {
        int var3 = this.controlListEntryMouseOver[var1];
        return var3;
    }

    private void method527(int font, int var2, int var3, int var4, boolean var5, String[] lines) {
        int var7 = 0;
        int var8 = lines.length;

        for (int var9 = 0; var8 > var9; ++var9) {
            var7 += this.surface.textWidth(font, -127, lines[var9]);
            if (-1 + var8 > var9) {
                var7 += this.surface.textWidth(font, -127, "  ");
            }
        }

        int x = -(var7 / 2) + var4;
        int y = var2 - -(this.surface.textHeight(font, true) / 3);
        if (var5) {
            this.method536(-33, 4, -32, (String[]) null, (byte) -32, -3);
        }

        for (int var12 = 0; var12 < var8; ++var12) {
            int color;
            if (!this.controlUseAlternativeColour[var3]) {
                color = 0;
            } else {
                color = 16777215;
            }

            if (x <= this.mouseX && this.mouseX <= (x - -this.surface.textWidth(font, -127, lines[var12])) && y >= this.mouseY && this.mouseY > -this.surface.textHeight(font, true) + y) {
                if (!this.controlUseAlternativeColour[var3]) {
                    color = 16777215;
                } else {
                    color = 8421504;
                }

                if (this.mouseLastButtonDown == 1) {
                    this.controlListEntryMouseButtonDown[var3] = var12;
                    this.controlClicked[var3] = true;
                }
            }

            if (this.controlListEntryMouseButtonDown[var3] == var12) {
                if (this.controlUseAlternativeColour[var3]) {
                    color = 16711680;
                } else {
                    color = 12582912;
                }
            }

            this.surface.drawstring(lines[var12], x, y, font, color, 0);
            x += this.surface.textWidth(font, -127, lines[var12] + "  ");
        }
        ;
    }

    final int method528(int var1, boolean var2, int var3, int var4) {
        int var5 = this.surface.spriteWidth[var1];
        int var6 = this.surface.spriteHeight[var1];
        this.controlType[this.controlCount] = 12;
        this.controlShown[this.controlCount] = true;
        this.controlClicked[this.controlCount] = var2;
        this.controlX[this.controlCount] = var3 - var5 / 2;
        this.controlY[this.controlCount] = var4 - var6 / 2;
        this.controlWidth[this.controlCount] = var5;
        this.controlHeight[this.controlCount] = var6;
        this.controlTextSize[this.controlCount] = var1;
        return this.controlCount++;
    }

    private int rgb2longMod(int var1, int var2, int var3, int var4) {
        return var3 != -12 ? -102 : Surface.rgb2long(var1 * anInt809 / 176, ClientStreamBase.anInt739 * var2 / 114, anInt867 * var4 / 114);
    }

    final int addTextListInput(int x, int y, int w, int h, int size, int maxLength, boolean maskText, boolean altCol) {
        this.controlType[this.controlCount] = 5;
        this.controlShown[this.controlCount] = true;
        this.controlMaskText[this.controlCount] = maskText;
        this.controlClicked[this.controlCount] = false;
        this.controlTextSize[this.controlCount] = size;
        this.controlUseAlternativeColour[this.controlCount] = altCol;
        this.controlX[this.controlCount] = x;
        this.controlY[this.controlCount] = y;
        this.controlWidth[this.controlCount] = w;
        this.controlHeight[this.controlCount] = h;
        this.controlInputMaxLen[this.controlCount] = maxLength;
        this.controlText[this.controlCount] = "";
        return this.controlCount++;
    }

    final void addListEntry(int control, int index, String text, String extra1, String extra2, int crown) {
        this.controlListEntries[control][index] = text;
        this.controlCrowns[control][index] = crown;
        this.controListEntriesExtra1[control][index] = extra1;
        this.controListEntriesExtra2[control][index] = extra2;
        if (this.controlListEntryCount[control] < 1 + index) {
            this.controlListEntryCount[control] = index + 1;
        }
    }

    private void drawTextInput(int x, int y, int width, int height, String text, int control, int textSize) {
        if (this.controlMaskText[control]) {
            int len = text.length();
            text = "";

            for (int var10 = 0; len > var10; ++var10) {
                text = text + "X";
            }
        }

        if (this.controlType[control] != 5) {
            if (this.controlType[control] == 6) {
                if (this.mouseLastButtonDown == 1 && this.mouseX >= -(width / 2) + x && -(height / 2) + y <= this.mouseY && this.mouseX <= x - -(width / 2) && height / 2 + y >= this.mouseY) {
                    this.focusControlIndex = control;
                }

                x -= this.surface.textWidth(textSize, -127, text) / 2;
            }
        } else if (this.mouseLastButtonDown == 1 && this.mouseX >= x && this.mouseY >= (-(height / 2) + y) && width + x >= this.mouseX && this.mouseY <= (height / 2 + y)) {
            this.focusControlIndex = control;
        }

        if (control == this.focusControlIndex) {
            text = text + "*";
        }

        int y2 = this.surface.textHeight(textSize, true) / 3 + y;
        this.drawstring(control, x, y2, text, textSize, 0);
    }

    final void updateText(int var1, String var2, int var3) {
        int var4 = -19 % ((var3 - -10) / 35);
        this.controlText[var1] = var2;
    }

    private void method534(int var1, byte var2, int var3, int var4) {
        this.surface.drawSprite((byte) 61, var4, var1, var3);
        if (var2 == -12) {
        }
    }

    final int method535(int var1, int var2, boolean var3, boolean var4, int var5, int var6, int var7, int var8, int var9) {
        this.controlType[this.controlCount] = 6;
        this.controlShown[this.controlCount] = true;
        this.controlMaskText[this.controlCount] = var3;
        this.controlClicked[this.controlCount] = false;
        this.controlTextSize[this.controlCount] = var6;
        this.controlUseAlternativeColour[this.controlCount] = var4;
        this.controlX[this.controlCount] = var1;
        this.controlY[this.controlCount] = var9;
        this.controlWidth[this.controlCount] = var5;
        this.controlHeight[this.controlCount] = var8;
        this.controlInputMaxLen[this.controlCount] = var7;
        if (var2 >= -6) {
            this.method514(14, 17, (byte) -111, -92, -62);
        }

        this.controlText[this.controlCount] = "";
        return this.controlCount++;
    }

    private void method536(int var1, int x, int font, String[] lines, byte var5, int var6) {
        if (var5 != -15) {
            this.controListEntriesExtra2 = null;
        }

        int var7 = lines.length;
        int y = var1 - this.surface.textHeight(font, true) * (var7 + -1) / 2;

        for (int var9 = 0; var7 > var9; ++var9) {
            int color;
            if (!this.controlUseAlternativeColour[var6]) {
                color = 0;
            } else {
                color = 16777215;
            }

            int var11 = this.surface.textWidth(font, -127, lines[var9]);
            if (this.mouseX >= (-(var11 / 2) + x) && this.mouseX <= x + var11 / 2 && y >= (this.mouseY + -2) && (this.mouseY + -2) > (y - this.surface.textHeight(font, true))) {
                if (!this.controlUseAlternativeColour[var6]) {
                    color = 16777215;
                } else {
                    color = 8421504;
                }

                if (this.mouseLastButtonDown == 1) {
                    this.controlListEntryMouseButtonDown[var6] = var9;
                    this.controlClicked[var6] = true;
                }
            }

            if (this.controlListEntryMouseButtonDown[var6] == var9) {
                if (!this.controlUseAlternativeColour[var6]) {
                    color = 12582912;
                } else {
                    color = 16711680;
                }
            }

            this.surface.drawstring(lines[var9], -(var11 / 2) + x, y, font, color, 0);
            y += this.surface.textHeight(font, true);
        }
        ;
    }

    final void keyPress(int var1, int key) {
        if (key == 0) {
            return;
        }
        if (focusControlIndex != -1 && controlText[focusControlIndex] != null && controlShown[focusControlIndex]) {
            int inputLen = controlText[focusControlIndex].length();
            if (key == 8 && inputLen > 0) {
                controlText[focusControlIndex] = controlText[focusControlIndex].substring(0, inputLen - 1);
            }
            if ((key == 10 || key == 13) && inputLen > 0) {
                controlClicked[focusControlIndex] = true;
            }
            String s = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"£$%^&*()-_=+[{]};:\'@#~,<.>/?\\| ";
            if (inputLen < controlInputMaxLen[focusControlIndex]) {
                for (int i = 0; i < s.length(); i++) {
                    if (key == s.charAt(i)) {
                        controlText[focusControlIndex] += (char) key;
                    }
                }
            }
            if (key == 9) {
                for (; controlType[focusControlIndex] != 5 && controlType[focusControlIndex] != 6;
                     focusControlIndex = (focusControlIndex + 1) % controlCount)
                    ;
            }
        } else if (focusControlIndex != -1 && controlShown[focusControlIndex]) {
            if (key == 10 || key == 13) {
                controlClicked[focusControlIndex] = true;
            }
        }
    }

    final int addTextListInteractive(int x, int y, int width, int height, int textSize, int maxLength, boolean altCol, boolean clicked) {
        this.controlType[this.controlCount] = 9;
        this.controlShown[this.controlCount] = true;
        this.controlClicked[this.controlCount] = clicked;
        this.controlTextSize[this.controlCount] = textSize;
        this.controlUseAlternativeColour[this.controlCount] = altCol;
        this.controlX[this.controlCount] = x;
        this.controlY[this.controlCount] = y;
        this.controlWidth[this.controlCount] = width;
        this.controlHeight[this.controlCount] = height;
        this.controlInputMaxLen[this.controlCount] = maxLength;
        this.controlListEntries[this.controlCount] = new String[maxLength];
        this.controlCrowns[this.controlCount] = new int[maxLength];
        this.controListEntriesExtra1[this.controlCount] = new String[maxLength];
        this.controListEntriesExtra2[this.controlCount] = new String[maxLength];
        this.controlListEntryCount[this.controlCount] = 0;
        this.controlFlashText[this.controlCount] = 0;
        this.controlListEntryMouseButtonDown[this.controlCount] = -1;
        this.controlListEntryMouseOver[this.controlCount] = -1;
        return this.controlCount++;
    }

    final int addTextList(int x, int y, int width, int height, int size, int maxLength, boolean altCol, boolean clicked) {
        this.controlType[this.controlCount] = 4;
        this.controlShown[this.controlCount] = true;
        this.controlClicked[this.controlCount] = clicked;
        this.controlX[this.controlCount] = x;
        this.controlY[this.controlCount] = y;
        this.controlWidth[this.controlCount] = width;
        this.controlHeight[this.controlCount] = height;
        this.controlUseAlternativeColour[this.controlCount] = altCol;
        this.controlTextSize[this.controlCount] = size;
        this.controlInputMaxLen[this.controlCount] = maxLength;
        this.controlListEntryCount[this.controlCount] = 0;
        this.controlFlashText[this.controlCount] = 0;
        this.controlListEntries[this.controlCount] = new String[maxLength];
        this.controlCrowns[this.controlCount] = new int[maxLength];
        this.controListEntriesExtra1[this.controlCount] = new String[maxLength];
        this.controListEntriesExtra2[this.controlCount] = new String[maxLength];
        return this.controlCount++;
    }

    final void clearList(int var2) {
        this.controlListEntryCount[var2] = 0;
    }

    private void drawstring(int control, int x, int y, String text, int font, int crown) {
        int color;
        if (!this.controlUseAlternativeColour[control]) {
            color = 0;
        } else {
            color = 16777215;
        }

        this.surface.drawstring(text, x, y, font, color, crown);
    }

    private void drawTextList(int control, int x, int y, int width, int height, int textSize, String[] listEntries, int listEntryCount, int flashText, int[] crowns) {
        int displayedEntryCount = height / this.surface.textHeight(textSize, true);
        if (flashText > -displayedEntryCount + listEntryCount) {
            flashText = -displayedEntryCount + listEntryCount;
        }

        if (flashText < 0) {
            flashText = 0;
        }

        this.controlFlashText[control] = flashText;
        int cornerTopRight;
        int cornerBottomLeft;
        int var15;
        int var16;
        if (listEntryCount > displayedEntryCount) {
            cornerTopRight = -12 + width + x;
            cornerBottomLeft = displayedEntryCount * (-27 + height) / listEntryCount;
            if (cornerBottomLeft < 6) {
                cornerBottomLeft = 6;
            }

            var15 = (-cornerBottomLeft + -27 + height) * flashText / (listEntryCount + -displayedEntryCount);
            if (this.mouseButtonDown == 1 && cornerTopRight <= this.mouseX && 12 + cornerTopRight >= this.mouseX) {
                if (this.mouseY > y && (y + 12) > this.mouseY && flashText > 0) {
                    --flashText;
                }

                if (this.mouseY > -12 + y + height && this.mouseY < y + height && flashText < (-displayedEntryCount + listEntryCount)) {
                    ++flashText;
                }

                this.controlFlashText[control] = flashText;
            }

            if (this.mouseButtonDown == 1 && (this.mouseX >= cornerTopRight && this.mouseX <= cornerTopRight - -12 || this.mouseX >= -12 + cornerTopRight && (24 + cornerTopRight) >= this.mouseX && this.controlListScrollbarHandleDragged[control])) {
                if (this.mouseY > (y + 12) && -12 + height + y > this.mouseY) {
                    this.controlListScrollbarHandleDragged[control] = true;
                    var16 = -(cornerBottomLeft / 2) + (-12 + this.mouseY - y);
                    flashText = var16 * listEntryCount / (height - 24);
                    if (listEntryCount + -displayedEntryCount < flashText) {
                        flashText = listEntryCount + -displayedEntryCount;
                    }

                    if (flashText < 0) {
                        flashText = 0;
                    }

                    this.controlFlashText[control] = flashText;
                }
            } else {
                this.controlListScrollbarHandleDragged[control] = false;
            }

            var15 = (-27 + (height - cornerBottomLeft)) * flashText / (-displayedEntryCount + listEntryCount);
            this.drawListContainer(cornerBottomLeft, y, x, var15, height, 12, width);
        }

        cornerTopRight = -(this.surface.textHeight(textSize, true) * displayedEntryCount) + height;
        cornerBottomLeft = this.surface.textHeight(textSize, true) * 5 / 6 + y - -(cornerTopRight / 2);

        for (var15 = flashText; var15 < listEntryCount; ++var15) {
            if (this.mouseLastButtonDown != 0 && this.mouseX >= x + 2 && this.mouseX <= this.surface.textWidth(textSize, -127, listEntries[var15]) + (x - -2) && cornerBottomLeft >= (-2 + this.mouseY) && (this.mouseY - 2) > (cornerBottomLeft - this.surface.textHeight(textSize, true))) {
                this.controlClicked[control] = true;
                this.controlListEntryMouseButtonDown[control] = Utility.bitwiseOr(var15, this.mouseLastButtonDown << 16);
            }

            this.drawstring(control, x - -2, cornerBottomLeft, listEntries[var15], textSize, crowns[var15]);
            cornerBottomLeft += this.surface.textHeight(textSize, true) - textListEntryHeightMod;
            if ((y + height) <= cornerBottomLeft) {
                return;
            }
        }
    }

    final int isMouseButtonDown(int control) {
        return this.controlListEntryMouseButtonDown[control];
    }

}
