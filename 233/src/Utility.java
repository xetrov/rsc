import java.awt.*;
import java.awt.image.PixelGrabber;
import java.io.*;
import java.net.URL;

/**
 * rsc
 * 02.01.2014
 */
public class Utility {

    static int[] data = new int[256];
    static byte[] gameFontData = new byte[100000];

    static {
        for (int var0 = 0; var0 < 256; ++var0) {
            int var1 = var0;

            for (int var2 = 0; var2 < 8; ++var2) {
                if ((var1 & 1) == 1) {
                    var1 = -306674912 ^ var1 >>> 1;
                } else {
                    var1 >>>= 1;
                }
            }

            data[var0] = var1;
        }
    }

    static int gameFontSize = 0;
    static byte[][][] aByteArrayArrayArray775;
    static int[] anIntArray446;

    static byte[] loadData(byte[] var1, int var2, String var3) {
        return unpackData(var1, var3, (byte[]) null, var2);
    }

    private static byte[] unpackData(byte[] archiveData, String filename, byte[] fileData, int var3) {
        int entryCount = (archiveData[0] & 255) * 256 + (255 & archiveData[1]);
        filename = filename.toUpperCase();
        int wantedHash = 0;

        for (int var7 = 0; var7 < filename.length(); ++var7) {
            wantedHash = 61 * wantedHash - (-filename.charAt(var7) - -32);
        }

        int offset = 2 - -(10 * entryCount);

        for (int var9 = 0; var9 < entryCount; ++var9) {
            int var10 = (archiveData[5 + var9 * 10] & 255) + 65536 * (255 & archiveData[10 * var9 + 3]) + (255 & archiveData[2 + 10 * var9]) * 16777216 + (archiveData[4 + 10 * var9] & 255) * 256;
            int var11 = 65536 * (255 & archiveData[6 + var9 * 10]) + 256 * (255 & archiveData[7 + var9 * 10]) + (archiveData[8 + var9 * 10] & 255);
            int var12 = 256 * (255 & archiveData[10 + var9 * 10]) + (65536 * (255 & archiveData[9 + 10 * var9]) - -(archiveData[11 + var9 * 10] & 255));
            if (wantedHash == var10) {
                if (fileData == null) {
                    fileData = new byte[var3 + var11];
                }

                if (var12 != var11) {
                    BZLib.decompress(fileData, var11, archiveData, var12, offset);
                } else {
                    for (int var13 = 0; var13 < var11; ++var13) {
                        fileData[var13] = archiveData[offset + var13];
                    }
                }

                return fileData;
            }

            offset += var12;
        }

        return null;
    }

    static int getUnsignedShort(int var0, byte[] var1, int var2) {
        if (var2 < 40) {
            getUnsignedShort(79, (byte[]) null, -33);
        }

        return (var1[var0 - -1] & 255) + ('\uff00' & var1[var0] << 8);
    }

    static void sendClientError(String description, int unused, Throwable error) {

        if (true) {
            System.out.println("sendClientError: " + description);
            error.printStackTrace();
            return;
        }

        try {
            String encDesc = "";
            if (error != null) {
                encDesc = throwable2string(error);
            }

            if (description != null) {
                if (error != null) {
                    encDesc = encDesc + " | ";
                }

                encDesc = encDesc + description;
            }

            printError(encDesc);
            encDesc = encode("%3a", ":", encDesc);
            encDesc = encode("%40", "@", encDesc);
            encDesc = encode("%26", "&", encDesc);
            encDesc = encode("%23", "#", encDesc);
            if (Isaac.anApplet445 != null) {
                CacheState var4 = Class11.aCachePackageManager_205.openURL(new URL(Isaac.anApplet445.getCodeBase(), "clienterror.ws?c=" + ClientStream.anInt1693 + "&u=" + (Scene.aString378 != null ? Scene.aString378 : String.valueOf(Character.aLong503)) + "&v1=" + CachePackageManager.javaVendor + "&v2=" + CachePackageManager.javaVersion + "&e=" + encDesc));

                while (var4.state == 0) {
                    GameApplet.sleep(1L, -17239);
                }

                if (var4.state == 1) {
                    DataInputStream var5 = (DataInputStream) var4.stateObject;
                    var5.read();
                    var5.close();
                }
            }
        } catch (Exception var6) {
            ;
        }
    }

    private static String throwable2string(Throwable var1) throws IOException {
        String var2;
        if (!(var1 instanceof GameException)) {
            var2 = "";
        } else {
            GameException var3 = (GameException) var1;
            var2 = var3.errorMessage + " | ";
            var1 = var3.errorThrowable;
        }

        StringWriter var13 = new StringWriter();
        PrintWriter var4 = new PrintWriter(var13);
        var1.printStackTrace(var4);
        var4.close();
        String var5 = var13.toString();
        BufferedReader var6 = new BufferedReader(new StringReader(var5));
        String var7 = var6.readLine();

        while (true) {
            String var8 = var6.readLine();
            if (var8 == null) {
                var2 = var2 + "| " + var7;
                return var2;
            }

            int var9 = var8.indexOf(40);
            int var10 = var8.indexOf(41, var9 - -1);
            String var11;
            if (var9 != -1) {
                var11 = var8.substring(0, var9);
            } else {
                var11 = var8;
            }

            var11 = var11.trim();
            var11 = var11.substring(1 + var11.lastIndexOf(32));
            var11 = var11.substring(var11.lastIndexOf(9) + 1);
            var2 = var2 + var11;
            if (var9 != -1 && var10 != -1) {
                int var12 = var8.indexOf(".java:", var9);
                if (var12 >= 0) {
                    var2 = var2 + var8.substring(var12 + 5, var10);
                }
            }

            var2 = var2 + ' ';
        }
    }

    private static void printError(String var1) {
        System.out.println("Error: " + encode("\n", "%0a", var1));
    }

    private static String encode(String to, String from, String unenc) {
        int var4 = unenc.indexOf(from);

        while (var4 != -1) {
            unenc = unenc.substring(0, var4) + to + unenc.substring(from.length() + var4);
            var4 = unenc.indexOf(from, to.length() + var4);
        }

        return unenc;
    }

    static int bitwiseOr(int var0, int var1) {
        return var0 | var1;
    }

    static int bitwiseAnd(int var0, int var1) {
        return var0 & var1;
    }

    static int bitwiseXor(int var0, int var1) {
        return var0 ^ var1;
    }

    static int getDataFileOffset(String filename, byte[] data) {
        int numEntries = getUnsignedShort(0, data, 76);
        filename = filename.toUpperCase();
        int wantedHash = 0;

        for (int var5 = 0; var5 < filename.length(); ++var5) {
            wantedHash = filename.charAt(var5) + wantedHash * 61 + -32;
        }

        int offset = numEntries * 10 + 2;

        for (int var7 = 0; var7 < numEntries; ++var7) {
            int fileHash = (data[5 + var7 * 10] & 255) + 256 * (255 & data[var7 * 10 - -4]) + ((255 & data[2 + 10 * var7]) * 16777216 - -(65536 * (255 & data[var7 * 10 - -3])));
            int fileSize = (data[9 + var7 * 10] & 255) * 65536 + ((data[10 + 10 * var7] & 255) * 256 - -(255 & data[11 + 10 * var7]));
            if (wantedHash == fileHash) {
                return offset;
            }

            offset += fileSize;
        }

        return 0;
    }

    static InputStream method545(String var0, int var1) throws IOException {
        if (var1 != -32341) {
            return null;
        } else {
            Object var2;
            if (GameApplet.appletCodeBase == null) {
                var2 = new BufferedInputStream(new FileInputStream(var0));
            } else {
                URL var3 = new URL(GameApplet.appletCodeBase, var0);
                var2 = var3.openStream();
            }

            return (InputStream) var2;
        }
    }

    static void insertBytes(byte[] in, int offIn, byte[] out, int offOut, int var4) {
        if (in == out) {
            if (offIn == offOut) {
                return;
            }

            if (offOut > offIn && offOut < offIn + var4) {
                --var4;
                offIn += var4;
                offOut += var4;
                var4 = offIn - var4;

                for (var4 += 7; offIn >= var4; out[offOut--] = in[offIn--]) {
                    out[offOut--] = in[offIn--];
                    out[offOut--] = in[offIn--];
                    out[offOut--] = in[offIn--];
                    out[offOut--] = in[offIn--];
                    out[offOut--] = in[offIn--];
                    out[offOut--] = in[offIn--];
                    out[offOut--] = in[offIn--];
                }

                for (var4 -= 7; offIn >= var4; out[offOut--] = in[offIn--]) {
                    ;
                }

                return;
            }
        }

        var4 += offIn;

        for (var4 -= 7; offIn < var4; out[offOut++] = in[offIn++]) {
            out[offOut++] = in[offIn++];
            out[offOut++] = in[offIn++];
            out[offOut++] = in[offIn++];
            out[offOut++] = in[offIn++];
            out[offOut++] = in[offIn++];
            out[offOut++] = in[offIn++];
            out[offOut++] = in[offIn++];
        }

        for (var4 += 7; offIn < var4; out[offOut++] = in[offIn++]) {
            ;
        }

    }

    static String formatName(String var0) {
        if (var0 == null) {
            return null;
        } else {
            int var2 = 0;

            int var3;
            for (var3 = var0.length(); var2 < var3 && method483(var0.charAt(var2), (byte) 48); ++var2) {
                ;
            }

            while (var2 < var3 && method483(var0.charAt(-1 + var3), (byte) 53)) {
                --var3;
            }

            int var4 = -var2 + var3;
            if (var4 >= 1 && var4 <= 12) {
                StringBuffer var5 = new StringBuffer(var4);

                for (int var6 = var2; var6 < var3; ++var6) {
                    char var7 = var0.charAt(var6);
                    if (GameFrame.method147(var7, 0)) {
                        char var8 = ClientStream.method478(var7);
                        if (var8 != 0) {
                            var5.append(var8);
                        }
                    }
                }

                if (var5.length() == 0) {
                    return null;
                } else {
                    return var5.toString();
                }
            } else {
                return null;
            }
        }
    }

    static boolean method483(char var0, byte var1) {
        return var1 <= 8 ? true : var0 == 160 || var0 == 32 || var0 == 95 || var0 == 45;
    }

    static int getUnsignedInt(byte[] var0, boolean var1, int var2) {
        if (var1) {
            getUnsignedInt((byte[]) null, true, -81);
        }

        return (var0[var2 + 3] & 255) + (16711680 & var0[var2 - -1] << 16) + ((255 & var0[var2]) << 24) + (var0[2 + var2] << 8 & '\uff00');
    }

    static synchronized long currentTimeMillis(int var0) {
        int var1 = 101 % ((25 - var0) / 52);
        long var2 = System.currentTimeMillis();
        if (BufferBase_Sub3.aLong1640 > var2) {
            Isaac.aLong449 += -var2 + BufferBase_Sub3.aLong1640;
        }

        BufferBase_Sub3.aLong1640 = var2;
        return Isaac.aLong449 + var2;
    }

    static int getDataFileLength(String filename, byte[] data) {
        int numEntries = getUnsignedShort(0, data, 53);
        int wantedHash = 0;
        filename = filename.toUpperCase();

        for (int var5 = 0; var5 < filename.length(); ++var5) {
            wantedHash = -32 + 61 * wantedHash + filename.charAt(var5);
        }

        for (int var6 = 0; var6 < numEntries; ++var6) {
            int fileHash = (data[10 * var6 + 5] & 255) + 256 * (data[4 + 10 * var6] & 255) + (data[var6 * 10 + 2] & 255) * 16777216 - -(65536 * (255 & data[10 * var6 + 3]));
            int fileSize = (data[7 + 10 * var6] & 255) * 256 + 65536 * (255 & data[10 * var6 - -6]) - -(255 & data[10 * var6 + 8]);
            if (wantedHash == fileHash) {
                return fileSize;
            }
        }

        return 0;
    }

    static int getnum(int length, int offset, byte[] bytes) {
        int var4 = -1;

        for (int var5 = offset; var5 < length; ++var5) {
            var4 = var4 >>> 8 ^ data[(bytes[var5] ^ var4) & 255];
        }

        var4 = ~var4;

        return var4;
    }

    static int writeUnicodeString(CharSequence charseq, int seqoff, int seqlen, byte[] buf, int bufoff) {
        int j1 = seqlen - seqoff;
        for (int k1 = 0; k1 < j1; k1++) {
            char c = charseq.charAt(seqoff + k1);
            if (c > 0 && c < '\200' || c >= '\240' && c <= '\377') {
                buf[bufoff + k1] = (byte) c;
                continue;
            }
            if (c == '\u20AC') {
                buf[bufoff + k1] = -128;
                continue;
            }
            if (c == '\u201A') {
                buf[bufoff + k1] = -126;
                continue;
            }
            if (c == '\u0192') {
                buf[bufoff + k1] = -125;
                continue;
            }
            if (c == '\u201E') {
                buf[bufoff + k1] = -124;
                continue;
            }
            if (c == '\u2026') {
                buf[bufoff + k1] = -123;
                continue;
            }
            if (c == '\u2020') {
                buf[bufoff + k1] = -122;
                continue;
            }
            if (c == '\u2021') {
                buf[bufoff + k1] = -121;
                continue;
            }
            if (c == '\u02C6') {
                buf[bufoff + k1] = -120;
                continue;
            }
            if (c == '\u2030') {
                buf[bufoff + k1] = -119;
                continue;
            }
            if (c == '\u0160') {
                buf[bufoff + k1] = -118;
                continue;
            }
            if (c == '\u2039') {
                buf[bufoff + k1] = -117;
                continue;
            }
            if (c == '\u0152') {
                buf[bufoff + k1] = -116;
                continue;
            }
            if (c == '\u017D') {
                buf[bufoff + k1] = -114;
                continue;
            }
            if (c == '\u2018') {
                buf[bufoff + k1] = -111;
                continue;
            }
            if (c == '\u2019') {
                buf[bufoff + k1] = -110;
                continue;
            }
            if (c == '\u201C') {
                buf[bufoff + k1] = -109;
                continue;
            }
            if (c == '\u201D') {
                buf[bufoff + k1] = -108;
                continue;
            }
            if (c == '\u2022') {
                buf[bufoff + k1] = -107;
                continue;
            }
            if (c == '\u2013') {
                buf[bufoff + k1] = -106;
                continue;
            }
            if (c == '\u2014') {
                buf[bufoff + k1] = -105;
                continue;
            }
            if (c == '\u02DC') {
                buf[bufoff + k1] = -104;
                continue;
            }
            if (c == '\u2122') {
                buf[bufoff + k1] = -103;
                continue;
            }
            if (c == '\u0161') {
                buf[bufoff + k1] = -102;
                continue;
            }
            if (c == '\u203A') {
                buf[bufoff + k1] = -101;
                continue;
            }
            if (c == '\u0153') {
                buf[bufoff + k1] = -100;
                continue;
            }
            if (c == '\u017E') {
                buf[bufoff + k1] = -98;
                continue;
            }
            if (c == '\u0178')
                buf[bufoff + k1] = -97;
            else
                buf[bufoff + k1] = '?';
        }

        return j1;
    }

    static String readUnicodeString(byte[] buf, int bufoff, int len) {
        char[] chars = new char[len];
        int off = 0;
        for (int j = 0; j < len; j++) {
            int uchar = buf[bufoff + j] & 0xff;
            if (uchar == 0) {
                continue;
            }
            if (uchar >= 128 && uchar < 160) {
                char c = BufferBase_Sub3.unicodeChars[uchar - 128];
                if (c == '\0') {
                    c = '?';
                }
                uchar = c;
            }
            chars[off++] = (char) uchar;
        }
        return new String(chars, 0, off);
    }

    static void sleep(long var0) {
        try {
            Thread.sleep(var0);
        } catch (InterruptedException var4) {
            ;
        }
    }

    static byte[] stringToUnicode(String str) {
        int strlen = str.length();
        byte[] buf = new byte[strlen];
        for (int i = 0; i < strlen; i++) {
            char c = str.charAt(i);
            if (c > 0 && c < '\200' || c >= '\240' && c <= '\377') {
                buf[i] = (byte) c;
                continue;
            }
            if (c == '\u20AC') {
                buf[i] = -128;
                continue;
            }
            if (c == '\u201A') {
                buf[i] = -126;
                continue;
            }
            if (c == '\u0192') {
                buf[i] = -125;
                continue;
            }
            if (c == '\u201E') {
                buf[i] = -124;
                continue;
            }
            if (c == '\u2026') {
                buf[i] = -123;
                continue;
            }
            if (c == '\u2020') {
                buf[i] = -122;
                continue;
            }
            if (c == '\u2021') {
                buf[i] = -121;
                continue;
            }
            if (c == '\u02C6') {
                buf[i] = -120;
                continue;
            }
            if (c == '\u2030') {
                buf[i] = -119;
                continue;
            }
            if (c == '\u0160') {
                buf[i] = -118;
                continue;
            }
            if (c == '\u2039') {
                buf[i] = -117;
                continue;
            }
            if (c == '\u0152') {
                buf[i] = -116;
                continue;
            }
            if (c == '\u017D') {
                buf[i] = -114;
                continue;
            }
            if (c == '\u2018') {
                buf[i] = -111;
                continue;
            }
            if (c == '\u2019') {
                buf[i] = -110;
                continue;
            }
            if (c == '\u201C') {
                buf[i] = -109;
                continue;
            }
            if (c == '\u201D') {
                buf[i] = -108;
                continue;
            }
            if (c == '\u2022') {
                buf[i] = -107;
                continue;
            }
            if (c == '\u2013') {
                buf[i] = -106;
                continue;
            }
            if (c == '\u2014') {
                buf[i] = -105;
                continue;
            }
            if (c == '\u02DC') {
                buf[i] = -104;
                continue;
            }
            if (c == '\u2122') {
                buf[+i] = -103;
                continue;
            }
            if (c == '\u0161') {
                buf[i] = -102;
                continue;
            }
            if (c == '\u203A') {
                buf[i] = -101;
                continue;
            }
            if (c == '\u0153') {
                buf[i] = -100;
                continue;
            }
            if (c == '\u017E') {
                buf[i] = -98;
                continue;
            }
            if (c == '\u0178')
                buf[i] = -97;
            else
                buf[i] = '?';
        }

        return buf;
    }

    static boolean createFont(String name, int id, GameApplet gameApplet) {
        boolean fontF = false;
        boolean fontD = false;
        name = name.toLowerCase();
        if (name.startsWith("helvetica")) {
            name = name.substring(9);
        }

        if (name.startsWith("h")) {
            name = name.substring(1);
        }

        if (name.startsWith("f")) {
            name = name.substring(1);
            fontF = true;
        }

        if (name.startsWith("d")) {
            name = name.substring(1);
            fontD = true;
        }

        if (name.endsWith(".jf")) {
            name = name.substring(0, name.length() - 3);
        }

        byte style = Font.PLAIN;
        if (name.endsWith("b")) {
            style = Font.BOLD;
            name = name.substring(0, name.length() - 1);
        }

        if (name.endsWith("p")) {
            name = name.substring(0, -1 + name.length());
        }

        int size = Integer.parseInt(name);
        Font font = new Font("Helvetica", style, size);
        FontMetrics metrics = gameApplet.getFontMetrics(font);
        gameFontSize = 855;
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"£$%^&*()-_=+[{]};:\'@#~,<.>/?\\| ";

        for (int charIndex = 0; charIndex < 95; ++charIndex) {
            if (!readFont(id, charIndex, font, characters.charAt(charIndex), metrics, gameApplet, fontD)) {
                return false;
            }
        }

        Surface.gameFonts[id] = new byte[gameFontSize];

        for (int var13 = 0; var13 < gameFontSize; ++var13) {
            Surface.gameFonts[id][var13] = gameFontData[var13];
        }

        if (style == Font.BOLD && Scene.aBooleanArray418[id]) {
            Scene.aBooleanArray418[id] = false;
            if (!createFont("f" + size + "p", id, gameApplet)) {
                return false;
            }
        }

        if (fontF && !Scene.aBooleanArray418[id]) {
            Scene.aBooleanArray418[id] = false;
            if (!createFont("d" + size + "p", id, gameApplet)) {
                return false;
            }
        }

        return true;
    }

    private static boolean readFont(int id, int charIndex, Font font, char character, FontMetrics metrics, GameApplet gameApplet, boolean fontD) {
        int charWidth = metrics.charWidth(character);
        int charWidth2 = charWidth;
        if (fontD) {
            try {
                if (character == 'f' || character == 't' || character == 'w' || character == 'v' || character == 'k' || character == 'x' || character == 'y' || character == 'A' || character == 'V' || character == 'W') {
                    ++charWidth;
                }

                if (character == '/') {
                    fontD = false;
                }
            } catch (Exception var30) {
                ;
            }
        }

        int maxAscent = metrics.getMaxAscent();
        int heightCalculated = metrics.getMaxAscent() + metrics.getMaxDescent();
        int height = metrics.getHeight();
        Image img = gameApplet.createImage(charWidth, heightCalculated);
        if (img == null) {
            return false;
        } else {
            Graphics graphics = img.getGraphics();
            graphics.setColor(Color.black);
            graphics.fillRect(0, 0, charWidth, heightCalculated);
            graphics.setColor(Color.white);
            graphics.setFont(font);
            graphics.drawString(String.valueOf(character), 0, maxAscent);
            if (fontD) {
                graphics.drawString(String.valueOf(character), 1, maxAscent);
            }

            int[] charPixels = new int[charWidth * heightCalculated];
            PixelGrabber pixelGrabber = new PixelGrabber(img, 0, 0, charWidth, heightCalculated, charPixels, 0, charWidth);

            try {
                pixelGrabber.grabPixels();
            } catch (InterruptedException var29) {
                return false;
            }

            img.flush();
            img = null;
            int right = 0;
            int bottom = 0;
            int var19 = charWidth;

            int var21;
            int var22;
            label213:
            for (int y = 0; y < heightCalculated; ++y) {
                for (int x = 0; charWidth > x; ++x) {
                    int pixel = charPixels[y * charWidth + x];
                    if ((0xffffff & pixel) != 0) {
                        bottom = y;
                        break label213;
                    }
                }
            }

            int heightCalculated2 = heightCalculated;

            int var25;
            int var24;
            label199:
            for (int x = 0; charWidth > x; ++x) {
                for (int y = 0; y < heightCalculated; ++y) {
                    int pixel = charPixels[y * charWidth + x];
                    if ((pixel & 0xffffff) != 0) {
                        right = x;
                        break label199;
                    }
                }
            }

            int var26;
            label185:
            for (var24 = heightCalculated + -1; var24 >= 0; --var24) {
                for (var25 = 0; charWidth > var25; ++var25) {
                    var26 = charPixels[charWidth * var24 + var25];
                    if ((var26 & 0xffffff) != 0) {
                        heightCalculated2 = 1 + var24;
                        break label185;
                    }
                }
            }

            int var27;
            label171:
            for (var25 = charWidth - 1; var25 >= 0; --var25) {
                for (var26 = 0; var26 < heightCalculated; ++var26) {
                    var27 = charPixels[charWidth * var26 + var25];
                    if ((var27 & 0xffffff) != 0) {
                        var19 = 1 + var25;
                        break label171;
                    }
                }
            }

            gameFontData[9 * charIndex] = (byte) (gameFontSize >> 14);
            gameFontData[charIndex * 9 + 1] = (byte) bitwiseAnd(127, gameFontSize >> 7);
            gameFontData[charIndex * 9 + 2] = (byte) bitwiseAnd(127, gameFontSize);
            gameFontData[9 * charIndex + 3] = (byte) (-right + var19);
            gameFontData[4 + 9 * charIndex] = (byte) (heightCalculated2 + -bottom);
            gameFontData[9 * charIndex - -5] = (byte) right;
            gameFontData[9 * charIndex + 6] = (byte) (maxAscent + -bottom);
            gameFontData[charIndex * 9 - -7] = (byte) charWidth2;
            gameFontData[8 + 9 * charIndex] = (byte) height;

            for (int y = bottom; heightCalculated2 > y; ++y) {
                for (int x = right; var19 > x; ++x) {
                    int var28 = 255 & charPixels[y * charWidth + x];
                    if (var28 > 30 && var28 < 230) {
                        Scene.aBooleanArray418[id] = true;
                    }

                    gameFontData[gameFontSize++] = (byte) var28;
                }
            }

            return true;
        }
    }

    static String format(int var0, String var1, byte var2) {
        int var3 = -109 / ((-78 - var2) / 46);
        String var4 = "";

        for (int var5 = 0; var5 < var0; ++var5) {
            if (var5 >= var1.length()) {
                var4 = var4 + " ";
            } else {
                char var6 = var1.charAt(var5);
                if (var6 >= 97 && var6 <= 122) {
                    var4 = var4 + var6;
                } else if (var6 >= 65 && var6 <= 90) {
                    var4 = var4 + var6;
                } else if (var6 >= 48 && var6 <= 57) {
                    var4 = var4 + var6;
                } else {
                    var4 = var4 + '_';
                }
            }
        }

        return var4;
    }

    static synchronized byte[] method354(int var1) {
        byte[] var5;
        if (var1 == 100 && GameApplet.anInt28 > 0) {
            var5 = Isaac.aByteArrayArray438[--GameApplet.anInt28];
            Isaac.aByteArrayArray438[GameApplet.anInt28] = null;
            return var5;
        } else if (var1 == 5000 && ClientStream.anInt1695 > 0) {
            var5 = BufferBase_Sub3.aByteArrayArray806[--ClientStream.anInt1695];
            BufferBase_Sub3.aByteArrayArray806[ClientStream.anInt1695] = null;
            return var5;
        } else if (var1 == 30000 && GameFrame.anInt108 > 0) {
            var5 = BufferBase_Sub3.aByteArrayArray820[--GameFrame.anInt108];
            BufferBase_Sub3.aByteArrayArray820[GameFrame.anInt108] = null;
            return var5;
        } else {
            int var2;
            if (aByteArrayArrayArray775 != null) {
                for (var2 = 0; var2 < BufferBase_Sub3.anIntArray1091.length; ++var2) {
                    if (var1 == BufferBase_Sub3.anIntArray1091[var2] && anIntArray446[var2] > 0) {
                        byte[] var3 = aByteArrayArrayArray775[var2][--anIntArray446[var2]];
                        aByteArrayArrayArray775[var2][anIntArray446[var2]] = null;
                        return var3;
                    }
                }
            }
            return new byte[var1];
        }
    }
}
