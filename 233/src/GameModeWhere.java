final class GameModeWhere {

    static GameModeWhere WTI = new GameModeWhere("WTI", "office", "_wti", 5);
    static GameModeWhere LOCAL = new GameModeWhere("LOCAL", "", "local", 4);
    static GameModeWhere WTQA = new GameModeWhere("WTQA", "office", "_qa", 2);
    static GameModeWhere INTBETA = new GameModeWhere("INTBETA", "office", "_intbeta", 6);
    static GameModeWhere LIVE = new GameModeWhere("LIVE", "", "", 0);
    static GameModeWhere WTRC = new GameModeWhere("WTRC", "office", "_rc", 1);
    static GameModeWhere WTWIP = new GameModeWhere("WTWIP", "office", "_wip", 3);

    int id;

    GameModeWhere(String name, String location, String var3, int id) {
        this.id = id;
    }

    static GameModeWhere[] getLocations(int var0) {
        return new GameModeWhere[]{
                LIVE,
                WTRC,
                WTQA,
                WTWIP,
                LOCAL,
                WTI,
                INTBETA
        };
    }

    static GameModeWhere method239(int var0, int var1) {
        GameModeWhere[] var2 = getLocations(4096);

        for (int var3 = 0; var2.length > var3; ++var3) {
            GameModeWhere var4 = var2[var3];
            if (var4.id == var1) {
                return var4;
            }
        }

        return null;
    }

    public final String toString() {
        throw new IllegalStateException();
    }

}
