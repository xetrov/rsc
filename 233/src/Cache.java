import java.io.EOFException;
import java.io.IOException;

final class Cache {

    static byte[] aByteArray805 = new byte[520];
    static Cache aCache_476;
    private static char[] aCharArray475 = new char[256];
    private int anInt466;
    private CacheFileManager aCacheFileManager_468 = null;
    private CacheFileManager aCacheFileManager_469 = null;
    private int anInt473 = '\ufde8';


    Cache(int var1, CacheFileManager var2, CacheFileManager var3, int var4) {
        this.aCacheFileManager_468 = var3;
        this.anInt473 = var4;
        this.anInt466 = var1;
        this.aCacheFileManager_469 = var2;
    }

    static {
        for (int var0 = 0; var0 < 256; ++var0) {
            aCharArray475[var0] = (char) var0;
        }

        aCharArray475[33] = 33;
        aCharArray475[43] = 43;
        aCharArray475[42] = 42;
        aCharArray475[34] = 34;
        aCharArray475[45] = 45;
        aCharArray475[92] = 92;
        aCharArray475[59] = 59;
        aCharArray475[47] = 47;
        aCharArray475[44] = 44;
        aCharArray475[46] = 46;
        aCharArray475[61] = 61;
        aCharArray475[124] = 124;
        aCache_476 = null;
    }

    final boolean method348(int var1, int var2, int var3, byte[] var4) {
        CacheFileManager var5 = this.aCacheFileManager_469;
        synchronized (var5) {
            boolean var7 = false;
            if (var3 != 998955376) {
                GameData.npcWalkModel = null;
            }

            if (var2 < 0 || this.anInt473 < var2) {
                throw new IllegalArgumentException();
            }

            boolean var6 = this.method349(var1, true, (byte) 42, var4, var2);
            if (!var6) {
                var6 = this.method349(var1, false, (byte) 42, var4, var2);
            }

            var7 = var6;
            return var7;
        }
    }

    public final String toString() {
        return "Cache:" + this.anInt466;
    }

    private boolean method349(int var1, boolean var2, byte var3, byte[] var4, int var5) {
        CacheFileManager var6 = this.aCacheFileManager_469;
        synchronized (var6) {
            boolean var7;
            try {
                int var8;
                if (var2) {
                    if (((long) (6 + var1 * 6)) > this.aCacheFileManager_468.method294(true)) {
                        var7 = false;
                        return var7;
                    }

                    this.aCacheFileManager_468.seek((long) (6 * var1), (byte) -112);
                    this.aCacheFileManager_468.write(0, aByteArray805, 6);
                    var8 = ((aByteArray805[4] & 255) << 8) + ((255 & aByteArray805[3]) << 16) - -(aByteArray805[5] & 255);
                    if (var8 <= 0 || (long) var8 > this.aCacheFileManager_469.method294(true) / 520L) {
                        var7 = false;
                        return var7;
                    }
                } else {
                    var8 = (int) ((519L + this.aCacheFileManager_469.method294(true)) / 520L);
                    if (var8 == 0) {
                        var8 = 1;
                    }
                }

                aByteArray805[0] = (byte) (var5 >> 16);
                aByteArray805[2] = (byte) var5;
                if (var3 != 42) {
                    GameException.fromThrowable((Throwable) null, (String) null);
                }

                aByteArray805[3] = (byte) (var8 >> 16);
                aByteArray805[5] = (byte) var8;
                aByteArray805[4] = (byte) (var8 >> 8);
                aByteArray805[1] = (byte) (var5 >> 8);
                this.aCacheFileManager_468.seek((long) (6 * var1), (byte) -126);
                this.aCacheFileManager_468.write(aByteArray805, 0, 6);
                int var9 = 0;
                int var10 = 0;

                while (true) {
                    if (var5 > var9) {
                        label262:
                        {
                            int var11 = 0;
                            int var12;
                            if (var2) {
                                this.aCacheFileManager_469.seek((long) (520 * var8), (byte) -111);

                                try {
                                    this.aCacheFileManager_469.write(0, aByteArray805, 8);
                                } catch (EOFException var20) {
                                    break label262;
                                }

                                var12 = (255 & aByteArray805[1]) + (aByteArray805[0] << 8 & '\uff00');
                                var11 = ((aByteArray805[5] & 255) << 8) + ((aByteArray805[4] << 16 & 16711680) - -(255 & aByteArray805[6]));
                                int var13 = (aByteArray805[3] & 255) + ('\uff00' & aByteArray805[2] << 8);
                                int var14 = aByteArray805[7] & 255;
                                if (var1 != var12 || var13 != var10 || this.anInt466 != var14) {
                                    var7 = false;
                                    return var7;
                                }

                                if (var11 < 0 || ((long) var11) > (this.aCacheFileManager_469.method294(true) / 520L)) {
                                    var7 = false;
                                    return var7;
                                }
                            }

                            if (var11 == 0) {
                                var2 = false;
                                var11 = (int) ((this.aCacheFileManager_469.method294(true) + 519L) / 520L);
                                if (var11 == 0) {
                                    ++var11;
                                }

                                if (var11 == var8) {
                                    ++var11;
                                }
                            }

                            if (var5 + -var9 <= 512) {
                                var11 = 0;
                            }

                            aByteArray805[2] = (byte) (var10 >> 8);
                            aByteArray805[0] = (byte) (var1 >> 8);
                            aByteArray805[1] = (byte) var1;
                            aByteArray805[3] = (byte) var10;
                            aByteArray805[5] = (byte) (var11 >> 8);
                            aByteArray805[7] = (byte) this.anInt466;
                            aByteArray805[6] = (byte) var11;
                            aByteArray805[4] = (byte) (var11 >> 16);
                            this.aCacheFileManager_469.seek((long) (var8 * 520), (byte) -119);
                            this.aCacheFileManager_469.write(aByteArray805, 0, 8);
                            var12 = -var9 + var5;
                            if (var12 > 512) {
                                var12 = 512;
                            }

                            this.aCacheFileManager_469.write(var4, var9, var12);
                            var9 += var12;
                            ++var10;
                            var8 = var11;
                            continue;
                        }
                    }

                    var7 = true;
                    return var7;
                }
            } catch (IOException var21) {
                var7 = false;
                return var7;
            }
        }
    }

    final byte[] method351(int var1, int var2) {
        synchronized (this.aCacheFileManager_469) {
            Object var4;
            try {
                if ((long) (6 + var1 * 6) > this.aCacheFileManager_468.method294(true)) {
                    var4 = null;
                    return (byte[]) var4;
                } else {
                    this.aCacheFileManager_468.seek((long) (6 * var1), (byte) -118);
                    this.aCacheFileManager_468.write(0, aByteArray805, 6);
                    int var5 = ('\uff00' & aByteArray805[1] << 8) + (aByteArray805[0] << 16 & 16711680) - -(aByteArray805[2] & 255);
                    int var6 = ((255 & aByteArray805[4]) << 8) + ((aByteArray805[3] & 255) << 16) + (aByteArray805[5] & 255);
                    if (var5 < 0 || this.anInt473 < var5) {
                        var4 = null;
                        return (byte[]) var4;
                    } else if (var6 <= 0 || (this.aCacheFileManager_469.method294(true) / 520L) < ((long) var6)) {
                        var4 = null;
                        return (byte[]) var4;
                    } else {
                        byte[] var7 = new byte[var5];
                        int var8 = 0;
                        if (var2 != 14625) {
                            this.toString();
                        }

                        int var9 = 0;

                        while (var8 < var5) {
                            if (var6 == 0) {
                                var4 = null;
                                return (byte[]) var4;
                            }

                            this.aCacheFileManager_469.seek((long) (var6 * 520), (byte) -121);
                            int var10 = -var8 + var5;
                            if (var10 > 512) {
                                var10 = 512;
                            }

                            this.aCacheFileManager_469.write(0, aByteArray805, var10 + 8);
                            int var11 = (aByteArray805[0] << 8 & '\uff00') - -(255 & aByteArray805[1]);
                            int var12 = ('\uff00' & aByteArray805[2] << 8) - -(255 & aByteArray805[3]);
                            int var13 = (aByteArray805[6] & 255) + ((255 & aByteArray805[5]) << 8) + ((aByteArray805[4] & 255) << 16);
                            int var14 = 255 & aByteArray805[7];
                            if (var11 == var1 && var9 == var12 && var14 == this.anInt466) {
                                if (var13 >= 0 && ((long) var13) <= (this.aCacheFileManager_469.method294(true) / 520L)) {
                                    var6 = var13;

                                    for (int var15 = 0; var10 > var15; ++var15) {
                                        var7[var8++] = aByteArray805[8 + var15];
                                    }

                                    ++var9;
                                    continue;
                                }

                                var4 = null;
                                return (byte[]) var4;
                            }

                            var4 = null;
                            return (byte[]) var4;
                        }

                        byte[] var23 = var7;
                        return var23;
                    }
                }
            } catch (IOException var20) {
                var4 = null;
                return (byte[]) var4;
            }
        }
    }
}
