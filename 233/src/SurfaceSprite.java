import java.awt.*;

final class SurfaceSprite extends Surface {

    client mudclientref;


    SurfaceSprite(int var1, int var2, int var3, Component var4) {
        super(var1, var2, var3, var4);
    }

    static void method428(int[] var0, Object[] var1, int var2, int var3, int var4) {
        if (var4 > var2) {
            int var5 = (var2 + var4) / 2;
            int var6 = var2;
            int var7 = var0[var5];
            var0[var5] = var0[var4];
            var0[var4] = var7;
            Object var8 = var1[var5];
            var1[var5] = var1[var4];
            var1[var4] = var8;
            int var9 = var7 == Integer.MAX_VALUE ? 0 : 1;

            for (int var10 = var2; var4 > var10; ++var10) {
                if ((var9 & var10) + var7 > var0[var10]) {
                    int var11 = var0[var10];
                    var0[var10] = var0[var6];
                    var0[var6] = var11;
                    Object var12 = var1[var10];
                    var1[var10] = var1[var6];
                    var1[var6++] = var12;
                }
            }

            var0[var4] = var0[var6];
            var0[var6] = var7;
            var1[var4] = var1[var6];
            var1[var6] = var8;
            method428(var0, var1, var2, -125, var6 + -1);
            method428(var0, var1, 1 + var6, -126, var4);
        }

        if (var3 <= -98) {
        }
    }

    final void method420(int var1, int var2, int var3, int var4, int var5, byte var6, int var7, int var8) {
        if (var6 > -53) {
            this.mudclientref = null;
        }

        if (var4 >= '\uc350') {
            this.mudclientref.drawTeleportBubble(var3, var8, -114, var1, var2, var5, var4 - '\uc350');
        } else if (var4 < '\u9c40') {
            if (var4 < 20000) {
                if (var4 < 5000) {
                    super.method426(var8, var3, var1, var4, (byte) -116, var2);
                } else {
                    this.mudclientref.method109(-48, var2, var1, var3, var8, -5000 + var4, var7, var5);
                }
            } else {
                this.mudclientref.method107((byte) 66, var8, var3, var1, var2, var7, var5, var4 - 20000);
            }
        } else {
            this.mudclientref.method91(var8, var2, var3, true, -40000 + var4, var1, var5);
        }
    }

}
