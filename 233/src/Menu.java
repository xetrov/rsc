import java.net.URL;

final class Menu {

    static char[] aCharArray706 = new char[]{' ', '\u00a0', '_', '-', '\u00e0', '\u00e1', '\u00e2', '\u00e4', '\u00e3', '\u00c0', '\u00c1', '\u00c2', '\u00c4', '\u00c3', '\u00e8', '\u00e9', '\u00ea', '\u00eb', '\u00c8', '\u00c9', '\u00ca', '\u00cb', '\u00ed', '\u00ee', '\u00ef', '\u00cd', '\u00ce', '\u00cf', '\u00f2', '\u00f3', '\u00f4', '\u00f6', '\u00f5', '\u00d2', '\u00d3', '\u00d4', '\u00d6', '\u00d5', '\u00f9', '\u00fa', '\u00fb', '\u00fc', '\u00d9', '\u00da', '\u00db', '\u00dc', '\u00e7', '\u00c7', '\u00ff', '\u0178', '\u00f1', '\u00d1', '\u00df'};
    static URL anURL707;
    static int[] anIntArray708 = new int[12];
    private MenuItem[] items;
    private int font;
    private int itemCount;
    private String title;
    private int height;
    private SurfaceSprite surface;
    private int width;

    Menu(SurfaceSprite surface, int font) {
        this(surface, font, (String) null);
    }


    Menu(SurfaceSprite surface, int font, String title) {
        this.width = 0;
        this.height = 0;
        this.itemCount = 0;

        this.items = new MenuItem[10];
        this.title = title;
        this.surface = surface;
        this.font = font;

        for (int var4 = 0; var4 < 10; ++var4) {
            this.items[var4] = new MenuItem();
        }

        this.calculateSize(0);
    }

    static void method482(int var0, Object[] items, int[] itemsIndex) {
        SurfaceSprite.method428(itemsIndex, items, 0, -109, itemsIndex.length + var0);
    }

    final int getItemMenuIndex(int var1) {
        return this.items[var1].menuIndex;
    }

    final int getMenuItemX(int var1) {
        return this.items[var1].menuItemX;
    }

    final void recalculateSize() {
        this.itemCount = 0;
        this.calculateSize(0);
    }

    final int getItemCount() {
        return this.itemCount;
    }

    final int drawNoBg(int y, int x, int var3, int var4) {
        return this.draw(x, y, 12, var4, false, var3);
    }

    final int getItemParam3(int var2) {
        return this.items[var2].param3;
    }

    final int getHeight() {
        return this.height;
    }

    final void addItem(String var1, int var2, String var3, int var5, int var6, int var7) {
        this.addItem(var2, 0, (String) null, 0, var1, var5, var7, var3, (String) null, (String) null, var6);
    }

    final void addItem(String text1, String var2, int var3, int var4, String text2, String var6) {
        this.addItem(var3, 0, (String) null, 0, text2, 0, var4, text1, var2, var6, 0);
    }

    private void calculateSize(int from) {
        int var2 = this.surface.textHeight(this.font, true) + 1;
        if (this.title != null) {
            this.height = var2;
            this.width = 5 + this.surface.textWidth(this.font, -127, this.title);
        } else {
            this.width = 0;
            this.height = 0;
        }

        for (int var3 = from; this.itemCount > var3; ++var3) {
            this.height += var2;
            int var4 = 5 + this.surface.textWidth(this.font, -127, this.items[var3].text1 + " " + this.items[var3].text2);
            if (var4 > this.width) {
                this.width = var4;
            }
        }
    }

    final String getItemText3(int var2) {
        return this.items[var2].text3;
    }

    final int getWidth() {
        return this.width;
    }

    final int getItemParam4(int var1) {
        return this.items[var1].param4;
    }

    final String getItemText2(int var1) {
        return this.items[var1].text2;
    }

    final int getMenuItemY(int var1) {
        return this.items[var1].menuItemY;
    }

    final void removeItem(int index) {
        if (index >= 0 && index < this.itemCount) {
            MenuItem var3 = this.items[index];

            for (int var4 = index; var4 < -1 + this.itemCount; ++var4) {
                this.items[var4] = this.items[1 + var4];
            }

            this.items[--this.itemCount] = var3;
            this.calculateSize(0);
        }
    }

    final int draw(int y, int var2, int x, int var5) {
        return this.draw(x, y, 12, var5, true, var2);
    }

    final void addItem(int var2, int menuIndex, String text1, String text2, int var1) {
        this.addItem(menuIndex, 0, (String) null, 0, text2, var2, var1, text1, (String) null, (String) null, 0);
    }

    private void addItem(int menuIndex, int var2, String var3, int var4, String text2, int y, int x, String text1, String text3, String unused, int var12) {
        if (this.items.length == this.itemCount) {
            MenuItem[] oldItems = this.items;
            this.items = new MenuItem[this.itemCount + 10];

            for (int index = 0; index < this.items.length; ++index) {
                if (index < this.itemCount) {
                    this.items[index] = oldItems[index];
                } else {
                    this.items[index] = new MenuItem();
                }
            }
        }

        this.items[this.itemCount++].initialise(unused, text3, x, y, var4, text1, var3, var2, var12, text2, menuIndex);
        this.calculateSize(0);
    }

    final void method453() {
        if (this.itemCount != 0) {
            int[] itemsIndex = new int[this.itemCount];
            Object[] items = new Object[this.itemCount];

            for (int index = 0; index < this.itemCount; ++index) {
                MenuItem item = this.items[index];
                itemsIndex[index] = item.menuIndex;
                items[index] = item;
            }

            method482(-1, items, itemsIndex);

            for (int index = 0; index < this.itemCount; ++index) {
                this.items[index] = (MenuItem) items[index];
            }

        }
    }

    final String getItemText1(int var2) {
        return this.items[var2].text1;
    }

    private int draw(int x, int y, int var3, int var4, boolean drawBackground, int var6) {
        if (this.width != 0 && this.height != 0) {
            if (drawBackground) {
                this.surface.drawBoxAlpha(x, y, this.width, this.height, 0xd0d0d0, 160);
            }

            int textHeight = 1 + this.surface.textHeight(this.font, true);
            int var8 = textHeight + (y - 3);
            int var9 = -1;
            if (this.title != null) {
                if (x < var6 && var4 > 3 + var8 + -textHeight && var4 < (3 + var8) && (x - -this.width) > var6) {
                    if (!drawBackground) {
                        return -2;
                    }

                    var9 = -2;
                }

                if (drawBackground) {
                    this.surface.drawstring(this.title, x - -2, var8, this.font, '\uffff');
                }

                var8 += textHeight;
            }

            for (int var10 = 0; var10 < this.itemCount; ++var10) {
                int var11 = 0xffffff;
                if (x < var6 && var4 > (var8 + 3 - textHeight) && var8 - -3 > var4 && var6 < this.width + x) {
                    var11 = 0xffff00;
                    if (!drawBackground) {
                        return var10;
                    }

                    var9 = var10;
                }

                if (drawBackground) {
                    this.surface.drawstring(this.items[var10].text1 + " " + this.items[var10].text2, 2 + x, var8, this.font, var11);
                }

                var8 += textHeight;
            }

            return var9;
        } else {
            return -1;
        }
    }

    final int getItemParam2(int var1) {
        return this.items[var1].param2;
    }

    final void addItem(int var10, int var12, int var13, String text2, int var14, int var15, String text1, int menuIndex) {
        this.addItem(menuIndex, var15, (String) null, var14, text2, var12, var13, text1, (String) null, (String) null, var10);
    }

    final void addItem(String text1, String text2, int menuIndex) {
        this.addItem(menuIndex, 0, (String) null, 0, text2, 0, 0, text1, (String) null, (String) null, 0);
    }

    final void addItem(int menuIndex, int x, String s, int y, int var6, int var7, String var8) {
        this.addItem(menuIndex, 0, (String) null, var7, s, y, x, var8, (String) null, (String) null, var6);
    }

    final void addItem(int menuIndex, String text1, int var1, String text2) {
        this.addItem(menuIndex, 0, (String) null, 0, text2, 0, var1, text1, (String) null, (String) null, 0);
    }

}
