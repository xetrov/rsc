import java.io.DataInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.URL;

final class JagGrab implements Runnable {

    static int[] caracterWidth;
    static long aLong1131;
    private BufferBase_Sub3 buffer;
    private CacheState aCacheState_1115;
    private CacheState aCacheState_1116;
    private CachePackageManager cachePackageManager;
    private URL url;
    private int anInt1120;
    private CacheState aCacheState_1122;
    private DataInputStream inputStream;


    JagGrab(CachePackageManager var1, URL var2, int var3) {
        this.url = var2;
        this.cachePackageManager = var1;
        this.buffer = new BufferBase_Sub3(var3);
    }

    static {
        String var0 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"£$%^&*()-_=+[{]};:\'@#~,<.>/?\\| ";
        caracterWidth = new int[256];

        for (int var1 = 0; var1 < 256; ++var1) {
            int var2 = var0.indexOf(var1);
            if (var2 == -1) {
                var2 = 74;
            }

            caracterWidth[var1] = var2 * 9;
        }

        aLong1131 = 0L;
    }

    public final void run() {
        try {
            while (this.buffer.buffer.length > this.buffer.offset) {
                int var1 = this.inputStream.read(this.buffer.buffer, this.buffer.offset, -this.buffer.offset + this.buffer.buffer.length);
                if (var1 < 0) {
                    break;
                }

                this.buffer.offset += var1;
            }

            if (this.buffer.offset == this.buffer.buffer.length) {
                throw new Exception("HG1: " + this.buffer.buffer.length + " " + this.url);
            } else {
                synchronized (this) {
                    this.finalize();
                    this.anInt1120 = 3;
                }

            }
        } catch (Exception var12) {
            synchronized (this) {
                this.finalize();
                ++this.anInt1120;
            }

        }
    }

    protected final void finalize() {
        if (this.aCacheState_1116 != null) {
            if (this.aCacheState_1116.stateObject != null) {
                try {
                    ((DataInputStream) this.aCacheState_1116.stateObject).close();
                } catch (Exception var4) {
                    ;
                }
            }

            this.aCacheState_1116 = null;
        }

        if (this.aCacheState_1122 != null) {
            if (this.aCacheState_1122.stateObject != null) {
                try {
                    ((Socket) this.aCacheState_1122.stateObject).close();
                } catch (Exception var3) {
                    ;
                }
            }

            this.aCacheState_1122 = null;
        }

        if (this.inputStream != null) {
            try {
                this.inputStream.close();
            } catch (Exception var2) {
                ;
            }

            this.inputStream = null;
        }

        this.aCacheState_1115 = null;
    }

    final BufferBase_Sub3 method604(int var1) {
        if (var1 < 21) {
            return null;
        } else {
            return this.anInt1120 == 3 ? this.buffer : null;
        }
    }

    final synchronized boolean method605(int var1) {
        if (this.anInt1120 >= 2) {
            return true;
        } else {
            if (this.anInt1120 == 0) {
                if (this.aCacheState_1116 == null) {
                    this.aCacheState_1116 = this.cachePackageManager.openURL(this.url);
                }

                if (this.aCacheState_1116.state == 0) {
                    return false;
                }

                if (this.aCacheState_1116.state != 1) {
                    ++this.anInt1120;
                    this.aCacheState_1116 = null;
                    return false;
                }
            }

            if (this.anInt1120 == 1) {
                if (this.aCacheState_1122 == null) {
                    this.aCacheState_1122 = this.cachePackageManager.method590(443, 837317288, this.url.getHost());
                }

                if (this.aCacheState_1122.state == 0) {
                    return false;
                }

                if (this.aCacheState_1122.state != 1) {
                    this.aCacheState_1122 = null;
                    ++this.anInt1120;
                    return false;
                }
            }

            if (this.inputStream == null) {
                try {
                    if (this.anInt1120 == 0) {
                        this.inputStream = (DataInputStream) this.aCacheState_1116.stateObject;
                    }

                    if (this.anInt1120 == 1) {
                        Socket var2 = (Socket) this.aCacheState_1122.stateObject;
                        var2.setSoTimeout(10000);
                        OutputStream var3 = var2.getOutputStream();
                        var3.write(17);
                        var3.write(Utility.stringToUnicode("JAGGRAB " + this.url.getFile() + "\n\n"));
                        this.inputStream = new DataInputStream(var2.getInputStream());
                    }

                    this.buffer.offset = 0;
                } catch (IOException var4) {
                    this.finalize();
                    ++this.anInt1120;
                }
            }

            if (this.aCacheState_1115 == null) {
                this.aCacheState_1115 = this.cachePackageManager.method585(5, (byte) -128, this);
            }

            if (var1 > -42) {
                return true;
            } else if (this.aCacheState_1115.state == 0) {
                return false;
            } else {
                if (this.aCacheState_1115.state != 1) {
                    this.finalize();
                    ++this.anInt1120;
                }

                return false;
            }
        }
    }
}
